# Novo site de vendas da Coobrastur

Um novo modo de efetuar vendas com a Coobrastur

### Instalação

Para que você possa rodar o projeto em sua máquina é necessário ter algumas coisas instaladas:

- NodeJS
- Npm ou Yarn

### Executando o projeto

- Basta clonar este repositório em seu PC
- Execute `yarn` ou `npm install`
- E agora é só rodar o projeto com um dos comandos a seguir: `yarn start OU npm start`
