import React from 'react';
import down from '../../assets/arrowDown.svg';
import { Button } from './styles';

export default function ButtonDivisor() {
  return (
    <Button type="push">
      <img src={down} alt="" width="100%" />
    </Button>
  );
}
