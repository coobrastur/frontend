import styled from 'styled-components';

export const Button = styled.button`
  background: transparent;
  cursor: pointer;
  width: 30px;
`;
