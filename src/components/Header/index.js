// Dependencies
import React from 'react';

// Images
import logoWhite from '../../assets/logo-white.svg';

// Styles
import { HeaderContainer } from './styles';

function Header() {
  return (
    <HeaderContainer>
      <div className="container">
        <img src={logoWhite} alt="" />

        <ul>
          <li>
            <a href="/home">Home</a>
          </li>

          <li>
            <a href="/contracts">Contratos</a>
          </li>

          <li>
            <a href="/myAccount">Minha conta</a>
          </li>
        </ul>
      </div>
    </HeaderContainer>
  );
}

export default Header;
