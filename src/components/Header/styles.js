import styled from 'styled-components';

export const HeaderContainer = styled.div`
  background-color: #08436f;
  height: 10vh;

  display: flex;
  justify-content: center;
  align-items: center;

  div {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  ul {
    display: flex;
  }

  ul li {
    margin-right: 50px;
    list-style: none;
  }

  ul li a {
    color: white;
    text-decoration: none;
    font-size: 18px;
  }

  ul li a:hover {
    text-decoration: underline;
  }
`;
