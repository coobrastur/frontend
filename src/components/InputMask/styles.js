import styled from 'styled-components';
import ReactInputMask from 'react-input-mask';

export const InputStyle = styled(ReactInputMask)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  border-radius: 6px;

  color: rgba(0, 0, 0, 0.3);

  font-size: 18px;

  margin-bottom: 20px;
  padding: 10px;

  ::placeholder {
    color: rgba(0, 0, 0, 0.3);
  }
`;
