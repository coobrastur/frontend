import styled from 'styled-components';
import ReactSelect from 'react-select';

export const InputStyle = styled(ReactSelect)`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  border-radius: 6px;

  color: rgba(0, 0, 0, 0.5);

  font-size: 18px;

  margin-bottom: 15px;
  padding: 2px;
  ::placeholder {
    color: rgba(0, 0, 0, 0.5);
  }

  div {
    border: none;
    color: rgba(0, 0, 0, 0.5);
  }
`;
