import React from 'react';
import SideMenu from '../SideMenu';
import Routes from '../../routes';

import { Container } from './styles';

function Layout() {
  return (
    <Container>
      <SideMenu />
      <Routes />
    </Container>
  );
}

export default Layout;
