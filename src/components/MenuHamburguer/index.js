import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as userActions from '../../store/ducks/auth';
import { Container } from './styles';
import { showToast } from '../Alert';

function MenuHamburguer() {
  const dispatch = useDispatch();
  const history = useHistory();

  function logout() {
    dispatch(userActions.login(null));
    history.push('/');
    showToast({
      type: 'success',
      message: 'Você saiu da sua conta com sucesso!',
    });
  }

  return (
    <Container>
      <input id="menu-hamburguer" type="checkbox" />
      <label htmlFor="menu-hamburguer">
        <div className="menu">
          <span className="hamburguer" />
        </div>
      </label>

      <ul>
        <li>
          <Link to="/myAccount">
            <p>Minha Conta</p>
          </Link>
        </li>

        <li>
          <Link to="/contracts">
            <p>Contratos</p>
          </Link>
        </li>

        <li>
          <Link to="/newContract">
            <p>Novo Contrato</p>
          </Link>
        </li>
        <li>
          <Link to="/prices">
            <p>Consultar Tabela de Preços</p>
          </Link>
        </li>
        <li>
          <Link to="/proportionality">
            <p>Tabela de Proporcionalidade</p>
          </Link>
        </li>
        <li>
          <Link to="/trocar-cupom">
            <p>Trocar Cupom</p>
          </Link>
        </li>

        <li>
          <button className="logout" type="button" onClick={() => logout()}>
            <p>Sair</p>
          </button>
        </li>
      </ul>

      {/* {nivCodigo > 1 && (
        <>
          <ul>
            <li>
              <Link to="/myAccount">
                <p>Minha Conta</p>
              </Link>
            </li>

            <li>
              <Link to="/allcontracts">
                <p>Todos os Contratos</p>
              </Link>
            </li>

            <li>
              <Link to="/allusers">
                <p>Todos os Vendedores</p>
              </Link>
            </li>

            <li>
              <Link to="/contracts">
                <p>Meus Contratos</p>
              </Link>
            </li>

            <li>
              <Link to="/newContract">
                <p>Novo Contrato</p>
              </Link>
            </li>
            <li>
              <button className="logout" type="button" onClick={() => logout()}>
                <p>Sair</p>
              </button>
            </li>
          </ul>
        </>
      )} */}
    </Container>
  );
}

export default MenuHamburguer;
