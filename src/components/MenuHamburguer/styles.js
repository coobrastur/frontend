import styled from 'styled-components';

export const Container = styled.div`
  *,
  *:before,
  *:after {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  .menu {
    z-index: 1;
    background: transparent;
    border-radius: 50%;
    width: 60px;
    height: 60px;
    position: fixed;
    top: 20px;
    right: 20px;
  }

  .hamburguer {
    position: relative;
    display: block;
    background: #01affd;
    width: 30px;
    height: 3px;
    top: 29px;
    left: 15px;
    transition: 0.5s ease-in-out;
  }

  .hamburguer:before,
  .hamburguer:after {
    background: #01affd;
    content: '';
    display: block;
    width: 100%;
    height: 100%;
    position: absolute;
    transition: 0.5s ease-in-out;
  }

  .hamburguer:before {
    top: -10px;
  }

  .hamburguer:after {
    bottom: -10px;
  }

  input {
    display: none;
  }

  input:checked ~ label .hamburguer {
    transform: rotate(45deg);
  }

  input:checked ~ label .hamburguer:before {
    transform: rotate(90deg);
    top: 0;
  }

  input:checked ~ label .hamburguer:after {
    transform: rotate(90deg);
    bottom: 0;
  }

  .menu {
    cursor: pointer;
    box-shadow: 0 0 0 0 #01affd, 0 0 0 0 #01affd;
    transition: box-shadow 1.1s cubic-bezier(0.19, 1, 0.22, 1);
  }

  // animação do menu
  input:checked ~ label .menu {
    box-shadow: 0 0 0 130vw #01affd, 0 0 0 130vh #01affd;
  }

  // só aparecer o menu ao clicar
  input:checked ~ ul {
    display: block;
  }

  // animação e estilo do menu
  ul {
    position: fixed;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    list-style: none;
    font-size: 25px;
    display: none;
    transition: 0.25s 0.1s cubic-bezier(0, 1.07, 0, 1.02);
    z-index: 2;
  }

  a,
  p {
    color: #fff;
    display: block;
    margin-bottom: 1em;
    text-decoration: none;
    text-align: center;
  }

  button.logout {
    background: transparent;
    width: 100%;
  }

  button.logout p {
    font-size: 25px;
    cursor: pointer;
    color: red;
  }

  @media (max-width: 1100px) {
    .menu {
      width: 40px;
      height: 40px;

      top: 5px;
      right: 50px;
    }

    .hamburguer {
      width: 30px;
      height: 3px;
      top: 18px;
      left: 6px;
    }

    ul {
      font-size: 20px;
    }

    button.logout p {
      font-size: 20px;
    }
  }
`;
