// Dependencies
import React, { useState } from 'react';
import { apiClei } from '../../services/api';

// Components
import { showToast } from '../Alert';
import Loader from '../Loader';

// Styles
import { Container, Content, Button, ButtonOutline } from './styles';

function Modal({ setModal, modal, id, token }) {
  const [loader, setLoader] = useState(false);

  async function deleteContract() {
    setLoader(true);
    const dados = {
      id,
      token,
    };

    await apiClei
      .get('Contrato.asmx/ExcluirContratoPorId', {
        params: {
          dados,
        },
      })
      .then(() => {
        showToast({
          type: 'success',
          message: 'Contrato removido com sucesso!',
        });
      })
      .catch(() => {
        showToast({
          type: 'error',
          message:
            'Algum erro aconteceu, não foi possível remover este contrato',
        });
      });

    setModal(false);
    setLoader(false);
  }

  return (
    <Container visible={modal}>
      <Loader loader={loader} />

      <Content className="container">
        <h2>Tem certeza que deseja excluir este contrato?</h2>

        <Button type="submit" onClick={() => deleteContract()}>
          Sim
        </Button>
        <ButtonOutline onClick={() => setModal(false)} type="button">
          Não
        </ButtonOutline>
      </Content>
    </Container>
  );
}

export default Modal;
