import styled from 'styled-components';

export const Container = styled.div`
  z-index: 10;
  background: rgba(0, 0, 0, 0.6);
  width: 100%;
  height: 100%;

  position: fixed;
  top: 0;
  right: 0;
  display: ${(props) => (props.visible ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  background-color: white;
  border-radius: 20px;
  padding: 60px 40px;

  h2 {
    color: #01affd;
    font-size: 30px;
    font-weight: 700;
    margin-bottom: 20px;
  }

  @media (max-width: 700px) {
    padding: 20px;
  }
`;

export const Button = styled.button`
  background-color: #f54756;
  border-radius: 10px;
  color: white;

  font-size: 14px;
  text-transform: uppercase;

  padding: 12px;
  margin-bottom: 5px;

  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #a74756;
  }
`;
export const ButtonOutline = styled.button`
  border: 1px solid #f54756;
  border-radius: 10px;
  background-color: white;
  color: #f54756;

  font-size: 14px;
  text-transform: uppercase;

  padding: 12px;
  margin-top: 5px;

  width: 100%;

  cursor: pointer;
`;
