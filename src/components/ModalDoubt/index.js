// Dependencies
import React from 'react';

// Styles
import { Container, Content, ButtonOutline } from './styles';

function ModalDoubt({ setModalDoubt, modalDoubt }) {
  return (
    <Container visible={modalDoubt}>
      <Content className="container">
        <h2>Status</h2>

        <p>
          <span>1. Aguardando dados de pagamento: </span>A aplicação está
          esperando o cliente preencher os dados bancários, para depois passar
          para a próxima etapa;
        </p>

        <p>
          <span>2. Aguardando assinatura: </span>Os dados de pagamento já foram
          preenchidos, agora a aplicação aguarda o cliente assinar o contrato;
        </p>

        <p>
          <span>3.Aguardando Envio: </span>O contrato foi assinado, agora só
          falta você clicar no botão de enviar;
        </p>

        <p>
          <span>4. Cadastrado: </span>É a última etapa do processo, neste
          momento o contrato já está no Siscoob
        </p>

        <ButtonOutline onClick={() => setModalDoubt(false)} type="button">
          Fechar
        </ButtonOutline>
      </Content>
    </Container>
  );
}

export default ModalDoubt;
