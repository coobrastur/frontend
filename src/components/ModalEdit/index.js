// Dependencies
import React, { useState } from 'react';

// Styles
import { Row, Col } from 'react-grid-system';
import {
  ContainerModal,
  Content,
  Button,
  ButtonEdit,
  ButtonDivisor,
} from './styles';

// Images
import arrowUP from '../../assets/arrowUP.svg';
import arrowDown from '../../assets/arrowDown.svg';

export default function Modal({
  dados,
  associacao,
  dFiador,
  onClose = () => {},
  onSend = () => {},
}) {
  const [dadosPessoais, setDadosPessoais] = useState(true);
  const [desabilitado, setDesabilitado] = useState(false);

  // Desestruturação dos dados
  const {
    cpf,
    nome,
    email,
    ddd,
    telefone,
    ufAssociado,
    cidade,
    bairroAssociado,
    rua,
    numeroAssociado,
    complemento,
    cepAssociado,
    restricao,
    formaPagamento,
    dataBoletoEscolhida,
    carDia,
    carMes,
    debDia,
    debMes,
    // campaingValue,
  } = dados;

  // Desestruturação de associação
  const {
    typeOfContract,
    selectedCompany,
    defaultCompany,
    nroPipeline,
    flat,
    dailyQuantities,
    family,
    // payment,
    // halfAdhesion,
    finalValue,
    // valorAdesao,
    finalAdesao,
    descontoAdesao,
    sellerMembershipForm,
    parcelasVendedor,
    dataEscolhida,
    // parcelasCoobrastur,
    indicador,
    cpfIndic,
    nomeIndic,
    emailIndic,
    celularIndic,
  } = associacao;

  const {
    fiador,
    aditamento,
    cpfFia,
    nomeFia,
    // dataFia,
    // nacioFia,
    // civilGuarantorState,
    maeFia,
    // paiFia,
    emailFia,
    celularFia,
    // conjugeFia,
    select,
  } = dFiador;

  const waitTime = 30 * 60 * 1000;

  const dia = String(dataEscolhida.getDate()).padStart(2, '0');
  const mes = String(dataEscolhida.getMonth() + 1).padStart(2, '0');
  const ano = dataEscolhida.getFullYear();

  const dataFormatada = `${dia}/${mes}/${ano}`;

  // console.log(dataFormatada);

  return (
    <ContainerModal>
      <Content className="container">
        <h4>Revisão do Contrato</h4>
        {dadosPessoais !== true && (
          <h5>
            Dados Pessoais
            <ButtonDivisor onClick={() => setDadosPessoais(true)}>
              <img src={arrowUP} alt="" width="100%" />
            </ButtonDivisor>
          </h5>
        )}
        {dadosPessoais === true && (
          <div className="rowData">
            <h5>
              Dados Pessoais
              <ButtonDivisor
                type="push"
                onClick={() => setDadosPessoais(false)}
              >
                <img src={arrowDown} alt="" width="100%" />
              </ButtonDivisor>
            </h5>
            <Row className="rowData">
              <Col md={4}>
                <h6>CPF</h6>
                <p> {cpf} </p>
              </Col>
              <Col md={4}>
                <h6>Nome Completo</h6>
                <p> {nome} </p>
              </Col>
            </Row>

            <Row className="rowData">
              <Col md={4}>
                <h6>E-mail</h6>
                <p> {email} </p>
              </Col>
              <Col md={4}>
                <h6>Telefone</h6>
                <p>
                  {' '}
                  {ddd} {telefone}{' '}
                </p>
              </Col>
            </Row>
          </div>
        )}
        <h5>Endereço</h5>
        <Row className="rowData">
          <Col md={4}>
            <h6>Rua/Avenida</h6>
            <p> {rua} </p>
          </Col>
          <Col md={4}>
            <h6>Número</h6>
            <p> {numeroAssociado} </p>
          </Col>
          <Col md={4}>
            <h6>Complemento</h6>
            {complemento === '' && <p>Não informado</p>}
            <p> {complemento} </p>
          </Col>
        </Row>
        <Row className="rowData">
          <Col md={4}>
            <h6>Bairro</h6>
            <p> {bairroAssociado} </p>
          </Col>
          <Col md={4}>
            <h6>Cidade</h6>
            <p> {cidade} </p>
          </Col>
          <Col md={4}>
            <h6>Estado</h6>
            <p> {ufAssociado} </p>
          </Col>
        </Row>
        <Row className="rowData">
          <Col md={4}>
            <h6>CEP</h6>
            <p> {cepAssociado} </p>
          </Col>
        </Row>

        {restricao === true && (
          <div className="rowData">
            {fiador === true && (
              <div className="rowData">
                <h5>Restrição</h5>
                <Row className="rowData">
                  <Col md={6}>
                    <h6>Restrição</h6>
                    <p>Fiador</p>
                  </Col>
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>CPF do Fiador</h6>
                    <p> {cpfFia} </p>
                  </Col>
                  <Col md={4}>
                    <h6>Nome do Fiador</h6>
                    <p> {nomeFia} </p>
                  </Col>
                  {/* <Col md={4}>
                    <h6>Data Nascimento</h6>
                    <p> {dataFia} </p>
                  </Col> */}
                </Row>
                <Row className="rowData">
                  {/* <Col md={4}>
                    <h6>Nacionalidade fiador</h6>
                    <p> {nacioFia} </p>
                  </Col> */}
                  {/* <Col md={4}>
                    <h6>Estado Civil</h6>
                    {civilGuarantorState === '1' && <p>Solteiro(a)</p>}
                    {civilGuarantorState === '2' && <p>Casado(a)</p>}
                    {civilGuarantorState === '3' && <p>Divorciado(a)</p>}
                    {civilGuarantorState === '4' && <p>Viúvo(a)</p>}
                  </Col> */}
                  {/* {civilGuarantorState === '2' && (
                    <Col md={4}>
                      <h6>Nome Cônjuge:</h6>
                      <p> {conjugeFia} </p>
                    </Col>
                  )} */}
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>Nome da Mãe do Fiador</h6>
                    <p> {maeFia} </p>
                  </Col>
                  {/* <Col md={4}>
                    <h6>Nome do Pai do Fiador</h6>
                    <p> {paiFia} </p>
                  </Col> */}
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>E-mail do Fiador</h6>
                    <p> {emailFia} </p>
                  </Col>
                  <Col md={4}>
                    <h6>Celular Fiador</h6>
                    <p> {celularFia} </p>
                  </Col>
                </Row>
              </div>
            )}
            {aditamento === true && (
              <div className="rowData">
                <h5>Restrição</h5>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>Aditamento</h6>
                    {select === '1' && <p>12 meses</p>}
                    {select === '2' && <p>Imediato</p>}
                  </Col>
                </Row>
              </div>
            )}
          </div>
        )}
        {restricao !== true && <div className="rowData" />}

        {indicador !== true && <div className="rowData" />}
        {indicador === true && (
          <div className="rowData">
            <h5>Indicação</h5>

            <Row className="rowData">
              <Col md={4}>
                <h6>CPF Indicador</h6>
                <p> {cpfIndic} </p>
              </Col>
              <Col md={4}>
                <h6>Nome Completo do Indicador</h6>
                <p> {nomeIndic} </p>
              </Col>
            </Row>

            <Row className="rowData">
              <Col md={4}>
                <h6>E-mail Indicador</h6>
                {emailIndic === '' && <p>Não informado</p>}
                <p> {emailIndic} </p>
              </Col>
              <Col md={4}>
                <h6>Celular Indicador</h6>
                {celularIndic === '' && <p>Não informado</p>}
                <p>{celularIndic}</p>
              </Col>
            </Row>
          </div>
        )}

        <h5>Opções de Assinaturas</h5>
        <Row className="rowData">
          <Col md={6}>
            <h3>Empresa</h3>
            {(selectedCompany || defaultCompany.empCodigo) === 38 && (
              <p>Coob+</p>
            )}
            {(selectedCompany || defaultCompany.empCodigo) === 58 && (
              <p>Banstur</p>
            )}
          </Col>

          <Col md={6}>
                <h3>N. PipeDrive</h3>
                {nroPipeline}
              </Col>
        </Row>
        <Row className="rowData">
          <Col md={3}>
            <h6>Plano</h6>
            {flat === 36 && <p>Plano - Vip+</p>}
            {flat === 37 && <p>Master+</p>}
            {flat === 38 && <p>Gold Vip+</p>}
            {flat === 39 && <p>Gold Master+ </p>}
            {flat === 40 && <p>Diamante+ </p>}
            {flat === 41 && <p>Go Vip+ </p>}
            {flat === 42 && <p>Go Master+ </p>}
            {flat === 1002 && <p>Banstur Ouro </p>}
            {flat === 10016 && <p>Banstur J3 Multi</p>}

            {flat === '36' && <h3>Vip+</h3>}
            {flat === '37' && <h3>Master+</h3>}
            {flat === '38' && <h3>Gold Vip+</h3>}
            {flat === '39' && <h3>Gold Master+ </h3>}
            {flat === '40' && <h3>Diamante+ </h3>}
            {flat === '41' && <h3>Go Vip+ </h3>}
            {flat === '42' && <h3>Go Master+ </h3>}
            {flat === '1002' && <h3>Banstur Ouro </h3>}
            {flat === '10016' && <h3>Banstur J3 Multi</h3>}
          </Col>
          <Col md={3}>
            <h6>Tipo de Contrato</h6>
            {typeOfContract == 1 && <p>Normal</p>}
            {typeOfContract == 2 && <p>Pré-Pago</p>}
            {typeOfContract == 11 && <p>Reativação</p>}
          </Col>
          <Col md={3}>
            <h6>Valor do Plano</h6>
            {flat === 36 && <p>R${finalValue}</p>}
            {flat === 37 && <p>R${finalValue}</p>}
            {flat === 38 && <p>R${finalValue}</p>}
            {flat === 39 && <p>R${finalValue} </p>}
            {flat === 40 && <p>R${finalValue} </p>}
            {flat === 41 && <p>R${finalValue} </p>}
            {flat === 42 && <p>R${finalValue} </p>}

            {flat === '36' && <p>R${finalValue}</p>}
            {flat === '37' && <p>R${finalValue}</p>}
            {flat === '38' && <p>R${finalValue}</p>}
            {flat === '39' && <p>R${finalValue} </p>}
            {flat === '40' && <p>R${finalValue} </p>}
            {flat === '41' && <p>R${finalValue} </p>}
            {flat === '42' && <p>R${finalValue} </p>}
          </Col>
          <Col md={3}>
            <h6>Diárias</h6>
            <p> {dailyQuantities} Diárias</p>
          </Col>
        </Row>

        <div>
          <Row className="rowData">
            {/* <Col md={4}>
              <h6>Valor Adesão</h6>
              <p>
                R${' '}
                {valorAdesao.toLocaleString('pt-br', {
                  minimumFractionDigits: 2,
                })}
              </p>
            </Col> */}
            {/* {finalAdesao !== '0,00' && ( */}
            <Col md={3}>
              <h6>Família</h6>
              {family === true && <p> Sim </p>}
              {family === false && <p> Não </p>}
            </Col>

            <Col md={4}>
              <h6>Desconto Adesão</h6>
              <p>
                R${' '}
                {descontoAdesao.toLocaleString('pt-br', {
                  minimumFractionDigits: 2,
                })}
              </p>
            </Col>
            {/* )} */}
          </Row>
          <Row className="rowData">
            <Col md={3}>
              <h6>Valor Total da Adesão</h6>
              <p>
                R${' '}
                {finalAdesao.toLocaleString('pt-br', {
                  minimumFractionDigits: 2,
                })}
              </p>
            </Col>
            <Col md={3}>
              <h6>Forma de Pagamento da Adesão</h6>
              {sellerMembershipForm == '' && <p> Não se Aplica </p>}
              {sellerMembershipForm == '1' && <p> Dinheiro </p>}
              {sellerMembershipForm == '2' && <p> Cartão </p>}
              {sellerMembershipForm == '3' && <p> Cheque </p>}
              {sellerMembershipForm == '4' && <p> Boleto </p>}
            </Col>
            <Col md={3}>
              <h6>Parcelas da Adesão</h6>
              {sellerMembershipForm == '' && <p> Não se Aplica </p>}
              {parcelasVendedor == '1' && <p> À vista </p>}
              {parcelasVendedor == '2' && <p> 2X </p>}
              {parcelasVendedor == '3' && <p> 3x </p>}
            </Col>
            {sellerMembershipForm == '' ? (
              <Col md={3}>
                <h6>Data da Primeira Adesão</h6>
                <p>Não se aplica</p>
              </Col>
            ) : (
              <Col md={3}>
                <h6>Data da Primeira Adesão</h6>
                <p>{dataFormatada}</p>
              </Col>
            )}
          </Row>
        </div>

        {/* {flat >= '42' && (
          <div>
            <Row className="rowData">
              <Col md={4}>
                <h6>Família</h6>
                {family === true && <p> Sim </p>}
                {family === false && <p> Não </p>}
              </Col>
              <Col md={4}>
                <h6>Valor Adesão</h6>
                <p>
                  R${' '}
                  {valorAdesao.toLocaleString('pt-br', {
                    minimumFractionDigits: 2,
                  })}
                </p>
              </Col>
              {finalAdesao.isNaN && (
                <Col md={4}>
                  <h6>Adesão com Desconto</h6>
                  <p>
                    R${' '}
                    {finalAdesao.toLocaleString('pt-br', {
                      minimumFractionDigits: 2,
                    })}
                  </p>
                </Col>
              )}
            </Row>
            <Row className="rowData">
              <Col md={4}>
                <h6>Forma de Pagamento da Adesão</h6>
                {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                {sellerMembershipForm == '1' && <p> Dinheiro </p>}
                {sellerMembershipForm == '2' && <p> Cartão </p>}
                {sellerMembershipForm == '3' && <p> Cheque </p>}
                {sellerMembershipForm == '4' && <p> Boleto </p>}
              </Col>
              <Col md={4}>
                <h6>Parcelas da Adesão</h6>
                {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                {parcelasVendedor == '1' && <p> À vista </p>}
                {parcelasVendedor == '2' && <p> 2X </p>}
                {parcelasVendedor == '3' && <p> 3x </p>}
              </Col>
            </Row>
          </div>
        )} */}
        {/* <h5>Campanhas</h5>
        <Row className="rowData">
          <Col md={4}>
            {campaingValue == '100' && <p>Isenção 1ª e 2ª mensalidades</p>}
            {campaingValue == null && (
              <p>
                O usuário não se enquadra em nenhuma das campanhas vigentes.
              </p>
            )}
          </Col>
        </Row> */}

        <h5>Forma de Pagamento da Mensalidade</h5>
        <Row className="rowData">
          <Col md={4}>
            {formaPagamento == '1' && <p>Débito em Conta</p>}
            {formaPagamento == '3' && <p>Cartão de Crédito</p>}
            {formaPagamento == '4' && <p>Boleto</p>}
          </Col>
        </Row>

        {formaPagamento == '4' && (
          <Row className="rowData">
            <Col md={4}>
              <h6>Data da Cobrança Escolhida</h6>
              <p>{dataBoletoEscolhida}</p>
            </Col>
          </Row>
        )}
        {formaPagamento == '3' && (
          <Row className="rowData">
            <Col md={4}>
              <h6>Dia da Cobrança</h6>
              <p>{carDia}</p>
            </Col>
            <Col md={4}>
              <h6>Mês da Cobrança</h6>
              {carMes == '1' && <p>Janeiro</p>}
              {carMes == '2' && <p>Fevereiro</p>}
              {carMes == '3' && <p>Março</p>}
              {carMes == '4' && <p>Abril</p>}
              {carMes == '5' && <p>Maio</p>}
              {carMes == '6' && <p>Junho</p>}
              {carMes == '7' && <p>Julho</p>}
              {carMes == '8' && <p>Agosto</p>}
              {carMes == '9' && <p>Setembro</p>}
              {carMes == '10' && <p>Outubro</p>}
              {carMes == '11' && <p>Novembro</p>}
              {carMes == '12' && <p>Dezembro</p>}
            </Col>
          </Row>
        )}
        {formaPagamento == '1' && (
          <Row className="rowData">
            <Col md={4}>
              <h6>Dia da Cobrança</h6>
              <p>{debDia}</p>
            </Col>
            <Col md={4}>
              <h6>Mês da Cobrança</h6>
              {debMes == '1' && <p>Janeiro</p>}
              {debMes == '2' && <p>Fevereiro</p>}
              {debMes == '3' && <p>Março</p>}
              {debMes == '4' && <p>Abril</p>}
              {debMes == '5' && <p>Maio</p>}
              {debMes == '6' && <p>Junho</p>}
              {debMes == '7' && <p>Julho</p>}
              {debMes == '8' && <p>Agosto</p>}
              {debMes == '9' && <p>Setembro</p>}
              {debMes == '10' && <p>Outubro</p>}
              {debMes == '11' && <p>Novembro</p>}
              {debMes == '12' && <p>Dezembro</p>}
            </Col>
          </Row>
        )}
        <div className="buttons">
          {desabilitado === false && (
            <div className="buttons">
              <Button
                type="submit"
                onClick={() => {
                  onSend(true);
                  setTimeout(() => {
                    setDesabilitado(true);
                  }, 100);
                  setTimeout(() => {
                    setDesabilitado(false);
                  }, waitTime);
                }}
              >
                {' '}
                Enviar{' '}
              </Button>
              <ButtonEdit onClick={onClose} type="button">
                Voltar
              </ButtonEdit>
            </div>
          )}
          {desabilitado === true && (
            <ButtonEdit onClick={onClose} type="button">
              Voltar
            </ButtonEdit>
          )}
        </div>
      </Content>
    </ContainerModal>
  );
}
