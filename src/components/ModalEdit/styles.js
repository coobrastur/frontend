import styled from 'styled-components';

export const ContainerModal = styled.div`
  z-index: 10;
  background: rgba(0, 0, 0, 0.6);
  width: 100%;
  height: 100%;

  position: fixed;
  top: 0;
  right: 0;
  display: ${(props) => (props.visible ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  align-itens: center;
  background-color: white;
  border-radius: 20px;
  padding: 20px 40px;

  margin-top: 12px;
  height: 100%;
  overflow-x: hidden;
  // &::-webkit-scrollbar {
  //   width: 0px;
  // }

  h4 {
    color: #01affd;
    text-align: center;
    font-size: 30px;
    font-weight: 700;
    margin-bottom: 20px;
  }

  h5 {
    color: #000;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 12px;
    &:after {
      content: ' ';
      background-color: black;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      margin-left: 10px;
      height: 1px;
      width: 75%;
    }
  }

  h6 {
    font-size: 15px;
    font-weight: 400;
  }

  h3 {
    color: #000 !important;
    font-size: 16px !important;
    font-weight: 600 !important;
    margin-bottom: 12px !important;
  }

  p {
    margin-bottom: -30px;
    width: 480px;
  }

  .buttons {
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;

    margin-top: 50px;
    margin-bottom: 0px;

    width: 100%;
  }
  .rowData {
    margin: 0;
  }
  @media (max-width: 1100px) {
    padding: 20px;
    .buttons {
      // background-color: #ccff00;
      width: 100%;
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
    p {
      margin-bottom: -30px;
      width: 300px;
    }
  }
  @media (max-width: 450px) {
    padding: 20px;
    .buttons {
      // background-color: #ccff00;
      flex-direction: column;
      // width: 100%;
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
    p {
      margin-bottom: -30px;
      width: 300px;
    }
  }
`;

export const Button = styled.button`
  background-color: #27a93b;
  border-radius: 5px;
  color: #ffffff;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px;
  padding: 7px 20px;
  text-decoration: none;
  width: 30%;

  box-shadow: 0px 4px 10px gray;

  :hover {
    background-color: #27a02b;
  }

  @media (max-width: 450px) {
    width: 50%;
  }
`;

export const ButtonEdit = styled.button`
  background-color: #076ac6;
  border-radius: 5px;
  color: #ffffff;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  padding: 7px 20px;
  text-decoration: none;
  width: 30%;

  box-shadow: 0px 4px 10px gray;

  :hover {
    background-color: #076ba6;
  }

  @media (max-width: 450px) {
    width: 50%;
  }
`;

export const ButtonDivisor = styled.button`
  background: transparent;
  cursor: pointer;
  width: 20px;
  // margin-right: 0px;
  float: right;
  justify-content: center;
  display: flex;
  margin-top: 10px;
  align-items: center;
`;
