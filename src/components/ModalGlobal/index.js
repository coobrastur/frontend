// Dependencies
import React from 'react';
import { useHistory } from 'react-router-dom';
// Styles
import { Container, Content, ButtonOutline } from './styles';

function ModalGlobal({ modalGlobal }) {
  const history = useHistory();
  const navigateTo = () => history.push('/contracts');

  return (
    <Container visible={modalGlobal}>
      <Content className="container">
        <p>
        No momento não será possível concluir o contrato, pois o CPF possui inadimplência em nosso sistema.
        Por favor solicite ao cliente que entre em contato com a Cobrança 
        para regularização antes da nova venda.
        </p>

        <div className="buttons">
          <ButtonOutline onClick={navigateTo} type="button">
            Confirmar
          </ButtonOutline>
        </div>
      </Content>
    </Container>
  );
}

export default ModalGlobal;
