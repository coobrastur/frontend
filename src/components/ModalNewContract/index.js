// Dependencies
import React, { useState } from 'react';
// import { useHistory } from 'react-router-dom';

// Styles
import { Row, Col } from 'react-grid-system';
import {
  ContainerModal,
  Content,
  Button,
  ButtonEdit,
  ButtonDivisor,
} from './styles';

// Images
import arrowUP from '../../assets/arrowUP.svg';
import arrowDown from '../../assets/arrowDown.svg';

export default function Modal({
  dados,
  associacao,
  dFiador,
  onClose = () => {},
  onSend = () => {},
}) {
  const [dadosPessoais, setDadosPessoais] = useState(true);
  const [desabilitado, setDesabilitado] = useState(false);

  // Desestruturação dos dados
  const {
    cpf,
    nome,
    email,
    birthDate,
    ddd,
    telefone,
    ufAssociado,
    cidade,
    bairroAssociado,
    rua,
    numeroAssociado,
    complemento,
    cepAssociado,
    restricao,
    formaPagamento,
    dataBoletoEscolhida,
    carDia,
    carMes,
    debDia,
    debMes,
    vendNomeSelecionado,
    cupNomeSelecionado,
    // campaingValue,
  } = dados;

  // Desestruturação de associação
  const {
    planos,
    selectedCompany,
    codigoPipeline,
    // typeOfContract,
    // flat,
    // dailyQuantities,
    // family,
    // payment,
    // halfAdhesion,
    // finalValue,
    // valorAdesao,
    totalAdesao,
    // totalAdesaoOriginal,
    totalAdesaoTable,
    // totalMensal,
    finalAdesao,
    sellerMembershipForm,
    parcelasVendedor,
    dataEscolhida,
    // parcelasCoobrastur,
    indicador,
    cpfIndic,
    nomeIndic,
    emailIndic,
    celularIndic,
  } = associacao;

  const {
    fiador,
    aditamento,
    cpfFia,
    nomeFia,
    // dataFia,
    // nacioFia,
    // civilGuarantorState,
    maeFia,
    // paiFia,
    emailFia,
    confirmaEmailFiador,
    celularFia,
    // conjugeFia,
    select,
  } = dFiador;

  // const history = useHistory();
  const waitTime = 30 * 60 * 1000;

  const totalPlanos = planos.map((x) => x.length).reduce((a, b) => a + b, 0);
  // console.log(totalPlanos);
  // console.log(planos.length);

  const dia = String(dataEscolhida.getDate()).padStart(2, '0');
  const mes = String(dataEscolhida.getMonth() + 1).padStart(2, '0');
  const ano = dataEscolhida.getFullYear();

  const dataFormatada = `${dia}/${mes}/${ano}`;

  // console.log(dataFormatada);

  return (
    <ContainerModal>
      <Content className="container">
        <h4>Revisão do Contrato</h4>
        {dadosPessoais !== true && (
          <h5>
            Dados Pessoais
            <ButtonDivisor onClick={() => setDadosPessoais(true)}>
              <img src={arrowUP} alt="" width="100%" />
            </ButtonDivisor>
          </h5>
        )}
        {dadosPessoais === true && (
          <div className="rowData">
            <h5>
              Dados Pessoais
              <ButtonDivisor
                type="push"
                onClick={() => setDadosPessoais(false)}
              >
                <img src={arrowDown} alt="" width="100%" />
              </ButtonDivisor>
            </h5>
            <Row className="rowData">
              <Col md={4}>
                <h6>CPF</h6>
                <p> {cpf} </p>
              </Col>
              <Col md={4}>
                <h6>Nome Completo</h6>
                <p> {nome} </p>
              </Col>
            </Row>

            <Row className="rowData">
              <Col md={4}>
                <h6>E-mail</h6>
                <p> {email} </p>
              </Col>
              <Col md={4}>
                <h6>Telefone</h6>
                <p>
                  {' '}
                  {ddd} {telefone}{' '}
                </p>
              </Col>
            </Row>

            <Row>
              <Col md={4}>
                <h6>Data de Nascimento</h6>
                <p>{birthDate}</p>
              </Col>
            </Row>
          </div>
        )}
        <h5>Endereço</h5>
        <Row className="rowData">
          <Col md={4}>
            <h6>Rua/Avenida</h6>
            <p> {rua} </p>
          </Col>
          <Col md={4}>
            <h6>Número</h6>
            <p> {numeroAssociado} </p>
          </Col>
          <Col md={4}>
            <h6>Complemento</h6>
            {complemento === '' && <p>Não informado</p>}
            <p> {complemento} </p>
          </Col>
        </Row>
        <Row className="rowData">
          <Col md={4}>
            <h6>Bairro</h6>
            <p className="bairro"> {bairroAssociado} </p>
          </Col>
          <Col md={4}>
            <h6>Cidade</h6>
            <p> {cidade} </p>
          </Col>
          <Col md={4}>
            <h6>Estado</h6>
            <p> {ufAssociado} </p>
          </Col>
        </Row>
        <Row className="rowData">
          <Col md={4}>
            <h6>CEP</h6>
            <p> {cepAssociado} </p>
          </Col>
        </Row>

        {restricao === true && (
          <div className="rowData">
            {fiador === true && (
              <div className="rowData">
                <h5>Restrição</h5>
                <Row className="rowData">
                  <Col md={6}>
                    <h6>Restrição</h6>
                    <p>Fiador</p>
                  </Col>
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>CPF do Fiador</h6>
                    <p> {cpfFia} </p>
                  </Col>
                  <Col md={4}>
                    <h6>Nome do Fiador</h6>
                    <p> {nomeFia} </p>
                  </Col>
                  {/* <Col md={4}>
                    <h6>Data Nascimento</h6>
                    <p> {dataFia} </p>
                  </Col> */}
                </Row>
                <Row className="rowData">
                  {/* <Col md={4}>
                    <h6>Nacionalidade fiador</h6>
                    <p> {nacioFia} </p>
                  </Col> */}
                  {/* <Col md={4}>
                    <h6>Estado Civil</h6>
                    {civilGuarantorState === '1' && <p>Solteiro(a)</p>}
                    {civilGuarantorState === '2' && <p>Casado(a)</p>}
                    {civilGuarantorState === '3' && <p>Divorciado(a)</p>}
                    {civilGuarantorState === '4' && <p>Viúvo(a)</p>}
                  </Col> */}
                  {/* {civilGuarantorState === '2' && (
                    <Col md={4}>
                      <h6>Nome Cônjuge:</h6>
                      <p> {conjugeFia} </p>
                    </Col>
                  )} */}
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>Nome da Mãe do Fiador</h6>
                    <p> {maeFia} </p>
                  </Col>
                  <Col md={4}>
                    <h6>Celular Fiador</h6>
                    <p> {celularFia} </p>
                  </Col>
                  {/* <Col md={4}>
                    <h6>Nome do Pai do Fiador</h6>
                    <p> {paiFia} </p>
                  </Col> */}
                </Row>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>E-mail do Fiador</h6>
                    <p> {emailFia} </p>
                  </Col>
                  <Col md={4}>
                    <h6>Confirmação E-mail do Fiador</h6>
                    <p> {confirmaEmailFiador} </p>
                  </Col>
                </Row>
              </div>
            )}
            {aditamento === true && (
              <div className="rowData">
                <h5>Restrição</h5>
                <Row className="rowData">
                  <Col md={4}>
                    <h6>Aditamento</h6>
                    {select === '1' && <p>Aditamento</p>}
                    {select === '2' && <p>Pré-Pago</p>}
                  </Col>
                </Row>
              </div>
            )}
          </div>
        )}
        {restricao !== true && <div className="rowData" />}

        {indicador !== true && <div className="rowData" />}
        {indicador === true && (
          <div className="rowData">
            <h5>Indicação</h5>

            <Row className="rowData">
              <Col md={4}>
                <h6>CPF Indicador</h6>
                <p> {cpfIndic} </p>
              </Col>
              <Col md={4}>
                <h6>Nome Completo do Indicador</h6>
                <p> {nomeIndic} </p>
              </Col>
            </Row>

            <Row className="rowData">
              <Col md={4}>
                <h6>E-mail Indicador</h6>
                {emailIndic === '' && <p>Não informado</p>}
                <p> {emailIndic} </p>
              </Col>
              <Col md={4}>
                <h6>Celular Indicicador</h6>
                {celularIndic === '' && <p>Não informado</p>}
                <p>{celularIndic}</p>
              </Col>
            </Row>
          </div>
        )}

        <h5>Opções de Assinaturas</h5>
        {planos.map((planos) => (
          <div>
            <Row className="rowData">
              <Col md={6}>
                <h3>Empresa</h3>
                {selectedCompany}
              </Col>

              <Col md={6}>
                <h3>N. Pipeline</h3>
                {codigoPipeline}
              </Col>
            </Row>

            {console.log(selectedCompany)}
            <Row className="rowData">
              <Col md={3}>
                <h6>Plano</h6>
                {planos.tpplcodigo === '36' && <p>Plano - Vip+</p>}
                {planos.tpplcodigo === '37' && <p>Master+</p>}
                {planos.tpplcodigo === '38' && <p>Gold Vip+</p>}
                {planos.tpplcodigo === '39' && <p>Gold Master+ </p>}
                {planos.tpplcodigo === '40' && <p>Diamante+ </p>}
                {planos.tpplcodigo === '41' && <p>Go Vip+ </p>}
                {planos.tpplcodigo === '42' && <p>Go Master+ </p>}
                {planos.tpplcodigo === '1002' && <p>Banstur Ouro </p>}
                {planos.tpplcodigo === '1016' && <p>Banstur J3 Multi </p>}
                {planos.tpplcodigo === '3000' && <p>Maridien Club </p>}
              </Col>

              <Col md={3}>
                <div>
                  <h6>Tipo de Contrato</h6>
                  {planos.tipoVenda == '1' && <p>Normal</p>}
                  {planos.tipoVenda == '2' && <p>Pré-Pago</p>}
                  {planos.tipoVenda === '11' && <p>Reativação</p>}
                </div>
              </Col>
              <Col md={3}>
                <h6>Valor da Mensalidade</h6>
                {planos.tpplcodigo === '36' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '37' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '38' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '39' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '40' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '41' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '42' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '1001' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '1002' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
                {planos.tpplcodigo === '1016' && (
                  <p>R${planos.valorMensalidade}</p>
                )}

                {planos.tpplcodigo === '3000' && (
                  <p>R${planos.valorMensalidade}</p>
                )}
              </Col>
            </Row>
            <Row className="rowData">
              <Col md={3}>
                <h6>Diárias</h6>
                <p> {planos.plQtdeDiarias} Diárias</p>
              </Col>
              <Col md={3}>
                <h6>Família</h6>
                {planos.plfamilia == 'true' && <p> Sim </p>}
                {planos.plfamilia == 'false' && <p> Não </p>}
              </Col>

              {planos.tipoVenda != '11' && (
                <Col md={3}>
                  <h6>Desconto Adesão</h6> <p>R$ {planos.descontoInput}</p>
                </Col>
              )}

              <Col md={3}>
                <h6>Parceiro</h6>
                <p>
                  {vendNomeSelecionado !== ''
                    ? vendNomeSelecionado
                    : 'Não se Aplica'}
                </p>
              </Col>
              <Col md={3}>
                <h6>Cupom</h6>
                <p>
                  {' '}
                  {cupNomeSelecionado !== ''
                    ? cupNomeSelecionado
                    : 'Não se Aplica'}
                </p>
              </Col>
            </Row>
          </div>
        ))}

        <div style={{ marginTop: '-50px', marginBottom: '-30px' }}>
          {/* {planos.map((planos) => ( */}
          <div>
            {planos.tipoVenda != '11' && (
              <Row className="rowData" style={{ marginTop: '50px' }}>
                {totalAdesao !== '0,00' && (
                  <Col md={3}>
                    <h6>Valor Total da Adesão</h6>
                    <p>
                      R${' '}
                      {totalAdesaoTable.toLocaleString('pt-br', {
                        minimumFractionDigits: 2,
                      })}
                    </p>
                  </Col>
                )}

                <Col md={3}>
                  <h6>
                    {' '}
                    Forma de Pagamento <br /> da Adesão
                  </h6>
                  {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                  {sellerMembershipForm == '1' && <p> Dinheiro </p>}
                  {sellerMembershipForm == '2' && <p> Cartão </p>}
                  {sellerMembershipForm == '3' && <p> Cheque </p>}
                  {sellerMembershipForm == '4' && <p> Boleto </p>}
                </Col>

                <Col md={3}>
                  <h6>Parcelas da Adesão</h6>
                  {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                  {parcelasVendedor == '1' && <p> À vista </p>}
                  {parcelasVendedor == '2' && <p> 2X </p>}
                  {parcelasVendedor == '3' && <p> 3x </p>}
                </Col>

                {sellerMembershipForm == '' ? (
                  <Col md={3}>
                    <h6>Data da Primeira Adesão</h6>
                    <p>Não se aplica</p>
                  </Col>
                ) : (
                  <Col md={3}>
                    <h6>Data da Primeira Adesão</h6>
                    <p>{dataFormatada}</p>
                  </Col>
                )}
              </Row>
            )}
          </div>
          {/* ))} */}

          {planos.tpplcodigo >= '41' && (
            <div>
              <Row className="rowData">
                <Col md={4}>
                  <h6>Família</h6>
                  {planos.plfamilia == 'true' && <p> Sim </p>}
                  {planos.plfamilia == 'false' && <p> Não </p>}
                </Col>
                <Col md={4}>
                  <h6>Valor Adesão</h6>
                  <p>
                    R${' '}
                    {planos.valorAdesaoOriginal.toLocaleString('pt-br', {
                      minimumFractionDigits: 2,
                    })}
                  </p>
                </Col>
                {finalAdesao.isNaN && (
                  <Col md={4}>
                    <h6>Adesão com Desconto</h6>
                    <p>
                      R${' '}
                      {finalAdesao.toLocaleString('pt-br', {
                        minimumFractionDigits: 2,
                      })}
                    </p>
                  </Col>
                )}
              </Row>
              <Row className="rowData">
                <Col md={4}>
                  <h6>Forma de Pagamento da Adesão</h6>
                  {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                  {sellerMembershipForm == '1' && <p> Dinheiro </p>}
                  {sellerMembershipForm == '2' && <p> Cartão </p>}
                  {sellerMembershipForm == '3' && <p> Cheque </p>}
                  {sellerMembershipForm == '4' && <p> Boleto </p>}
                </Col>
                <Col md={4}>
                  <h6>Parcelas da Adesão</h6>
                  {sellerMembershipForm == '' && <p> Não se Aplica </p>}
                  {parcelasVendedor == '1' && <p> À vista </p>}
                  {parcelasVendedor == '2' && <p> 2X </p>}
                  {parcelasVendedor == '3' && <p> 3x </p>}
                </Col>
              </Row>
            </div>
          )}
        </div>
        {/* ))} */}
        {/* <Row className="rowData">
          <Col md={4}>
            <h6>Forma de Pagamento</h6>
            {payment === '1' && sellerMembershipForm === '1' && <p>Espécie</p>}
            {payment === '2' && halfAdhesion === '1' && <p> Boleto</p>}
          </Col>
          <Col md={4}>
            <h6>Parcelas</h6>
            {payment === '1' && parcelasVendedor === '1' && <p> À Vista </p>}
            {payment === '2' && parcelasCoobrastur === '1' && <p> À Vista </p>}
            {payment === '2' && parcelasCoobrastur === '2' && <p> 2X </p>}
            {payment === '2' && parcelasCoobrastur === '3' && <p> 3X </p>}
          </Col>
        </Row> */}
        {/* <h5>Campanhas</h5>
        <Row className="rowData">
          <Col md={4}>
            {campaingValue == '100' && <p>Isenção 1ª e 2ª mensalidades</p>}
            {campaingValue == '0' && (
              <p>
                O usuário não se enquadra em nenhuma das campanhas vigentes.
              </p>
            )}
            {/* {campaingValue === null && (
              <p>
                O usuário não se enquadra em nenhuma das campanhas vigentes.
              </p>
            )}
          </Col>
        </Row> */}

        <h5>Forma de Pagamento da Mensalidade</h5>
        <Row className="rowData">
          <Col md={4}>
            {formaPagamento == '1' && <p>Débito em Conta Corrente</p>}
            {formaPagamento == '2' && <p>Cartão de Crédito</p>}
            {formaPagamento == '4' && <p>Boleto</p>}
          </Col>
        </Row>

        {formaPagamento == '4' && (
          <Row className="rowData">
            <Col md={4}>
              <h6>Data da Cobrança Escolhida</h6>
              <p>{dataBoletoEscolhida}</p>
            </Col>
          </Row>
        )}
        {formaPagamento == '2' && (
          <Row className="rowData">
            <Col md={4}>
              <h6>Dia da Cobrança</h6>
              <p>{carDia}</p>
            </Col>
            <Col md={4}>
              <h6>Mês da Cobrança</h6>
              {carMes == '1' && <p>Janeiro</p>}
              {carMes == '2' && <p>Fevereiro</p>}
              {carMes == '3' && <p>Março</p>}
              {carMes == '4' && <p>Abril</p>}
              {carMes == '5' && <p>Maio</p>}
              {carMes == '6' && <p>Junho</p>}
              {carMes == '7' && <p>Julho</p>}
              {carMes == '8' && <p>Agosto</p>}
              {carMes == '9' && <p>Setembro</p>}
              {carMes == '10' && <p>Outubro</p>}
              {carMes == '11' && <p>Novembro</p>}
              {carMes == '12' && <p>Dezembro</p>}
            </Col>
          </Row>
        )}
        {formaPagamento == '1' && (
          <Row className="rowData">
            {/* <Col md={3}>
              <h6>Total Mensalidade</h6>
              <p>
                R${' '}
                {totalMensal.toLocaleString('pt-br', {
                  minimumFractionDigits: 2,
                })}
              </p>
            </Col> */}
            <Col md={4}>
              <h6>Dia da Cobrança</h6>
              <p>{debDia}</p>
            </Col>
            <Col md={4}>
              <h6>Mês da Cobrança</h6>
              {debMes == '1' && <p>Janeiro</p>}
              {debMes == '2' && <p>Fevereiro</p>}
              {debMes == '3' && <p>Março</p>}
              {debMes == '4' && <p>Abril</p>}
              {debMes == '5' && <p>Maio</p>}
              {debMes == '6' && <p>Junho</p>}
              {debMes == '7' && <p>Julho</p>}
              {debMes == '8' && <p>Agosto</p>}
              {debMes == '9' && <p>Setembro</p>}
              {debMes == '10' && <p>Outubro</p>}
              {debMes == '11' && <p>Novembro</p>}
              {debMes == '12' && <p>Dezembro</p>}
            </Col>
          </Row>
        )}
        <div className="buttons">
          {desabilitado === false && (
            <div className="buttons">
              <Button
                id="contract.modalBtnSend"
                type="submit"
                onClick={() => {
                  onSend(true);
                  setTimeout(() => {
                    setDesabilitado(true);
                  }, 100);
                  setTimeout(() => {
                    setDesabilitado(false);
                  }, waitTime);
                }}
              >
                {' '}
                Enviar{' '}
              </Button>
              <ButtonEdit
                onClick={onClose}
                type="button"
                id="contract.modalBtnBack"
              >
                Voltar
              </ButtonEdit>
            </div>
          )}
          {desabilitado === true && (
            <ButtonEdit onClick={onClose} type="button">
              Voltar
            </ButtonEdit>
          )}
        </div>
      </Content>
    </ContainerModal>
  );
}
