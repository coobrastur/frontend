// Dependencies
import React from 'react';
import { useHistory } from 'react-router-dom';
// Styles
import { Container, Content, ButtonOutline } from './styles';

function ModalNoEdit({ modalGlobal }) {
  const history = useHistory();
  const navigateTo = () => history.push('/contracts');

  return (
    <Container visible={modalGlobal}>
      <Content className="container">
        <p>Não é possível editar esse contrato. Exclua e faça um novo</p>

        <div className="buttons">
          <ButtonOutline onClick={navigateTo} type="button">
            Confirmar
          </ButtonOutline>
        </div>
      </Content>
    </Container>
  );
}

export default ModalNoEdit;
