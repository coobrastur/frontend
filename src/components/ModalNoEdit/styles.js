import styled from 'styled-components';

export const Container = styled.div`
  z-index: 99 !important;
  background: rgba(0, 0, 0, 0.6);
  width: 100%;
  height: 100%;

  position: fixed;
  top: 0;
  right: 0;
  display: ${(props) => (props.visible ? 'flex' : 'none')};
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  background-color: white;
  border-radius: 20px;
  padding: 50px;

  h2 {
    color: #01affd;
    font-size: 50px;
    font-weight: 700;
    margin-bottom: 20px;
  }

  p span {
    font-family: 'Livvic', sans-serif;
    color: #f54756;
    font-size: 20px;
  }

  p {
    font-family: 'Livvic', sans-serif;
    font-size: 20px;
    text-align: center;
    color: rgba(0, 0, 0, 0.6);
    margin-bottom: 20px;
  }

  .buttons {
    display: flex;
    justify-content: center;
  }

  @media (max-width: 700px) {
    h2 {
      font-size: 30px;
    }

    p span {
      font-size: 18px;
    }

    p {
      font-size: 16px;
    }
    .buttons {
      display: grid;
    }
  }
`;

export const ButtonOutline = styled.button`
  border: 1px solid #f54756;
  border-radius: 10px;
  background-color: white;
  color: #f54756;

  font-size: 14px;
  text-transform: uppercase;

  padding: 12px;
  margin-top: 5px;
  margin-right: 10px;

  width: 30%;

  cursor: pointer;
  transition: all.2s;

  :hover {
    background-color: #f54756;
    color: white;
    cursor: pointer;
  }

  @media (max-width: 700px) {
    width: 100%;
  }
`;
