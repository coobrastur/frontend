// Dependencies
import React from 'react';
import { useHistory } from 'react-router-dom';
import info from '../../assets/info-tooltip.svg';
// Styles
import { Container, Content, Button, ButtonOutline, InfoView } from './styles';

export default function ModalReativacao({
  modalReativacao,
  onSend = () => {},
}) {
  const history = useHistory();
  const navigateTo = () => history.push('/contracts');

  return (
    <Container visible={modalReativacao}>
      <Content className="container">
        <InfoView>
          <img src={info} alt="" width="10%" />
        </InfoView>

        <h3> Existem contratos inativos para esse associado.</h3>

        <div className="buttons">
          <Button onClick={onSend} type="button">
            Reativar
          </Button>
          <ButtonOutline onClick={navigateTo} type="button">
            Voltar
          </ButtonOutline>
        </div>
      </Content>
    </Container>
  );
}
