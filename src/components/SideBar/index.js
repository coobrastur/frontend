import React from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import * as userActions from '../../store/ducks/auth';

import { Container } from './styles';

// Images
import iconUser from '../../assets/icon-user.svg';
import iconAllUser from '../../assets/all-users.svg';
import iconContract from '../../assets/icon-contract.svg';
import allContracts from '../../assets/all-contracts.svg';
import iconNewContract from '../../assets/icon-new-contract.svg';
import iconGreat from '../../assets/great.svg';
import iconLogout from '../../assets/icon-logout.svg';

// components
import { showToast } from '../Alert';

function SideMenu() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { nivCodigo } = useSelector((state) => state.auth);

  async function logout() {
    dispatch(userActions.login(null));
    history.push('/');
    showToast({
      type: 'success',
      message: 'Você saiu da sua conta com sucesso!',
    });
  }

  return (
    <Container>
      {nivCodigo === 1 && (
        <nav>
          <ul>
            <li>
              <Link to="/myAccount">
                <img src={iconUser} alt="" />
                <p>Minha Conta</p>
              </Link>
            </li>

            <li>
              <Link to="/contracts">
                <img src={iconContract} alt="" />
                <p>Contratos</p>
              </Link>
            </li>

            <li>
              <Link to="/newContract">
                <img src={iconNewContract} alt="" />
                <p>Novo Contrato</p>
              </Link>
            </li>
          </ul>
        </nav>
      )}

      {nivCodigo > 1 && (
        <nav>
          <li>
            <Link to="/myAccount">
              <img src={iconUser} alt="" />
              <p>Minha Conta</p>
            </Link>
          </li>

          <li>
            <Link to="/allcontracts">
              <img src={allContracts} alt="" />
              <p>Todos os Contratos</p>
            </Link>
          </li>

          <li>
            <Link to="/adduser">
              <img src={iconGreat} alt="" />
              <p>Criar vendedor</p>
            </Link>
          </li>

          <li>
            <Link to="/allusers">
              <img src={iconAllUser} alt="" />
              <p>Todos os Vendedores</p>
            </Link>
          </li>
        </nav>
      )}

      <button type="button" onClick={() => logout()}>
        <img src={iconLogout} alt="" />
        <p>Sair</p>
      </button>
    </Container>
  );
}

export default SideMenu;
