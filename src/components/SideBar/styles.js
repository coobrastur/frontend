import styled from 'styled-components';

export const Container = styled.div`
  width: 15%;
  height: 100%;
  background-color: #01affd;

  position: fixed;
  top: 0;
  left: 0;

  display: flex;
  flex-direction: column;
  justify-content: space-between;

  padding: 30px 20px;

  li {
    margin-bottom: 25px;
    list-style: none;
  }

  a {
    color: white;
    display: flex;
    align-items: center;
    text-decoration: none;
  }

  img {
    height: 30px;
  }

  a p {
    margin-left: 10px;
  }

  a:hover {
    text-decoration: underline;
  }

  button {
    background-color: transparent;
    display: flex;
    align-items: center;
    cursor: pointer;
  }

  button p {
    color: white;
    margin-left: 10px;
  }

  button p:hover {
    text-decoration: underline;
  }

  @media (max-width: 1200px) {
    width: 20%;
  }

  @media (max-width: 900px) {
    width: 25%;
  }

  @media (max-width: 700px) {
    width: 70px;

    a p,
    button p {
      display: none;
    }
  }
`;
