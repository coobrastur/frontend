import styled from 'styled-components';

export const InputStyle = styled.input`
  border: 1px solid #c4c4c4;
  box-sizing: border-box;
  border-radius: 6px;

  color: rgba(0, 0, 0, 0.3);

  font-size: 18px;

  margin-bottom: 20px;
  padding: 10px;

  ::placeholder {
    color: rgba(0, 0, 0, 0.3);
  }

  // @media (max-width: 1100px) {
  //   width: 30% !important;
  // }
  @media (max-width: 500px) {
    width: 100% !important;
  }
`;
