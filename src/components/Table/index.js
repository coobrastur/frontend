import React from 'react';
import Table from 'react-bootstrap/Table';

import 'bootstrap/dist/css/bootstrap.min.css';

function BasicExample({ plan, remove, tipo }) {
  // console.log(plan);
  // console.log(remove);
  return (
    <Table striped responsive>
      <thead style={{ textAlign: 'center' }}>
        <tr>
          <th>Ação</th>
          <th>Plano</th>
          <th>Diárias</th>
          <th>Tipo</th>
          <th>Família</th>
          <th>Adesão</th>
          <th>Desconto Aplicado</th>
          <th>Valor Cobrado na Adesão</th>
          <th>Valor Mensalidade</th>
          <th>N. Pipeline</th>
        </tr>
      </thead>
      <tbody style={{ textAlign: 'center' }}>
        {plan.map((planos) => (
          <tr key={planos.id}>
            <td>
              <button
                type="button"
                style={{
                  color: 'red',
                  backgroundColor: 'transparent',
                  fontWeight: '700',
                }}
                onClick={() => remove(planos.id)}
              >
                Excluir
              </button>
            </td>
            {planos.tpplcodigo == '' && <td>-</td>}
            {planos.tpplcodigo == 36 && <td>Vip+</td>}
            {planos.tpplcodigo == 37 && <td>Master+</td>}
            {planos.tpplcodigo == 38 && <td>Gold Vip+</td>}
            {planos.tpplcodigo == 39 && <td>Gold Master+</td>}
            {planos.tpplcodigo == 40 && <td>Diamante+</td>}
            {planos.tpplcodigo == 41 && <td>Go Vip+</td>}
            {planos.tpplcodigo == 42 && <td>Go Master+</td>}
            {planos.tpplcodigo == 1001 && <td>Banstur Executivo</td>}
            {planos.tpplcodigo == 1002 && <td>Banstur Ouro</td>}
            {planos.tpplcodigo == 1016 && <td>Banstur J3 Multi</td>}
            {planos.tpplcodigo == 3000 && <td>Meridien Club</td>}
            {/* <td>{planos.tpplcodigo == 37 || 'Vip+'}</td> */}
            <td>{planos.plQtdeDiarias}</td>
            {planos.tipoVenda == '1' && <td>Normal</td>}
            {planos.tipoVenda == '2' && <td>Pré-Pago</td>}
            {planos.tipoVenda == '11' && <td>Reativação</td>}
            {/* <td>{planos.tipoVenda}</td> */}
            {planos.plfamilia == 'true' && <td>Sim</td>}
            {planos.plfamilia == 'false' && <td>Não</td>}
            {/* <td>{planos.plfamilia}</td> */}

            {planos.adesao == true && <td>Sim</td>}
            {planos.adesao == false && <td>Não</td>}
            <td>
              R${' '}
              {planos.descontoInput.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
              })}
            </td>
            {tipo === '1' && (
              <td>
                R${' '}
                {typeof planos.valorAdesao === 'number'
                  ? planos.valorAdesao
                      .toFixed(2)
                      .toLocaleString()
                      .replace('.', ',')
                  : 'Valor inválido'}
              </td>
            )}
            {tipo === '2' && (
              <td>
                R${' '}
                {planos.valorAdesao
                  .toFixed(2)
                  .toLocaleString()
                  .replace('.', ',')}
              </td>
            )}
            {tipo === '11' && <td>R$ {planos.valorAdesao}</td>}
            <td>R$ {planos.valorMensalidade}</td>
            <td>{planos.nroPipeline}</td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
}

export default BasicExample;
