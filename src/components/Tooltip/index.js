import React from 'react';
import { TooltipField, TooltipText } from './styles';

export default function Tooltip({ text, iconColor, color, backgroundColor }) {
  return (
    <TooltipField>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="100%"
        viewBox="0 0 24 24"
        fill={iconColor} // Aqui você está aplicando diretamente a cor ao ícone
      >
        <path d="M12 0C5.383 0 0 5.383 0 12s5.383 12 12 12 12-5.383 12-12S18.617 0 12 0zm1 18H11v-6h2v6zm0-8H11V5h2v5z" />
      </svg>
      <TooltipText
        style={{
          color: `${color}`,
          backgroundColor: `${backgroundColor}`,
          borderRightColor: `${backgroundColor}`,
          fontSize: '15px',
        }}
        className="tooltiptext"
      >
        {text}
      </TooltipText>
    </TooltipField>
  );
}
