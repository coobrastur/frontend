import React from 'react';
// import info from '../../assets/info-tooltip.svg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { TooltipField, TooltipText } from './styles';

export default function Tooltip({ text, color, backgroundColor, adding }) {
  return (
    <TooltipField>
      <FontAwesomeIcon icon={faPlus} onClick={() => adding()} />
      <TooltipText
        style={{
          color: `${color}`,
          backgroundColor: `${backgroundColor}`,
          borderRightColor: `${backgroundColor}`,
        }}
        className="tooltiptext"
      >
        {text}
      </TooltipText>
    </TooltipField>
  );
}
