import styled from 'styled-components';

export const TooltipField = styled.div`
  position: relative;
  display: flex;
  justify-content: center;
  float: right;
  align-items: center;
  padding: 5px;
  width: 25px;
  heigth: 25px;
  text-decoration: none;
  color: #01affd;
  .tooltiptext {
    visibility: hidden;
    width: 120px;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    top: -5px;
    left: 105%;
  }
  :hover {
    cursor: pointer;
  }
  :hover .tooltiptext {
    visibility: visible;
    cursor: auto;
  }
`;

export const TooltipText = styled.p`
  text-decoration: none;
  font-size: 16px;
  visibility: hidden;
  width: 120px;
  text-align: center;
  margin-left: 10px;
  border-radius: 6px;
  padding: 5px 0;
  position: absolute;
  z-index: 1;
  top: -5px;
  left: 105%;
  :before {
    width: 0;
    height: 0;
    border: 10px solid transparent;
    border-right-color: #01affd;
    content: '';
    left: -20px;
    position: absolute;
  }
  :hover {
    visibility: visible;
  }
`;
