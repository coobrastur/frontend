// Dependencies
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { api } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';

// Styles
import {
  Container,
  SeeButton,
  EditButton,
  BlockItems,
  BlockItem,
  Line,
  Type,
} from './styles';

function AllContracts() {
  const history = useHistory();
  const [contracts, setContracts] = useState([]);
  const { token } = useSelector((state) => state.auth);

  async function getContracts() {
    const config = {
      headers: { authorization: `Bearer ${token}` },
    };

    await api.get('contratos/all', config).then((response) => {
      console.log(response.data);
      setContracts(response.data);
    });
  }

  useEffect(() => {
    getContracts();
  }, []);

  return (
    <Container>
      <MenuHamburguer />
      <div className="container">
        <h2>Todos os Contratos</h2>
        <BlockItems>
          {contracts.map((c) => (
            <BlockItem key={c._id}>
              <h3>{c.assNome_RazaoSocial}</h3>
              <Line />

              <div className="grid">
                <p>
                  <span>CPF: </span>
                  {c.assCPF_CNPJ}
                </p>

                <p>
                  <span>Nome Vendedor: </span>
                  {c.usuNome}
                </p>

                <p>
                  <span>Usuário Vendedor: </span>
                  {c.usuUsuario}
                </p>

                <p>
                  <span>Data de criação: </span>
                  {new Date(c.criadoEm).toLocaleDateString('pt-BR', {
                    timeZone: 'UTC',
                  })}
                </p>

                <p>
                  <span>Status: </span>
                  {c.status}
                </p>
              </div>

              <Type>{c.plano}</Type>

              <div className="buttons">
                <SeeButton
                  type="button"
                  onClick={() => history.push('/viewcontract', { id: c.id })}
                >
                  Ver
                </SeeButton>

                <EditButton
                  type="button"
                  onClick={() => history.push('/edit  contract', { id: c.id })}
                >
                  Editar
                </EditButton>
              </div>
            </BlockItem>
          ))}
        </BlockItems>
      </div>
    </Container>
  );
}

export default AllContracts;
