import styled from 'styled-components';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  min-height: 100vh;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    padding-top: 50px;
  }

  @media (max-width: 1100px) {
    padding: 10px 0;

    h2 {
      text-align: center;
    }
  }
`;

export const SeeButton = styled.button`
  background-color: #00cecb;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px 0;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #129e9c;
  }
`;

export const EditButton = styled.button`
  background-color: #fbf33b;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px 0;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #dfd82c;
  }
`;

export const BlockItems = styled.div`
  padding: 50px 0px;

  /* width: 60%; */
  margin: 0 auto;

  @media (max-width: 1100px) {
    padding: 10px;

    width: 100%;
    margin: 0 auto;
  }
`;

export const BlockItem = styled.div`
  background-color: white;
  border-radius: 10px;

  padding: 10px;
  margin-bottom: 30px;

  h3 {
    color: #01affd;
    font-family: 'Livvic', sans-serif;
    font-size: 30px;
  }

  .grid {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr;
  }

  p span {
    font-family: 'Livvic', sans-serif;
    color: #01affd;
  }

  p {
    font-family: 'Livvic', sans-serif;
    font-size: 16px;
    color: rgba(0, 0, 0, 0.6);
    margin-bottom: 10px;
  }

  .buttons {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media (max-width: 1100px) {
    .grid {
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 1fr 1fr 1fr 1fr 1fr;
    }

    .buttons {
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
  }
`;

export const Line = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.2);
  margin: 10px 0;
`;

export const Type = styled.div`
  background-color: #01affd;
  border-radius: 50px;
  color: white;

  display: inline-block;

  font-family: 'Livvic', sans-serif;
  font-size: 18px;

  margin: 10px 0;
  padding: 5px 15px;
  text-decoration: none;
  text-align: center;
`;
