// Dependencies
import React, { useEffect, useState } from 'react';
import { api } from '../../services/api';

// Components
import MenuHamburguer from '../../components/MenuHamburguer';
import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';

// Styles
import { Container, DeleteButton, BlockItems, BlockItem, Line } from './styles';

function AllUsers() {
  const [users, setUsers] = useState([]);
  const [loader, setLoader] = useState(false);

  async function getUsers() {
    await api.get('/users/index').then((response) => {
      setUsers(response.data);
    });
  }

  async function deleteUser(id) {
    setLoader(true);

    try {
      await api.delete(`/users/delete/${id}`).then((response) => {
        showToast({
          type: 'success',
          message: response.data.message,
        });

        getUsers();
        setLoader(false);
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    getUsers();
  }, []);

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />

      <div className="container">
        <h2>Todos os Vendedores</h2>
        <BlockItems>
          {users.map((u) => (
            <BlockItem key={u._id}>
              <h3>{u.name}</h3>
              <Line />

              <p>
                <span>Usuário: </span>
                {u.user}
              </p>

              <p>
                <span>CPF: </span>
                {u.cpf}
              </p>

              <p>
                <span>Email: </span>
                {u.email}
              </p>

              <p>
                <span>Status: </span>
                {u.status}
              </p>

              <div className="buttons">
                <DeleteButton type="button" onClick={() => deleteUser(u._id)}>
                  Deletar
                </DeleteButton>
              </div>
            </BlockItem>
          ))}
        </BlockItems>
      </div>
    </Container>
  );
}

export default AllUsers;
