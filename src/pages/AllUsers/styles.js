import styled from 'styled-components';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  min-height: 100vh;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    padding-top: 50px;
  }
`;

export const DeleteButton = styled.button`
  background-color: #d2254e;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1.2em;

  margin: 10px 0;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #ac2545;
  }
`;

export const BlockItems = styled.div`
  padding: 50px 0px;

  /* width: 60%; */
  margin: 0 auto;

  @media (max-width: 1100px) {
    padding: 10px;

    width: 100%;
    margin: 0 auto;
  }
`;

export const BlockItem = styled.div`
  background-color: white;
  border-radius: 10px;

  padding: 20px;
  margin-bottom: 30px;

  h3 {
    color: #01affd;
    font-family: 'Livvic', sans-serif;
    font-size: 30px;
  }

  p span {
    font-family: 'Livvic', sans-serif;
    color: #01affd;
  }

  p {
    font-family: 'Livvic', sans-serif;
    font-size: 18px;
    color: rgba(0, 0, 0, 0.6);
    margin-bottom: 10px;
  }

  .buttons {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media (max-width: 1100px) {
    .buttons {
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
  }
`;

export const Line = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.2);
  margin: 10px 0;
`;

export const Type = styled.div`
  background-color: #01affd;
  border-radius: 50px;
  color: white;

  display: inline-block;

  font-family: 'Livvic', sans-serif;
  font-size: 18px;

  margin: 10px 0;
  padding: 10px 20px;
  text-decoration: none;
  text-align: center;
`;
