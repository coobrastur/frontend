// Dependencies
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { apiClei } from '../../services/api';
import { Row, Col } from 'react-grid-system';

// Components
import MenuHamburguer from '../../components/MenuHamburguer';
import InputMask from '../../components/InputMask';
import Loader from '../../components/Loader';
import { showToast } from '../../components/Alert';
import Tooltip from '../../components/Tooltip';

// Images
import imageBackArrow from '../../assets/arrow-left.png';
import arrowUP from '../../assets/arrowUP.svg';
import arrowDown from '../../assets/arrowDown.svg';

// Styles
import {
  Container,
  Content,
  BackArrow,
  FormContent,
  BlockInputs,
  Button,
  ButtonDivisor,
  RadioInputs,
} from './styles';

// Constants
const API_URL = 'https://telemarketing.coobrastur.com.br/wsAppVendasV2';

export default function ChangeCupom() {
  const history = useHistory();
  const [loader, setLoader] = useState(false);
  const { token } = useSelector((state) => state.auth);

  // Accordion
  const [dadosAssinante, setDadosAssinante] = useState(true);
  const [dadosPlano, setDadosPlano] = useState(true);
  const [dadosParceiros, setDadosParceiros] = useState(true);

  // Dados assinante
  const [cpf, setCPF] = useState('');
  const [assinanteData, setAssinanteData] = useState([]);
  const [nicSelecionado, setNICSelecionado] = useState('');
  const [empresasFiltradas, setEmpresasFiltradas] = useState([]);
  const [company, setCompany] = useState('');

  // Dados plano
  const [planos, setPlanos] = useState([]);
  const [planoSelecionado, setPlanoSelecionado] = useState('');
  const [planoCodigo, setPlanoCodigo] = useState('');
  const [family, setFamily] = useState('');

  // Dados cupons
  const [vendNomes, setVendNomes] = useState([]);
  const [cupNomes, setCupNomes] = useState([]);
  const [vendNomeSelecionado, setVendNomeSelecionado] = useState('');
  const [vendCodigoSelecionado, setVendCodigoSelecionado] = useState('');
  const [cupNomeSelecionado, setCupNomeSelecionado] = useState('');
  const [cupCodigoSelecionado, setCupCodigoSelecionado] = useState('');
  const [dados, setDados] = useState([]);

  // Dados promoções
  const [promocoes, setPromocoes] = useState([]); // Lista de promoções
  const [proCodigoSelecionado, setProCodigoSelecionado] = useState(''); // Código da promoção selecionada
  const [proDescricaoSelecionada, setProDescricaoSelecionada] = useState(''); // Descrição da promoção selecionada

  // Função para remover máscara do CPF
  const retiraMascara = (cpf) => cpf.replace(/[^\d]/g, '');

  // Função para verificar CPF na API
  const fetchNicsByCPF = async () => {
    if (cpf.length !== 14 || cpf.includes('_')) {
      showToast({
        type: 'warning',
        message: 'Por favor, insira um CPF válido.',
      });
      return;
    }

    try {
      setLoader(true);
      const response = await apiClei.get(
        `${API_URL}/ConsultaCrm.asmx/ListaNics?in_pesquisa=${retiraMascara(
          cpf
        )}`
      );
      setAssinanteData(response.data); // Armazena os dados da API
    } catch (error) {
      showToast({
        type: 'error',
        message: 'Erro ao consultar CPF. Tente novamente mais tarde.',
      });
    } finally {
      setLoader(false);
    }
  };

  // Função para lidar com mudanças no CPF
  const handleCPFChange = (e) => {
    const novoCPF = e.target.value;
    setCPF(novoCPF);

    // Limpa os dados do NIC e da empresa quando o CPF é alterado
    setAssinanteData([]); // Limpa os dados da API
    setNICSelecionado(''); // Limpa o NIC selecionado
    setEmpresasFiltradas([]); // Limpa as empresas filtradas
    setCompany(''); // Limpa a empresa selecionada
    setPlanos([]); // Limpa os planos
    setPlanoSelecionado(''); // Limpa o plano selecionado
    setFamily(''); // Limpa a família
  };

  // Função para filtrar empresas com base no NIC selecionado
  const handleNICChange = (e) => {
    const nicSelecionado = e.target.value;
    setNICSelecionado(nicSelecionado);
    setLoader(true);
    setCompany('');
    setTimeout(() => {
      // Filtra as empresas com base no NIC selecionado após 3 segundos
      const empresasFiltradas = assinanteData.filter(
        (item) => item.assnic === Number(nicSelecionado)
      );
      setEmpresasFiltradas(empresasFiltradas);
      setLoader(false);
    }, 600);
  };

  // Função para consultar os planos do assinante
  const fetchPlanosByNIC = async (nic) => {
    try {
      setLoader(true);
      const response = await apiClei.get(
        `${API_URL}/ConsultaCrm.asmx/ListaPlanos?in_assnic=${nic}&in_tpBusca=0`
      );
      setPlanos(response.data); // Armazena os planos retornados pela API
    } catch (error) {
      showToast({
        type: 'error',
        message: 'Erro ao consultar os planos. Tente novamente mais tarde.',
      });
    } finally {
      setLoader(false);
    }
  };

  // Função para consultar os cupons do plano
  const fetchCuponsByPlano = async () => {
    try {
      setLoader(true);
      const response = await apiClei.get(
        `${API_URL}/Contrato.asmx/ListaParceiroCupons?vendCodigo=&reprCodigo=&cupCodigo=&empCod=${company}`
      );
      setDados(response.data); // Atualiza os dados com os cupons disponíveis
    } catch (error) {
      showToast({
        type: 'error',
        message: 'Erro ao consultar os cupons. Tente novamente mais tarde.',
      });
    } finally {
      setLoader(false);
    }
  };

  const fetchPromocoes = async () => {
    try {
      setLoader(true);
      const response = await apiClei.get(
        `${API_URL}/ConsultaCrm.asmx/ListarPromocoes?in_proCodigo=`
      );
      setPromocoes(response.data); // Armazena a lista de promoções
    } catch (error) {
      showToast({
        type: 'error',
        message: 'Erro ao consultar as promoções. Tente novamente mais tarde.',
      });
    } finally {
      setLoader(false);
    }
  };
  useEffect(() => {
    fetchPromocoes();
  }, []);

  const handlePromocaoChange = (e) => {
    const proCodigo = e.target.value;

    if (proCodigo === '0') {
      setProCodigoSelecionado('0');
      setProDescricaoSelecionada('0');
      return;
    }

    const promocaoSelecionada = promocoes.find(
      (promocao) => String(promocao.proCodigo) === String(proCodigo) // Converte ambos para string
    );

    if (promocaoSelecionada) {
      setProCodigoSelecionado(promocaoSelecionada.proCodigo);
      setProDescricaoSelecionada(promocaoSelecionada.proDescricao);
    } else {
      setProCodigoSelecionado('');
      setProDescricaoSelecionada('');
    }
  };

  const handleVendNomeChange = (event) => {
    const vendNome = event.target.value;

    if (vendNome == '0') {
      setVendNomeSelecionado('0');
      setVendCodigoSelecionado(0);
      setCupNomeSelecionado('0');
      setCupCodigoSelecionado(0);
      setCupNomes([]); // Zera a lista de cupons
      return;
    }

    setVendNomeSelecionado(vendNome);
    setCupNomeSelecionado('');
    setCupCodigoSelecionado('');

    // Filtrar os cupons correspondentes ao vendNome selecionado
    const cuponsFiltrados = dados
      .filter((item) => item.vendNome === vendNome)
      .map((item) => item.cupNome);

    setCupNomes(cuponsFiltrados);

    // Encontrar o vendCodigo correspondente ao vendNome selecionado
    const vendCodigo = dados.find(
      (item) => item.vendNome === vendNome
    )?.vendCodigo;
    setVendCodigoSelecionado(vendCodigo);
  };

  const handleCupNomeChange = (event) => {
    const cupNome = event.target.value;

    if (cupCodigoSelecionado == '0') {
      setCupNomeSelecionado('0');
      setCupCodigoSelecionado(0);
      setCupNomes([]); // Zera a lista de cupons
      return;
    }
    setCupNomeSelecionado(cupNome);

    // Encontrar o cupom selecionado pelo nome
    const cupomSelecionado = dados.find(
      (item) =>
        item.vendNome === vendNomeSelecionado && item.cupNome === cupNome
    );

    if (cupomSelecionado) {
      setCupCodigoSelecionado(cupomSelecionado.cupCodigo);
    } else {
      setCupCodigoSelecionado('');
    }
  };

  // Função para lidar com a seleção da empresa
  const handleCompanyChange = (e) => {
    const empresaSelecionada = e.target.value;
    setCompany(empresaSelecionada);

    // Limpa os planos ao alterar a empresa
    setPlanos([]);
    setPlanoSelecionado('');
    setFamily('');

    // Consulta os planos após selecionar a empresa
    if (empresaSelecionada && nicSelecionado) {
      fetchPlanosByNIC(nicSelecionado);
    }
  };

  // Função para lidar com a seleção do plano
  const handlePlanChange = async (e) => {
    const planoSelecionado = e.target.value;
    setPlanoSelecionado(planoSelecionado);

    // Encontra o plano selecionado
    const planoAtual = planos.find((p) => p.plCodigo == planoSelecionado);
    setPlanoCodigo(planoAtual.tpplCodigo);

    if (planoAtual) {
      console.log(planoAtual);
      if (planoAtual.proCodigo && planoAtual.proCodigo !== -1) {
        // Busca todas as promoções
        await fetchPromocoes();

        // Filtra a promoção correspondente ao proCodigo do plano
        const promocaoCorrespondente = promocoes.find(
          (promocao) => promocao.proCodigo === planoAtual.proCodigo
        );

        if (promocaoCorrespondente) {
          // Define a promoção correspondente
          setProCodigoSelecionado(promocaoCorrespondente.proCodigo);
          setProDescricaoSelecionada(promocaoCorrespondente.proDescricao);
        } else {
          // Se não encontrar a promoção, limpa os campos
          setProCodigoSelecionado('');
          setProDescricaoSelecionada('');
        }
      } else {
        // Se o plano não tiver proCodigo, exibe a lista completa de promoções
        await fetchPromocoes();
        setProCodigoSelecionado('');
        setProDescricaoSelecionada('');
      }

      // Verifica se o plano possui um cupom associado
      if (planoAtual.cupCodigo) {
        // Define o parceiro e o cupom associado ao plano
        setVendNomeSelecionado(planoAtual.vendedor || '');
        setCupNomeSelecionado(planoAtual.cupom || '');
        setCupCodigoSelecionado(planoAtual.cupCodigo || '');

        // Carrega os cupons antes de filtrar
        await fetchCuponsByPlano(planoSelecionado);
      } else {
        // Limpa os campos se não houver cupom associado
        await fetchCuponsByPlano(planoSelecionado);
        setVendNomeSelecionado('');
        setCupNomeSelecionado('');
        setCupCodigoSelecionado('');
        setCupNomes([]); // Limpa os cupons disponíveis
      }

      // Atualiza o estado da família
      setFamily(planoAtual.isFamilia === 1);
    }

    if (planoAtual.vendCodigo == '0') {
      setVendNomeSelecionado('0');
      setCupNomeSelecionado('0');
    }

    if (planoAtual.promocao == '') {
      setProCodigoSelecionado('0');
    }
  };

  useEffect(() => {
    if (promocoes.length > 0) {
      const promoDesc = [
        ...new Set(promocoes.map((item) => item.proDescricao)),
      ];
      setProDescricaoSelecionada(promoDesc);
    }
  }, [promocoes]);
  useEffect(() => {
    if (dados.length > 0) {
      // Filtra os nomes dos parceiros únicos
      const parceirosUnicos = [...new Set(dados.map((item) => item.vendNome))];
      setVendNomes(parceirosUnicos);
    }
  }, [dados]);

  useEffect(() => {
    if (vendNomeSelecionado && dados.length > 0) {
      // Filtra os cupons correspondentes ao parceiro selecionado
      const cuponsFiltrados = dados
        .filter((item) => item.vendNome === vendNomeSelecionado)
        .map((item) => item.cupNome);

      setCupNomes(cuponsFiltrados);
    }
  }, [dados, vendNomeSelecionado]);

  // Atualiza o estado da família quando o plano é selecionado
  useEffect(() => {
    if (planoSelecionado) {
      const planoAtual = planos.find((p) => p.plCodigo == planoSelecionado);
      if (planoAtual) {
        setFamily(planoAtual.isFamilia === 1);
      }
    }
  }, [planoSelecionado, planos]);

  const enviarDadosContrato = async () => {
    try {
      setLoader(true);

      // Dados que serão enviados para o endpoint
      const dados = {
        // vendCodigo: vendCodigoSelecionado,
        in_assnic: nicSelecionado,
        in_tpplCodigo: planoCodigo,
        in_plCodigo: planoSelecionado,
        in_proCodigo: proCodigoSelecionado,
        in_cupCodigo: cupCodigoSelecionado,
      };

      // Faz a requisição POST para o endpoint
      const response = await apiClei.get(
        `${API_URL}/Contrato.asmx/InserirPromocao`,
        {
          params: {
            dados,
          },
        }
      );

      // Verifica se a requisição foi bem-sucedida
      if (response.status === 200) {
        showToast({
          type: 'success',
          message: 'Cupom alterado com sucesso!',
        });
        history.push('/contracts'); // Redireciona para /contracts
      } else {
        showToast({
          type: 'error',
          message: 'Erro ao alterar cupom. Tente novamente mais tarde.',
        });
      }
    } catch (error) {
      showToast({
        type: 'error',
        message: 'Erro ao enviar os dados. Tente novamente mais tarde.',
      });
      console.error('Erro ao enviar dados:', error);
    } finally {
      setLoader(false);
    }
  };

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />

      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="Voltar" />
      </BackArrow>

      <div className="container">
        <h2>Trocar Cupom</h2>
        <Content>
          <FormContent>
            <BlockInputs>
              {dadosAssinante !== true && (
                <h3>
                  Assinante{' '}
                  <ButtonDivisor onClick={() => setDadosAssinante(true)}>
                    <img src={arrowDown} alt="Expandir" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}

              {dadosAssinante === true && (
                <div className="form">
                  <h3>
                    Assinante{' '}
                    <ButtonDivisor onClick={() => setDadosAssinante(false)}>
                      <img src={arrowUP} alt="Recolher" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  <Row className="rowContract">
                    <Col md={4}>
                      <label>CPF:</label>
                      <InputMask
                        id="contract.cpf"
                        name="assCPF_CNPJ"
                        placeholder="CPF"
                        mask="999.999.999-99"
                        type="text"
                        value={cpf}
                        onChange={handleCPFChange}
                      />
                    </Col>

                    <Col md={6}>
                      <Button
                        className="buttonAdd"
                        type="button"
                        onClick={fetchNicsByCPF}
                      >
                        Consultar
                      </Button>
                    </Col>
                  </Row>

                  {assinanteData.length > 0 && (
                    <Row>
                      <Col md={4}>
                        <label>NIC:</label>
                        <select
                          className="input-select"
                          id="contract.nic"
                          value={nicSelecionado}
                          onChange={handleNICChange}
                        >
                          <option value="">Selecione um NIC</option>
                          {assinanteData.map((item) => (
                            <option key={item.assnic} value={item.assnic}>
                              {item.assnic}
                            </option>
                          ))}
                        </select>
                      </Col>
                      {nicSelecionado && (
                        <Col md={4}>
                          <label>Empresa:</label>
                          <select
                            className="input-select"
                            id="contract.company"
                            value={company}
                            onChange={handleCompanyChange}
                          >
                            <option value="">Selecione uma empresa</option>
                            {empresasFiltradas.map((item) => (
                              <option
                                key={item.empCodigo}
                                value={item.empCodigo}
                              >
                                {item.empresa}
                              </option>
                            ))}
                          </select>
                        </Col>
                      )}
                    </Row>
                  )}
                </div>
              )}
            </BlockInputs>

            {planos.length > 0 && company == 38 && (
              <div>
                <BlockInputs>
                  {dadosPlano !== true && (
                    <h3>
                      Informações do Plano{' '}
                      <ButtonDivisor onClick={() => setDadosPlano(true)}>
                        <img src={arrowDown} alt="Expandir" width="100%" />
                      </ButtonDivisor>
                    </h3>
                  )}
                  {dadosPlano === true && (
                    <div className="form">
                      <h3>
                        Informações do Plano
                        <ButtonDivisor onClick={() => setDadosPlano(false)}>
                          <img src={arrowUP} alt="Recolher" width="100%" />
                        </ButtonDivisor>
                      </h3>
                      <Row className="rowContract">
                        <Col md={4}>
                          <label>Plano:</label>
                          <select
                            className="input-select"
                            id="contract.Plano"
                            value={planoSelecionado}
                            onChange={handlePlanChange}
                          >
                            <option value="">Selecione um Plano</option>
                            {planos.map((plano) => (
                              <option
                                key={plano.plCodigo}
                                value={plano.plCodigo}
                              >
                                {plano.plano}
                              </option>
                            ))}
                          </select>
                        </Col>

                        <Col md={3}>
                          <RadioInputs>
                            <label
                              className="rowFamily"
                              style={{ fontWeight: '700' }}
                            >
                              Família
                              {family === '' && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="quantidades de hóspedes"
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === true && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano família pode incluir até 3 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === false && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano pode incluir até 2 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                            </label>
                            <div className="inlineFamily">
                              <div className="items">
                                <input
                                  type="radio"
                                  value="true"
                                  name="family"
                                  checked={family === true}
                                  disabled
                                />
                                Sim
                              </div>

                              <div className="items">
                                <input
                                  type="radio"
                                  value="false"
                                  name="family"
                                  checked={family === false}
                                  disabled
                                />
                                Não
                              </div>
                            </div>
                          </RadioInputs>
                        </Col>
                      </Row>
                    </div>
                  )}
                </BlockInputs>
                <BlockInputs>
                  {dadosParceiros !== true && (
                    <h3>
                      Parceiros/Convênios{' '}
                      <ButtonDivisor onClick={() => setDadosParceiros(true)}>
                        <img src={arrowDown} alt="Expandir" width="100%" />
                      </ButtonDivisor>
                    </h3>
                  )}
                  {dadosParceiros === true && (
                    <div className="form">
                      <h3>
                        Parceiros/Convênios
                        <ButtonDivisor onClick={() => setDadosParceiros(false)}>
                          <img src={arrowUP} alt="Recolher" width="100%" />
                        </ButtonDivisor>
                      </h3>
                      <Row className="rowContract">
                        <Col md={12}>
                          <label>Parceiros:</label>
                          <select
                            className="input-select"
                            id="contract.Plano"
                            value={vendNomeSelecionado}
                            onChange={handleVendNomeChange}
                          >
                            <option value="">Selecione um Parceiro:</option>
                            <option value="0">Nenhum Parceiro</option>
                            {vendNomes.map((vendNome) => (
                              <option key={vendNome} value={vendNome}>
                                {vendNome}
                              </option>
                            ))}
                          </select>
                        </Col>

                        <Col md={12}>
                          <label>Cupom de Desconto:</label>
                          <select
                            className="input-select"
                            id="contract.Plano"
                            value={cupNomeSelecionado}
                            onChange={handleCupNomeChange}
                          >
                            <option value="">Selecione um Cupom</option>
                            {vendNomeSelecionado == '0' && ( // Mostra "Nenhum Cupom" apenas se o parceiro for "Nenhum Parceiro"
                              <option value="0">Nenhum Cupom</option>
                            )}

                            {cupNomes.map((cupNome) => (
                              <option key={cupNome} value={cupNome}>
                                {cupNome}
                              </option>
                            ))}
                          </select>
                        </Col>
                      </Row>
                    </div>
                  )}
                </BlockInputs>
                <BlockInputs>
                  {dadosParceiros !== true && (
                    <h3>
                      Promoção{' '}
                      <ButtonDivisor onClick={() => setDadosParceiros(true)}>
                        <img src={arrowDown} alt="Expandir" width="100%" />
                      </ButtonDivisor>
                    </h3>
                  )}
                  {dadosParceiros === true && (
                    <div className="form">
                      <h3>
                        Promoção
                        <ButtonDivisor onClick={() => setDadosParceiros(false)}>
                          <img src={arrowUP} alt="Recolher" width="100%" />
                        </ButtonDivisor>
                      </h3>
                      <Row className="rowContract">
                        <Col md={12}>
                          <label>Promoção:</label>
                          <select
                            className="input-select"
                            id="contract.Plano"
                            value={proCodigoSelecionado}
                            onChange={handlePromocaoChange}
                          >
                            <option value="">Selecione uma Promoção:</option>
                            <option value="0">Nenhuma Promoção</option>
                            {
                              // Exibe a lista completa de promoções
                              promocoes.map((promocao) => (
                                <option
                                  key={promocao.proCodigo}
                                  value={promocao.proCodigo}
                                >
                                  {promocao.proDescricao}
                                </option>
                              ))
                            }
                            {/* ) */}
                          </select>
                        </Col>
                      </Row>
                    </div>
                  )}
                </BlockInputs>

                <Button
                  id="contract.btnConcl"
                  className="buttonEnvio"
                  type="submit"
                  onClick={enviarDadosContrato}
                  disabled={loader} // Desabilita o botão durante o carregamento
                >
                  {loader ? 'Enviando...' : 'Enviar Contrato'}
                </Button>
              </div>
            )}
            {company != 38 && company != '' && (
              <BlockInputs>
                <h4 style={{ color: 'red' }}>
                  Não é possível adicionar cupons para essa empresa.
                </h4>
              </BlockInputs>
            )}
          </FormContent>
        </Content>
      </div>
    </Container>
  );
}
