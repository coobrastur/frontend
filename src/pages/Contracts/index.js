// Dependencies
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import Swal from 'sweetalert2';
// import { format } from 'date-fns';

import { FaAngleUp, FaAngleDown } from 'react-icons/fa';
import { pt } from 'date-fns/esm/locale';
import { apiClei } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';
import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';
import Modal from '../../components/Modal';
import ModalDoubt from '../../components/ModalDoubt';
// import SimpleInput from '../../components/SimpleInput';

// Images
import imageEmpty from '../../assets/empty.png';
import imageHelp from '../../assets/help.png';
import refresh from '../../assets/refresh.svg';
import malaCoob from '../../assets/malaCoob.svg';
import copy from '../../assets/copy.svg';
import resend from '../../assets/resend.svg';
import signature from '../../assets/signature.svg';

// Styles
import 'react-datepicker/dist/react-datepicker.css';

import {
  UserAv,
  Container,
  Empty,
  SeeButton,
  DeleteButton,
  EditButton,
  SendButton,
  BlockItems,
  BlockItem,
  Line,
  // Type,
  ButtonHelp,
  // InputData,
  InputFiltros,
  RefreshButton,
  DetailsButton,
  DisabledButton,
  SelectFiltros,
  Filtros,
} from './styles';
import Tooltip from '../../components/Tooltip';
// import useScrollBlock from '../../utils/useScrollBlock';

function Contracts() {
  // const date = new Date().toLocaleDateString('fr-CA');
  const history = useHistory();
  const { token } = useSelector((state) => state.auth);
  const [contracts, setContracts] = useState([]);
  const [loader, setLoader] = useState(false);
  const [modal, setModal] = useState(false);
  const [modalDoubt, setModalDoubt] = useState(false);
  const [idDelete, setIdDelete] = useState();
  const [buscaNomeCPF, setBuscaNomeCPF] = useState('');
  const [filtrarStatus, setFiltrarStatus] = useState('');
  const [buscaData, setBuscaData] = useState('');
  const [endData, setEndData] = useState(null);
  // const [blockScroll, allowScroll] = useScrollBlock();
  // allowScroll();

  const [desabilitado, setDesabilitado] = useState(false);
  const waitTime = 30 * 60 * 1000;

  // const [isOpen, setOpen] = useState(false);

  const [hiddenFilters, setHiddenFilters] = useState(true);

  const range = (dates) => {
    const [start, end] = dates;
    setBuscaData(start);
    setEndData(end);
  };
  const [date, setDate] = useState();

  const getYear = () => setDate(new Date().getFullYear());

  useEffect(() => {
    getYear();
  }, []);

  function getModal(id) {
    setIdDelete(id);
    setModal(true);
  }

  async function getContracts() {
    setLoader(true);

    const dados = {
      token,
    };

    await apiClei
      .get('Contrato.asmx/MeusContratos', {
        params: {
          dados,
        },
      })
      .then((response) => {
        const arr = [];

        for (let i = 0; i < response.data.length; i++) {
          if (response.data[i].lotAppVenda != -1) {
            for (let j = i + 1; j < response.data.length; j++) {
              if (
                response.data[i].lotAppVenda == response.data[j].lotAppVenda
              ) {
                const arrLotes = [];
                arrLotes.push(response.data[i], response.data[j]);
                arr.push(arrLotes);
              }
            }
          } else {
            arr.push(response.data[i]);
          }
        }

        setContracts(response.data.reverse());
        setLoader(false);
      })
      .catch((err) => {
        console.log(err);
        setLoader(false);
      });
  }

  // async function duplyContract() {
  //   const dados = {
  //     token,
  //   };

  //   await apiClei
  //     .get('Contrato.asmx/MeusContratos', {
  //       params: {
  //         dados,
  //       },
  //     })
  //     .then((response) => {
  //       setContracts(response.data.reverse());
  //       // console.log(response.data[[]].id);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // }
  // console.log(contracts);
  async function envioRecursivoContratos(
    contratosEnviar,
    indexParaEnvioRecursivo = 0
  ) {
    const dataAtual = new Date(); // Esta é a variável que estava faltando

    const contrato = contratosEnviar[indexParaEnvioRecursivo];
    const dados = {
      token,
      id: contrato.id,
    };

    try {
      const response = await apiClei.get(`Contrato.asmx/ObterContratoPorId`, {
        params: {
          dados,
        },
      });

      console.log('TESTE:', response.data[0]);
      const contract = {
        cpfAssociado: String(response.data[0].assCPF_CNPJ),
        nomeAssociado: response.data[0].assNome_RazaoSocial || '',

        ufAssociado: response.data[0].endUF || '',
        cepAssociado: response.data[0].endCepAssociado || '',
        cidadeAssociado: response.data[0].endCidadeAssociado || '',
        bairroAssociado: response.data[0].endBairroAssociado || '',
        ruaAssociado: response.data[0].endLogradouroAssociado || '',
        numeroRuaAssociado: response.data[0].endNumLogradouroAssociado || '',
        complementoRuaAssociado: response.data[0].endComplementoAssociado || '',

        emailAssociado: response.data[0].assEmailPessoal || '',
        dddCelularAssociado: String(response.data[0].assNumCelularDDD) || '',
        celularAssociado: String(response.data[0].assNumCelular) || '',
        DtNascimento: response.data[0].dtNascimento,

        indicacao: response.data[0].indicacao || false,
        nomeIndicador: response.data[0].nomeIndicador || '',
        cpfIndicador: response.data[0].cpfIndicador || '',
        assnicIndicador: response.data[0].assnicIndicador || '',
        emailIndicador: response.data[0].emailIndicador || '',
        dddCelularIndicador:
          response.data[0].dddCelularIndicador === 0
            ? ''
            : String(response.data[0].dddCelularIndicador),
        celularIndicador: String(response.data[0].celularIndicador) || '',

        idTipoVenda: response.data[0].tipoVenda || null,
        TpplanoReativado: response.data[0].tpplanoReativado || null,
        plCodigoReativado: response.data[0].plCodigoReativado || null,
        planoResgatado: response.data[0].planoResgatado || '',
        empCodigo: response.data[0].empCodigo || null,
        idPlano: response.data[0].tpplcodigo || null,
        diariasPlano: response.data[0].plQtdeDiarias || '',
        familiaPlano: response.data[0].plfamilia,
        idFormaPagamentoMensalidade: response.data[0].formaPagamento || null,

        cpfConta:
          response.data[0].cpfTitularContaCorrente === null
            ? ''
            : String(response.data[0].cpfTitularContaCorrente),
        nomeConta: response.data[0].nomeTitularContaCorrente || '',
        idBancoConta: response.data[0].bcoCodigo || null,
        numeroConta: response.data[0].conContaCorrente,
        conContaCorrente: response.data[0].conContaCorrente,
        dvConta: response.data[0].conContaCorrenteDV || '',
        agenciaConta: response.data[0].conAgencia || '',
        diaDebitoConta: response.data[0].debDia || 0,
        mesDebitoConta: response.data[0].debMes || 0,
        anoDebitoConta: 2021,

        // parametros boleto
        bolCpf: response.data[0].bolCpf,
        bolDia: response.data[0].bolDia || 0,
        bolDia2: response.data[0].bolDia2 || 0,
        bolMes: response.data[0].bolMes || 0,
        bolMes2: response.data[0].bolMes2 || 0,
        bolAno: date,

        cpfCartao: String(response.data[0].cpfTitularCartao) || '',
        nomeCartao: response.data[0].nomeTitularCartao || '',
        idBandeiraCartao: Number(response.data[0].banCodigo) || null,
        numeroCartao: response.data[0].carNumero,
        validadeCartao: response.data[0].carValidade,
        cvvCartao: response.data[0].carcodigo_seguranca,
        dataVendaSemDados: null,

        dataAdesao: response.data[0].dataAdesao,
        adesao: response.data[0].adesao,
        valorPagamentoAdesao: response.data[0].valorAdesao || 0,
        valorPagamentoMensalidade: response.data[0].valorMensalidade || 0,
        adesaoValorDesconto: response.data[0].adesaoValorDesconto || 0,
        adesaoValorOriginal: response.data[0].adesaoValorOriginal || 0,
        idMeioPagamentoAdesao: response.data[0].meioAdesao || 0,

        idFormaPagamentoAdesaoVendedor:
          response.data[0].formaAdesaoVendedor || 0,
        parcelasPagamentoAdesaoVendedor:
          response.data[0].formaAdesaoVendedor || 0,
        dataPagamentoAdesaoVendedor: response.data[0].dataAdesao || '',
        obsAdesaoVendedor: response.data[0].obsVendedor || '',

        idFormaPagamentoAdesaoCoobrastur:
          response.data[0].formaAdesaoCoobrastur || 0,
        parcelasPagamentoAdesaoCoobrastur:
          response.data[0].formaAdesaoCoobrastur || 0,
        dataPagamentoAdesaoCoobrastur: response.data[0].dataAdesao || '',
        obsAdesaoCoobrastur: response.data[0].obsCoobrastur || '',

        terceiro: response.data[0].terceiro || false,
        cpfTerceiro: response.data[0].cpfTerceiro || '',
        nomeTerceiro: response.data[0].nomeTerceiro || '',
        nascimentoTerceiro: response.data[0].nascimentoTerceiro || '',
        nacionalidadeTerceiro: response.data[0].nacionalidadeTerceiro || '',
        idEstadoCivilTerceiro: response.data[0].estadoCivilTerceiro || 0,
        conjugeTerceiro: response.data[0].nomeConjugeTerceiro || '',
        maeTerceiro: response.data[0].nomeMaeTerceiro || '',
        paiTerceiro: response.data[0].nomePaiTerceiro || '',
        emailTerceiro: response.data[0].emailTerceiro || '',
        dddCelularTerceiro:
          response.data[0].dddTelefoneTerceiro === null
            ? ''
            : String(response.data[0].dddTelefoneTerceiro),
        celularTerceiro:
          response.data[0].numCelularTerceiro === null
            ? (response.data[0].numCelularTerceiro = '')
            : String(response.data[0].numCelularTerceiro) || '',
        dddTelefoneTerceiro:
          response.data[0].dddTelefoneTerceiro === null
            ? ''
            : String(response.data[0].dddTelefoneTerceiro),
        telefoneTerceiro:
          response.data[0].TelefoneTerceiro === null
            ? ''
            : String(response.data[0].TelefoneTerceiro),
        idFormaAssinaturaTerceiro:
          response.data[0].formaAssinaturaTerceiro || 0,
        assinaturaTerceiro: response.data[0].assinaturaTerceiro || '',

        retricaoAssociado: response.data[0].restricaoAssociado || false,
        aditamento: response.data[0].aditamento || false,
        parcelasPrePagas: response.data[0].parcelasPrePagas || 0,
        idFormaAditamento: response.data[0].formaaditamento || 0,

        fiador: response.data[0].fiador || false,
        cpfFiador: response.data[0].fiaCpf || '',
        nomeFiador: response.data[0].fianome || '',
        dataNascimentoFiador: response.data[0].nascimentoTerceiro || '',
        nacionalidadeFiador: response.data[0].fianacionalidade || '',
        idEstadoCivilFiador: response.data[0].civCodigo || 0,
        conjugeFiador: response.data[0].fiaConjuge || '',
        maeFiador: response.data[0].fiaConjugeFiliacao || '',
        paiFiador: response.data[0].fiaConjugeFiliacao2 || '',
        emailFiador: response.data[0].fiaEmailPessoal || '',
        dddCelularFiador: String(response.data[0].fiaNumeroCelularDDD) || '',
        celularFiador: String(response.data[0].fiaNumeroCelular) || '',
        dddTelefoneFiador: String(response.data[0].fiaddFone) || '',
        telefoneFiador: response.data[0].fiaFone || '',
        idFormaAssinaturaFiador: response.data[0].formaAssinaturaFiador || 0,
        assinaturaFiador: response.data[0].assinaturaFiador || '',

        nroPipeline: response.data[0].nroPipeline,
        cupCodigo: response.data[0].cupCodigo || 0,
        codErro: 0,
        textoErro: '',
        campanha: response.data[0].campanha || '',
        idFormaAssinaturaAssociado: response.data[0].formaAssinatura || 1,
        assinaturaAssociado: response.data[0].assinatura || '',
        codigoVendedor: String(response.data[0].vendCodigo1) || '',
        usuarioVendedor: response.data[0].usuUsuario || '',
      };

      if (
        response.data[0].conAgencia == null &&
        response.data[0].formaPagamento == 1
      ) {
        showToast({
          id: 'toast-contract',
          type: 'error',
          message: 'Agência da Conta Corrente Inválida.',
        });
        setDesabilitado(false);
        setLoader(false);
        return false;
      }
      if (
        response.data[0].conContaCorrente == null &&
        response.data[0].formaPagamento == 1
      ) {
        showToast({
          id: 'toast-contract',
          type: 'error',
          message: 'Número da Conta Corrente Inválida.',
        });
        setDesabilitado(false);
        setLoader(false);
        return false;
      }
      if (
        response.data[0].conContaCorrenteDV == null &&
        response.data[0].formaPagamento == 1
      ) {
        showToast({
          id: 'toast-contract',
          type: 'error',
          message: 'Dígito da Conta Corrente Inválida.',
        });
        setDesabilitado(false);
        setLoader(false);
        return false;
      }

      const car = new Date(
        dataAtual.getMonth() < response.data[0].carMes
          ? dataAtual.getFullYear()
          : dataAtual.getFullYear() + 1,
        response.data[0].carMes - 1,
        response.data[0].carDia
      );
      const deb = new Date(
        dataAtual.getMonth() < response.data[0].debMes
          ? dataAtual.getFullYear()
          : dataAtual.getFullYear() + 1,
        response.data[0].debMes - 1,
        response.data[0].debDia
      );
      const bol = new Date(
        dataAtual.getMonth() < response.data[0].bolMes
          ? dataAtual.getFullYear()
          : dataAtual.getFullYear() + 1,
        response.data[0].bolMes - 1,
        response.data[0].bolDia
      );

      // Defina as horas, minutos, segundos e milissegundos de car para zero
      car.setHours(0, 0, 0, 0);

      // Defina as horas, minutos, segundos e milissegundos de dataAtual para zero
      dataAtual.setHours(0, 0, 0, 0);

      if (response.data[0].carDia != '0') {
        if (car < dataAtual) {
          showToast({
            id: 'toast-contract',
            type: 'error',
            message:
              'Data de cobrança do cartão não pode ser menor que a atual. Por favor, refaça o contrato.',
          });
          setDesabilitado(false);
          setLoader(false);
          return false;
        }
      }
      if (response.data[0].debDia != '0') {
        if (deb == dataAtual) {
          showToast({
            id: 'toast-contract',
            type: 'error',
            message:
              'Data de cobrança do débito não pode ser menor que a atual. Por favor, edite o contrato e tente novamente',
          });
          setDesabilitado(false);
          setLoader(false);
          return false;
        }
      }
      if (response.data[0].bolDia != '0') {
        if (bol < dataAtual) {
          showToast({
            id: 'toast-contract',
            type: 'error',
            message:
              'Data de cobrança do boleto não pode ser menor que a atual. Por favor, refaça o contrato.',
          });
          setDesabilitado(false);
          setLoader(false);
          return false;
        }
      }

      const formatDataProgramada = new Date(
        response.data[0].dataProgramada
      ).toLocaleDateString('pt-BR', {
        timeZone: 'UTC',
      });

      const getDate = formatDataProgramada.split('/');

      const contratoString = JSON.stringify(contract);

      setLoader(true);

      const result = await apiClei.post(
        'ConsultaCrm.asmx/InsertAppVendaOnline',
        {
          cpfAssociado: String(response.data[0].assCPF_CNPJ),
          nomeAssociado: response.data[0].assNome_RazaoSocial || '',

          ufAssociado: response.data[0].endUF || '',
          cepAssociado: response.data[0].endCepAssociado || '',
          cidadeAssociado: response.data[0].endCidadeAssociado || '',
          bairroAssociado: response.data[0].endBairroAssociado || '',
          ruaAssociado: response.data[0].endLogradouroAssociado || '',
          numeroRuaAssociado: response.data[0].endNumLogradouroAssociado || '',
          complementoRuaAssociado:
            response.data[0].endComplementoAssociado || '',

          emailAssociado: response.data[0].assEmailPessoal || '',
          dddCelularAssociado: String(response.data[0].assNumCelularDDD) || '',
          celularAssociado: String(response.data[0].assNumCelular) || '',
          DtNascimento: response.data[0].dtNascimento,

          indicacao: response.data[0].indicacao || false,
          nomeIndicador: response.data[0].nomeIndicador || '',
          cpfIndicador: response.data[0].cpfIndicador,
          assnicIndicador: response.data[0].assnicIndicador || '',
          emailIndicador: response.data[0].emailIndicador || '',
          dddCelularIndicador:
            response.data[0].dddCelularIndicador === 0
              ? ''
              : String(response.data[0].dddCelularIndicador),
          celularIndicador: String(response.data[0].celularIndicador) || '',

          idTipoVenda: response.data[0].tipoVenda || null,
          TpplanoReativado: response.data[0].tpplcodigoReativado || null,
          plCodigoReativado: response.data[0].plcodigoReativado || null,
          planoResgatado: response.data[0].planoResgatado || '',
          empCodigo: response.data[0].empCodigo || null,
          idPlano: response.data[0].tpplcodigo || null,
          diariasPlano: response.data[0].plQtdeDiarias || '',
          familiaPlano: response.data[0].plfamilia,
          idFormaPagamentoMensalidade: response.data[0].formaPagamento || null,

          cpfConta:
            response.data[0].cpfTitularContaCorrente === null
              ? ''
              : String(response.data[0].cpfTitularContaCorrente),
          nomeConta: response.data[0].nomeTitularContaCorrente || '',
          idBancoConta: response.data[0].bcoCodigo || null,
          numeroConta: response.data[0].conContaCorrente
            ? response.data[0].conContaCorrente.replace('undefined', '')
            : '',
          conContaCorrente: response.data[0].conContaCorrente
            ? response.data[0].conContaCorrente.replace('undefined', '')
            : '',
          dvConta: response.data[0].conContaCorrenteDV || '',
          agenciaConta: response.data[0].conAgencia || '',
          diaDebitoConta: response.data[0].debDia || 0,

          // parametros boleto
          bolCpf: response.data[0].bolCpf,
          bolDia: response.data[0].bolDia || 0,
          bolDia2: response.data[0].bolDia2 || 0,
          bolMes: response.data[0].bolMes || 0,
          bolMes2: response.data[0].bolMes2 || 0,
          bolAno:
            dataAtual.getMonth() < response.data[0].bolMes
              ? dataAtual.getFullYear()
              : dataAtual.getFullYear() + 1,

          // Arrumar
          mesDebitoConta: response.data[0].debMes || null,
          anoDebitoConta: response.data[0].conContaCorrente
            ? Number(getDate[2])
            : null,

          cpfCartao: String(response.data[0].cpfTitularCartao) || '',
          nomeCartao: response.data[0].nomeTitularCartao || '',
          idBandeiraCartao: Number(response.data[0].banCodigo) || null,
          numeroCartao: response.data[0].carNumero || '',
          validadeCartao: response.data[0].carValidade || '',
          cvvCartao: response.data[0].carcodigo_seguranca || '',
          // Arrumar
          diaFaturaCartao: response.data[0].carDia || 0,
          mesFaturaCartao: response.data[0].carMes || 0,
          anoFaturaCartao: response.data[0].carNumero
            ? Number(getDate[2])
            : null,

          dataVendaSemDados: null,

          dataAdesao: response.data[0].dataAdesao,
          adesao: response.data[0].adesao,
          valorPagamentoAdesao:
            response.data[0].adesao === true ? response.data[0].valorAdesao : 0,
          valorPagamentoMensalidade: response.data[0].valorMensalidade || 0,
          adesaoValorDesconto: response.data[0].adesaoValorDesconto || 0,
          adesaoValorOriginal: response.data[0].adesaoValorOriginal || 0,
          idMeioPagamentoAdesao:
            response.data[0].adesao === true ? response.data[0].meioAdesao : 0,

          idFormaPagamentoAdesaoVendedor:
            response.data[0].adesao === true
              ? response.data[0].formaAdesaoVendedor
              : 0,
          parcelasPagamentoAdesaoVendedor:
            response.data[0].adesao === true
              ? response.data[0].formaAdesaoVendedor
              : 0,
          dataPagamentoAdesaoVendedor:
            response.data[0].adesao === true ? response.data[0].dataAdesao : '',
          obsAdesaoVendedor:
            response.data[0].adesao === true
              ? response.data[0].obsVendedor
              : '',

          idFormaPagamentoAdesaoCoobrastur:
            response.data[0].adesao === true
              ? response.data[0].formaAdesaoCoobrastur
              : 0,
          parcelasPagamentoAdesaoCoobrastur:
            response.data[0].adesao === true
              ? response.data[0].formaAdesaoCoobrastur
              : 0,
          dataPagamentoAdesaoCoobrastur:
            response.data[0].adesao === true ? response.data[0].dataAdesao : '',
          obsAdesaoCoobrastur:
            response.data[0].adesao === true
              ? response.data[0].obsCoobrastur
              : '',

          terceiro: response.data[0].terceiro || false,
          cpfTerceiro: response.data[0].cpfTerceiro || '',
          nomeTerceiro: response.data[0].nomeTerceiro || '',
          nascimentoTerceiro:
            response.data[0].terceiro === true
              ? response.data[0].nascimentoTerceiro
              : '',
          nacionalidadeTerceiro: response.data[0].nacionalidadeTerceiro || '',
          idEstadoCivilTerceiro: response.data[0].estadoCivilTerceiro || 0,
          conjugeTerceiro: response.data[0].nomeConjugeTerceiro || '',
          maeTerceiro: response.data[0].nomeMaeTerceiro || '',
          paiTerceiro: response.data[0].nomePaiTerceiro || '',
          emailTerceiro: response.data[0].emailTerceiro || '',
          dddCelularTerceiro:
            response.data[0].dddTelefoneTerceiro === null
              ? ''
              : String(response.data[0].dddTelefoneTerceiro),
          celularTerceiro:
            response.data[0].numCelularTerceiro === null
              ? (response.data[0].numCelularTerceiro = '')
              : String(response.data[0].numCelularTerceiro) || '',
          dddTelefoneTerceiro:
            response.data[0].dddTelefoneTerceiro === null
              ? ''
              : String(response.data[0].dddTelefoneTerceiro),
          telefoneTerceiro:
            response.data[0].TelefoneTerceiro === null
              ? ''
              : String(response.data[0].TelefoneTerceiro),
          idFormaAssinaturaTerceiro:
            response.data[0].formaAssinaturaTerceiro || 0,
          assinaturaTerceiro: response.data[0].assinaturaTerceiro || '',

          retricaoAssociado: response.data[0].restricaoAssociado || false,
          aditamento: response.data[0].aditamento || false,
          parcelasPrePagas: response.data[0].parcelasPrePagas || 0,
          idFormaAditamento: response.data[0].formaaditamento || 0,

          fiador: response.data[0].fiador || false,
          cpfFiador: response.data[0].fiaCpf || '',
          nomeFiador: response.data[0].fianome || '',
          dataNascimentoFiador:
            response.data[0].fiador === true
              ? response.data[0].nascimentoTerceiro
              : '',
          nacionalidadeFiador: response.data[0].fianacionalidade || '',
          idEstadoCivilFiador: response.data[0].civCodigo || 0,
          conjugeFiador: response.data[0].fiaConjuge || '',
          maeFiador: response.data[0].fiaConjugeFiliacao || '',
          paiFiador: response.data[0].fiaConjugeFiliacao2 || '',
          emailFiador: response.data[0].fiaEmailPessoal || '',
          dddCelularFiador: String(response.data[0].fiaNumeroCelularDDD) || '',
          celularFiador: String(response.data[0].fiaNumeroCelular) || '',
          dddTelefoneFiador: String(response.data[0].fiaddFone) || '',
          telefoneFiador: response.data[0].fiaFone || '',
          idFormaAssinaturaFiador:
            response.data[0].fiador === true
              ? response.data[0].formaAssinaturaFiador
              : 0,
          assinaturaFiador: response.data[0].assinaturaFiador || '',

          nroPipeline: response.data[0].nroPipeline,
          cupCodigo: response.data[0].cupCodigo || 0,
          codErro: 0,
          textoErro: '',
          campanha: response.data[0].campanha,
          idFormaAssinaturaAssociado: response.data[0].formaAssinatura || 1,
          assinaturaAssociado: response.data[0].assinatura || '',
          codigoVendedor: String(response.data[0].vendCodigo1) || '',
          contratoString,
          usuarioVendedor: response.data[0].usuUsuario || '',
          appVenda: response.data[0].id,
        },
        {
          headers: { Authorization: token },
        }
      );

      console.log('Resultado da inserção:', result.data);

      if (result.data.body.code == 25) {
        showToast({
          id: 'toast-contract',
          type: 'error',
          message: 'Data de envio superior a data Escolhida.',
        });
        setDesabilitado(false);
        setLoader(false);
        return;
        // return false;
      }
      // if (result.data.body.code == 11) {
      //   showToast({
      //     id: 'toast-contract',
      //     type: 'error',
      //     message: 'Contrato já adicionado.',
      //   });
      //   setDesabilitado(false);
      //   setLoader(false);
      //   return false;
      // }

      if (
        result.data.code !== 400 ||
        (result.data.code === 400 &&
          result.data.body.mensagem ===
            'Contrato importado para o siscoob, mas download de contrato assinado não realizado.')
      ) {
        const res = await apiClei.get(`/ConsultaCrm.asmx/AtualizaStatus`, {
          params: {
            appVenda: response.data[0].id,
          },
        });

        console.log(res);

        showToast({
          id: 'toast-contract',
          type: 'success',
          message: 'Associado cadastrado com sucesso',
        });

        indexParaEnvioRecursivo++;

        if (indexParaEnvioRecursivo < contratosEnviar.length) {
          envioRecursivoContratos(contratosEnviar, indexParaEnvioRecursivo);
        } else {
          console.log('Todos os contratos foram enviados.');
          setLoader(false);
          getContracts();
          setDesabilitado(true);
          return;
        }
      } else {
        showToast({
          id: 'toast-contract',
          type: 'error',
          message:
            'Associado não foi cadastrado. Entre em contato com o suporte',
        });
        setLoader(false);
        setDesabilitado(false);
      }

      // if (result.data.code === 400) {
      //   showToast({
      //     id: 'toast-contract',
      //     type: 'error',
      //     message:
      //       'Associado não foi cadastrado. Entre em contato com o suporte',
      //   });
      //   setDesabilitado(false);
      //   setLoader(false);
      //   return false;
      // }
      // if (result.data === 400) {
      //   showToast({
      //     id: 'toast-contract',
      //     type: 'error',
      //     message:
      //       'Associado não foi cadastrado. Entre em contato com o suporte',
      //   });
      //   setDesabilitado(false);
      //   // setLoader(false);
      //   // return false;
      //   continue;
      // }

      // setLoader(false);
      // return true;
    } catch (error) {
      console.log(error);
      showToast({
        id: 'toast-contract',
        type: 'error',
        message: 'Erro ao atualizar status. Contate o Suporte do App.',
      });
      setDesabilitado(true);
      setLoader(false);
      return;
      // break;
    }
    // // Após o envio do contrato atual, chamar a função recursiva para o próximo contrato
    // envioRecursivoContratos(contratosEnviar, indexParaEnvioRecursivo + 1);
  }

  async function envioCoobrastur(id) {
    const contrato = contracts.filter((contrato) => contrato.id == id);
    if (!contrato || contrato.length === 0) {
      return;
    }

    const contratosEnviar = contracts.filter(
      (contratoFiltro) => contratoFiltro.lotAppVenda == contrato[0].lotAppVenda
    );

    setLoader(true);

    // Inicia o envio recursivo com o índice inicial de 0
    let index = 0;
    envioRecursivoContratos(contratosEnviar, index);
  }

  async function refreshContracts() {
    setLoader(true);
    getContracts();
    setTimeout(() => {
      setLoader(false);
    }, 500);
  }

  useEffect(() => {
    getContracts();
  }, [modal]);

  async function duplicateContracts(id) {
    setLoader(true);
    // getContracts();

    console.log(id);
    const dados = {
      token,
      id,
    };
    await apiClei
      .get(`Contrato.asmx/ObterContratoPorId`, {
        params: {
          dados,
        },
      })
      .then((response) => {
        console.log(response.data[0]);

        history.push({
          pathname: '/newcontract',
          search: 'reu',
          state: {
            copy: 1,

            nome: response.data[0].assNome_RazaoSocial,
            cpf: response.data[0].assCPF_CNPJ,
            email: response.data[0].assEmailPessoal,
            numeroCelDDD: response.data[0].assNumCelularDDD,
            numeroCel: response.data[0].assNumCelular,

            cep: response.data[0].endCepAssociado,
            rua: response.data[0].endLogradouroAssociado,
            bairro: response.data[0].endBairroAssociado,
            complemento: response.data[0].endComplementoAssociado,
            numeroCasa: response.data[0].endNumLogradouroAssociado,
            cidade: response.data[0].endCidadeAssociado,
            estado: response.data[0].endUF,
          },
        });
      });
    setTimeout(() => {
      setLoader(false);
    }, 500);
  }

  // useEffect(() => {
  //   getContracts();
  // }, [modal]);

  let contractsFiltrados = [];
  function filtrarContratados() {
    if (buscaNomeCPF !== '' && buscaData === '' && filtrarStatus === '') {
      contractsFiltrados = contracts.filter(
        (c) =>
          c.assNome_RazaoSocial
            .toLowerCase()
            .includes(buscaNomeCPF.toLowerCase()) ||
          c.assCPF_CNPJ
            .toString()
            .toLowerCase()
            .includes(buscaNomeCPF.toLowerCase())
      );
    }
    const dataFim = new Date(endData);
    const dataFimAjustada = dataFim.setDate(dataFim.getDate() + 1);
    if (buscaNomeCPF === '' && buscaData !== '' && filtrarStatus === '') {
      if (endData !== '') {
        contractsFiltrados = contracts.filter(
          (c) =>
            new Date(c.criadoEm) >= new Date(buscaData) &&
            new Date(c.criadoEm) <= new Date(dataFimAjustada)
        );
      }
    }

    if (buscaNomeCPF === '' && buscaData === '' && filtrarStatus !== '') {
      contractsFiltrados = contracts.filter((c) =>
        c.status.startsWith(filtrarStatus)
      );
    }

    if (buscaNomeCPF !== '' && buscaData !== '' && filtrarStatus === '') {
      contractsFiltrados = contracts.filter(
        (c) =>
          (c.assNome_RazaoSocial
            .toLowerCase()
            .includes(buscaNomeCPF.toLowerCase()) ||
            c.assCPF_CNPJ
              .toString()
              .toLowerCase()
              .includes(buscaNomeCPF.toLowerCase())) &&
          new Date(c.criadoEm) >= new Date(buscaData) &&
          new Date(c.criadoEm) <= new Date(dataFimAjustada)
      );
    }

    if (buscaNomeCPF !== '' && buscaData === '' && filtrarStatus !== '') {
      contractsFiltrados = contracts.filter(
        (c) =>
          (c.assNome_RazaoSocial
            .toLowerCase()
            .includes(buscaNomeCPF.toLowerCase()) ||
            c.assCPF_CNPJ
              .toString()
              .toLowerCase()
              .includes(buscaNomeCPF.toLowerCase())) &&
          c.status.startsWith(filtrarStatus)
      );
    }

    if (buscaNomeCPF === '' && buscaData !== '' && filtrarStatus !== '') {
      contractsFiltrados = contracts.filter(
        (c) =>
          new Date(c.criadoEm) >= new Date(buscaData) &&
          new Date(c.criadoEm) <= new Date(dataFimAjustada) &&
          c.status.startsWith(filtrarStatus)
      );
    }

    if (buscaNomeCPF !== '' && buscaData !== '' && filtrarStatus !== '') {
      contractsFiltrados = contracts.filter(
        (c) =>
          (c.assNome_RazaoSocial
            .toLowerCase()
            .includes(buscaNomeCPF.toLowerCase()) ||
            c.assCPF_CNPJ
              .toString()
              .toLowerCase()
              .includes(buscaNomeCPF.toLowerCase())) &&
          new Date(c.criadoEm) >= new Date(buscaData) &&
          new Date(c.criadoEm) <= new Date(dataFimAjustada) &&
          c.status.startsWith(filtrarStatus)
      );
    }

    if (buscaData === '' && buscaNomeCPF === '' && filtrarStatus === '') {
      return contracts;
    }
    if (
      buscaData === '' &&
      buscaNomeCPF === '' &&
      dataFimAjustada === '' &&
      filtrarStatus === ''
    ) {
      return contracts;
    }

    if (
      buscaData !== '' &&
      buscaNomeCPF === '' &&
      dataFimAjustada === '' &&
      filtrarStatus === ''
    ) {
      return contracts;
    }

    return contractsFiltrados;
  }
  const resendEmail = (emailPessoal, assCPF_CNPJ, tokenPagamento) => {
    Swal.fire({
      text: `Deseja reenviar os dados de pagamento para ${emailPessoal} ?`,
      showDenyButton: true,
      confirmButtonText: 'Sim',
      confirmButtonColor: '#01affd',
      denyButtonText: `Não`,
    }).then((resp) => {
      if (resp) {
        if (resp.isConfirmed) {
          Swal.fire('Saved!', '', 'success');

          apiClei
            .get(`Contrato.asmx/EnviaEmailDadosPagamento`, {
              params: {
                cpf: assCPF_CNPJ,
                email: emailPessoal,
                env: window.location.href.includes('homolog') ? '1' : '0',
                urlPagamento: tokenPagamento,
              },
            })
            .then((res) => {
              console.log(res);
            });
          Swal.fire({
            text: 'Dados reenviados com sucesso!',
            icon: 'success',
            timer: '1500',
            showConfirmButton: false,
          });
        }
      } else if (resp.isDenied) {
        Swal.fire('Changes are not saved', '', 'info');
      }
    });
  };
  const resendContract = (emailPessoal, assinatura, id) => {
    Swal.fire({
      text: `Deseja reenviar o contrato para ${emailPessoal} ?`,
      showDenyButton: true,
      confirmButtonText: 'Sim',
      confirmButtonColor: '#01affd',
      denyButtonText: `Não`,
    }).then((resp) => {
      if (resp) {
        if (resp.isConfirmed) {
          // Swal.fire('Saved!', '', 'success');

          apiClei
            .get(`D4Sign.asmx/ReenviarLinkD4Sing`, {
              params: {
                email: emailPessoal,
                uuid: assinatura,
                env: window.location.href.includes('homolog') ? '1' : '0',
                codContract: id,
              },
            })
            .then((response) => {
              console.log('RESPONSE', response);
              Swal.fire({
                text: 'Contrato reenviado com sucesso!',
                icon: 'success',
                timer: '1500',
                showConfirmButton: false,
              });
            })
            .catch((error) => {
              console.log('ERRO', error.response);
              console.log('ERRO', error.response.data.resposta);
              const minutos = error.response.data.resposta;
              Swal.fire({
                text: `Ops, parece que você ja enviou o contrato, espere ${minutos} minutos para fazer um próximo reenvio.`,
                icon: 'error',
                showConfirmButton: true,
              });
            });
        }
      } else if (resp.isDenied) {
        Swal.fire('Changes are not saved', '', 'info');
      }
    });
  };

  return (
    <Container>
      <ButtonHelp onClick={() => setModalDoubt(true)}>
        <img src={imageHelp} alt="" />
      </ButtonHelp>

      <MenuHamburguer />
      <Loader loader={loader} />
      <Modal setModal={setModal} modal={modal} token={token} id={idDelete} />
      <ModalDoubt setModalDoubt={setModalDoubt} modalDoubt={modalDoubt} />

      <div className="container">
        <h2 className="title">Contratos</h2>
        <Filtros>
          <div className="Filters">
            <RefreshButton
              onClick={() => {
                refreshContracts();
                setBuscaData('');
                setEndData('');
                setBuscaNomeCPF('');
                setFiltrarStatus('');
              }}
            >
              <img src={refresh} alt="" />
              <p>Atualizar</p>
            </RefreshButton>
            {hiddenFilters !== true && (
              <h4 className="hiddenFiltros">
                Filtros <FaAngleUp onClick={() => setHiddenFilters(true)} />
              </h4>
            )}
          </div>

          {hiddenFilters === true && (
            <div className="filter">
              <div className="FiltersShow">
                <h4
                  className="hiddenFiltros"
                  style={{ justifyContent: 'flex-end', position: 'relative' }}
                >
                  Filtros{' '}
                  <FaAngleDown onClick={() => setHiddenFilters(false)} />
                </h4>
              </div>

              <InputFiltros
                name="buscaNomeCPF"
                placeholder="Nome ou CPF"
                type="text"
                value={buscaNomeCPF}
                onChange={(e) => {
                  setBuscaNomeCPF(e.target.value);
                  filtrarContratados(contractsFiltrados);
                }}
              />
              <div>
                <DatePicker
                  className="daterange"
                  placeholderText="dd-mm-yyyy"
                  selected={buscaData}
                  onChange={range}
                  startDate={buscaData}
                  endDate={endData}
                  selectsRange
                  locale={pt}
                  dateFormat="P"
                  // isClearable
                />
              </div>
              <div>
                {/* {buscaData === '' && (
              <InputFiltros
              name="buscaData"
              type="date"
              value={buscaData}
              autofocus
              onFocus={(e) => e.currentTarget.select()}
              onChange={(e) => setBuscaData(e.target.value)}
              />
              )}
              {buscaData !== '' && (
                <InputData
                name="buscaData"
                type="date"
                value={buscaData}
                autofocus
                onFocus={(e) => e.currentTarget.select()}
                onChange={(e) => setBuscaData(e.target.value)}
                />
                )}
                {buscaData !== '' && (
                  <InputData
                  name="buscaData"
                  type="date"
                  value={endData}
                  onChange={(e) => setEndData(e.target.value)}
                  />
                )} */}
              </div>
              <label htmlFor="select" className="input-select">
                <SelectFiltros
                  id="select"
                  value={filtrarStatus}
                  onChange={(e) => setFiltrarStatus(e.target.value)}
                >
                  <option value="" defaultValue>
                    Filtrar por Status
                  </option>
                  <option value="Aguardando dados de pagamento">
                    Aguardando dados de pagamento
                  </option>
                  <option value="Aguardando assinatura">
                    Aguardando assinatura
                  </option>
                  <option value="Aguardando Envio">Aguardando Envio</option>
                  <option value="Cadastrado">Cadastrado</option>
                </SelectFiltros>
              </label>
            </div>
          )}
        </Filtros>
        {filtrarContratados().length === 0 && (
          <Empty>
            <img src={imageEmpty} alt="" />
            <p>Nenhum contrato encontrado</p>
          </Empty>
        )}

        <BlockItems>
          {filtrarContratados()
            .slice(0)
            .reverse()
            .map((c) => (
              <BlockItem key={c._id}>
                <div className="copyInfo">
                  {c.campanha !== 0 && (
                    <div className="userInfo">
                      <UserAv>
                        <img src={malaCoob} alt="" />
                        <span className="hoverCampaing">Campanha Isenção </span>
                      </UserAv>
                      <h3>{c.assNome_RazaoSocial}</h3>
                    </div>
                  )}
                  {c.campanha === 0 && (
                    <h3>
                      {c.assNome_RazaoSocial}{' '}
                      {c.CodErro != 0 && c.CodErro != null && (
                        <Tooltip
                          iconColor="#ff0000" // Cor do ícone
                          text={`${c.msgError}`} // Texto do tooltip
                          color="#ff0000" // Cor do texto do tooltip
                          backgroundColor="#f0f0f0" // Cor de fundo do tooltip
                        />
                      )}
                    </h3>
                  )}

                  <div className="detailsButton">
                    <DetailsButton
                      onClick={() => {
                        duplicateContracts(c.id);
                        console.log(c.id);
                      }}
                      style={{ marginRight: 10 }}
                    >
                      <img src={copy} alt="" />
                      <span className="hoverText">
                        Utilizar dados novamente
                      </span>
                    </DetailsButton>

                    {c.status === 'Aguardando dados de pagamento' && (
                      <DetailsButton
                        onClick={() => {
                          resendEmail(
                            c.emailPessoal,
                            c.assCPF_CNPJ,
                            c.tokenPagamento
                          );
                        }}
                        style={{ marginRight: 10 }}
                      >
                        <img src={resend} alt="" />
                        <span className="hoverText">
                          Reenviar link de pagamento
                        </span>
                      </DetailsButton>
                    )}
                    {c.status !== 'Aguardando dados de pagamento' && (
                      <DisabledButton
                        disabled="true"
                        style={{ marginRight: 10 }}
                      >
                        <img src={resend} alt="" />
                      </DisabledButton>
                    )}

                    {c.status === 'Aguardando assinatura' && (
                      <DetailsButton
                        onClick={() => {
                          resendContract(c.emailPessoal, c.assinatura, c.id);
                        }}
                      >
                        <img src={signature} alt="" />
                        <span className="hoverText">
                          Reenviar link de assinatura
                        </span>
                      </DetailsButton>
                    )}
                    {c.status != 'Aguardando assinatura' && (
                      <DisabledButton disabled="true">
                        <img src={signature} alt="" />
                      </DisabledButton>
                    )}
                  </div>
                </div>
                <Line />

                <div className="grid">
                  <p>
                    <span>CPF: </span>
                    {c.assCPF_CNPJ}
                  </p>

                  <p>
                    <span>Vendedor: </span>
                    {c.usuNome}
                    {/* <span>Plano: </span>
                    {c.plano} | {c.qtdeDiarias} diárias */}
                  </p>

                  <p>
                    <span>Data: </span>
                    {new Date(c.criadoEm).toLocaleDateString('pt-BR', {
                      timeZone: 'UTC',
                    })}
                  </p>

                  <p>
                    <span>Usuário: </span>
                    {c.usuUsuario}
                    {/* {c.formaPagamento === null && (
                      <span style={{ color: 'rgba(0, 0, 0, 0.6)' }}>
                        Cartão de Crédito
                      </span>
                    )} */}
                  </p>

                  <p>
                    <span>E-mail: </span>
                    {c.emailPessoal}
                  </p>

                  {/* <Type>{c.plano}</Type> */}
                  <p>
                    <span>Tipo de Venda: </span>
                    {c.tipoVenda}
                    {/* <span style={{ position: 'relative' }}>Status: </span>
                    {c.status} */}
                  </p>

                  <div>
                    <p>
                      <span>Plano: </span>
                      {c.plano} | {c.qtdeDiarias} diárias
                    </p>

                    {c.plNumero_Contrato !== 0 &&
                      c.plNumero_Contrato !== null && (
                        <p>
                          <span>Nº Contrato: </span>
                          {c.plNumero_Contrato}
                        </p>
                      )}
                  </div>

                  {/* <p /> */}
                  {c.restricao !== 'Não' && (
                    <div>
                      <p>
                        <span>Restrição: </span>
                        {c.restricao}
                      </p>
                      <p>
                        <span style={{ position: 'relative' }}>Status: </span>
                        {c.status}
                      </p>
                    </div>
                  )}
                  {c.restricao === 'Não' && (
                    <div>
                      <p>
                        <span style={{ position: 'relative' }}>Status: </span>
                        <span
                          style={{
                            color:
                              c.status === 'Aguardando dados de pagamento'
                                ? '#ff0000' // Amarelo
                                : c.status === 'Aguardando assinatura'
                                ? '#FFA500' // Laranja
                                : c.status === 'Aguardando Envio'
                                ? '#0000FF' // Azul
                                : c.status === 'Cadastrado'
                                ? '#008000' // Verde
                                : 'inherit', // Padrão se não corresponder a nenhum status
                          }}
                        >
                          {c.status}
                        </span>
                      </p>
                    </div>
                  )}
                  {/* {c.plNumero_Contrato !== 0 && c.plNumero_Contrato !== null && (
                    <p>
                      <span>Nº Contrato: </span>
                      {c.plNumero_Contrato}
                    </p>
                  )} */}
                  {/* {c.campanha === 100 && (
                    <p>
                      <span>Campanha: </span>
                      Isenção 1ª e 2ª mensalidades
                    </p>
                  )}
                  {c.campanha === 0 && <p />} */}

                  <p />
                  <p
                    className="updateData"
                    style={{
                      color: '#9D9D9C',
                      fontSize: 14,
                      fontWeight: 300,
                    }}
                  >
                    Última atualização{' '}
                    {new Date(c.dataAlteracaoStatus).getHours()}:
                    {new Date(c.dataAlteracaoStatus).getMinutes() < 10
                      ? '0'
                      : ''}
                    {new Date(c.dataAlteracaoStatus).getMinutes()}{' '}
                    {new Date(c.dataAlteracaoStatus).toLocaleDateString(
                      'pt-BR',
                      {
                        timeZone: 'UTC',
                      }
                    )}
                  </p>
                </div>
                <div className="buttons">
                  <SeeButton
                    type="button"
                    onClick={() => history.push('/viewcontract', { id: c.id })}
                  >
                    Ver
                  </SeeButton>

                  {c.status === 'Aguardando dados de pagamento' && (
                    <EditButton
                      type="button"
                      onClick={() =>
                        history.push('/EditContract', {
                          id: c.id,
                          status: c.status,
                        })
                      }
                    >
                      Editar
                    </EditButton>
                  )}
                  {/* {c.status !== 'Cadastrado' &&
                    c.status !== 'Aguardando Envio' && (
                      <DeleteButton
                        type="button"
                        onClick={() => getModal(c.id)}
                      >
                        Excluir
                      </DeleteButton>
                    )} */}

                  {/* {c.status === 'Aguardando assinatura' && (
                    <SendButton
                      type="button"
                      onClick={() => envioCoobrastur(c.id)}
                    >
                      Enviar
                    </SendButton>
                  )} */}
                  {c.status === 'Aguardando Envio' && desabilitado === true && (
                    <SendButton
                      type="button"
                      onClick={() => {
                        envioCoobrastur(c.id);
                        setTimeout(() => {
                          setDesabilitado(true);
                        }, 100);
                        setTimeout(() => {
                          setDesabilitado(false);
                        }, waitTime);
                      }}
                    >
                      Enviar
                    </SendButton>
                  )}
                  {c.status === 'Aguardando Envio' && desabilitado !== true && (
                    <SendButton
                      type="button"
                      onClick={() => envioCoobrastur(c.id)}
                    >
                      Enviar
                    </SendButton>
                  )}

                  {/* {c.status === 'Cadastrado' && desabilitado !== true && (
                    <SendButton
                      type="button"
                      onClick={() => envioCoobrastur(c.id)}
                    >
                      Reenviar
                    </SendButton>
                  )} */}
                </div>
              </BlockItem>
            ))}
        </BlockItems>
      </div>
    </Container>
  );
}

export default Contracts;
