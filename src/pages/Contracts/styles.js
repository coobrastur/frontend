import styled from 'styled-components';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  min-height: 100vh;

  h2.title {
    color: #01affd;
    font-size: 2.8em;
    padding-top: 50px;
  }

  .copyInfo {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .userInfo {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .userInfo > img {
  }

  .hiddenFiltros {
    display: none;
  }

  @media (max-width: 1100px) {
    padding: 50px 0;
    overflow: hidden;
    h2 {
      text-align: center;
    }

    .Filters {
      display: flex !important;
      justify-content: space-between;
      align-items: center;
    }
    .FiltersShow {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      position: relative;
      top: -45px;
    }
    .hiddenFiltros {
      display: flex;
      color: #01affd;
      margin-right: 20px;
      align-items: center;
    }

    h2.title {
      color: #01affd;
      font-size: 2em;
      padding-top: 10px;
      padding-bottom: 10px;

      text-align: left;
      // width: 1%;
    }

    .copyInfo {
      display: grid;
    }
  }

  @media (min-width: 1100px) {
    .Filters {
      margin-left: 40px;
    }
    .filter {
      display: flex;
      align-itens: center;
    }
  }
`;

export const Empty = styled.div`
  padding: 20px 0px;

  width: 100%;
  margin: 0 auto;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: 35%;
    margin-bottom: 10px;
  }

  p {
    color: rgba(255, 0, 0, 0.5);
  }

  @media (max-width: 1100px) {
    padding: 10px;

    width: 100%;
    margin: 0 auto;
  }
`;

export const BlockItems = styled.div`
  padding: 10px 0px;

  width: 100%;
  margin: 0 auto;

  @media (max-width: 1100px) {
    padding: 10px;

    width: 100%;
    margin: 0 auto;
  }
`;

export const BlockItem = styled.div`
  background-color: white;
  border-radius: 10px;

  padding: 20px;
  margin-bottom: 30px;

  h3 {
    color: #01affd;
    font-family: Tahoma, sans-serif;
    font-size: 18px;
  }

  .grid {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: 1fr 1fr 1fr;
  }

  p span {
    font-family: Tahoma, sans-serif;
    color: #01affd;
    font-weight: 700;
    font-style: normal;
  }

  p {
    font-family: Tahoma, sans-serif;
    font-size: 16px;
    color: rgba(0, 0, 0, 0.6);
    margin-bottom: 10px;
    font-weight: 700;
    font-style: normal;
  }

  p .updateData {
    font-size: '13px';
    text-align: 'center';
    margin-right: 15;
  }

  .buttons {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .detailsButton {
    display: flex;
  }

  @media (max-width: 1100px) {
    .grid {
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 1fr 1fr 1fr 1fr 1fr;
    }

    .buttons {
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
  }

  @media (max-width: 450px) {
    overflow: hidden;
    .grid {
      display: grid;
      grid-template-columns: 1fr;
      grid-template-rows: 1fr 1fr 1fr 1fr 1fr;
    }
    p {
      // background-color: #ccff00;
      font-size: 13px;

      margin-bottom: 5px;
    }

    .detailsButton {
      margin-top: 5px;
    }
  }
`;

export const Line = styled.div`
  border: 1px solid rgba(0, 0, 0, 0.2);
  margin: 10px 0;
`;

export const Type = styled.div`
  background-color: #01affd;
  border-radius: 50px;
  color: white;

  display: inline-block;

  font-family: 'Livvic', sans-serif;
  font-size: 18px;

  margin: 10px 0;
  padding: 10px 20px;
  text-decoration: none;
  text-align: center;
`;

export const SeeButton = styled.button`
  background-color: #00cecb;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #129e9c;
  }

  @media (max-width: 1081px) {
    // width: 30%;

    margin: 5px;
  }
`;

export const Filtros = styled.div`
  position: relative;

  @media (min-width: 1081px) {
    display: flex;
  }
  @media (max-width: 1081px) {
    padding: 0px 0px 15% 0px;
  }

  .daterange {
    z-index: 0;

    @media (min-width: 1081px) {
      box-sizing: border-box;
      font-size: 16px;
      width: 90%;
      height: 15px;
      margin: 10px;
      padding: 10px;
      height: 30px;
      border-radius: 5px;
    }
    @media (max-width: 1081px) {
      width: 95%;
      height: 30px;
      margin: 10px;
      padding: 10px;
      border-radius: 5px;
    }
  }
`;

export const InputFiltros = styled.input`
  background-color: #fff;
  z-index: 0;
  @media (min-width: 1081px) {
    position: relative;
    box-sizing: border-box;
    font-size: 16px;
    float: left;
    // width: 22%;
    margin: 10px;
    padding: 10px;
    height: 30px;
    border-radius: 5px;
    color: #000 !important;
  }
  @media (max-width: 1081px) {
    width: 95%;
    height: 30px;
    margin: 10px;
    padding: 10px;
    border-radius: 5px;
    background-color: #fff;
    color: #000 !important;
  }
`;

export const InputData = styled.input`
  background-color: #fff;
  z-index: 0;
  @media (min-width: 1081px) {
    position: relative;
    box-sizing: border-box;
    font-size: 16px;
    float: left;
    width: 12%;
    margin: 10px;
    padding: 10px;
    height: 30px;
    border-radius: 5px;
    color: #000 !important;
  }
  @media (max-width: 1081px) {
    width: 95%;
    height: 30px;
    margin: 10px;
    padding: 10px;
    border-radius: 5px;
    background-color: #fff;
    color: #000 !important;
  }
`;

export const RefreshButton = styled.button`
  z-index: 0;
  background-color: transparent;
  cursor: pointer;
  @media (min-width: 1081px) {
    font-size: 16px;
    float: left;
    width: 15%;
    text-decoration: none;
    text-align: center;
    color: #4b506d;
    justify-content: center;
    display: flex;
    margin: 10px;
    align-items: center;
  }
  @media (max-width: 1081px) {
    float: left;
    width: 17%;
    height: 30px;
    margin: 10px;
    padding: 10px;
    color: #4b506d !important;
    text-decoration: none;
    justify-content: center;
    display: flex;
    align-items: center;
  }
  :hover {
    color: #ccc;
  }
`;

export const UserAv = styled.image`
  display: flex;
  justify-content: center;
  align-items: center;

  @media (min-width: 1081px) {
    margin-right: 10px;
    height: 30px;
    width: 30px;

    border-radius: 50%;
    box-shadow: none;
    background-color: #ec6726;
    transition: top ease 0.5s;

    :hover {
      width: 177px;
      border-radius: 17px;
    }

    .hoverCampaing {
      display: none;
    }

    :hover .hoverCampaing {
      // font-size: 16px;

      margin-left: 10px;
      color: #fff;
      display: flex;
      align-items: center;

      font-weight: 700;
    }
  }

  @media (max-width: 1081px) {
    margin-right: 10px;
    height: 30px;
    width: 30px;

    border-radius: 50%;
    box-shadow: none;
    background-color: #ec6726;
    transition: top ease 0.5s;

    .hoverCampaing {
      display: none;
    }
  }
`;

export const DetailsButton = styled.button`
  z-index: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  align-items: center;

  @media (min-width: 1081px) {
    display: flex;
    align-itens: center;
    height: 30px;
    width: 30px;

    padding-left: 7px;
    border-radius: 50%;
    box-shadow: none;
    background-color: #01affd;
    transition: top ease 0.5s;

    // margin: 10px;
    // align-items: center;

    :hover {
      width: 200px;
      border-radius: 17px;
    }
    .hoverText {
      display: none;
    }

    :hover .hoverText {
      font-size: 13px;
      // // border: 2px solid #333333;
      // height: 30px;
      // // width: 300px;

      // border-radius: 15px;
      // box-shadow: none;

      margin-left: 5px;
      color: #fff;
      display: flex;
      align-items: center;
      // background-color: #01affd;
    }
  }
  @media (max-width: 1081px) {
    display: flex;
    align-itens: center;
    height: 30px;
    width: 30px;

    padding-left: 8px;
    border-radius: 50%;
    box-shadow: none;
    background-color: #01affd;
    transition: top ease 0.5s;

    .hoverText {
      display: none;
    }
  }

  @media (max-width: 450px) {
    height: 30px;
    width: 32px;
    border-radius: 50%;
  }
`;
export const DisabledButton = styled.button`
  z-index: 0;
  background-color: transparent;
  cursor: pointer;
  display: flex;
  align-items: center;

  @media (min-width: 1081px) {
    display: flex;
    align-itens: center;
    height: 30px;
    width: 30px;

    padding-left: 7px;
    border-radius: 50%;
    box-shadow: none;
    background-color: #9d9d9c;
    transition: top ease 0.5s;

    // margin: 10px;
    // align-items: center;
  }
  @media (max-width: 1081px) {
    display: flex;
    align-itens: center;
    height: 30px;
    width: 30px;

    padding-left: 8px;
    border-radius: 50%;
    box-shadow: none;
    background-color: #9d9d9c;
    transition: top ease 0.5s;

    .hoverText {
      display: none;
    }
  }

  @media (max-width: 450px) {
    height: 30px;
    width: 32px;
    border-radius: 50%;
  }
`;

export const SelectFiltros = styled.select`
  color: #000 !important;
  z-index: 0;
  background-color: #fff;
  position: relative;
  box-sizing: border-box;
  font-size: 16px;
  float: left;
  width: 90%;
  margin: 10px;
  height: 30px;
  border-radius: 5px;
  @media (max-width: 1081px) {
    width: 95%;
    height: 30px;
    border-radius: 5px;
    background-color: #fff;
    color: #000 !important;
  }
`;

export const DeleteButton = styled.button`
  background-color: #f54756;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #a74756;
  }

  @media (max-width: 1081px) {
    // width: 30%;

    margin: 5px;
  }
`;

export const EditButton = styled.button`
  background-color: #00bfff;
  border-radius: 5px;
  color: #ffffff;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px;
  padding: 10px 20px;
  text-decoration: none;
  /* cursor: pointer; */
  width: 100%;

  :hover {
    background-color: #00008b;
  }

  @media (max-width: 1081px) {
    // width: 30%;

    margin: 1px;
    // padding: 10px 20px;
  }
`;

export const SendButton = styled.button`
  background-color: #6fce42;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1em;

  margin: 10px;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;
  width: 100%;

  :hover {
    background-color: #5eae38;
  }

  @media (max-width: 1081px) {
    width: 30%;
  }
`;

export const ButtonHelp = styled.button`
  background-color: transparent;
  position: absolute;

  top: 20px;
  left: 20px;

  img {
    width: 50px;
    height: 50px;
  }

  :hover {
    cursor: pointer;
  }

  @media (max-width: 700px) {
    img {
      width: 40px;
      height: 40px;
    }

    top: 5px;
    right: 5px;
  }

  @media (max-width: 1081px) {
    width: 10%;
  }
`;
