// Dependencies
import React, { useEffect, useRef, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import { Row, Col } from 'react-grid-system';
import { addDays, subDays } from 'date-fns';
import { pt } from 'date-fns/esm/locale';
import { useSelector } from 'react-redux';
import { apiClei, apiViacep } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';
import Tooltip from '../../components/Tooltip';
import SimpleInput from '../../components/SimpleInput';
import InputMask from '../../components/InputMask';
import InputCurrency from '../../components/InputCurrency';
import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';
import Modal from '../../components/ModalEdit';
import ModalGlobal from '../../components/ModalNoEdit';
import ModalReativacao from '../../components/ModalReativacao';

// Images
import imageBackArrow from '../../assets/arrow-left.png';
import arrowUP from '../../assets/arrowUP.svg';
import arrowDown from '../../assets/arrowDown.svg';

// Styles
import 'react-datepicker/dist/react-datepicker.css';

import {
  Container,
  Content,
  BackArrow,
  FormContent,
  DatePickers,
  BlockInputs,
  RadioInputs,
  Button,
  ButtonDisabled,
  ButtonDivisor,
} from './styles';
import useScrollBlock from '../../utils/useScrollBlock';

export default function EditContract() {
  const history = useHistory();
  const [loader, setLoader] = useState();
  const formRef = useRef();
  const { token } = useSelector((state) => state.auth);
  const location = useLocation();
  const { id } = location.state;
  const { status } = location.state;
  const [valorMensalidade, setValorMensalidade] = useState('');
  const finalValue = parseFloat(
    valorMensalidade.replace(',', '.')
  ).toLocaleString('pt-br', { minimumFractionDigits: 2 });

  // modal inadimplência
  const [modalGlobal, setModalGlobal] = useState(false);
  const [modalReativacao, setModalReativacao] = useState(false);
  const [desabilitado, setDesabilitado] = useState(false);

  const [birthDate, setBirthDate] = useState('');
  const handleDateChange = (event) => {
    setBirthDate(event.target.value);
  };

  const [indicador, setIndicador] = useState('');
  const [restricao, setRestricao] = useState();
  const [valorRestricao, setValorRestricao] = useState();
  const [restricaoFia, setRestricaoFia] = useState();
  const [fiador, setFiador] = useState();
  const [ufAssociado, setUfAssociado] = useState('');
  const [cidade, setCidade] = useState('');
  const [bairroAssociado, setBairroAssociado] = useState('');
  const [rua, setRua] = useState('');
  const [numeroAssociado, setNumeroAssociado] = useState('');
  const [complemento, setComplemento] = useState('');
  const [enderecoLiberado, setEnderecoLiberado] = useState(false);
  const [aditamento, setAditamento] = useState(false);
  const [prepago, setPrePago] = useState();
  const [select, setSelect] = useState('');
  const [parcelasPrePagas, setParcelasPrePagas] = useState('');
  // const [civilGuarantorState, setCivilGuarantorState] = useState('');
  const [typeOfContract, setTypeOfContract] = useState('');
  const [planoResgatado, setPlanoResgatado] = useState(
    'Nenhum plano encontrado'
  );
  const [planoSubstituido, setPlanoSubstituido] = useState('');

  const [company, setCompany] = useState([]);
  const [codCompany, setCodCompany] = useState('');
  const [selectedCompany, setSelectedCompany] = useState('');

  const [flat, setFlat] = useState('');
  const [dailyQuantities, setDailyQuantities] = useState('');
  const [family, setFamily] = useState('');
  const [adhesion, setAdhesion] = useState(true);
  const [payment, setPayment] = useState('');
  const [checkOffer, setCheckOffer] = useState(false);
  const [offer, setOffer] = useState(false);
  const [offerApply, setOfferApply] = useState(false);
  const [parcelasVendedor, setParcelasVendedor] = useState('');
  const [parcelasCoobrastur, setParcelasCoobrastur] = useState('');
  const [sellerMembershipForm, setSellerMembershipForm] = useState('');
  const [halfAdhesion, setHalfAdhesion] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [blockScroll, allowScroll] = useScrollBlock();
  const [dadosPessoais, setDadosPessoais] = useState(true);
  const [dadosRestricao, setDadosRestricao] = useState(true);
  const [dadosIndicacao, setDadosIndicacao] = useState(true);
  const [dadosEmpresa, setDadosEmpresa] = useState(true);
  const [dadosAssociacao, setDadosAssociacao] = useState(true);
  const [dadosBoleto, setDadosBoleto] = useState(true);
  const [dadosAdesao, setDadosAdesao] = useState(true);

  const [valoresAdesao, setValoresAdesao] = useState(false);

  const [dataBoletoEscolhida, setDataBoletoEscolhida] = useState();

  const [isSendModalClick, setIsSendModalClick] = useState(false);

  const [cpf, setCPF] = useState('');
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [confirmaEmail, setConfirmaEmail] = useState('');

  const [telefone, setTelefone] = useState('');
  const [cepAssociado, setCepAssociado] = useState('');
  const [valorAdesao, setValorAdesao] = useState('');
  const [finalAdesao, setFinalAdesao] = useState('');
  const [descontoAdesao, setDescontoAdesao] = useState('');
  const descontoAdesaoFinal = parseFloat(descontoAdesao);

  const [isPayable, setIsPayable] = useState('');
  const [primeiraAdesao, setPrimeiraAdesao] = useState('');

  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  const [dataEscolhida, setDataEscolhida] = useState(tomorrow);

  const [dataAdesao, setDataAdesao] = useState('');
  const [checaCPF, setChecaCPF] = useState(false);
  const [checaCPFFia, setChecaCPFFia] = useState(false);
  const [checaCEP, setChecaCEP] = useState(false);
  const [cpfIndic, setCpfIndic] = useState('');
  const [nicData, setNicData] = useState([]);
  const [nicSelecionado, setNICSelecionado] = useState('');
  const [nomeIndic, setNomeIndic] = useState('');
  const [emailIndic, setEmailIndic] = useState('');
  const [celularIndic, setCelularIndic] = useState('');
  const [cpfFia, setCpfFia] = useState('');
  const [nomeFia, setNomeFia] = useState('');
  // const [dataFia, setDataFia] = useState('');
  // const [nacioFia, setNacioFia] = useState('');
  const [maeFia, setMaeFia] = useState('');
  // const [paiFia, setPaiFia] = useState('');
  const [emailFia, setEmailFia] = useState('');
  const [confirmaEmailFiador, setConfirmaEmailFiador] = useState('');
  const [celularFia, setCelularFia] = useState('');
  // const [conjugeFia, setConjugeFia] = useState('');

  const [adFormaPagamento, setAdFormaPagamento] = useState(true);

  const [formaPagamento, setFormaPagamento] = useState('');
  const [datasPagamento, setDatasPagamento] = useState('');
  const [nroPipeline, setNroPipeLine] = useState(0);
  const dealRequerido = localStorage.getItem('dealRequerido');

  // campanha
  const [campaingValue, setCampaignValue] = useState('');

  const defaultCompany = company.find(
    (empresa) => empresa.empCodigo === codCompany
  );

  if (defaultCompany == 60) {
    setAdhesion(false);
  }

  /* eslint-disable-next-line no-unused-vars */
  // const [planos, setPlanos] = useState([
  //   {
  //     tipoVenda: typeOfContract,
  //     tpplcodigo: flat,
  //     plQtdeDiarias: dailyQuantities,
  //     plfamilia: family.toLocaleString(),
  //     adesaoValorOriginal: valorAdesao,
  //     adesaoValorDesconto: descontoAdesao,
  //     valorMensalidade: valorMensalidade.toLocaleString('pt-br', {
  //       minimumFractionDigits: 2,
  //     }),
  //   },
  // ]);

  // console.log(planos);
  const [lotAppVenda, setLotAppVenda] = useState('');
  const [carMes, setCarMes] = useState('');
  const [carDia, setCarDia] = useState('');
  const [debMes, setDebMes] = useState('');
  const [debDia, setDebDia] = useState('');

  // split data boleto
  const [boletoMonth, setBoletoMonth] = useState('');
  // console.log(dataBoletoEscolhida);
  const parts = boletoMonth.split('/');
  const monthBoleto = parts[1];
  // console.log(parts);
  const currentDate = Date.now();
  const today = new Date(currentDate);

  const [arrayDays, setArrayDays] = useState([]);

  const [plPlanoReativ, setPlPlanoReativ] = useState('');
  const [plCodigoReativ, setPlCodigoReativ] = useState('');

  // v2 ambiente de produção e v3 de homologação
  const urlGlobal = 'https://telemarketing.coobrastur.com.br/wsAppVendasV2';

  const [vendNomes, setVendNomes] = useState([]);
  const [cupNomes, setCupNomes] = useState([]);
  // const [vendNomeAPI, setVendNomeAPI] = useState('');
  const [vendNomeSelecionado, setVendNomeSelecionado] = useState('');
  const [vendCodigoSelecionado, setVendCodigoSelecionado] = useState('');
  const [cupNomeSelecionado, setCupNomeSelecionado] = useState('');
  const [cupCodigoSelecionado, setCupCodigoSelecionado] = useState('');
  const [dados, setDados] = useState([]);

  // estruturação dos dados Pessoais
  const dadosCliente = {
    cpf,
    rua,
    nome,
    email,
    telefone,
    ufAssociado,
    cidade,
    bairroAssociado,
    numeroAssociado,
    complemento,
    cepAssociado,
    valorRestricao,
    restricao,
    dadosRestricao,
    fiador,
    aditamento,
    formaPagamento,
    dataBoletoEscolhida,
    carDia,
    carMes,
    debDia,
    debMes,
    // campaingValue,
  };

  const dAssociacao = {
    typeOfContract,
    selectedCompany,
    defaultCompany,
    nroPipeline,
    flat,
    dailyQuantities,
    family,
    // payment,
    // halfAdhesion,
    // parcelasCoobrastur,
    finalValue,
    valorAdesao,
    finalAdesao,
    dataAdesao,
    descontoAdesao,
    sellerMembershipForm,
    dataEscolhida,
    parcelasVendedor,
    indicador,
    cpfIndic,
    nomeIndic,
    emailIndic,
    celularIndic,
  };

  const dadosFia = {
    fiador,
    aditamento,
    select,
    cpfFia,
    nomeFia,
    // dataFia,
    // nacioFia,
    // civilGuarantorState,
    maeFia,
    // paiFia,
    emailFia,
    celularFia,
    // conjugeFia,
  };

  function adicionaMascaraCPF(valor) {
    let valorEditado = 0;
    valorEditado = valor
      .toString()
      .replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
    // console.log(valorEditado);
    return valorEditado;
  }

  async function asyncEndereco(value) {
    const res = await apiViacep.get(`${value}/json`);
    const { logradouro, bairro, localidade, uf, numero, cep } = res.data;
    const input = document.getElementById('input');
    const input2 = document.getElementById('input2');
    const input3 = document.getElementById('input2');

    if (!logradouro) {
      setEnderecoLiberado(true);
      // setRua('');
      // setBairroAssociado('');
      input.disabled = false;
      input2.disabled = false;
      input3.disabled = false;
    }

    if (uf) {
      setUfAssociado(uf);
    }

    if (localidade) {
      setCidade(localidade);
    }

    if (bairro) {
      setBairroAssociado(bairro);
    }

    if (logradouro) {
      // setRua(rua);
      setEnderecoLiberado(true);
      input.disabled = true;
      input2.disabled = true;
      input3.disabled = true;
    }

    if (numero) {
      setNumeroAssociado(numero);
      setEnderecoLiberado(true);
    }
    if (cepAssociado) {
      setCepAssociado(cep);
      setEnderecoLiberado(true);
    }
  }

  async function verificaStatus(idContract, statusContract) {
    try {
      apiClei
        .get(
          urlGlobal.concat(
            `/Contrato.asmx/VerificaStatus?appVenda=${idContract}&status=${statusContract}`
          )
        )
        .then((response) => {
          console.log('RESPONSE: ', response);
          setLoader(false);
          if (!response.data.valido) {
            setModalGlobal(true);
            return false;
          }
        })
        .catch(() => {
          showToast({
            type: 'error',
            message:
              'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
          });

          setLoader(false);
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  async function verificarCPF(strCPF) {
    function RetiraMascara(userCpff) {
      // Converte para string (caso seja número ou outro tipo)
      userCpff = String(userCpff).replace(/[^\d]/g, ''); // <--- Conversão explícita aqui

      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    // debugger;
    strCPF = RetiraMascara(strCPF);
    let Soma;
    let Resto;
    Soma = 0;
    // if (strCPF == '00000000000') return false;

    // debugger;
    if (
      strCPF == '00000000000' ||
      strCPF == '11111111111' ||
      strCPF == '22222222222' ||
      strCPF == '33333333333' ||
      strCPF == '44444444444' ||
      strCPF == '55555555555' ||
      strCPF == '66666666666' ||
      strCPF == '77777777777' ||
      strCPF == '88888888888' ||
      strCPF == '99999999999'
    ) {
      return false;
    }

    // console.log(strCPF);
    for (let i = 1; i <= 9; i++)
      Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;

    for (let i = 1; i <= 10; i++)
      Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
  }

  // Validar CPf Cliente
  async function verificaRestricao(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    if (verificarCPF(cpf)) {
      try {
        apiClei
          .get(
            urlGlobal.concat(
              `/Usuario.asmx/RestricacaoSerasa?cpf=${RetiraMascara(userCpf)}`
            )
            // .get(
            // HML: https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // PROD https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // `https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=${RetiraMascara(
            //   userCpf
            // )}`
            // { cpf: RetiraMascara(userCpf) },
            // { headers: { Authorization: token || '' } }
          )
          .then((response) => {
            // console.log('RESPONSE: ', response);
            // console.log('PEGAR O DADO: ', response.data.body.restricao);

            if (response.data.restriction === false) {
              showToast({
                type: 'success',
                message: 'Resultado da análise de crédito: Alto Score.',
              });
              setRestricao(false);
              setFiador(false);
              setAditamento(false);
              setValorRestricao(false);
            }

            if (response.data.restriction === true) {
              showToast({
                type: 'error',
                message: 'Resultado da análise de crédito: Baixo Score.',
              });
              setRestricao(true);
              setValorRestricao(true);
            }

            // if (response.data.body.code === 4) {
            //   showToast({
            //     type: 'error',
            //     message: 'O CPF informado é invalido.',
            //   });
            //   setRestricao(true);
            // }
            setLoader(false);
            return false;
          })
          .catch(() => {
            showToast({
              type: 'error',
              message:
                'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
            });

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });
        setLoader(false);
      }
    } else {
      showToast({
        type: 'error',
        message: 'CPF Invalido. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  async function fetchNicsByCPF() {
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    console.log(cpfIndic);
    console.log(cpfIndic.length);
    // if (cpfIndic.length !== 14) {
    //   showToast({
    //     type: 'error',
    //     message: 'Por favor, insira um CPF válido.',
    //   });
    //   return;
    // }
    if (verificarCPF(cpfIndic)) {
      try {
        setLoader(true);
        const response = await apiClei.get(
          urlGlobal.concat(
            `/ConsultaCrm.asmx/ListaNics?in_pesquisa=${RetiraMascara(cpfIndic)}`
          )
        );
        setNicData(response.data); // Armazena os dados da API
      } catch (error) {
        console.log(error);
        showToast({
          type: 'error',
          message: 'Erro ao consultar CPF. Tente novamente mais tarde.',
        });
      } finally {
        setLoader(false);
      }
    } else {
      showToast({
        type: 'error',
        message: 'CPF Invalido. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }
  useEffect(() => {
    if (cpfIndic && cpfIndic !== '000.000.000-00') {
      // Adapte a condição conforme sua regra
      fetchNicsByCPF(cpfIndic.replace(/\D/g, '')); // Remove a máscara antes de enviar
    }
  }, [cpfIndic]);
  const handleNICChange = (e) => {
    const nicSelecionado = e.target.value;
    setNICSelecionado(nicSelecionado);
    setLoader(true);
    setLoader(false);
  };

  // validação do CPF fiador
  async function verificaRestricaoFia(userCpf) {
    userCpf = adicionaMascaraCPF(userCpf);
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    if (verificarCPF(cpf)) {
      try {
        apiClei
          .get(
            urlGlobal.concat(
              `/Usuario.asmx/RestricacaoSerasa?cpf=${RetiraMascara(userCpf)}`
            )
            // .get(
            // HML: https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // PROD https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // `https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=${RetiraMascara(
            //   userCpf
            // )}`
            // { cpf: RetiraMascara(userCpf) },
            // { headers: { Authorization: token || '' } }
          )
          .then((response) => {
            // console.log('RESPONSE: ', response);
            // console.log('PEGAR O DADO: ', response.data.body.restricao);

            if (response.data.restriction === false) {
              showToast({
                type: 'success',
                message: 'Resultado da análise de crédito: Alto Score.',
              });
              setRestricaoFia(false);
              setValorRestricao(false);
            }

            if (response.data.restriction === true) {
              showToast({
                type: 'error',
                message: 'Resultado da análise de crédito: Baixo Score.',
              });
              setRestricaoFia(true);
              setValorRestricao(true);
            }

            // if (response.data.body.code === 4) {
            //   showToast({
            //     type: 'error',
            //     message: 'O CPF informado é invalido.',
            //   });
            //   setRestricaoFia(true);
            // }
            setLoader(false);
            return false;
          })
          .catch(() => {
            showToast({
              type: 'error',
              message:
                'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
            });

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });
        setLoader(false);
      }
    } else {
      showToast({
        type: 'error',
        message: 'CPF Invalido. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  async function getContracts() {
    const dados = {
      token,
      id,
    };
    await apiClei
      .get(`Contrato.asmx/ObterContratoPorId`, {
        params: {
          dados,
        },
      })
      .then((response) => {
        // console.log(response.data[0]);
        setCPF(parseFloat(response.data[0].assCPF_CNPJ));
        if (response.data[0].assCPF_CNPJ !== 0) {
          verificaRestricao(response.data[0].assCPF_CNPJ);
        }
        setNome(response.data[0].assNome_RazaoSocial);
        setEmail(response.data[0].assEmailPessoal);
        setConfirmaEmail(response.data[0].assEmailPessoal);
        setTelefone(
          `(${response.data[0].assNumCelularDDD})${response.data[0].assNumCelular}`
        );
        setCepAssociado(response.data[0].endCepAssociado);
        setEnderecoLiberado(true);
        // asyncEndereco(response.data[0].endCepAssociado);
        // ENDEREÇO
        setBairroAssociado(response.data[0].endBairroAssociado);
        setNumeroAssociado(response.data[0].endNumLogradouroAssociado);
        setRua(response.data[0].endLogradouroAssociado);
        setComplemento(response.data[0].endComplementoAssociado);
        setCidade(response.data[0].endCidadeAssociado);
        setUfAssociado(response.data[0].endUF);
        setRua(response.data[0].endLogradouroAssociado);

        function formataData(dataIso) {
          const data = new Date(dataIso);
          const dia = String(data.getDate()).padStart(2, '0');
          const mes = String(data.getMonth() + 1).padStart(2, '0');
          const ano = data.getFullYear();
          return `${dia}/${mes}/${ano}`;
        }

        setBirthDate(formataData(response.data[0].dtNascimento));
        setCpfIndic(response.data[0].cpfIndicador);
        // if (response.data[0].cpfIndicador !== 0) {
        //   fetchNicsByCPF(response.data[0].cpfIndicador);
        // }

        console.log(response.data[0].cpfIndic);
        setNICSelecionado(response.data[0].assnicIndicador);
        setNomeIndic(response.data[0].nomeIndicador);
        setEmailIndic(response.data[0].emailIndicador);
        setCelularIndic(
          `(${response.data[0].dddCelularIndicador})${response.data[0].celularIndicador}`
        );
        if (response.data[0].dddCelularIndicador !== 0) {
          setCelularIndic('');
        }
        // PLANO
        setTypeOfContract(response.data[0].tipoVenda);
        if (response.data[0].tipoVenda == 2) {
          setTypeOfContract('1');
        }

        setCodCompany(response.data[0].empCodigo);
        console.log('Código da Empresa:', response.data[0].empCodigo);

        setNroPipeLine(response.data[0].nroPipeline);
        setFlat(response.data[0].tpplcodigo);
        setFamily(response.data[0].plfamilia);
        setDailyQuantities(response.data[0].plQtdeDiarias);
        // console.log(response.data[0].plQtdeDiarias);

        if (
          response.data[0].tipoVenda != '11' &&
          response.data[0].adesao == true
        ) {
          setValoresAdesao(true);
          // setAdhesion(true);
        } else {
          setParcelasVendedor(0);
        }
        // prepago
        if (response.data[0].parcelasPrePagas != 0) {
          setParcelasPrePagas(response.data[0].parcelasPrePagas);
          setPrePago(true);
        }
        // ADESÃO
        // if (response.data[0].adesao == false) {
        //   setAde
        // }
        // setDataBoletoEscolhida(response.data[0].dataAdesao);
        const dataApi = response.data[0].dataAdesao;
        setDataEscolhida(new Date(dataApi));
        // console.log('data edit: ', dataEscolhida);
        setValorAdesao(response.data[0].adesaoValorOriginal);
        // console.log(
        //   response.data[0].adesaoValorOriginal -
        //     response.data[0].adesaoValorDesconto
        // );
        setDescontoAdesao(response.data[0].adesaoValorDesconto);
        if (
          response.data[0].valorAdesao !=
            response.data[0].adesaoValorOriginal &&
          response.data[0].valorAdesao !== 0 &&
          response.data[0].adesao == true
        ) {
          setCheckOffer(true);
          setOffer(true);
          setOfferApply(true);

          const resultado =
            response.data[0].adesaoValorOriginal -
            response.data[0].adesaoValorDesconto;

          setFinalAdesao(resultado);
        }
        if (
          response.data[0].valorAdesao ==
            response.data[0].adesaoValorOriginal &&
          response.data[0].adesao == true
        ) {
          setCheckOffer(false);
          setOffer(false);
          setOfferApply(false);

          const resultado = response.data[0].valorAdesao;

          setFinalAdesao(resultado);
        }
        if (
          response.data[0].adesaoValorDesconto ==
            response.data[0].adesaoValorOriginal &&
          response.data[0].adesao == false
        ) {
          setCheckOffer(false);
          setOffer(false);
          setAdFormaPagamento(false);
          setOfferApply(true);

          const resultado =
            response.data[0].adesaoValorOriginal -
            response.data[0].adesaoValorDesconto;

          setFinalAdesao(resultado);
        }
        setSellerMembershipForm(response.data[0].formaPagamentoAdesao);
        if (response.data[0].formaPagamentoAdesao == 'Dinheiro') {
          setSellerMembershipForm('1');
        }
        if (response.data[0].formaPagamentoAdesao == 'Cartão') {
          setSellerMembershipForm('2');
        }
        if (response.data[0].formaPagamentoAdesao == 'Cheque') {
          setSellerMembershipForm('3');
        }
        if (response.data[0].formaPagamentoAdesao == 'Boleto') {
          setSellerMembershipForm('4');
        }
        setParcelasVendedor(response.data[0].parcelamentoAdesao);

        // DADOS PAGAMENTO
        setFormaPagamento(response.data[0].formaPagamento);
        setCarMes(response.data[0].carMes);
        // console.log(response.data[0].carMes);
        // console.log(response.data[0].debMes);
        if (response.data[0].carMes !== 0) {
          /* eslint-disable-next-line no-use-before-define */
          onSelectMonth(response.data[0].carMes);
        }
        setCarDia(response.data[0].carDia);
        setDebMes(response.data[0].debMes);
        if (response.data[0].debMes !== 0) {
          /* eslint-disable-next-line no-use-before-define */
          onSelectMonth(response.data[0].debMes);
        }
        setDebDia(response.data[0].debDia);
        // if (response.data[0].carDia !== '') {
        //   console.log(carDia)
        // }

        // CAMPANHA
        setCampaignValue(response.data[0].campanha);

        // CUPOM
        setCupCodigoSelecionado(response.data[0].cupCodigo);
        setVendCodigoSelecionado(response.data[0].vendCodigo);
        if (response.data[0].vendCodigo) {
          setCheckOffer(false);
          setOffer(false);
          setSellerMembershipForm(0);
          setAdhesion(false);
          setValoresAdesao(false);
          setParcelasVendedor('');
        }
        // console.log(cupCodigoSelecionado);
        // FIADOR
        setFiador(response.data[0].fiador);
        if (fiador !== false) {
          setRestricaoFia(true);
          setValorRestricao(true);
        }
        setAditamento(response.data[0].aditamento);
        setSelect(response.data[0].formaaditamento);
        if (response.data[0].fiaCpf == 0) {
          setCpfFia('');
        } else {
          setCpfFia(response.data[0].fiaCpf);
        }
        if (response.data[0].fiaCpf !== 0) {
          verificaRestricaoFia(response.data[0].fiaCpf);
        }
        setNomeFia(response.data[0].fianome);
        setMaeFia(response.data[0].fiaConjugeFiliacao);
        setEmailFia(response.data[0].fiaEmailPessoal);
        setConfirmaEmailFiador(response.data[0].fiaEmailPessoal);
        setCelularFia(
          `(${response.data[0].fiaNumeroCelularDDD})${response.data[0].fiaNumeroCelular}`
        );
        setIndicador(response.data[0].indicacao);
        setRestricao(response.data[0].assRestricao);
        setAdhesion(response.data[0].adesao);
        if (response.data[0].adesao == false) {
          setValoresAdesao(false);
        }
        // setSellerMembershipForm(response.data[0].formaPagamentoAdesao);
        setPayment(response.data[0].meioAdesao);
        setParcelasCoobrastur(response.data[0].formaAdesaoCoobrastur);
        setHalfAdhesion(response.data[0].tipoAdesaoCoobrastur);
        verificaStatus(response.data[0].id, status);
        setPlCodigoReativ(response.data[0].plcodigoReativado);
        setPlPlanoReativ(response.data[0].tpplcodigoReativado);
        setPlanoResgatado(response.data[0].planoResgatado);

        setLotAppVenda(response.data[0].lotAppVenda);
      });
  }

  async function handleFormSubmit(data) {
    // Validações de campos obrigatórios
    if (!cpf) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de CPF do associado',
      });

      setLoader(false);
      return false;
    }

    if (!nome) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de Nome do associado',
      });

      setLoader(false);
      return false;
    }

    const regNome = /^[a-záàâãéèêíïóôõöúçñA-ZÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ ]+$/;
    // console.log(regNome.test(data.assNome_RazaoSocial));
    if (regNome.test(nome) === false) {
      showToast({
        type: 'error',
        message: 'Nome do associado precisa ter apenas letras.',
      });

      setLoader(false);
      return false;
    }

    // Validando E-mail
    if (!email) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de E-mail do associado',
      });

      setLoader(false);
      return false;
    }
    const regEmail =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    if (!regEmail.test(email)) {
      showToast({
        type: 'error',
        message: 'E-mail do associado é inválido',
      });

      setLoader(false);
      return false;
    }

    if (email !== confirmaEmail) {
      showToast({
        type: 'error',
        message: 'Os campos de e-mail e confirmar e-mails não correspondem.',
      });

      setLoader(false);
      return false;
    }
    // removendo hífen do número de celular
    let newAssNumber = telefone;
    let newIndicatorNumber = data.celularIndicador;
    let newGuarantorNumber = data.fiaNumeroCelular;
    // let newGuarantorPhone = data.fiaFone;

    let newCpfIndicator = data.cpfIndicador;
    let newCpfGuarantor = data.fiaCpf;

    newAssNumber = newAssNumber.replace(/-/g, '');

    if (!newAssNumber) {
      showToast({
        type: 'error',
        message: 'Preencha o campo Celular do associado',
      });

      setLoader(false);
      return false;
    }

    if (indicador === undefined) {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Indicador',
      });

      setLoader(false);
      return false;
    }

    if (indicador === true) {
      newIndicatorNumber = newIndicatorNumber.replace(/-/g, '');
      newCpfIndicator = newCpfIndicator.replace(/[^\d]+/g, '');
      console.log(newCpfIndicator);
    }

    if (fiador === true) {
      newGuarantorNumber = newGuarantorNumber.replace(/-/g, '');
      // newGuarantorPhone = newGuarantorPhone.replace(/-/g, '');
      newCpfGuarantor = newCpfGuarantor.replace(/[^\d]+/g, '');
    }

    if (indicador === true && !data.nomeIndicador) {
      showToast({
        type: 'error',
        message: 'Preencha o Nome do Indicador',
      });

      setLoader(false);
      return false;
    }
    if (indicador === true) {
      if (cpfIndic < 13) {
        showToast({
          type: 'error',
          message: 'CPF indicador inválido.',
        });

        setLoader(false);
        return false;
      }
    }

    if (indicador === true && !newCpfIndicator) {
      showToast({
        type: 'error',
        message: 'Preencha o CPF do Indicador',
      });

      setLoader(false);
      return false;
    }

    if (restricao === undefined) {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Restrição',
      });

      setLoader(false);
      return false;
    }

    if (formaPagamento == 4 && !dataBoletoEscolhida) {
      showToast({
        type: 'error',
        message: 'Preencha dia para pagamento do boleto',
      });

      setLoader(false);
      return false;
    }
    if (restricao === true && fiador === true) {
      if (
        !newCpfGuarantor ||
        !data.fianome ||
        // !data.fiaDtNascimento ||
        // !data.fianacionalidade ||
        // !civilGuarantorState ||
        !data.fiaConjugeFiliacao ||
        !data.fiaEmailPessoal ||
        // !data.fia ||
        // !data.fiaNumeroCelularDDD ||
        !newGuarantorNumber
      ) {
        showToast({
          type: 'error',
          message: 'Preencha todas as informações de Fiador',
        });

        setLoader(false);
        return false;
      }
    }

    if (!typeOfContract || typeOfContract === 'Selecione') {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Tipo de Contrato',
      });

      setLoader(false);
      return false;
    }

    if (!flat || flat === 'Selecione') {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Plano',
      });

      setLoader(false);
      return false;
    }

    if (!dailyQuantities || dailyQuantities === 'Selecione') {
      showToast({
        type: 'error',
        message: 'Escolha a quantidade de diárias',
      });

      setLoader(false);
      return false;
    }

    if (family === '') {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Família',
      });

      setLoader(false);
      return false;
    }

    if (
      typeOfContract !== '11' &&
      adFormaPagamento === true &&
      sellerMembershipForm == ''
    ) {
      showToast({
        type: 'error',
        message: 'Escolha uma forma de pagamento para adesão',
      });

      setLoader(false);
      return false;
    }

    if (
      typeOfContract !== '11' &&
      (localStorage.getItem('repreCodigo').startsWith(13) ||
        localStorage.getItem('repreCodigo').startsWith(22) ||
        localStorage.getItem('repreCodigo').startsWith(28)) &&
      isPayable == 0 &&
      parcelasVendedor != 0
    ) {
      showToast({
        type: 'error',
        message: 'Confira a data de adesão.',
      });

      setLoader(false);
      return false;
    }

    if (
      typeOfContract !== '11' &&
      adFormaPagamento === true &&
      sellerMembershipForm !== '1' &&
      !parcelasVendedor
    ) {
      showToast({
        type: 'error',
        message: 'Escolha uma forma de parcelamento para adesão',
      });

      setLoader(false);
      return false;
    }

    // Valor de adesão
    // setValorMensalidade(
    //   formatReal(
    //     getMensalidade({
    //       plano: flat,
    //       diarias: dailyQuantities,
    //       familia: family,
    //     })
    //   )
    // );
    const informacoesPlano = {
      tpPlCodigo: flat,
      plFamilia: family,
      qtdeDiarias: dailyQuantities,
      adesao: false,
      vendCodigo: 675,
    };

    apiClei
      .post(urlGlobal.concat('/ConsultaCrm.asmx/RetornaValoresPlanos'), {
        dados: JSON.stringify(informacoesPlano),
      })
      .then((res) => {
        setValorMensalidade(res.data.ValorPlano);
        // console.log(finalValue);
      })
      .catch((e) => {
        console.log(e);
      });

    // if (adhesion === '') {
    //   // console.log(adhesion);
    //   showToast({
    //     type: 'error',
    //     message: 'Escolha uma opção de Adesão',
    //   });
    //   setLoader(false);
    //   return false;
    // }

    // if (adhesion === true && data.valorAdesao < 1) {
    //   // console.log(adhesion);
    //   showToast({
    //     type: 'error',
    //     message: 'O valor da adesão não pode ser igual a zero.',
    //   });
    //   setLoader(false);
    //   return false;
    // }

    if (!cepAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o CEP',
      });

      setLoader(false);
      return false;
    }

    if (!ufAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o Estado',
      });

      setLoader(false);
      return false;
    }

    if (!cidade) {
      showToast({
        type: 'error',
        message: 'Preencha a Cidade',
      });

      setLoader(false);
      return false;
    }

    if (!bairroAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o bairro',
      });

      setLoader(false);
      return false;
    }

    if (!rua) {
      showToast({
        type: 'error',
        message: 'Preencha a rua',
      });

      setLoader(false);
      return false;
    }
    if (fiador === true) {
      if (email.toUpperCase() === emailFia.toUpperCase()) {
        showToast({
          type: 'error',
          message: 'O e-mail do associado e do fiador não podem ser iguais.',
        });
        setLoader(false);
        return false;
      }
    }

    if (!numeroAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o número',
      });

      setLoader(false);
      return false;
    }
    setLoader(true);
    if (verificaStatus(id, status)) {
      setIsModalVisible(true);
      blockScroll();
    }
    setLoader(false);

    function formataNumero(numero) {
      return numero ? numero.replace(/\(|-/g, '').split(')') : '';
    }
    // function RetiraMascara(userCpff) {
    //   userCpff = userCpff.replace(/[^\d]/g, '');
    //   return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    // }
    if (isSendModalClick !== false) {
      try {
        const dados = {
          id: id,
          lotAppVenda: lotAppVenda,
          // assCPF_CNPJ: RetiraMascara(cpf),
          assCPF_CNPJ: cpf,
          assNome_RazaoSocial: nome,
          assEmailPessoal: email,
          assNumCelularDDD: formataNumero(telefone)[0],
          assNumCelular: formataNumero(telefone)[1],
          dtNascimento: birthDate,

          indicacao: indicador || false,
          nomeIndicador: data.nomeIndicador || '',
          cpfIndicador: newCpfIndicator || '',
          assnicIndicador: nicSelecionado || '',
          emailIndicador: data.emailIndicador || '',
          dddCelularIndicador: formataNumero(newIndicatorNumber)[0] || '',
          celularIndicador: formataNumero(newIndicatorNumber)[1] || '',

          assRestricao: restricao,
          tipoVenda: typeOfContract,
          aditamento,
          prepago: prepago || false,
          // formaAditamento: select,
          parcelasPrePagas: parcelasPrePagas || 0,

          fiador,
          fiaCpf: newCpfGuarantor || '',
          fianome: data.fianome || '',
          fiaDtNascimento: data.fiaDtNascimento || '',
          fianacionalidade: data.fianacionalidade || '',
          // civCodigo: civilGuarantorState || '',
          civCodigo: '',
          fiaConjuge: data.fiaConjuge || '',
          fiaConjugeFiliacao: data.fiaConjugeFiliacao || '',
          fiaConjugeFiliacao2: data.fiaConjugeFiliacao2 || '',
          fiaEmailPessoal: data.fiaEmailPessoal || '',
          fiaNumeroCelularDDD: formataNumero(newGuarantorNumber)[0] || '',
          fiaNumeroCelular: formataNumero(newGuarantorNumber)[1] || '',
          // fiaddFone: formataNumero(newGuarantorPhone)[0] || '',
          // fiaFone: formataNumero(newGuarantorPhone)[1] || '',
          formaAssinaturaFiador: 1,
          assinaturaFiador: '',

          TpplanoReativado: plPlanoReativ,
          plCodigoReativado: plCodigoReativ,
          planoResgatado: planoResgatado,
          //

          empCodigo: codCompany,
          planos: [
            {
              adesao: adhesion,
              nroPipeline: nroPipeline,
              DealRequerido: dealRequerido,
              tipoVenda: typeOfContract,
              tpplcodigo: flat,
              plQtdeDiarias: dailyQuantities,
              plfamilia: family.toLocaleString(),
              valorAdesaoOriginal: valorAdesao,
              valorAdesao: finalAdesao,
              valorMensalidade: valorMensalidade.toLocaleString('pt-br', {
                minimumFractionDigits: 2,
              }),
            },
          ],
          valorMensalidade: valorMensalidade,
          dataAdesao: dataEscolhida,
          adesao: adhesion || false,
          // valorAdesao: data.valorAdesao || valorAdesao || '',
          adesaoValorDesconto: descontoAdesao || 0,
          adesaoValorOriginal: valorAdesao || 0,
          formaPagamentoAdesao: sellerMembershipForm || 0,
          parcelamentoAdesao: parcelasVendedor || 0,
          meioAdesao: payment,
          tipoAdesaoVendedor: sellerMembershipForm || '',
          formaAdesaoVendedor: parcelasVendedor || '',
          dataVendedor: data.dataVendedor || '',
          obsVendedor: data.obsVendedor || '',

          tipoAdesaoCoobrastur: halfAdhesion || '',
          formaAdesaoCoobrastur: parcelasCoobrastur || '',
          dataCoobrastur: data.dataCoobrastur || '',
          obsCoobrastur: data.obsCoobrastur || '',

          endUF: ufAssociado,
          endCepAssociado: cepAssociado.replace('-', '') || '',
          endCidadeAssociado: cidade || '',
          endBairroAssociado: bairroAssociado || '',
          endLogradouroAssociado: rua || '',
          endNumLogradouroAssociado: numeroAssociado || '',
          endComplementoAssociado: data.endComplementoAssociado || '',

          terceiro: false,
          cpfTerceiro: data.cpfTerceiro || '',
          nomeTerceiro: data.nomeTerceiro || '',
          nascimentoTerceiro: data.nascimentoTerceiro || '',
          nacionalidadeTerceiro: data.nacionalidadeTerceiro || '',
          estadoCivilTerceiro: data.estadoCivilTerceiro || 0,
          nomeConjugeTerceiro: data.nomeConjugeTerceiro || '',
          nomeMaeTerceiro: data.nomeMaeTerceiro || '',
          nomePaiTerceiro: data.nomePaiTerceiro || '',
          emailTerceiro: data.emailTerceiro || '',
          dddCelularTerceiro: data.dddCelularTerceiro || '',
          numCelularTerceiro: data.numCelularTerceiro || '',
          dddTelefoneTerceiro: data.dddTelefoneTerceiro || '',
          TelefoneTerceiro: data.TelefoneTerceiro || '',
          formaAssinaturaTerceiro: data.formaAssinaturaTerceiro || 0,
          assinaturaTerceiro: '',
          env: 0, // VARIAVEL DE AMBIENTE: env: 1 (HOMOLOG) / env: 0 (PROD)

          campanha: campaingValue,
          formaPagamento,
          dataBoletoEscolhida: dataBoletoEscolhida || '',

          carDia: carDia || '',
          carMes: carMes || '',
          debDia: debDia || '',
          debMes: debMes || '',

          formaAssinatura: 1,
          assinatura: '',
          update: true,
          status: status,
          cupCodigo: cupCodigoSelecionado || 0,
          vendCodigo: vendCodigoSelecionado || 0,
          token,
        };
        // console.log(dados);
        allowScroll();
        apiClei
          .get(`Contrato.asmx/IncluirContratoMaisPlanos`, {
            params: {
              dados,
            },
          })
          .then((response) => {
            // const info = response.data[0];
            // console.log(dados);

            // console.log(response.data[0]);

            showToast({
              type: 'success',
              message: 'Contrato criado com sucesso',
            });

            if (formaPagamento == 4 && codCompany !== 60) {
              setLoader(true);

              try {
                apiClei
                  .get(`/D4Sign.asmx/EnviarContratosD4Sing`, {
                    params: {
                      dados: {
                        info: {
                          id: response.data[0].Id,
                          token: response.data[0].Token,
                        },
                      },
                    },
                  })

                  .then((res) => {
                    console.log(res);
                  })
                  .catch((e) => {
                    console.log(e);
                  });

                setLoader(false);
                history.push('/contracts');
              } catch (err) {
                showToast({
                  type: 'error',
                  message:
                    'Algum erro interno aconteceu. Tente novamente mais tarde',
                });

                setLoader(false);
              }
              return false;
            }

            // inserir aqui endpoint envio de dados cadastrais
            apiClei
              .get(`Contrato.asmx/EnviaEmailDadosPagamento`, {
                params: {
                  cpf: response.data[0].Cpf,
                  email: response.data[0].Email,
                  env: response.data[0].Env,
                  urlPagamento: response.data[0].UrlPagamento,
                },
              })
              .then((res) => {
                console.log(res);
              });

            history.push('/contracts');

            setLoader(false);
          })
          .catch((error) => {
            // console.log(error.response.status);

            if (error.response.data.match('CPF do indicador incorreto!')) {
              showToast({
                type: 'error',
                message: 'CPF do indicador incorreto!',
              });

              setLoader(false);
              return false;
            }

            if (error.response.status == 500) {
              setTimeout(() => {
                setDesabilitado(true);
              }, 100);
              setTimeout(() => {}, 3000);

              showToast({
                type: 'error',
                message:
                  'Algum erro interno aconteceu. Não foi possível enviar seus dados, entre em contato com o suporte',
              });
              setLoader(false);
              return false;
            }

            showToast({
              type: 'error',
              message:
                'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
            });

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });

        setLoader(false);
      }

      return true;
    }
  }

  function validaParcelas(datasEscolhida) {
    const dataAtual = new Date();
    const data = new Date();

    if (formaPagamento === '3') {
      data.setMonth(carMes - 1, datasEscolhida);
    } else {
      data.setMonth(debMes - 1, datasEscolhida);
    }

    // console.log('data antes', data.toLocaleDateString());
    if (data.getMonth() < dataAtual.getMonth()) {
      data.setFullYear(data.getFullYear() + 1);
    }
    // console.log('data depois', data.toLocaleDateString());
    // console.log(dataEscolhida);
    const dataFormatada =
      datasEscolhida.indexOf('/') > 0
        ? datasEscolhida
        : `${datasEscolhida}/${carMes || debMes}/${data.getFullYear()}`; // console.log(dataCartao);

    const dados = {
      dataPrimeiraMensalidade: dataFormatada,
      parcelas: parcelasVendedor,
    };

    apiClei
      .get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/PrimeiraCobranca?dados=${JSON.stringify(dados)}`
        )
      )
      .then((resp) => {
        // console.log(resp);

        setIsPayable(resp.data[0].isPayable);
        setPrimeiraAdesao(
          resp.data[0].dtPrimeiraAdesao.toLocaleString().substr(0, 10)
        );

        // console.log(isPayable);
        // console.log(primeiraAdesao.toLocaleString().substr(0, 10));
      });
  }

  async function valuesAdesao() {
    const informacoesPlano = {
      tpPlCodigo: flat,
      plFamilia: family,
      qtdeDiarias: dailyQuantities,
      adesao: false,
      vendCodigo: 675,
    };

    apiClei
      .post(urlGlobal.concat('/ConsultaCrm.asmx/RetornaValoresPlanos'), {
        dados: JSON.stringify(informacoesPlano),
      })
      .then((res) => {
        setValorMensalidade(res.data.ValorPlano);

        const dados = {
          tpplcodigo: flat,
          plqtdediarias: dailyQuantities,
          plfamilia: family,
          vendcodigo: 675,
          tipdata: today.toLocaleString(),
        };

        apiClei
          .get(
            urlGlobal.concat(
              `/ConsultaCrm.asmx/RetoraValorAdesao?dados=${JSON.stringify(
                dados
              )}`
            )
          )
          .then((resp) => {
            setValorAdesao(resp.data[0].valAdesao);
            // console.log(resp);
          })
          .catch((e) => {
            console.log(e);
          });
        // console.log(finalValue);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  if (dailyQuantities || family) {
    valuesAdesao();
  }

  // parceiros
  async function buscaParceiro() {
    const repreCodigo = localStorage.getItem('repreCodigo'); // Pega o valor de 'reprecodigo' do localStorage

    // console.log(repreCodigo);
    const endpoint =
      repreCodigo == '1'
        ? `/Contrato.asmx/ListaParceiroCupons?vendCodigo=21300&reprCodigo=1&cupCodigo=&empCod=${codCompany}`
        : `/Contrato.asmx/ListaParceiroCupons?vendCodigo=&reprCodigo=&cupCodigo=&empCod=${codCompany}`;

    try {
      const response = await apiClei.get(endpoint);
      const dados = response.data;

      const vendNomes = [...new Set(dados.map((item) => item.vendNome))];
      setVendNomes(vendNomes);
      setDados(dados);

      // Verificar se o vendCodigoSelecionado está definido e encontrar o vendNome correspondente
      if (vendCodigoSelecionado) {
        const vendNomeCorrespondente = dados.find(
          (item) => item.vendCodigo === vendCodigoSelecionado
        );

        if (vendNomeCorrespondente) {
          setVendNomeSelecionado(vendNomeCorrespondente.vendNome);

          // Filtrar os cupons correspondentes ao vendNome selecionado
          const cuponsFiltrados = dados
            .filter((item) => item.vendNome === vendNomeCorrespondente.vendNome)
            .map((item) => item.cupNome);

          setCupNomes(cuponsFiltrados);

          // Verificar se o cupCodigoSelecionado pertence a algum cupom
          const cupomSelecionado = dados.find(
            (item) => item.cupCodigo === cupCodigoSelecionado
          );

          if (cupomSelecionado) {
            setCupNomeSelecionado(cupomSelecionado.cupNome);
          } else {
            setCupNomeSelecionado('');
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    buscaParceiro();
  }, [vendCodigoSelecionado]);

  // busca empresa
  async function buscaEmpresa() {
    apiClei
      .get(urlGlobal.concat('/ConsultaCrm.asmx/RetornaEmpresas?empcod='))
      .then((response) => {
        const dados = response.data;
        setCompany(dados);
      });
  }
  useEffect(() => {
    buscaEmpresa();
  }, []);
  // console.log(vendNomeAPI);
  const handleVendNomeChange = (event) => {
    const vendNome = event.target.value;
    if (vendNome == 'Selecione') {
      setOfferApply(false);
      setAdhesion(true);
      setAdFormaPagamento(true);
      setValoresAdesao(true);
      setDescontoAdesao('0,00');
      setFinalAdesao(valorAdesao);
      // setValorAdesao(false);
      // console.log("Opção 'Selecione' selecionada");
    }
    setVendNomeSelecionado(vendNome);
    setCupNomeSelecionado('');
    setCupCodigoSelecionado('');

    // Filtrar os cupons correspondentes ao vendNome selecionado
    const cuponsFiltrados = dados
      .filter((item) => item.vendNome === vendNome)
      .map((item) => item.cupNome);

    setCupNomes(cuponsFiltrados);

    // Encontrar o vendCodigo correspondente ao vendNome selecionado
    const vendCodigo = dados.find(
      (item) => item.vendNome === vendNome
    )?.vendCodigo;
    setVendCodigoSelecionado(vendCodigo);
  };

  const handleCupNomeChange = (event) => {
    const cupNome = event.target.value;
    setCupNomeSelecionado(cupNome);

    const cupomSelecionado = dados.find(
      (item) =>
        item.vendNome === vendNomeSelecionado && item.cupNome === cupNome
    );

    if (cupomSelecionado) {
      setCupNomeSelecionado(cupNome);
      setCupCodigoSelecionado(cupomSelecionado.cupCodigo);
    } else {
      setCupNomeSelecionado('');
      setCupCodigoSelecionado('');
    }
  };

  const updateItem = () => {
    apiClei
      .post(
        'https://api.coobrastur.com.br/ApiDotz/api/VendaSemAdesao/ValidateDiscountCoupon',
        {
          plCodigo: flat,
          plFamilia: false,
          qtdDiarias: dailyQuantities,
          cupomDesconto: cupNomeSelecionado,
          vendCodigo: vendCodigoSelecionado.toString(),
        }
      )
      .then((response) => {
        if (response.data.cupomValid === false) {
          showToast({
            type: 'error',
            message: 'Cupom inválido.',
          });
          return;
        }

        showToast({
          type: 'success',
          message: 'Cupom válido! Isenção da adesão.',
        });
        setCheckOffer(false);
        setOffer(false);
        setSellerMembershipForm(0);
        setAdhesion(false);
        setValoresAdesao(false);
        setParcelasVendedor('');
        setAdFormaPagamento(false);

        const vendCodigo = vendCodigoSelecionado.toString();

        if (
          vendCodigo === '99024' ||
          vendCodigo === '99022' ||
          vendCodigo === '99019' ||
          vendCodigo === '99021' ||
          cupCodigoSelecionado == 1055
        ) {
          setAdhesion(false);
          setDescontoAdesao(valorAdesao);
          setFinalAdesao(0);
          // valorMensalidade: valorMensalidadeCalculado.toLocaleString(
          //   'pt-br',
          //   {
          //     minimumFractionDigits: 2,
          //   }
          // ),
        }
      })
      .catch((error) => {
        console.error('An error occurred:', error);
        showToast({
          type: 'error',
          message: 'Ocorreu um erro ao processar o cupom.',
        });
      });
  };

  // data boleto
  async function buscaDataPagamento() {
    let valorInicial;
    // const infoData = {
    //   dataEscolhida: dataEscolhida.toLocaleDateString(),
    //   numeroParcelas: parseInt(parcelasVendedor),
    // };
    const numeroParcelas = parseInt(0);
    apiClei
      .get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/RetornarDatasValidas?dataEscolhida=${dataEscolhida.toLocaleDateString()}&numeroParcelas=${numeroParcelas}`
        )
      )
      .then((response) => {
        // console.log(response);
        valorInicial = response.data;
        valorInicial.unshift({ dataExibicao: 'Selecione', dataReal: '' });
        setDatasPagamento(valorInicial);
      });
  }

  // Validação Inadimplência Coob+

  async function verificaRestricaoCoob(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }

    try {
      // inadimplência coob+
      apiClei
        .post(
          urlGlobal.concat('/ConsultaCrm.asmx/AssociadoInadimplenciaAPP'),
          { cpf: RetiraMascara(userCpf) },
          { headers: { Authorization: token || '' } }
        )
        .then((response) => {
          // console.log('RESPONSE: ', response);
          // console.log('PEGAR O DADO: ', response.data.body.inadimplente);

          if (response.data.body.inadimplente === false) {
            verificaRestricao(userCpf);
          }

          if (response.data.body.inadimplente === true) {
            setModalGlobal(true);
            setLoader(false);
            return 0;
          }
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  const stateArray = [
    { value: '', label: 'Selecione' },
    { value: 'AC', label: 'AC' },
    { value: 'AL', label: 'AL' },
    { value: 'AP', label: 'AP' },
    { value: 'AM', label: 'AM' },
    { value: 'BA', label: 'BA' },
    { value: 'CE', label: 'CE' },
    { value: 'DF', label: 'DF' },
    { value: 'ES', label: 'ES' },
    { value: 'GO', label: 'GO' },
    { value: 'MA', label: 'MA' },
    { value: 'MT', label: 'MT' },
    { value: 'MS', label: 'MS' },
    { value: 'MG', label: 'MG' },
    { value: 'PA', label: 'PA' },
    { value: 'PB', label: 'PB' },
    { value: 'PR', label: 'PR' },
    { value: 'PE', label: 'PE' },
    { value: 'PI', label: 'PI' },
    { value: 'RJ', label: 'RJ' },
    { value: 'RN', label: 'RN' },
    { value: 'RS', label: 'RS' },
    { value: 'RO', label: 'RO' },
    { value: 'RR', label: 'RR' },
    { value: 'SC', label: 'SC' },
    { value: 'SP', label: 'SP' },
    { value: 'SE', label: 'SE' },
    { value: 'TO', label: 'TO' },
  ];

  const size = 50;
  const days = new Array(size);

  /* eslint-disable-next-line no-plusplus */
  for (let i = 2; i < size; i++) {
    days[i] = {
      value: i,
      label: `${i} diárias`,
    };
  }

  // const diariasArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 3, label: '3 diárias' },
  //   { value: 5, label: '5 diárias' },
  //   { value: 7, label: '7 diárias' },
  //   { value: 9, label: '9 diárias' },
  //   { value: 11, label: '11 diárias' },
  //   { value: 13, label: '13 diárias' },
  //   { value: 15, label: '15 diárias' },
  //   { value: 17, label: '17 diárias' },
  //   { value: 19, label: '19 diárias' },
  //   { value: 21, label: '21 diárias' },
  //   { value: 23, label: '23 diárias' },
  //   { value: 25, label: '25 diárias' },
  //   { value: 27, label: '27 diárias' },
  //   { value: 29, label: '29 diárias' },
  //   { value: 31, label: '31 diárias' },
  //   { value: 33, label: '33 diárias' },
  //   { value: 35, label: '35 diárias' },
  //   { value: 37, label: '37 diárias' },
  //   { value: 39, label: '39 diárias' },
  //   { value: 41, label: '41 diárias' },
  //   { value: 43, label: '43 diárias' },
  //   { value: 45, label: '45 diárias' },
  //   { value: 47, label: '47 diárias' },
  //   { value: 49, label: '49 diárias' },
  // ];

  // const diariasGoArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 4, label: '4 diárias' },
  //   { value: 7, label: '7 diárias' },
  //   { value: 9, label: '9 diárias' },
  //   { value: 11, label: '11 diárias' },
  //   { value: 13, label: '13 diárias' },
  //   { value: 15, label: '15 diárias' },
  //   { value: 17, label: '17 diárias' },
  //   { value: 19, label: '19 diárias' },
  //   { value: 21, label: '21 diárias' },
  //   { value: 23, label: '23 diárias' },
  //   { value: 25, label: '25 diárias' },
  //   { value: 27, label: '27 diárias' },
  //   { value: 29, label: '29 diárias' },
  //   { value: 31, label: '31 diárias' },
  //   { value: 33, label: '33 diárias' },
  //   { value: 35, label: '35 diárias' },
  //   { value: 37, label: '37 diárias' },
  //   { value: 39, label: '39 diárias' },
  //   { value: 41, label: '41 diárias' },
  //   { value: 43, label: '43 diárias' },
  //   { value: 45, label: '45 diárias' },
  //   { value: 47, label: '47 diárias' },
  //   { value: 49, label: '49 diárias' },
  // ];

  const planoArray = [
    { value: '', label: 'Selecione' },
    { value: 36, label: 'Vip +' },
    { value: 37, label: 'Master +' },
    { value: 38, label: 'Gold Vip +' },
    { value: 39, label: 'Gold Master +' },
    { value: 40, label: 'Diamante +' },
    { value: 41, label: 'Go Vip +' },
    { value: 42, label: 'Go Master +' },
    { value: 1002, label: 'Banstur Ouro' },
    { value: 1016, label: 'Banstur J3 Multi' },
    { value: 3000, label: 'Meridien Club' },
  ];

  const hasPlano1001OrGreater = flat >= 1001;
  const hasPlano1001 = flat < 1000;

  let filteredPlanoArray;

  if (hasPlano1001OrGreater) {
    filteredPlanoArray = planoArray.filter((option) => option.value >= 1001);
  } else if (hasPlano1001) {
    filteredPlanoArray = planoArray.filter((option) => option.value < 1000);
  } else {
    filteredPlanoArray = planoArray;
  }

  const bolformaPagamento = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Débito em Conta Corrente' },
    { value: 3, label: 'Cartão de Crédito' },
    { value: 4, label: 'Boleto' },
  ];
  const monthOptions = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Janeiro' },
    { value: 2, label: 'Fevereiro' },
    { value: 3, label: 'Março' },
    { value: 4, label: 'Abril' },
    { value: 5, label: 'Maio' },
    { value: 6, label: 'Junho' },
    { value: 7, label: 'Julho' },
    { value: 8, label: 'Agosto' },
    { value: 9, label: 'Setembro' },
    { value: 10, label: 'Outubro' },
    { value: 11, label: 'Novembro' },
    { value: 12, label: 'Dezembro' },
  ];

  /* eslint-disable no-shadow */
  let currentMonth = new Date(dataEscolhida).getMonth() + 1; // Aqui pega o mês atual - de 1 a 12
  /* es-lint-enable */

  // if (parcelasVendedor == 1) {
  //   currentMonth = dataEscolhida.getMonth() + 2;
  // }
  if (parcelasVendedor == 2) {
    currentMonth = dataEscolhida.getMonth() + 2;
  }
  // if (parcelasVendedor == 0) {
  //   currentMonth = dataEscolhida.getMonth() + 1;
  // }

  function nextThreeMonths(currMonth) {
    let options = [];
    let month = currMonth;

    /* eslint-disable-next-line no-plusplus */
    if (parcelasVendedor == 1) {
      for (let i = 0; i < 2; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }

    if (parcelasVendedor == 2) {
      for (let i = 0; i < 2; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }
    if (parcelasVendedor == 0) {
      for (let i = 0; i < 3; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }

    return options;
  }

  function onSelectMonth(carMes, debMes) {
    const date = new Date();
    const lastDay = new Date(date.getFullYear(), carMes || debMes, 0);

    const arrayDays = [];

    /* eslint-disable-next-line no-plusplus */
    for (let i = 1; i <= lastDay.getDate(); i++) {
      arrayDays.push(i);
    }
    setArrayDays(arrayDays);
  }

  function validDay(dia) {
    const dataAtual = new Date(dataEscolhida);
    const data = new Date();
    const dataLimit = new Date();

    if (parcelasVendedor == 0) {
      dataLimit.setDate(dataAtual.getDate() + 100);
    } else {
      dataLimit.setDate(dataAtual.getDate() + 125);
    }

    // console.log(dataLimit);
    if (formaPagamento == '3') {
      data.setMonth(carMes - 1, dia);
    } else {
      data.setMonth(debMes - 1, dia);
    }

    if (data.getMonth() < dataAtual.getMonth()) {
      data.setFullYear(data.getFullYear() + 1);
    }

    if (parcelasVendedor == 2) {
      dataAtual.setMonth(dataAtual.getMonth() + 1);
      // console.log('data: ', data);
      // console.log('data atual: ', dataAtual);
    }

    // console.log(data.getFullYear());
    // if (dataLimit.getMonth() < dataAtual.getMonth()) {
    //   data.setFullYear(data.getFullYear() + 1);
    // }

    // console.log(`dia: ${dia}`, data.toLocaleDateString(), data < dataAtual);

    if (data < dataAtual || data > dataLimit) {
      return false;
    }

    return true;
  }

  const filterDays = arrayDays.filter((option) => validDay(option));

  // console.log(filterDays);
  // const estadoCivilArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 2, label: 'Casado(a)' },
  //   { value: 1, label: 'Solteiro(a)' },
  //   { value: 3, label: 'Divorciado(a)' },
  //   { value: 4, label: 'Viúvo(a)' },
  // ];

  const formaPagamentoAdesaoVendedorArray = [
    { value: '1', label: 'Dinheiro' },
    { value: '2', label: 'Cartão' },
    // { value: '3', label: 'Cheque' },
    { value: '4', label: 'Boleto' },
  ];

  const parcelasPagamentoAdesaoArray = [
    // { value: '', label: 'Selecione' },
    { value: 1, label: 'À vista' },
    { value: 2, label: '2x' },
  ];

  const parcelasPagamentoAdesaoVendedorArray = [
    // { value: '', label: 'Selecione' },
    { value: 1, label: 'À vista' },
    { value: 2, label: '2x' },
  ];

  const formaPagamentoAdesaoCoobrasturArray = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Boleto' },
  ];

  /* eslint-disable react/jsx-no-bind */
  useEffect(() => {
    getContracts();
  }, []);

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />
      <ModalGlobal setModalGlobal={setModalGlobal} modalGlobal={modalGlobal} />;
      <ModalReativacao
        onClose={() => setModalReativacao(false)}
        onSend={() => {
          setTypeOfContract('11');
          setModalReativacao(false);
        }}
        setModalReativacao={ModalReativacao}
        modalReativacao={modalReativacao}
      />
      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>
      {/* {console.log(adhesion)} */}
      <div className="container">
        <h2>Editar Contrato</h2>
        <Content>
          <FormContent ref={formRef} onSubmit={handleFormSubmit}>
            <BlockInputs>
              {dadosPessoais !== true && (
                <h3>
                  Dados Pessoais
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosPessoais(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosPessoais === true && (
                <div className="form">
                  <h3>
                    Dados Pessoais
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosPessoais(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  {/* {console.log(cpf)} */}
                  <Row className="rowContract">
                    <Col md={3}>
                      <label>CPF:</label>
                      <InputMask
                        name="assCPF_CNPJ"
                        disabled
                        mask="999.999.999-99"
                        value={cpf}
                        // onCopy={(e) => {
                        //   e.preventDefault();
                        //   return false;
                        // }}
                        onChange={(e) => {
                          setCPF(e.target.value);
                        }}
                        onKeyUp={() => {
                          if (
                            cpf.length > 13 &&
                            !cpf.includes('_') &&
                            checaCPF !== true
                          ) {
                            setChecaCPF(true);
                            verificaRestricaoCoob(cpf);

                            // console.log(cpf);
                          }

                          if (cpf.includes('_') && checaCPF === true) {
                            setRestricao(false);
                            setChecaCPF(false);
                          }
                        }}
                      />
                    </Col>
                    <Col md={9}>
                      <label>Nome:</label>
                      <SimpleInput
                        name="assNome_RazaoSocial"
                        placeholder="Nome Completo"
                        type="text"
                        value={nome}
                        onChange={(e) => setNome(e.target.value)}
                        onBlur={(e) => {
                          const trimmedValue = e.target.value.trimEnd();
                          setNome(trimmedValue);
                        }}
                      />
                    </Col>
                  </Row>
                  <Row className="rowContract">
                    <Col md={6}>
                      <label>E-mail:</label>
                      <SimpleInput
                        name="assEmailPessoal"
                        placeholder="E-mail"
                        type="text"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </Col>
                    <Col md={6}>
                      <label>Confirmar E-mail:</label>
                      <SimpleInput
                        id="confirmarEmail"
                        name="assConfirmaEmailPessoal"
                        placeholder="E-mail"
                        type="text"
                        value={confirmaEmail}
                        onChange={(e) => setConfirmaEmail(e.target.value)}
                        onPaste={(e) => {
                          e.preventDefault();
                          return false;
                        }}
                        onCopy={(e) => {
                          e.preventDefault();
                          return false;
                        }}
                      />
                    </Col>
                  </Row>
                  <Row className="rowContract">
                    <Col md={6}>
                      <label>Número do Celular:</label>
                      <InputMask
                        name="assNumCelular"
                        placeholder="Número de celular"
                        mask="(99)99999-9999"
                        type="text"
                        value={telefone}
                        onChange={(e) => setTelefone(e.target.value)}
                      />
                    </Col>

                    <Col md={6}>
                      <label>Data de Nascimento:</label>
                      <InputMask
                        id="contract.birth"
                        name="assBirth"
                        mask="99/99/9999"
                        // maskChar={null}
                        // type="number"
                        value={birthDate}
                        onChange={handleDateChange}
                        placeholder="dd/mm/aaaa"
                      />
                    </Col>
                  </Row>
                  <Row className="row Contract">
                    <Col md={3}>
                      <label>CEP:</label>
                      <InputMask
                        name="endCepAssociado"
                        placeholder="CEP"
                        mask="99999-999"
                        type="text"
                        value={cepAssociado}
                        onChange={(e) => {
                          asyncEndereco(e.target.value);
                          setCepAssociado(e.target.value);
                        }}
                        onKeyUp={() => {
                          if (
                            cepAssociado.length > 8 &&
                            !cepAssociado.includes('_') &&
                            checaCEP !== true
                          ) {
                            setChecaCEP(true);
                            asyncEndereco(cepAssociado);
                          }

                          if (cepAssociado.includes('_') && checaCEP) {
                            setChecaCEP(false);
                          }
                        }}
                      />
                    </Col>
                  </Row>
                  {enderecoLiberado && (
                    <>
                      <Row className="rowContract">
                        <Col md={7}>
                          <label>Endereço:</label>
                          <SimpleInput
                            id="input"
                            name="endLogradouroAssociado"
                            placeholder="Endereço"
                            type="text"
                            value={rua}
                            onChange={(e) => setRua(e.target.value)}
                          />
                        </Col>
                        <Col md={5}>
                          <label>Bairro:</label>
                          <SimpleInput
                            id="input2"
                            name="endBairroAssociado"
                            placeholder="Bairro"
                            type="text"
                            value={bairroAssociado}
                            onChange={(e) => setBairroAssociado(e.target.value)}
                          />
                        </Col>
                      </Row>
                      <Row className="rowContract">
                        <Col md={2}>
                          <label>Número:</label>
                          <SimpleInput
                            name="endNumLogradouroAssociado"
                            placeholder="Número"
                            type="number"
                            value={numeroAssociado}
                            onChange={(e) => setNumeroAssociado(e.target.value)}
                          />
                        </Col>
                        <Col md={4}>
                          <label>Complemento:</label>
                          <SimpleInput
                            name="endComplementoAssociado"
                            placeholder="Complemento"
                            type="text"
                            value={complemento}
                            onChange={(e) => setComplemento(e.target.value)}
                          />
                        </Col>
                        <Col md={4}>
                          <label>Cidade:</label>
                          <SimpleInput
                            id="input3"
                            name="endCidadeAssociado"
                            placeholder="Cidade"
                            type="text"
                            value={cidade}
                          />
                        </Col>
                        <Col md={2}>
                          <label>Estado:</label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="select"
                              value={ufAssociado}
                              onChange={(e) => setUfAssociado(e.target.value)}
                            >
                              {stateArray.map((option) => (
                                <option key={option.value} value={option.value}>
                                  {option.label}
                                </option>
                              ))}
                            </select>
                          </label>
                        </Col>
                      </Row>
                    </>
                  )}
                </div>
              )}
            </BlockInputs>
            {restricao === true && (
              <BlockInputs>
                {/* Bloco de restrição fechado */}
                {dadosRestricao !== true && (
                  <h3>
                    Área de Baixo Score
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosRestricao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosRestricao === true && (
                  // Bloco de restrição fechado
                  <div className="Form">
                    <h3>
                      Área de Baixo Score
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosRestricao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>
                    <RadioInputs>
                      <Row className="rowContract">
                        {/* <Col md="1">
                          <div className="inline">
                            <div className="items">
                              <input
                                type="radio"
                                name="restricao"
                                value="Sim"
                                onClick={(e) => {
                                  setValorRestricao(e.target.value);
                                  setRestricao(true);
                                }}
                              />
                              Sim
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                name="restricao"
                                value="Não"
                                onClick={(e) => {
                                  setRestricao(false);
                                  setValorRestricao(e.target.value);
                                  setFiador(false);
                                  setAditamento(false);
                                }}
                              />
                              Não
                            </div>
                          </div>
                        </Col> */}
                        {restricao === true && (
                          <Col md="3">
                            <RadioInputs>
                              <div className="inline">
                                {codCompany != 58 && (
                                  <div className="items">
                                    <input
                                      type="radio"
                                      value
                                      name="alternative"
                                      onClick={() => {
                                        setFiador(true);
                                        setAditamento(false);
                                        setParcelasPrePagas('0');
                                        setPrePago(false);
                                      }}
                                    />
                                    Fiador
                                  </div>
                                )}

                                <div className="items">
                                  <input
                                    type="radio"
                                    value={false}
                                    name="alternative"
                                    checked={prepago == true}
                                    onClick={() => {
                                      // setAditamento(true);
                                      setPrePago(true);
                                      setParcelasPrePagas('9999');
                                      setFiador(false);
                                    }}
                                  />
                                  Pré-Pago
                                </div>
                              </div>
                            </RadioInputs>
                          </Col>
                        )}
                      </Row>
                    </RadioInputs>

                    {fiador === true && restricao === true && (
                      <div className="form">
                        <Row className="rowContract">
                          <Col md={3}>
                            <label>CPF do Fiador:</label>
                            <InputMask
                              name="fiaCpf"
                              placeholder="CPF do Fiador"
                              mask="999.999.999-99"
                              type="text"
                              value={cpfFia}
                              onChange={(e) => setCpfFia(e.target.value)}
                              onKeyUp={() => {
                                if (
                                  cpfFia.length > 13 &&
                                  !cpfFia.includes('_') &&
                                  checaCPFFia !== true
                                ) {
                                  setChecaCPFFia(true);
                                  verificaRestricaoFia(cpfFia);
                                }

                                if (
                                  cpfFia.includes('_') &&
                                  checaCPFFia === true
                                ) {
                                  // setRestricaoFia(false);
                                  setChecaCPFFia(false);
                                }
                              }}
                            />
                          </Col>
                        </Row>
                        {restricaoFia === false && (
                          <div>
                            <Row className="rowContract">
                              <Col md={12}>
                                <label>Nome do Fiador:</label>
                                <SimpleInput
                                  name="fianome"
                                  placeholder="Nome do Fiador"
                                  type="text"
                                  value={nomeFia}
                                  onChange={(e) => setNomeFia(e.target.value)}
                                />
                              </Col>
                            </Row>
                            <Row className="rowContract">
                              <Col md={6}>
                                <label>Nome da Mãe do Fiador:</label>
                                <SimpleInput
                                  name="fiaConjugeFiliacao"
                                  placeholder="Nome da Mãe do Fiador"
                                  type="text"
                                  value={maeFia}
                                  onChange={(e) => setMaeFia(e.target.value)}
                                />
                              </Col>

                              <Col md={6}>
                                <label>Celular do Fiador:</label>
                                <InputMask
                                  name="fiaNumeroCelular"
                                  placeholder="Celular do Fiador(com o DDD)"
                                  mask="(99)99999-9999"
                                  type="text"
                                  value={celularFia}
                                  onChange={(e) =>
                                    setCelularFia(e.target.value)
                                  }
                                />
                              </Col>
                            </Row>
                            <Row className="rowContract">
                              <Col md={6}>
                                <label>E-mail do Fiador:</label>
                                <SimpleInput
                                  name="fiaEmailPessoal"
                                  placeholder="E-mail do Fiador"
                                  type="email"
                                  value={emailFia}
                                  onChange={(e) => setEmailFia(e.target.value)}
                                />
                              </Col>

                              <Col md={6}>
                                <label>Confirmar E-mail do Fiador:</label>
                                <InputMask
                                  name="confirmaEmailFiador"
                                  placeholder="E-mail do Fiador"
                                  type="email"
                                  value={confirmaEmailFiador}
                                  onChange={(e) =>
                                    setConfirmaEmailFiador(e.target.value)
                                  }
                                  onPaste={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                  onCopy={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        )}
                      </div>
                    )}

                    {/* {aditamento === true && (
                      <div>
                        <label>Forma de aditamento:</label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            value={select}
                            onChange={(e) => {
                              setSelect(e.target.value);
                              setFormaPagamento('');
                              setDataBoletoEscolhida('');

                              if (e.target.value === '2') {
                                setParcelasPrePagas(0);
                                // console.log('foi', parcelasPrePagas);
                              }
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            <option value="1">AD12</option>
                            {/* <option value="2">Imediato</option> 
                          </select>
                        </label>
                      </div>
                    )} */}

                    <p>
                      Caso o associado tenha Inadimplência com a Coob+ ou baixo
                      score, deverá constar a opção de pré-pago.
                    </p>
                  </div>
                )}
              </BlockInputs>
            )}
            <BlockInputs>
              {dadosIndicacao !== true && (
                <h3>
                  Indicação
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosIndicacao(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosIndicacao === true && (
                <div className="form">
                  <h3>
                    Indicação
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosIndicacao(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  <RadioInputs>
                    <div className="inline">
                      <div className="items">
                        <input
                          type="radio"
                          value
                          checked={indicador === true}
                          name="indicador"
                          onClick={
                            ((e) => setIndicador(e.target.value),
                            () => setIndicador(true))
                          }
                        />
                        Sim
                      </div>

                      <div className="items">
                        <input
                          type="radio"
                          value={false}
                          checked={indicador === false}
                          name="indicador"
                          onClick={
                            ((e) => setIndicador(e.target.value),
                            () => setIndicador(false))
                          }
                        />
                        Não
                      </div>
                    </div>
                  </RadioInputs>
                  {indicador === true && (
                    <div className="form">
                      <Row className="rowContract">
                        <Col md={7}>
                          <label>CPF do Indicador:</label>
                          <InputMask
                            name="cpfIndicador"
                            placeholder="CPF Indicador"
                            mask="999.999.999-99"
                            type="text"
                            value={cpfIndic}
                            onChange={(e) => setCpfIndic(e.target.value)}
                            onKeyUp={() => {
                              // Corrigindo a variável (cpf → cpfIndic)
                              if (
                                cpfIndic.length > 13 &&
                                !cpfIndic.includes('_') &&
                                checaCPF !== true
                              ) {
                                setChecaCPF(true);
                              }
                              // Corrigindo aqui também ↓
                              if (cpfIndic.includes('_') && checaCPF) {
                                // <--- cpfIndic em vez de cpf
                                setChecaCPF(false);
                              }
                            }}
                          />
                        </Col>
                        <Col md={5}>
                          <Button
                            className="buttonAddNic"
                            style={{ marginTop: '22px' }}
                            type="button"
                            onClick={fetchNicsByCPF}
                          >
                            Consultar
                          </Button>
                        </Col>
                        <Col md={12}>
                          <label>NIC:</label>
                          <label htmlFor="select" className="input-select">
                            <select
                              className="input-select"
                              id="contract.nic"
                              value={nicSelecionado}
                              onChange={handleNICChange}
                            >
                              <option value="">Selecione um NIC</option>
                              {nicData.map((item) => (
                                <option key={item.assnic} value={item.assnic}>
                                  {item.assnic}
                                </option>
                              ))}
                            </select>
                          </label>
                        </Col>

                        <Col md={9}>
                          <label>Nome do Indicador:</label>
                          <SimpleInput
                            name="nomeIndicador"
                            placeholder="Nome do Indicador"
                            type="text"
                            value={nomeIndic}
                            onChange={(e) => setNomeIndic(e.target.value)}
                          />
                        </Col>
                      </Row>
                      <Row className="rowContract">
                        <Col md={6}>
                          <label>E-mail do Indicador: (Opcional)</label>
                          <SimpleInput
                            name="emailIndicador"
                            placeholder="E-mail Indicador"
                            type="email"
                            value={emailIndic}
                            onChange={(e) => setEmailIndic(e.target.value)}
                          />
                        </Col>
                        <Col md={6}>
                          <label>Celular do Indicador: (Opcional)</label>
                          <InputMask
                            name="celularIndicador"
                            placeholder="Celular do Indicador (Com o DDD)"
                            mask="(99)99999-9999"
                            type="text"
                            value={celularIndic}
                            onChange={(e) => setCelularIndic(e.target.value)}
                          />
                        </Col>
                      </Row>
                    </div>
                  )}
                </div>
              )}
            </BlockInputs>
            <BlockInputs>
              {dadosEmpresa !== true && (
                <h3>
                  Empresa{' '}
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosEmpresa(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosEmpresa === true && (
                <div className="form">
                  <h3>
                    Empresa{' '}
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosEmpresa(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  <Row className="rowContract">
                    <Col md={4}>
                      <label style={{ fontWeight: '700' }}>
                        Empresa Selecionada:
                      </label>
                      <label htmlFor="select" className="input-select">
                        <select
                          disabled
                          style={{ backgroundColor: '#f1f1f3' }}
                          id="companySelect"
                          value={
                            defaultCompany
                              ? defaultCompany.empDescTela
                              : selectedCompany
                          }
                          onChange={(e) => {
                            const selectedEmpcod = company.find(
                              (empresa) =>
                                empresa.empDescTela === e.target.value
                            )?.empCodigo;
                            setSelectedCompany(e.target.value);
                            setCodCompany(selectedEmpcod);
                          }}
                        >
                          <option value="">Selecione</option>

                          {company
                            .filter((empresa) => {
                              const repreCodigo =
                                localStorage.getItem('repreCodigo');

                              const allowedRepreCodigos = [
                                '20',
                                '13',
                                '28',
                                '12',
                              ];

                              if (
                                allowedRepreCodigos.some((code) =>
                                  repreCodigo.startsWith(code)
                                )
                              ) {
                                return true; // incluir todas as empresas se repreCodigo começar com '12'
                              } else {
                                return empresa.empDescTela === 'Coob+ '; // incluir apenas empresas com o nome 'Coobrastur'
                              }
                              {
                              }
                            })
                            .map((empresa, index) => (
                              <option key={index} value={empresa.empDescTela}>
                                {empresa.empDescTela}
                              </option>
                            ))}
                        </select>
                      </label>
                    </Col>
                  </Row>
                </div>
              )}
            </BlockInputs>

            <BlockInputs>
              {dadosAssociacao !== true && (
                <h3>
                  Opções de Assinaturas
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosAssociacao(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosAssociacao === true && (
                <div className="form">
                  <h3>
                    Opções de Assinaturas
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosAssociacao(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>

                  <Row className="rowContract">
                    <RadioInputs>
                      <div className="inline">
                        <div className="items">
                          <input
                            type="radio"
                            value
                            checked={typeOfContract == '1'}
                            disabled={typeOfContract == '11'}
                            name="associacao"
                            onClick={() => setTypeOfContract('1')}
                          />
                          Normal
                        </div>

                        <div className="items">
                          <input
                            type="radio"
                            value={false}
                            disabled
                            name="associacao"
                            checked={typeOfContract == '11'}
                            onClick={() => {
                              setTypeOfContract('11');
                              // verificaPlanosInativos(cpf);
                            }}
                          />
                          Reativação
                        </div>
                        {/* <div className="items">
                          <input
                            type="radio"
                            value={false}
                            name="associacao"
                            onClick={() => {
                              setTypeOfContract('3');
                              verificaPlanosInativos(cpf);
                            }}
                          />
                          Substituição
                        </div> */}
                      </div>
                    </RadioInputs>
                  </Row>
                  {typeOfContract == '11' && (
                    <Row className="rowContract">
                      <Col md={4}>
                        <label>Plano Resgatado</label>
                        <SimpleInput
                          disabled
                          style={{ backgroundColor: '#DADADA' }}
                          name="planoResgato"
                          placeholder="Plano Resgatado"
                          type="text"
                          value={planoResgatado}
                          onChange={(e) => setPlanoResgatado(e.target.value)}
                        />
                      </Col>
                    </Row>
                  )}

                  {typeOfContract === '3' && (
                    <Row className="rowContract">
                      <Col md={4}>
                        <label>Número Contrato Substituido</label>
                        <SimpleInput
                          disabled
                          name="planoSubstituido"
                          placeholder="Plano Substituido"
                          type="text"
                          value={planoSubstituido}
                          onChange={(e) => setPlanoSubstituido(e.target.value)}
                        />
                      </Col>
                    </Row>
                  )}
                  <Row
                    className="rowContract"
                    style={{ marginBottom: '-20px' }}
                  >
                    <Col md={5}>
                      <label style={{ fontWeight: '700' }}>
                        Código Pipeline
                      </label>
                      <SimpleInput
                        id="contract.pipeline"
                        name="assCod_pipeline"
                        placeholder="Código PipeDrive"
                        type="number"
                        value={nroPipeline}
                        onChange={(e) => setNroPipeLine(e.target.value)}
                      />
                    </Col>
                  </Row>
                  <Row
                    className="rowContract"
                    style={{ marginBottom: '-100px' }}
                  >
                    <Col md={3}>
                      <label
                        style={{ fontWeight: '700', marginBottom: '-53px' }}
                      >
                        Plano{' '}
                        {flat == '36' && (
                          <Tooltip
                            text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: Ano todo"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '37' && (
                          <Tooltip
                            text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: 15/03 à 15/12"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '38' && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: Ano todo"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '39' && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: 15/03 à 15/12"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '40' && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Superior Luxo ou Resorts
                           Validade: Ano todo"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '41' && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: Ano todo"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat == '42' && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: 15/03 à 15/12"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {flat >= 1001 && (
                          <Tooltip
                            text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: 15/03 à 15/12"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                      </label>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="select"
                          value={flat}
                          onChange={(e) => {
                            setFlat(e.target.value);
                            if (e.target.value <= 40) {
                              // setAdhesion(false);
                              setDailyQuantities(false);
                              setFamily('');
                              setValorMensalidade('0,00');
                              setSellerMembershipForm('');
                              setParcelasVendedor('');
                              setOfferApply(false);
                              setOffer(false);
                              setValorAdesao('0,00');
                              setDescontoAdesao('0,00');
                              setFinalAdesao(0);
                            }
                            if (
                              e.target.value <= 40 &&
                              dailyQuantities === '4'
                            ) {
                              setDailyQuantities(false);
                            }
                            if (e.target.value > 40) {
                              // setAdhesion(false);
                              setFamily(false);
                              setValorMensalidade('0,00');
                              setSellerMembershipForm('');
                              setParcelasVendedor('');
                              setOfferApply(false);
                              setValorMensalidade('0,00');
                              setValorAdesao('0,00');
                              setDailyQuantities(false);
                              // setAdhesion(true);
                            }

                            if (e.target.value >= 1001) {
                              // setAdhesion(false);
                              setDailyQuantities(false);
                              setFamily(true);
                              setSellerMembershipForm(0);
                              setValorMensalidade('0,00');
                              setOfferApply(false);
                              setOffer(false);
                              setValorAdesao('0,00');
                              setDescontoAdesao('0,00');
                              setFinalAdesao('0,00');
                            }
                          }}
                        >
                          {/* <option defaultValue>Selecione</option> */}
                          {filteredPlanoArray
                            .filter((option) => {
                              return (
                                (codCompany === 38 && option.value < 999) ||
                                (codCompany === 58 &&
                                  option.value > 1000 &&
                                  option.value < 1999) ||
                                (codCompany === 60 && option.value > 2000)
                              );
                            })
                            .map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                        </select>
                      </label>
                    </Col>
                    <Col md={3}>
                      <label
                        style={{ fontWeight: '700', marginBottom: '-53px' }}
                      >
                        Diárias{' '}
                        {dailyQuantities === '' && (
                          <Tooltip
                            text="selecione um dia"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {dailyQuantities !== '' && (
                          <Tooltip
                            text="Diária escolhida"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                      </label>{' '}
                      <label htmlFor="select" className="input-select">
                        <select
                          id="select"
                          value={dailyQuantities}
                          onChange={(e) => {
                            setDailyQuantities(e.target.value);
                            setOfferApply(false);
                            setOffer(false);
                            setSellerMembershipForm('');
                            setFormaPagamento('');
                            setValorAdesao('0,00');
                            setDescontoAdesao('0,00');
                            setFinalAdesao(0);
                          }}
                        >
                          <option defaultValue>Selecione</option>
                          {flat >= 41
                            ? days
                                .filter((option) =>
                                  flat >= 1001
                                    ? option.label == '7 diárias'
                                    : true
                                )
                                .map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))
                            : days.map((option) => (
                                <option key={option.value} value={option.value}>
                                  {option.label}
                                </option>
                              ))}
                        </select>
                      </label>
                    </Col>
                    {flat < 41 && (
                      <Col md={3}>
                        <RadioInputs>
                          <label
                            className="rowFamily"
                            style={{ fontWeight: '700' }}
                          >
                            Família
                            {family === '' && (
                              <Tooltip
                                iconColor="#909090"
                                text="quantidades de hóspedes"
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                            {family === true && (
                              <Tooltip
                                iconColor="#909090"
                                text="O plano familía pode incluir até 3 hóspedes."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                            {family === false && (
                              <Tooltip
                                iconColor="#909090"
                                text="O plano pode incluir até 2 hóspedes."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                          </label>
                          <div className="inlineFamily">
                            <div className="items">
                              <input
                                type="radio"
                                value="1"
                                name="family"
                                checked={family === true}
                                onClick={() => {
                                  setFamily(true);
                                  // setAdhesion(true);
                                  // valuesAdesao();
                                }}
                              />
                              Sim
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                value="2"
                                name="family"
                                checked={family === false}
                                onClick={() => {
                                  setFamily(false);
                                  // setAdhesion(true);
                                  // valuesAdesao();
                                }}
                              />
                              Não
                            </div>
                          </div>
                        </RadioInputs>
                      </Col>
                    )}
                    {flat >= 41 && (
                      <Col md={3}>
                        <RadioInputs>
                          <label
                            className="rowFamily"
                            style={{ fontWeight: '700' }}
                          >
                            Família
                            {family === '' && (
                              <Tooltip
                                iconColor="#909090"
                                text="quantidades de hóspedes"
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                            {family === true && (
                              <Tooltip
                                iconColor="#909090"
                                text="O plano família pode incluir até 3 hóspedes."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                            {family === false && (
                              <Tooltip
                                iconColor="#909090"
                                text="O plano pode incluir até 2 hóspedes."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            )}
                          </label>
                          <div className="inlineFamily">
                            <div className="items">
                              <input
                                type="radio"
                                value="true"
                                name="family"
                                disabled
                                checked={family === true}
                                onChange={() => {
                                  setFamily(true);
                                  //   // setAdhesion(true);
                                  //   // valuesAdesao();
                                }}
                              />
                              Sim
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                value="false"
                                name="family"
                                disabled
                                checked={family === false}
                                onChange={() => {
                                  setFamily(false);
                                  // setAdhesion(true);
                                  // valuesAdesao();
                                }}
                              />
                              Não
                            </div>
                          </div>
                        </RadioInputs>
                      </Col>
                    )}
                  </Row>

                  <Col style={{ marginTop: '10%' }}>
                    <div className="adesaoValues">
                      <label>Valor da Mensalidade:</label>
                      <span style={{ marginLeft: '5%' }}>
                        R$ {finalValue || '0,00'}
                      </span>
                      {/* <SimpleInput
                              name="valorAdesao"
                              type="number"
                              maxLength="3400"
                              value={valorAdesao}
                              onChange={(e) => {
                                setValorAdesao(e.target.value);
                              }}
                            /> */}
                    </div>
                  </Col>
                  <Col>
                    <div className="adesaoValues">
                      <label>Valor da Adesão:</label>

                      <span style={{ marginLeft: '8.5%' }}>
                        R${' '}
                        {valorAdesao.toLocaleString('pt-br', {
                          minimumFractionDigits: 2,
                        }) || '0,00'}
                      </span>
                    </div>
                    {/* <SimpleInput
                            name="valorAdesao"
                            type="number"
                            maxLength="3400"
                            value={valorAdesao}
                             
                          /> */}
                  </Col>

                  <Row className="rowContract">
                    <div className="rowAdhesion" style={{ paddingTop: '50px' }}>
                      {typeOfContract !== '11' && codCompany !== 60 && (
                        <div>
                          <Col>
                            <div className="checkbox">
                              <input
                                type="checkbox"
                                checked={checkOffer}
                                style={{ marginRight: 10 }}
                                // checked={infoTurma.includes(item.turma)}
                                onChange={(e) => {
                                  if (e.target.checked) {
                                    setCheckOffer(true);
                                    setOffer(true);
                                    setDescontoAdesao('0,00');
                                    setFinalAdesao(0);
                                  } else {
                                    setCheckOffer(false);
                                    setOffer(false);
                                    setOfferApply(false);
                                    setDescontoAdesao('0,00');
                                    setFinalAdesao(0);

                                    // const resultado = planos.map((plano, i) => ({
                                    //   ...plano,
                                    //   valorAdesao: parseFloat(
                                    //     planos[i].valorAdesaoOriginal
                                    //   ),
                                    // }));
                                    // setPlanos(resultado);
                                  }
                                }}
                              />{' '}
                              <label>Oferecer Desconto na Adesão</label>
                            </div>
                          </Col>

                          {offer === false && (
                            <div className="buttonDiscount">
                              <SimpleInput
                                id="nodescontoAdesao"
                                name="assDescontoAdesao"
                                placeholder="Digite o valor do desconto"
                                type="text"
                                disabled
                                style={{
                                  width: '30%',
                                  marginLeft: 10,
                                  cursor: 'not-allowed',
                                }}
                              />
                              <ButtonDisabled
                                // type="push"
                                disabled
                                style={{
                                  // width: '20%',
                                  marginLeft: '5px',
                                  fontFamily: 'Tahoma',

                                  textTransform: 'uppercase',
                                  padding: '10px',
                                  cursor: 'not-allowed',
                                }}
                              >
                                {' '}
                                Aplicar Desconto
                              </ButtonDisabled>
                            </div>
                          )}
                          {offer !== false && (
                            <div className="buttonDiscount">
                              <InputCurrency
                                id="descontoAdesao"
                                // name="assDescontoAdesao"
                                placeholder="Digite o valor do desconto"
                                type="text"
                                value={descontoAdesao}
                                onChange={(e) => {
                                  parseFloat(
                                    setDescontoAdesao(
                                      e.target.value.substring(2)
                                    )
                                  );

                                  setOfferApply(false);
                                  // const resultado = planos.map((plano, i) => ({
                                  //   ...plano,
                                  //   valorAdesao: parseFloat(
                                  //     planos[i].valorAdesaoOriginal
                                  //   ),
                                  // }));
                                  // setPlanos(resultado);
                                }}
                              />
                              <ButtonDisabled
                                // type="push"
                                style={{
                                  // width: '40%',
                                  marginLeft: '5px',
                                  fontFamily: 'Tahoma',
                                  // fontSize: '18px',
                                  textTransform: 'uppercase',
                                  padding: '10px',
                                  // marginBottom: '15px',
                                  backgroundColor: '#01affd',
                                  // cursor: 'pointer',
                                }}
                                onClick={() => {
                                  // debugger;
                                  if (descontoAdesao > valorAdesao) {
                                    showToast({
                                      type: 'error',
                                      message:
                                        'Valor do Desconto não pode ser maior que adesão.',
                                    });

                                    setOfferApply(false);
                                  } else {
                                    setOfferApply(true);
                                  }

                                  if (descontoAdesao == '0,00') {
                                    setOfferApply(false);
                                  }
                                  if (descontoAdesao == valorAdesao) {
                                    setCheckOffer(false);
                                    setOffer(false);
                                    // setOfferApply(false);
                                    setAdFormaPagamento(false);
                                    setSellerMembershipForm('');
                                    setAdhesion(false);
                                    // console.log(finalAdesao);
                                  } else {
                                    setAdFormaPagamento(true);
                                    setAdhesion(true);
                                  }
                                  if (
                                    valorAdesao !== '' &&
                                    descontoAdesao !== ''
                                  ) {
                                    const resultado =
                                      valorAdesao -
                                      descontoAdesao.replace('.', '');
                                    // console.log('valorAdesao', valorAdesao);
                                    // console.log('descontoadesao', descontoAdesao);

                                    setFinalAdesao(resultado);
                                    // console.log(finalAdesao);
                                  }
                                  // console.log(finalAdesao);
                                  // adesaoComDesconto();
                                }}
                              >
                                Aplicar Desconto
                              </ButtonDisabled>
                            </div>
                          )}
                          {adhesion === true && offerApply === true && (
                            <div
                              className="paymentAdesao"
                              style={{
                                marginLeft: 10,
                                marginTop: '-20px',
                              }}
                            >
                              <Row
                                className="rowAdesao"
                                style={{ marginTop: '0px' }}
                              >
                                <Col className="adesaoValues">
                                  <label>Subtotal da Adesão</label>
                                  <span style={{ marginLeft: 80 }}>
                                    R${' '}
                                    {valorAdesao.toLocaleString('pt-br', {
                                      minimumFractionDigits: 2,
                                    })}
                                  </span>
                                </Col>
                              </Row>

                              <Row className="rowAdesao">
                                <Col className="adesaoValues">
                                  <label style={{ marginRight: 67 }}>
                                    Descontos da Adesão
                                  </label>
                                  <span>
                                    R${' '}
                                    {descontoAdesaoFinal.toLocaleString(
                                      'pt-br',
                                      {
                                        minimumFractionDigits: 2,
                                      }
                                    )}
                                  </span>
                                </Col>
                              </Row>

                              <Row className="rowAdesao">
                                <Col className="adesaoValues">
                                  <label>Total da Adesão</label>
                                  <span
                                    id="adesaoTotal"
                                    style={{ marginLeft: 107 }}
                                  >
                                    R${' '}
                                    {finalAdesao.toLocaleString('pt-br', {
                                      minimumFractionDigits: 2,
                                    })}
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          )}
                          {adhesion === false && offerApply === true && (
                            <div
                              className="paymentAdesao"
                              style={{
                                marginLeft: 10,
                                marginTop: '-20px',
                                paddingBottom: '50px',
                              }}
                            >
                              <Row
                                className="rowAdesao"
                                style={{ marginTop: '10px' }}
                              >
                                <Col className="adesaoValues">
                                  <label>Subtotal da Adesão</label>
                                  <span style={{ marginLeft: 80 }}>
                                    R${' '}
                                    {valorAdesao.toLocaleString('pt-br', {
                                      minimumFractionDigits: 2,
                                    })}
                                  </span>
                                </Col>
                              </Row>

                              <Row className="rowAdesao">
                                <Col className="adesaoValues">
                                  <label style={{ marginRight: 67 }}>
                                    Descontos da Adesão
                                  </label>
                                  <span>
                                    R${' '}
                                    {descontoAdesaoFinal.toLocaleString(
                                      'pt-br',
                                      {
                                        minimumFractionDigits: 2,
                                      }
                                    )}
                                  </span>
                                </Col>
                              </Row>

                              <Row className="rowAdesao">
                                <Col className="adesaoValues">
                                  <label>Total da Adesão</label>
                                  <span
                                    id="adesaoTotal"
                                    style={{ marginLeft: 107 }}
                                  >
                                    R${' '}
                                    {finalAdesao.toLocaleString('pt-br', {
                                      minimumFractionDigits: 2,
                                    })}
                                  </span>
                                </Col>
                              </Row>
                            </div>
                          )}
                        </div>
                      )}
                    </div>
                  </Row>

                  {valoresAdesao == 'true' && (
                    <div>
                      {/* <Row className="rowContract">
                        <Col md={3}>
                          <div className="inline">
                            <label>
                              Adesão
                              <Tooltip
                                text="Selecione a opção de quem irá receber a adesão."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            </label>
                          </div>
                        </Col>
                      </Row>
                      <Row className="rowVenda">
                        <Col md={4}>
                          <div className="inline">
                            <div className="items">
                              <input
                                type="radio"
                                value="1"
                                name="payment"
                                onClick={(e) => setPayment(e.target.value)}
                              />
                              Vendedor
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                value="2"
                                name="payment"
                                onClick={(e) => setPayment(e.target.value)}
                              />
                              Coob+
                            </div>
                          </div>
                        </Col>
                      </Row> */}
                    </div>
                  )}
                </div>
              )}
            </BlockInputs>
            <BlockInputs>
              <h3>Parceiros/Convênios</h3>
              <Row
                style={{
                  marginBottom: '-55px',
                  marginTop: '50px',
                  // marginLeft: 5,
                }}
              >
                <Col>
                  <label style={{ fontWeight: 700 }}>Parceiros</label>
                  <label htmlFor="select" className="input-select">
                    <select
                      id="select"
                      value={vendNomeSelecionado}
                      onChange={handleVendNomeChange}
                    >
                      <option defaultValue>Selecione</option>
                      {vendNomes.map((vendNome) => (
                        <option key={vendNome} value={vendNome}>
                          {vendNome}
                        </option>
                      ))}
                    </select>
                  </label>
                </Col>
              </Row>
              <Row
                style={{
                  marginBottom: '-25px',
                  marginTop: '30px',
                  // marginLeft: 5,
                }}
              >
                <Col>
                  <label style={{ fontWeight: 700 }}>Cupom de Desconto</label>
                  <label htmlFor="select" className="input-select">
                    <select
                      id="select"
                      value={cupNomeSelecionado}
                      onChange={handleCupNomeChange}
                    >
                      <option value="">Selecione</option>
                      {cupNomes.map((cupNome) => (
                        <option key={cupNome} value={cupNome}>
                          {cupNome}
                        </option>
                      ))}
                    </select>
                  </label>
                </Col>
              </Row>

              <Row style={{ alignSelf: 'center' }}>
                <Col md={3}>
                  <button
                    className={`buttonParceiro ${
                      cupNomeSelecionado == 'Selecione' ||
                      cupCodigoSelecionado == ''
                        ? 'buttonParceiro-disabled'
                        : ''
                    }`}
                    type="button"
                    onClick={() => {
                      updateItem();
                    }}
                    // disabled={vendNomeSelecionado == 'Selecione'}
                  >
                    Aplicar Cupom
                  </button>
                </Col>
              </Row>
            </BlockInputs>
            {valoresAdesao !== false && (
              <BlockInputs>
                {dadosAdesao !== true && (
                  <h3>
                    Forma de Pagamento da Adesão
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosAdesao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosAdesao === true && (
                  <div className="form">
                    <h3>
                      Forma de Pagamento da Adesão
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosAdesao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>

                    <Row className="rowContract">
                      {adFormaPagamento === true && (
                        <div style={{ marginLeft: 10, marginTop: '50px' }}>
                          <Row
                            style={{
                              marginBottom: '-10px',
                              marginTop: '-5px',
                              // marginLeft: 5,
                            }}
                          >
                            <Col>
                              <label style={{ fontWeight: 700 }}>
                                Forma de Pagamento da Adesão
                              </label>

                              {/* {localStorage
                                .getItem('repreCodigo')
                                .startsWith(13) && (
                                <label
                                  htmlFor="select"
                                  className="input-select"
                                >
                                  <select
                                    id="select"
                                    value={sellerMembershipForm}
                                    onChange={(e) => {
                                      setSellerMembershipForm(e.target.value);
                                      setParcelasVendedor('1');
                                      // console.log(sellerMembershipForm);
                                    }}
                                  >
                                    {formaPagamentoAdesaoVendedorArray
                                      .filter((option) =>
                                        restricao && fiador
                                          ? option.label !== 'Boleto'
                                          : true
                                      )
                                      .map((option) => (
                                        <option
                                          key={option.value}
                                          value={option.value}
                                        >
                                          {option.label}
                                        </option>
                                      ))}
                                  </select>
                                </label>
                              )} */}
                              <label htmlFor="select" className="input-select">
                                <select
                                  id="select"
                                  value={sellerMembershipForm}
                                  onChange={(e) => {
                                    setSellerMembershipForm(e.target.value);
                                    setFormaPagamento('');
                                    // console.log(sellerMembershipForm);
                                  }}
                                >
                                  <option defaultValue>Selecione</option>
                                  {formaPagamentoAdesaoVendedorArray
                                    .filter((option) =>
                                      localStorage
                                        .getItem('repreCodigo')
                                        .startsWith(22) ||
                                      localStorage
                                        .getItem('repreCodigo')
                                        .startsWith(13) ||
                                      localStorage
                                        .getItem('repreCodigo')
                                        .startsWith(28)
                                        ? option.label == 'Boleto'
                                        : true
                                    )
                                    .map((option) => (
                                      <option
                                        key={option.value}
                                        value={option.value}
                                      >
                                        {option.label}
                                      </option>
                                    ))}
                                </select>
                              </label>
                            </Col>
                          </Row>
                          <div style={{ marginBottom: '-25px' }}>
                            <Row>
                              <Col>
                                <label style={{ fontWeight: 700 }}>
                                  Parcelas da Adesão
                                </label>
                                <label
                                  htmlFor="select"
                                  className="input-select"
                                >
                                  <select
                                    id="select"
                                    value={parcelasVendedor}
                                    onChange={(e) => {
                                      setParcelasVendedor(e.target.value);
                                    }}
                                  >
                                    <option defaultValue>Selecione</option>

                                    {parcelasPagamentoAdesaoVendedorArray
                                      .filter((option) =>
                                        localStorage
                                          .getItem('repreCodigo')
                                          .startsWith(22) ||
                                        localStorage
                                          .getItem('repreCodigo')
                                          .startsWith(13) ||
                                        // localStorage
                                        //   .getItem('repreCodigo')
                                        //   .startsWith(35) ||
                                        localStorage
                                          .getItem('repreCodigo')
                                          .startsWith(28)
                                          ? option.label == 'Boleto'
                                          : true
                                      )
                                      .map((option) => (
                                        <option
                                          key={option.value}
                                          value={option.value}
                                        >
                                          {option.label}
                                        </option>
                                      ))}
                                  </select>
                                </label>
                              </Col>
                            </Row>
                          </div>

                          {parcelasVendedor == '1' && (
                            <Row>
                              <Col>
                                <div>
                                  <label
                                    style={{
                                      fontSize: '16px',
                                      fontWeight: 700,
                                    }}
                                  >
                                    {' '}
                                    Data da Primeira Adesão{' '}
                                  </label>
                                  <DatePickers>
                                    <DatePicker
                                      className="daterange"
                                      selected={dataEscolhida}
                                      onChange={(date) => {
                                        setDataEscolhida(date);
                                        setFormaPagamento('');
                                        // setIsPayable('');
                                        setCarMes('');
                                        setCarDia('');
                                        setDebDia('');
                                        setDebMes('');
                                      }}
                                      includeDateIntervals={[
                                        {
                                          start: subDays(new Date(), 0),
                                          end: addDays(new Date(), 60),
                                        },
                                      ]}
                                      placeholderText="dd-mm-yyyy"
                                      locale={pt}
                                      dateFormat="P"
                                    />
                                  </DatePickers>
                                </div>
                              </Col>
                            </Row>
                          )}
                          {parcelasVendedor == '2' && (
                            <Row>
                              <Col>
                                <div>
                                  <label
                                    style={{
                                      fontSize: '16px',
                                      fontWeight: 700,
                                    }}
                                  >
                                    {' '}
                                    Data da Primeira Adesão{' '}
                                  </label>
                                  <DatePickers>
                                    <DatePicker
                                      className="daterange"
                                      selected={dataEscolhida}
                                      onChange={(date) => {
                                        setDataEscolhida(date);
                                        setFormaPagamento('');
                                        // setIsPayable('');
                                        setCarMes('');
                                        setCarDia('');
                                        setDebDia('');
                                        setDebMes('');
                                      }}
                                      includeDateIntervals={[
                                        {
                                          start: subDays(new Date(), 0),
                                          end: addDays(new Date(), 41),
                                        },
                                      ]}
                                      placeholderText="dd-mm-yyyy"
                                      locale={pt}
                                      dateFormat="P"
                                    />
                                  </DatePickers>
                                </div>
                              </Col>
                            </Row>
                          )}
                        </div>
                      )}
                      {payment === '2' && (
                        <>
                          <Col md={3}>
                            <label>Forma de pagamento:</label>
                            <label htmlFor="select" className="input-select">
                              <select
                                id="select"
                                value={halfAdhesion}
                                onChange={(e) => {
                                  setHalfAdhesion(e.target.value);
                                  setDataEscolhida('');
                                }}
                              >
                                {formaPagamentoAdesaoCoobrasturArray.map(
                                  (option) => (
                                    <option
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </option>
                                  )
                                )}
                              </select>
                            </label>
                          </Col>
                          <Col md={3}>
                            <label>Parcelas:</label>
                            <label htmlFor="select" className="input-select">
                              <select
                                id="select"
                                value={parcelasCoobrastur}
                                onChange={(e) =>
                                  setParcelasCoobrastur(e.target.value)
                                }
                              >
                                {parcelasPagamentoAdesaoArray.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                              </select>
                            </label>
                          </Col>
                          <Col md={3}>
                            <label>Data da cobrança:</label>
                            <InputMask
                              name="dataCoobrastur"
                              placeholder="Data de Cobrança"
                              mask="99/99/9999"
                              type="text"
                              value={dataAdesao}
                              onChange={(e) => setDataAdesao(e.target.value)}
                            />
                          </Col>
                          <Col md={12}>
                            <label>Observação:</label>
                            <SimpleInput
                              name="obsCoobrastur"
                              placeholder="Observação(opcional)"
                              type="text"
                            />
                          </Col>
                        </>
                      )}
                    </Row>

                    {/* {adhesion === false && offerApply === true && (
                      <div
                        className="paymentAdesao"
                        style={{
                          marginLeft: 10,
                          marginTop: '30px',
                        }}
                      >
                        <Row
                          className="rowAdesao"
                          style={{ marginTop: '10px' }}
                        >
                          <Col className="adesaoValues">
                            <label>Subtotal da Adesão</label>
                            <span style={{ marginLeft: 80 }}>
                              R${' '}
                              {valorAdesao.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row>

                        <Row className="rowAdesao">
                          <Col className="adesaoValues">
                            <label style={{ marginRight: 67 }}>
                              Descontos da Adesão
                            </label>
                            <span>
                              R${' '}
                              {descontoAdesaoFinal.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row>

                        <Row className="rowAdesao">
                          <Col className="adesaoValues">
                            <label>Total da Adesão</label>
                            <span id="adesaoTotal" style={{ marginLeft: 107 }}>
                              R${' '}
                              {finalAdesao.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row>
                      </div>
                    )} */}
                  </div>
                )}
              </BlockInputs>
            )}
            {/* <BlockInputs>
              <h3>Campanhas</h3>
              {campaign === true && (
                <Row className="rowContract">
                  <Col md={4}>
                    <label
                      style={{
                        fontWeight: '700',
                        marginLeft: '3px',
                        marginBottom: '3px',
                      }}
                    />

                    <label htmlFor="select" className="input-select-campaign">
                      <select
                        id="select"
                        disabled
                        value={campaingValue}
                        // onChange={(e) => setCampaignValue(e.target.value)}
                      >
                        {' '}
                        {campaignArray.map((option) => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </select>
                    </label>
                  </Col>
                </Row>
              )}
              {campaign !== true && (
                <Row className="rowContract">
                  <Col md={4}>
                    <h6 className="noCampaign">
                      O usuário não se enquadra em nenhuma das campanhas
                      vigentes.
                    </h6>
                  </Col>
                </Row>
              )}
            </BlockInputs> */}
            <BlockInputs>
              {dadosBoleto !== true && (
                <h3>
                  Forma de Pagamento da Mensalidade
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosBoleto(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}

              {dadosBoleto === true && (
                <div className="form">
                  <h3>
                    Forma de Pagamento da Mensalidade
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosBoleto(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>

                  <Row className="rowContract">
                    <Col md={4}>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="select"
                          value={formaPagamento}
                          onClick={() => buscaDataPagamento()}
                          onChange={(e) => {
                            setDataBoletoEscolhida('');
                            setFormaPagamento(e.target.value);
                            setIsPayable('');
                            setCarMes('');
                            setCarDia('');
                            setDebDia('');
                            setDebMes('');
                            // console.log(formaPagamento);
                          }}
                        >
                          {bolformaPagamento
                            // .filter((option) =>
                            //   restricao && fiador
                            //     ? option.label !== 'Boleto'
                            //     : true
                            // )
                            .filter(
                              (option) =>
                                !(codCompany == 58 && option.value === 3)
                            )
                            .map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                        </select>
                      </label>
                    </Col>
                  </Row>

                  {formaPagamento == 1 && (
                    <div className="carStyle">
                      <div style={{ marginRight: '20px' }}>
                        <label style={{ fontWeight: '700' }}>
                          Mês da Cobrança
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            value={debMes}
                            onChange={(e) => {
                              setDebMes(e.target.value);
                              onSelectMonth(e.target.value);
                              setDebDia('');
                              setIsPayable('');
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            {nextThreeMonths(currentMonth).map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                          </select>{' '}
                        </label>
                      </div>
                      {debMes && (
                        <div>
                          <label style={{ fontWeight: '700' }}>
                            Dia da Cobrança
                          </label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="select"
                              value={debDia}
                              onChange={(e) => {
                                setDebDia(e.target.value);
                                validaParcelas(e.target.value);
                                setBoletoMonth(e.target.value);
                                // console.log(e.target.value);
                              }}
                            >
                              <option defaultValue>Selecione</option>
                              {filterDays
                                .filter((option) =>
                                  formaPagamento == 1
                                    ? option == 5 ||
                                      option == 10 ||
                                      option == 15 ||
                                      option == 20 ||
                                      option == 25 ||
                                      option == 30
                                    : true
                                )
                                .map((option) => (
                                  <option
                                    // disabled={validDay(option)}
                                    key={option}
                                    value={option}
                                  >
                                    {option}
                                  </option>
                                ))}
                            </select>
                          </label>
                        </div>
                      )}
                    </div>
                  )}
                  {formaPagamento == '3' && (
                    <div className="carStyle">
                      <div style={{ marginRight: '20px' }}>
                        <label style={{ fontWeight: '700' }}>
                          Mês da Cobrança
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            value={carMes}
                            onChange={(e) => {
                              setCarMes(e.target.value);
                              onSelectMonth(e.target.value);
                              setCarDia('');
                              setIsPayable('');
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            {nextThreeMonths(currentMonth).map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                          </select>{' '}
                        </label>
                      </div>
                      {carMes && (
                        <div>
                          <label style={{ fontWeight: '700' }}>
                            Dia da Cobrança
                          </label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="select"
                              value={carDia}
                              onChange={(e) => {
                                setCarDia(e.target.value);
                                validaParcelas(e.target.value);
                                // console.log(e.target.value);
                              }}
                            >
                              <option defaultValue>Selecione</option>
                              {filterDays.map((option) => (
                                <option
                                  // disabled={validDay(option)}
                                  key={option}
                                  value={option}
                                >
                                  {option}
                                </option>
                              ))}
                            </select>
                          </label>
                        </div>
                      )}
                    </div>
                  )}

                  {formaPagamento == 4 && (
                    <div>
                      <label>Data de Cobrança Escolhida</label>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="select"
                          value={dataBoletoEscolhida}
                          onChange={(e) => {
                            setDataBoletoEscolhida(e.target.value);
                            validaParcelas(e.target.value);
                            setBoletoMonth(e.target.value);
                          }}
                        >
                          {datasPagamento.map((data) => (
                            <option
                              key={data.dataExibicao}
                              value={data.dataReal}
                            >
                              {data.dataExibicao}
                            </option>
                          ))}
                        </select>
                      </label>
                    </div>
                  )}
                  {sellerMembershipForm != 0 &&
                    dataEscolhida.getMonth() + 1 ==
                      (carMes || debMes || monthBoleto) && (
                      <span style={{ color: '#01affd' }}>
                        {' '}
                        <strong>
                          {' '}
                          Caso você tenha escolhido o mesmo mês de adesão como o
                          mês de cobrança da mensalidade, por favor, ignore esta
                          mensagem.{' '}
                        </strong>
                      </span>
                    )}
                  {/* {isPayable == '0' &&
                    (localStorage.getItem('repreCodigo').startsWith(13) ||
                      localStorage.getItem('repreCodigo').startsWith(22)) && (
                      <label
                        style={{
                          color: 'red',
                          fontSize: '20px',
                          fontWeight: 700,
                        }}
                      >
                        {' '}
                        A Adesão ficará para o dia {primeiraAdesao}{' '}
                      </label>
                    )} */}
                  {typeOfContract !== '11' &&
                    (dataBoletoEscolhida || carDia || debDia) &&
                    finalAdesao !== 0 && (
                      <div style={{ marginTop: '-20px', display: 'none' }}>
                        <label
                          style={{
                            fontSize: '16px',
                            fontWeight: 700,
                          }}
                        >
                          {' '}
                          Data da Primeira Adesão{' '}
                        </label>
                        <SimpleInput
                          name="valorAdesao"
                          type="number"
                          maxLength="3400"
                          disabled
                          placeholder={primeiraAdesao}
                          style={{ width: '30%' }}
                        />
                      </div>
                    )}
                  {isPayable == '0' && finalAdesao !== 0 && (
                    <div style={{ marginTop: '-20px' }}>
                      {/* {localStorage.getItem('repreCodigo').startsWith(13) ||
                        (localStorage.getItem('repreCodigo').startsWith(22) && ( */}
                      {(localStorage.getItem('repreCodigo').startsWith(13) ||
                        localStorage.getItem('repreCodigo').startsWith(22) ||
                        localStorage.getItem('repreCodigo').startsWith(35) ||
                        localStorage.getItem('repreCodigo').startsWith(28)) && (
                        <p style={{ color: 'red', fontWeight: 700 }}>
                          Data de cobrança da adesão inválida!
                        </p>
                      )}
                      {/* ))} */}
                      {/* <p style={{ color: 'red', fontWeight: 700 }}>
                        Verifique os parâmetros de Pagamento da mensalidade.
                      </p> */}
                    </div>
                  )}
                  {/*  retornar data primeira adesão

                  "A adesão ficará para o dia ....."

                  if ( isPayable == 0 && repreCod 13 || 22) {
                    showToast error
                    data da primeira cobrança da primeira mensalidade não pode ser inferior
                     a última adesão
                  }


                  resultado desconto == valor adesão { adesão == false}; 
                   */}
                </div>
              )}
            </BlockInputs>

            {desabilitado === false && <div />}
            <Button
              type="submit"
              onClick={() => {
                // updateItem();
                if (prepago === true) {
                  setTypeOfContract(2);
                }
              }}
            >
              Concluir
            </Button>
            {isModalVisible ? (
              <Modal
                onClose={() => {
                  setIsModalVisible(false);
                  setIsSendModalClick(false);
                  // setDailyQuantities('');
                  allowScroll();
                }}
                onSend={() => {
                  if (verificaStatus(id, status) !== true) {
                    setIsSendModalClick(true);
                    allowScroll();
                  }
                }}
                dados={dadosCliente}
                associacao={dAssociacao}
                dFiador={dadosFia}
              />
            ) : null}
          </FormContent>
        </Content>
      </div>
    </Container>
  );
}
