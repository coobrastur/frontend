// Dependencies
import React, { useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import * as userActions from '../../store/ducks/auth';
import { apiClei } from '../../services/api';

// Components
import SimpleInput from '../../components/SimpleInput';
import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';

// Images
import logo from '../../assets/new-icon.png';
import bgLogin from '../../assets/bg-login.svg';

// styles
import { Container, Left, Right, FormContent, Button } from './styles';

function Login() {
  const formRef = useRef();
  const history = useHistory();
  const dispatch = useDispatch();

  const [loader, setLoader] = useState(false);

  async function handleFormSubmit() {
    setLoader(true);

    const username = formRef.current.getFieldValue('username');
    const password = formRef.current.getFieldValue('password');

    if (!username) {
      showToast({
        id: 'toast-login.error',
        type: 'error',
        message: 'Preencha o campo de usuário',
      });
    }

    if (!password) {
      showToast({
        id: 'toast-login.error',
        type: 'error',
        message: 'Preencha o campo de senha',
      });
    }

    try {
      const dados = {
        username,
        password,
        ipAcesso: '10.1.2.174',
      };

      apiClei
        .get('Usuario.asmx/Login', {
          params: {
            dados,
          },
        })
        .then((response) => {
          const {
            token,
            codigoVendas,
            usuCPF_CNPJ,
            usuCodigo,
            usuEmail,
            usuNome,
            usuUsuario,
            repreCodigo,
            venda_com_adesao,
          } = response.data.Table[0];

          dispatch(
            userActions.login(
              token,
              codigoVendas,
              usuCPF_CNPJ === null ? '---' : usuCPF_CNPJ,
              usuCodigo,
              usuEmail,
              usuNome,
              usuUsuario,
              repreCodigo,
              venda_com_adesao
            )
          );

          showToast({
            id: 'toast-login.sucess',
            type: 'success',
            message: 'Usuário logado com sucesso',
          });

          localStorage.setItem(
            'codigoVendas',
            response.data.Table[0].codigoVendas
          );
          localStorage.setItem(
            'podeVenderCOOB',
            response.data.Table[0].podeVenderCOOB
          );
          localStorage.setItem(
            'podeVenderBNS',
            response.data.Table[0].podeVenderBNS
          );
          localStorage.setItem(
            'podeVenderVFB',
            response.data.Table[0].podeVenderVFB
          );
          localStorage.setItem(
            'podeVenderMRD',
            response.data.Table[0].podeVenderMRD
          );
          localStorage.setItem(
            'podeVenderConvenios',
            response.data.Table[0].podeVenderConvenios
          );
          localStorage.setItem(
            'dealRequerido',
            response.data.Table[0].dealRequerido
          );
          localStorage.setItem(
            'repreCodigo',
            response.data.Table[0].repreCodigo
          );
          localStorage.setItem('usuEmail', response.data.Table[0].usuEmail);
          localStorage.setItem('usuNome', response.data.Table[0].usuNome);
          localStorage.setItem('usuCodigo', response.data.Table[0].usuCodigo);
          localStorage.setItem('usuUsuario', response.data.Table[0].usuUsuario);
          history.push('contracts');

          setLoader(false);
        })
        .catch(() => {
          showToast({
            id: 'toast-login.error2',
            type: 'error',
            message: 'Algo deu errado. Tente novamente mais tarde',
          });

          setLoader(false);
        });
    } catch (err) {
      showToast({
        id: 'toast-login.error2',
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });

      setLoader(false);
    }
  }

  return (
    <Container>
      <Loader loader={loader} />

      <Left>
        <img src={logo} alt="" />

        <FormContent ref={formRef} onSubmit={() => handleFormSubmit()}>
          <h2>Bem-Vindo(a)</h2>
          <SimpleInput
            name="username"
            placeholder="Usuário"
            type="text"
            id="username"
          />
          <SimpleInput
            name="password"
            placeholder="Senha"
            type="password"
            id="password"
          />
          <Button type="submit" id="btn-entrar">
            Entrar
          </Button>
        </FormContent>
      </Left>

      <Right>
        <div className="container">
          <h1>Venda mais fácil</h1>
          <h2>
            Traga <span>muitos</span> assinantes em <span>poucos</span> passos.
          </h2>

          <img src={bgLogin} alt="" />
        </div>
      </Right>
    </Container>
  );
}

export default Login;
