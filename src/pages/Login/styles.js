import styled from 'styled-components';
import { Form } from '@unform/web';

export const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr 2fr;

  height: 100vh;

  @media (max-width: 700px) {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;

export const Left = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  position: relative;

  img {
    position: absolute;
    top: 20px;
    left: 20px;
  }

  h2 {
    font-size: 1.8em;
    font-family: 'Livvic', sans-serif;
    text-align: center;

    margin-bottom: 20px;
  }

  @media (max-width: 700px) {
    height: 100vh;
    width: 100%;
  }
`;

export const Right = styled.div`
  background-image: linear-gradient(
    to bottom,
    #01affd,
    #00a4fb,
    #0098f7,
    #188cf3,
    #2f80ed
  );

  display: flex;
  justify-content: center;
  flex-direction: column;

  h1 {
    color: white;
    font-size: 4em;
    text-align: right;
    text-transform: uppercase;
    font-family: 'Livvic', sans-serif;
  }

  h2 {
    color: white;
    text-align: right;
    font-size: 1.8em;
    font-weight: 300;
    font-style: italic;
  }

  span {
    font-family: 'Livvic', sans-serif;
    font-style: normal;
  }

  img {
    width: 50%;
    float: right;
  }

  @media (max-width: 700px) {
    display: none;
  }
`;

export const FormContent = styled(Form)`
  display: flex;
  flex-direction: column;
  width: 80%;
  margin: 0 auto;

  @media (max-width: 700px) {
    width: 300px;
  }
`;

export const Button = styled.button`
  background-color: #01affd;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1.4em;

  margin: 0px auto;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;

  width: 100%;

  :hover {
    background-color: #0c92ce;
  }
`;
