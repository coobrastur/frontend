// Dependencies
import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';

// components
import MenuHamburguer from '../../components/MenuHamburguer';

// Images
import imageBackArrow from '../../assets/arrow-left.png';

// Styles
import { Container, Content, BackArrow } from './styles';

function MyAccount() {
  const history = useHistory();
  const {
    codigoVendas,
    usuCPF_CNPJ,
    usuCodigo,
    usuEmail,
    usuNome,
    // usuUsuario,
    repreCodigo,
    // venda_com_adesao,
  } = useSelector((state) => state.auth);

  console.log(useSelector((state) => state.auth));

  return (
    <Container>
      <MenuHamburguer />

      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>

      <div className="container">
        <h2>Minha Conta</h2>
        <Content>
          <div className="items">
            <p>
              <span>Código Usuário: </span>
              {usuCodigo}
            </p>

            <p>
              <span>Nome: </span>
              {usuNome}
            </p>

            <p>
              <span>CPF: </span>
              {usuCPF_CNPJ}
              {/* {repreCodigo} */}
            </p>
            <p>
              <span>E-mail: </span>
              {usuEmail}
            </p>

            <p>
              <span>Código de Vendas: </span>
              {codigoVendas}
            </p>

            <p>
              <span>Código de Representante: </span>
              {repreCodigo}
            </p>

            {/* <p>
              <span>Vendas com adesão: </span>
              {venda_com_adesao}
            </p> */}
          </div>
        </Content>
      </div>
    </Container>
  );
}

export default MyAccount;
