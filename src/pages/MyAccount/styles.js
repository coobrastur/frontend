import styled from 'styled-components';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  min-height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }

  @media (max-width: 1100px) {
    padding: 50px 0;

    h2 {
      text-align: center;
    }
  }
`;

export const Content = styled.div`
  background-color: #fff;
  border-radius: 10px;

  padding: 50px;
  margin: 0 auto;

  .items {
    display: grid;
    grid-gap: 30px;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: repeat(2, 1fr);
  }

  .items p {
    font-size: 1.4em;
    color: rgba(0, 0, 0, 0.4);
    display: flex;
    flex-direction: column;
  }

  .items p span {
    color: #01affd;
    font-family: 'Livvic', sans-serif;
  }

  @media (max-width: 1250px) {
    padding: 30px;
  }

  @media (max-width: 1100px) {
    padding: 5px 5px 100px 5px;
    width: 100%;

    .items {
      grid-template-columns: 1fr;
      grid-template-rows: repeat(7, 1fr);
    }

    .items p {
      font-size: 1em;
      text-align: center;
    }
  }
`;

export const BackArrow = styled.div`
  position: absolute;
  top: 20px;
  left: 20px;
  transition: all.2s;

  img:hover {
    transform: scale(1.2);
  }
`;
