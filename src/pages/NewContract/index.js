// Dependencies
import React, { useRef, useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import DatePicker from 'react-datepicker';
import { Row, Col } from 'react-grid-system';
import { addDays, subDays } from 'date-fns';
import { pt } from 'date-fns/esm/locale';
// import { NumericFormat } from 'react-number-format';
// import Swal from 'sweetalert2';
import { apiClei, apiViacep } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';
import Table from '../../components/Table';
import Tooltip from '../../components/Tooltip';
// import TooltipPlus from '../../components/TooltipPlus';
import SimpleInput from '../../components/SimpleInput';
import InputMask from '../../components/InputMask';
import InputCurrency from '../../components/InputCurrency';

import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';
import Modal from '../../components/ModalNewContract';
import ModalGlobal from '../../components/ModalGlobal';
import ModalReativacao from '../../components/ModalReativacao';

// Images
import imageBackArrow from '../../assets/arrow-left.png';
import arrowUP from '../../assets/arrowUP.svg';
import arrowDown from '../../assets/arrowDown.svg';

// Styles
import 'react-datepicker/dist/react-datepicker.css';

import {
  Container,
  Content,
  BackArrow,
  FormContent,
  DatePickers,
  BlockInputs,
  RadioInputs,
  Button,
  ButtonDivisor,
  ButtonDisabled,
} from './styles';
import useScrollBlock from '../../utils/useScrollBlock';
import Swal from 'sweetalert2';

export default function NewContract() {
  const history = useHistory();
  const [loader, setLoader] = useState();
  const formRef = useRef();
  const { token } = useSelector((state) => state.auth);

  const dealRequerido = localStorage.getItem('dealRequerido');

  const [valorMensalidade, setValorMensalidade] = useState('0,00');
  const [valorMensalidadeOriginal, setValorMensalidadeOriginal] =
    useState('0,00');
  // const finalValueA = parseFloat(
  //   valorAdesao.replace(',', '.')
  // ).toLocaleString('pt-br', { minimumFractionDigits: 2 });
  const finalValue = parseFloat(
    valorMensalidade.replace(',', '.')
  ).toLocaleString('pt-br', { minimumFractionDigits: 2 });

  // modal inadimplência
  const [modalGlobal, setModalGlobal] = useState(false);

  // desabilita botões de error
  const [modalReativacao, setModalReativacao] = useState(false);
  const [desabilitado, setDesabilitado] = useState(false);
  const [send, setSend] = useState('');

  //data nascimento
  const [birthDate, setBirthDate] = useState('');
  const handleDateChange = (event) => {
    setBirthDate(event.target.value);
  };

  const [indicador, setIndicador] = useState('');
  const [restricao, setRestricao] = useState();
  const [valorRestricao, setValorRestricao] = useState();
  const [restricaoFia, setRestricaoFia] = useState();
  const [fiador, setFiador] = useState();
  const [ufAssociado, setUfAssociado] = useState('');
  const [cidade, setCidade] = useState('');
  const [bairroAssociado, setBairroAssociado] = useState('');
  const [rua, setRua] = useState('');
  const [numeroAssociado, setNumeroAssociado] = useState('');
  const [complemento, setComplemento] = useState('');
  const [enderecoLiberado, setEnderecoLiberado] = useState(false);
  const [aditamento, setAditamento] = useState();
  const [prepago, setPrePago] = useState();
  // const [select, setSelect] = useState('');
  /* eslint-disable-next-line no-unused-vars */
  const [parcelasPrePagas, setParcelasPrePagas] = useState('');
  // const [civilGuarantorState, setCivilGuarantorState] = useState('');
  const [typeOfContract, setTypeOfContract] = useState('');
  const [planoResgatado, setPlanoResgatado] = useState(
    'Nenhum plano encontrado'
  );
  const [plPlanoReativ, setPlPlanoReativ] = useState('');
  const [plCodigoReativ, setPlCodigoReativ] = useState('');
  const [planoSubstituido, setPlanoSubstituido] = useState('');
  // const [planoInativo, setPlanoInativo] = useState('');
  const [nicData, setNicData] = useState([]);
  const [nicSelecionado, setNICSelecionado] = useState('');

  const [company, setCompany] = useState([]);
  const [codCompany, setCodCompany] = useState('');
  const [selectedCompany, setSelectedCompany] = useState('');

  const [flat, setFlat] = useState('');
  const [dailyQuantities, setDailyQuantities] = useState('');
  const [family, setFamily] = useState('');
  const [adhesion, setAdhesion] = useState(true);
  const [payment, setPayment] = useState('');
  const [checkOffer, setCheckOffer] = useState(false);
  const [offer, setOffer] = useState(false);
  const [offerApply, setOfferApply] = useState(false);
  const [parcelasVendedor, setParcelasVendedor] = useState(0);
  const [parcelasCoobrastur, setParcelasCoobrastur] = useState('');
  const [sellerMembershipForm, setSellerMembershipForm] = useState('');
  const [halfAdhesion, setHalfAdhesion] = useState('');
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [blockScroll, allowScroll] = useScrollBlock();
  const [dadosPessoais, setDadosPessoais] = useState(true);
  const [dadosRestricao, setDadosRestricao] = useState(true);
  const [dadosIndicacao, setDadosIndicacao] = useState(true);
  const [dadosEmpresa, setDadosEmpresa] = useState(true);
  const [dadosAssociacao, setDadosAssociacao] = useState(true);
  const [dadosBoleto, setDadosBoleto] = useState(true);
  const [dadosAdesao, setDadosAdesao] = useState(true);
  const [valoresAdesao, setValoresAdesao] = useState(false);
  const [isSendModalClick, setIsSendModalClick] = useState(false);
  const [dataBoletoEscolhida, setDataBoletoEscolhida] = useState();
  // const [parceiroEscolhido, setParceiroEscolhido] = useState();
  // const [cupomEscolhido, setCupomEscolhido] = useState();

  const [cpf, setCPF] = useState('');
  const [cpfInvalid, setCPFInvalid] = useState(false);
  const [nome, setNome] = useState('');
  const [email, setEmail] = useState('');
  const [confirmaEmail, setConfirmaEmail] = useState('');

  const [telefone, setTelefone] = useState('');
  const [cepAssociado, setCepAssociado] = useState('');
  const [valorAdesao, setValorAdesao] = useState('');
  const [valorAdesaoOriginal, setValorAdesaoOriginal] = useState('');
  const [finalAdesao, setFinalAdesao] = useState('');
  const [descontoAdesao, setDescontoAdesao] = useState('');
  const descontoAdesaoFinal = parseFloat(descontoAdesao);
  // console.log('original', valorAdesaoOriginal);
  // console.log('desconto', valorAdesao);
  // const [isPayable, setIsPayable] = useState('');
  const tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate());
  const [dataEscolhida, setDataEscolhida] = useState(tomorrow);

  //   const [startDate, setStartDate] = useState(new Date());
  // const [endDate, setEndDate] = useState(new Date());
  // const [startDate, setStartDate] = useState(null);

  const [dataAdesao, setDataAdesao] = useState('');
  const [checaCPF, setChecaCPF] = useState(false);
  const [checaCPFFia, setChecaCPFFia] = useState(false);
  const [checaCEP, setChecaCEP] = useState(false);
  const [cpfIndic, setCpfIndic] = useState('');
  const [nomeIndic, setNomeIndic] = useState('');
  const [emailIndic, setEmailIndic] = useState('');
  const [celularIndic, setCelularIndic] = useState('');
  const [cpfFia, setCpfFia] = useState('');
  const [nomeFia, setNomeFia] = useState('');
  // const [dataFia, setDataFia] = useState('');
  // const [nacioFia, setNacioFia] = useState('');
  const [maeFia, setMaeFia] = useState('');
  // const [paiFia, setPaiFia] = useState('');
  const [emailFia, setEmailFia] = useState('');
  const [confirmaEmailFiador, setConfirmaEmailFiador] = useState('');
  const [celularFia, setCelularFia] = useState('');
  // const [conjugeFia, setConjugeFia] = useState('');

  const [adFormaPagamento, setAdFormaPagamento] = useState(true);
  const [formaPagamento, setFormaPagamento] = useState();
  const [datasPagamento, setDatasPagamento] = useState('');
  const [codigoPipeline, setCodigoPipeline] = useState('');

  // campanha
  // const [campaign, setCampaign] = useState(false);
  // const [campaingValue, setCampaignValue] = useState('');

  const [carMes, setCarMes] = useState('');
  const [carDia, setCarDia] = useState('');
  const [debMes, setDebMes] = useState('');
  const [debDia, setDebDia] = useState('');

  // split data boleto
  const [boletoMonth, setBoletoMonth] = useState('');

  const parts = boletoMonth.split('/');
  const monthBoleto = parts[1]; // acessando o primeiro elemento do array, que é o mês

  // const debMes = carMes;
  // const debDia = carDia;
  const currentDate = Date.now();
  const today = new Date(currentDate);

  // const dateTime = [`${carDia}/${carMes}/${currentDate.getFullYear()}`];

  const [planos, setPlanos] = useState([]);
  const [dadosAntigos, setDadosAntigos] = useState([]);

  useEffect(() => {
    if (typeOfContract == '11') {
      setAdhesion(false);
    }
  }, [typeOfContract]);

  const getFilteredOptions = (options) => {
    const repreCodigo = localStorage.getItem('repreCodigo');
    const startsWithDesiredPrefix = repreCodigo
      ? ['22', '13', '28', '38'].some((prefix) =>
          repreCodigo.startsWith(prefix)
        )
      : false;

    const filteredOptions = options.filter((option) => {
      if (startsWithDesiredPrefix) {
        return option.label === 'Boleto';
      }
      return true;
    });

    return filteredOptions.map((option) => (
      <option key={option.value} value={option.value}>
        {option.label}
      </option>
    ));
  };

  const [vendNomes, setVendNomes] = useState([]);
  const [cupNomes, setCupNomes] = useState([]);
  const [vendNomeSelecionado, setVendNomeSelecionado] = useState('');
  const [vendCodigoSelecionado, setVendCodigoSelecionado] = useState('');
  const [cupNomeSelecionado, setCupNomeSelecionado] = useState('');
  const [cupCodigoSelecionado, setCupCodigoSelecionado] = useState('');
  const [dados, setDados] = useState([]);

  // useEffect(() => {
  //   buscaParceiro();
  // }, []);

  // console.log(adhesion);
  // console.log('descontoAdesão', descontoAdesao);
  // console.log('valorAdesao', valorAdesao);
  const addItem = () => {
    if (planos.length < 10) {
      setPlanos([
        ...planos,
        {
          id: new Date().getTime(),
          // parceiro: vendNomeSelecionado,
          // cupom: cupNomeSelecionado,
          /* eslint-disable-next-line no-unneeded-ternary */
          adesao: adhesion,
          tipoVenda: prepago === true ? '2' : typeOfContract,
          nroPipeLine: Number(codigoPipeline), // conversão para número
          DealRequerido: dealRequerido,
          tpplcodigo: flat,
          plQtdeDiarias: dailyQuantities,
          plfamilia: family.toLocaleString(),
          valorAdesaoOriginal: valorAdesaoOriginal,
          descontoInput: descontoAdesaoFinal,
          valorAdesao: finalAdesao !== '0,00' ? finalAdesao : valorAdesao,
          valorMensalidade: valorMensalidade.toLocaleString('pt-br', {
            minimumFractionDigits: 2,
          }),
          valorMensalidadeOriginal: valorMensalidadeOriginal.toLocaleString(
            'pt-br',
            {
              minimumFractionDigits: 2,
            }
          ),
        },
      ]);
    } else {
      showToast({
        type: 'error',
        message: 'Limite de 10 contratos atingidos.',
      });
    }
    // console.log(valorMensalidadeOriginal);
    // console.log(valorMensalidade);
    setFlat('');
    setDailyQuantities('');
    setFamily('');
    setValorAdesao('');
    setValorMensalidade('0,00');
    setDescontoAdesao('0,00');
    setFinalAdesao('0,00');

    if (typeOfContract == '11') {
      setAdhesion(false);
    }
    if (descontoAdesao != valorAdesao && typeOfContract == '1') {
      setAdFormaPagamento(true);
      // setAdhesion(true);
      setValoresAdesao(true);
    }
    if (typeOfContract == '1' && adhesion === true) {
      setValoresAdesao(true);
      // setAdhesion(true);
    } else {
      setParcelasVendedor(0);
    }
  };

  const [isValid, setIsValid] = useState(false);

  useEffect(() => {
    const podeVenderConvenios = localStorage.getItem('podeVenderConvenios');

    if (podeVenderConvenios === '1') {
      setIsValid(true);
    } else if (podeVenderConvenios === '0') {
      setIsValid(false);
    }
  }, []);

  // soma dos valores
  const totalAdesaoOriginal = planos
    .map((x) => x.valorAdesaoOriginal)
    .reduce((a, b) => a + b, 0);
  const totalAdesao = valorAdesao - descontoAdesao;
  const totalAdesaoTable = planos
    .map((x) => x.valorAdesao)
    .reduce((a, b) => a + b, 0);
  const totalMensal = planos
    .map((x) => parseFloat(x.valorMensalidade.replace(',', '.')))
    .reduce((a, b) => a + b, 0);

  // remover itens do array
  const removeitem = (id) => {
    setPlanos((oldValues) => oldValues.filter((planos) => planos.id !== id));
    // console.log(planos);
    setCheckOffer(false);
    setOffer(false);
    setOfferApply(false);
    setDescontoAdesao('0,00');
    setFinalAdesao('0,00');
    setValorAdesao('0,00');
    setVendNomeSelecionado('');
    setCupNomeSelecionado('');
    setFormaPagamento('');
    setCarMes('');
    setDataBoletoEscolhida('');
    setCarDia('');
    setDebDia('');
    setDebMes('');
  };

  const [arrayDays, setArrayDays] = useState([]);

  const urlGlobal = 'https://telemarketing.coobrastur.com.br/wsAppVendasV2';

  const location = useLocation();

  const dadosCliente = {
    cpf,
    rua,
    nome,
    email,
    birthDate,
    telefone,
    ufAssociado,
    cidade,
    bairroAssociado,
    numeroAssociado,
    complemento,
    cepAssociado,
    valorRestricao,
    restricao,
    dadosRestricao,
    fiador,
    aditamento,
    send,
    formaPagamento,
    dataBoletoEscolhida,
    carDia,
    carMes,
    debDia,
    debMes,
    vendNomeSelecionado,
    cupNomeSelecionado,
    // campaingValue,
  };

  const dAssociacao = {
    planos,
    selectedCompany,
    codigoPipeline,
    typeOfContract,
    flat,
    dailyQuantities,
    family,
    // payment,
    // halfAdhesion,
    // parcelasCoobrastur,
    finalValue,
    totalAdesao,
    totalMensal,
    totalAdesaoOriginal,
    totalAdesaoTable,
    valorAdesao,
    finalAdesao,
    sellerMembershipForm,
    parcelasVendedor,
    dataEscolhida,
    dataAdesao,
    indicador,
    cpfIndic,
    nomeIndic,
    emailIndic,
    celularIndic,
  };

  const dadosFia = {
    fiador,
    aditamento,
    // select,
    cpfFia,
    nomeFia,
    // dataFia,
    // nacioFia,
    // civilGuarantorState,
    maeFia,
    // paiFia,
    emailFia,
    confirmaEmailFiador,
    celularFia,
    // conjugeFia,
  };

  function validarNome() {
    const palavras = nome.split(' ');
    return palavras.length >= 2;
  }
  // console.log(parseFloat(planos.valorA) + parseFloat(planos.valorA));
  async function handleFormSubmit(data) {
    // Validações de campos obrigatórios
    if (!cpf) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de CPF do associado',
      });

      setLoader(false);
      return false;
    }

    if (!birthDate || birthDate.replace(/_/g, '').length < 10) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de Data de Nascimento',
      });

      setLoader(false);
      return false;
    }
    if (!validarNome()) {
      showToast({
        type: 'error',
        message: 'O nome deve conter um sobrenome.',
      });

      setLoader(false);
      return false;
    }

    if (!birthDate || birthDate.replace(/_/g, '').length < 10) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de Data de Nascimento',
      });

      setLoader(false);
      return false;
    }

    if (descontoAdesao > totalAdesaoOriginal) {
      // console.log(descontoAdesao);
      // console.log(totalAdesao);
      showToast({
        type: 'error',
        message: 'Valor do Desconto não pode ser maior que adesão.',
      });
      setLoader(false);
      return false;
    }

    if (cpfInvalid === true) {
      showToast({
        type: 'error',
        message: 'O CPF informado é invalido.',
      });

      setLoader(false);
      return false;
    }

    if (!nome) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de Nome do associado',
      });

      setLoader(false);
      return false;
    }

    const regNome = /^[a-záàâãéèêíïóôõöúçñA-ZÁÀÂÃÉÈÊÍÏÓÔÕÖÚÇÑ ]+$/;
    // console.log(regNome.test(data.assNome_RazaoSocial));
    if (regNome.test(nome || nomeFia) === false) {
      showToast({
        type: 'error',
        message: 'Nome do associado precisa ter apenas letras.',
      });

      setLoader(false);
      return false;
    }

    // Validando E-mail
    if (!email) {
      showToast({
        type: 'error',
        message: 'Preencha o campo de E-mail do associado',
      });

      setLoader(false);
      return false;
    }
    const regEmail =
      /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

    if (!regEmail.test(email)) {
      showToast({
        type: 'error',
        message: 'E-mail do associado é inválido',
      });

      setLoader(false);
      return false;
    }

    if (email !== confirmaEmail) {
      showToast({
        type: 'error',
        message: 'Os campos de e-mail e confirmar e-mails não correspondem.',
      });

      setLoader(false);
      return false;
    }

    if (emailFia !== confirmaEmailFiador) {
      showToast({
        type: 'error',
        message:
          'Os campos de e-mail do fiador e confirmar e-mail do fiador não correspondem.',
      });

      setLoader(false);
      return false;
    }
    // removendo hífen do número de celular
    let newAssNumber = telefone;
    let newIndicatorNumber = data.celularIndicador;
    let newGuarantorNumber = data.fiaNumeroCelular;
    // let newGuarantorPhone = data.fiaFone;

    let newCpfIndicator = data.cpfIndicador;
    let newCpfGuarantor = data.fiaCpf;

    newAssNumber = newAssNumber.replace(/-/g, '');

    if (!newAssNumber) {
      showToast({
        type: 'error',
        message: 'Preencha o campo Celular do associado',
      });

      setLoader(false);
      return false;
    }

    if (indicador === undefined) {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Indicador',
      });

      setLoader(false);
      return false;
    }

    if (indicador === true) {
      newIndicatorNumber = newIndicatorNumber.replace(/-/g, '');
      newCpfIndicator = newCpfIndicator.replace(/[^\d]+/g, '');
      // console.log(newCpfIndicator);
    }

    // if (fiador === true) {
    //   newGuarantorNumber = newGuarantorNumber.replace(/-/g, '');
    //   // newGuarantorPhone = newGuarantorPhone.replace(/-/g, '');
    //   newCpfGuarantor = newCpfGuarantor.replace(/[^\d]+/g, '');
    // }

    if (indicador === true && !data.nomeIndicador) {
      showToast({
        type: 'error',
        message: 'Preencha o Nome do Indicador',
      });

      setLoader(false);
      return false;
    }

    if (indicador === true && !newCpfIndicator) {
      showToast({
        type: 'error',
        message: 'Preencha o CPF do Indicador',
      });

      setLoader(false);
      return false;
    }

    if (restricao === undefined) {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Restrição',
      });

      setLoader(false);
      return false;
    }

    if (planos == '') {
      showToast({
        type: 'error',
        message: 'Adicione ao menos um plano',
      });

      setLoader(false);
      return false;
    }
    if (!formaPagamento) {
      showToast({
        type: 'error',
        message: 'Preencha uma forma de pagamento',
      });

      setLoader(false);
      return false;
    }

    if (formaPagamento == 4 && !dataBoletoEscolhida) {
      showToast({
        type: 'error',
        message: 'Preencha dia para pagamento do boleto',
      });

      setLoader(false);
      return false;
    }
    if (restricao === true && fiador === true) {
      if (
        !newCpfGuarantor ||
        !data.fianome ||
        // !data.fiaDtNascimento ||
        // !data.fianacionalidade ||
        // !civilGuarantorState ||
        !data.fiaConjugeFiliacao ||
        !data.fiaEmailPessoal ||
        // !data.fia ||
        // !data.fiaNumeroCelularDDD ||
        !newGuarantorNumber
      ) {
        showToast({
          type: 'error',
          message: 'Preencha todas as informações de Fiador',
        });

        setLoader(false);
        return false;
      }
    } else if (fiador === true) {
      if (!newCpfGuarantor || !newGuarantorNumber) {
        showToast({
          type: 'error',
          message: 'Preencha todas as informações de Fiador',
        });

        setLoader(false);
        return false;
      }

      newGuarantorNumber = newGuarantorNumber.replace(/-/g, '');
      // newGuarantorPhone = newGuarantorPhone.replace(/-/g, '');
      newCpfGuarantor = newCpfGuarantor.replace(/[^\d]+/g, '');
    }

    if (!typeOfContract || typeOfContract === 'Selecione') {
      showToast({
        type: 'error',
        message: 'Escolha uma opção de Tipo de Contrato',
      });

      setLoader(false);
      return false;
    }

    // if (typeOfContract == '2') {
    //   setParcelasPrePagas(1);
    // }

    // if (!flat || flat === 'Selecione') {
    //   showToast({
    //     type: 'error',
    //     message: 'Escolha uma opção de Plano',
    //   });

    //   setLoader(false);
    //   return false;
    // }

    // if (!dailyQuantities || dailyQuantities === 'Selecione') {
    //   showToast({
    //     type: 'error',
    //     message: 'Escolha a quantidade de diárias',
    //   });

    //   setLoader(false);
    //   return false;
    // }

    // if (family === '') {
    //   showToast({
    //     type: 'error',
    //     message: 'Escolha uma opção de Família',
    //   });

    //   setLoader(false);
    //   return false;
    // }

    if (
      typeOfContract == '1' &&
      adhesion === true &&
      sellerMembershipForm == ''
    ) {
      showToast({
        type: 'error',
        message: 'Escolha uma forma de pagamento para adesão',
      });

      setLoader(false);
      return false;
    }

    // const dealRequerido = localStorage.getItem('dealRequerido') === 'true';
    if (dealRequerido === 'true' && !codigoPipeline) {
      showToast({
        type: 'error',
        message:
          'Por favor, preencha o campo "Código Pipedrive" para continuar!',
      });

      setLoader(false);
      return false;
    }

    // if (
    //   typeOfContract !== '11' &&
    //   (localStorage.getItem('repreCodigo').startsWith(13) ||
    //     localStorage.getItem('repreCodigo').startsWith(22)) &&
    //   isPayable == 0 &&
    //   parcelasVendedor !== 0
    // ) {
    //   showToast({
    //     type: 'error',
    //     message: 'Confira a data de adesão.',
    //   });

    //   setLoader(false);
    //   return false;
    // }

    if (
      typeOfContract == '1' &&
      adhesion === true &&
      sellerMembershipForm !== '1' &&
      !parcelasVendedor
    ) {
      showToast({
        type: 'error',
        message: 'Escolha uma forma de parcelamento para adesão',
      });

      setLoader(false);
      return false;
    }

    // if (
    //   dataEscolhida.getDate() === new Date().getDate() &&
    //   parcelasVendedor !== 0
    // ) {
    //   showToast({
    //     type: 'error',
    //     message: 'Data da primeira cobrança inválida.',
    //   });

    //   setLoader(false);
    //   return false;
    // }
    if (formaPagamento == 1 && debMes == '') {
      showToast({
        type: 'error',
        message: 'Escolha mês para cobrança.',
      });

      setLoader(false);
      return false;
    }
    if (formaPagamento == 1 && debDia == '') {
      showToast({
        type: 'error',
        message: 'Escolha dia para cobrança.',
      });

      setLoader(false);
      return false;
    }
    if (formaPagamento == 2 && carMes == '') {
      showToast({
        type: 'error',
        message: 'Escolha mês para cobrança.',
      });

      setLoader(false);
      return false;
    }
    if (formaPagamento == 2 && carDia == '') {
      showToast({
        type: 'error',
        message: 'Escolha dia para cobrança.',
      });

      setLoader(false);
      return false;
    }
    // Valor de adesão
    // setValorMensalidade(
    //   formatReal(
    //     getMensalidade({
    //       plano: flat,
    //       diarias: dailyQuantities,
    //       familia: family,
    //     })
    //   )
    // );

    const informacoesPlano = {
      tpPlCodigo: flat,
      plFamilia: family,
      qtdeDiarias: dailyQuantities,
      adesao: false,
      vendCodigo: 675,
    };

    apiClei
      .post(urlGlobal.concat('/ConsultaCrm.asmx/RetornaValoresPlanos'), {
        dados: JSON.stringify(informacoesPlano),
      })
      .then((res) => {
        setValorMensalidade(res.data.ValorPlano);
        setValorMensalidadeOriginal(res.data.ValorPlano);
        // console.log(finalValue);
      })
      .catch((e) => {
        console.log(e);
      });

    // if (adhesion === '') {
    //   // console.log(adhesion);
    //   showToast({
    //     type: 'error',
    //     message: 'Escolha uma opção de Adesão',
    //   });
    //   setLoader(false);
    //   return false;
    // }

    // if (adhesion === true && data.valorAdesao < 1) {
    //   // console.log(adhesion);
    //   showToast({
    //     type: 'error',
    //     message: 'O valor da adesão não pode ser igual a zero.',
    //   });
    //   setLoader(false);
    //   return false;
    // }

    if (!cepAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o CEP',
      });

      setLoader(false);
      return false;
    }

    if (!ufAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o Estado',
      });

      setLoader(false);
      return false;
    }

    if (!cidade) {
      showToast({
        type: 'error',
        message: 'Preencha a Cidade',
      });

      setLoader(false);
      return false;
    }

    if (!bairroAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o bairro',
      });

      setLoader(false);
      return false;
    }

    if (!rua) {
      showToast({
        type: 'error',
        message: 'Preencha a rua',
      });

      setLoader(false);
      return false;
    }
    if (fiador === true) {
      if (email.toUpperCase() === emailFia.toUpperCase()) {
        showToast({
          type: 'error',
          message: 'O e-mail do associado e do fiador não podem ser iguais.',
        });
        setLoader(false);
        return false;
      }
    }

    if (!numeroAssociado) {
      showToast({
        type: 'error',
        message: 'Preencha o número do endereço.',
      });

      setLoader(false);
      return false;
    }

    setIsModalVisible(true);
    blockScroll();

    function formataNumero(numero) {
      return numero ? numero.replace(/\(|-/g, '').split(')') : '';
    }
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    if (isSendModalClick !== false) {
      // console.log(send);
      try {
        const dados = {
          assCPF_CNPJ: RetiraMascara(cpf),
          assNome_RazaoSocial: nome,
          assEmailPessoal: email,
          assNumCelularDDD: formataNumero(telefone)[0],
          assNumCelular: formataNumero(telefone)[1],
          dtNascimento: birthDate,
          indicacao: indicador || false,
          nomeIndicador: data.nomeIndicador || '',
          cpfIndicador: newCpfIndicator || '',
          assnicIndicador: nicSelecionado || '',
          emailIndicador: data.emailIndicador || '',
          dddCelularIndicador: formataNumero(newIndicatorNumber)[0] || '',
          celularIndicador: formataNumero(newIndicatorNumber)[1] || '',

          assRestricao: restricao || false,
          tipoVenda: typeOfContract,
          aditamento: aditamento || false,
          prepago: prepago || false,
          // formaAditamento: select || 0,
          parcelasPrePagas: parcelasPrePagas || 0,

          fiador,
          fiaCpf: newCpfGuarantor || '',
          fianome: data.fianome || '',
          fiaDtNascimento: data.fiaDtNascimento || '',
          fianacionalidade: data.fianacionalidade || '',
          // civCodigo: civilGuarantorState || '',
          civCodigo: '',
          fiaConjuge: data.fiaConjuge || '',
          fiaConjugeFiliacao: data.fiaConjugeFiliacao || '',
          fiaConjugeFiliacao2: data.fiaConjugeFiliacao2 || '',
          fiaEmailPessoal: data.fiaEmailPessoal || '',
          fiaNumeroCelularDDD: formataNumero(newGuarantorNumber)[0] || '',
          fiaNumeroCelular: formataNumero(newGuarantorNumber)[1] || '',
          // fiaddFone: formataNumero(newGuarantorPhone)[0] || '',
          // fiaFone: formataNumero(newGuarantorPhone)[1] || '',
          formaAssinaturaFiador: 1,
          assinaturaFiador: '',

          TpplanoReativado: plPlanoReativ,
          plCodigoReativado: plCodigoReativ,
          planoResgatado: planoResgatado,
          empCodigo: codCompany,
          planos: planos || '',
          // tpplcodigo: flat,
          // plQtdeDiarias: dailyQuantities || '',
          // plfamilia: family,
          valorMensalidade: valorMensalidade,
          dataAdesao: dataEscolhida,
          adesao: adhesion,
          // valorAdesao: data.valorAdesao || '',
          adesaoValorDesconto: valorAdesao || 0,
          adesaoValorOriginal: valorAdesaoOriginal || 0,
          formaPagamentoAdesao: sellerMembershipForm || 0,
          parcelamentoAdesao: parcelasVendedor || 0,
          meioAdesao: payment,
          tipoAdesaoVendedor: sellerMembershipForm || 0,
          formaAdesaoVendedor: parcelasVendedor || '',
          dataVendedor: data.dataVendedor || '',
          obsVendedor: data.obsVendedor || '',

          tipoAdesaoCoobrastur: halfAdhesion || '',
          formaAdesaoCoobrastur: parcelasCoobrastur || '',
          dataCoobrastur: data.dataCoobrastur || '',
          obsCoobrastur: data.obsCoobrastur || '',

          endUF: ufAssociado,
          endCepAssociado: cepAssociado.replace('-', '') || '',
          endCidadeAssociado: cidade || '',
          endBairroAssociado: bairroAssociado || '',
          endLogradouroAssociado: rua || '',
          endNumLogradouroAssociado: numeroAssociado || '',
          endComplementoAssociado: data.endComplementoAssociado || '',

          terceiro: false,
          cpfTerceiro: data.cpfTerceiro || '',
          nomeTerceiro: data.nomeTerceiro || '',
          nascimentoTerceiro: data.nascimentoTerceiro || '',
          nacionalidadeTerceiro: data.nacionalidadeTerceiro || '',
          estadoCivilTerceiro: data.estadoCivilTerceiro || 0,
          nomeConjugeTerceiro: data.nomeConjugeTerceiro || '',
          nomeMaeTerceiro: data.nomeMaeTerceiro || '',
          nomePaiTerceiro: data.nomePaiTerceiro || '',
          emailTerceiro: data.emailTerceiro || '',
          dddCelularTerceiro: data.dddCelularTerceiro || '',
          numCelularTerceiro: data.numCelularTerceiro || '',
          dddTelefoneTerceiro: data.dddTelefoneTerceiro || '',
          TelefoneTerceiro: data.TelefoneTerceiro || '',
          formaAssinaturaTerceiro: data.formaAssinaturaTerceiro || 0,
          assinaturaTerceiro: '',
          env: 0, // VARIAVEL DE AMBIENTE: env: 1 (HOMOLOG) / env: 0 (PROD)
          empCodigo: codCompany,

          campanha: 0,
          formaPagamento,
          dataBoletoEscolhida: dataBoletoEscolhida || '',

          carDia: carDia || '',
          carMes: carMes || '',
          debDia: debDia || '',
          debMes: debMes || '',
          // carAno: carYear || '',

          formaAssinatura: 1,
          assinatura: '',
          update: false,
          cupCodigo: cupCodigoSelecionado || 0,
          vendCodigo: vendCodigoSelecionado || 0,
          token,
        };

        // console.log(dados);
        // console.log(formaPagamento);
        allowScroll();
        apiClei
          // .get(`/Contrato.asmx/IncluirContrato`, {
          .get(`/Contrato.asmx/IncluirContratoMaisPlanos`, {
            params: {
              dados,
            },
          })
          .then((response) => {
            // const info = response.data[0];
            // console.log(dados);

            console.log(response.data[0]);

            setSend('1');

            showToast({
              type: 'success',
              message: 'Contrato criado com sucesso',
            });

            if (formaPagamento == 4 && codCompany !== 60) {
              setLoader(true);

              try {
                apiClei
                  .get(`/D4Sign.asmx/EnviarContratosD4Sing`, {
                    params: {
                      dados: {
                        info: {
                          id: response.data[0].Id,
                          token: response.data[0].Token,
                        },
                      },
                    },
                  })

                  .then((res) => {
                    console.log(res);
                  })
                  .catch((e) => {
                    console.log(e);
                  });

                setLoader(false);
                history.push('/contracts');
              } catch (err) {
                showToast({
                  type: 'error',
                  message:
                    'Algum erro interno aconteceu. Tente novamente mais tarde',
                });

                setLoader(false);
              }
              return false;
            }
            if (formaPagamento == 4 && codCompany == 60) {
              setLoader(true);

              try {
                apiClei
                  .get(`/Contrato.asmx/AceiteMeridien`, {
                    params: {
                      in_linkinterno: response.data[0].LinkInterno,
                    },
                  })

                  .then((res) => {
                    console.log(res);
                  })
                  .catch((e) => {
                    console.log(e);
                  });

                setLoader(false);
                history.push('/contracts');
              } catch (err) {
                showToast({
                  type: 'error',
                  message:
                    'Algum erro interno aconteceu. Tente novamente mais tarde',
                });

                setLoader(false);
              }
              return false;
            }

            // inserir aqui endpoint envio de dados cadastrais
            apiClei
              .get(`Contrato.asmx/EnviaEmailDadosPagamento`, {
                params: {
                  cpf: response.data[0].Cpf,
                  email: response.data[0].Email,
                  env: response.data[0].Env,
                  urlPagamento: response.data[0].UrlPagamento,
                },
              })
              .then((res) => {
                console.log(res);
              });

            history.push('/contracts');

            setLoader(false);
          })
          .catch((error) => {
            setSend('2');
            // console.log(error.response.status);

            if (error.response.data.match('CPF do indicador incorreto!')) {
              showToast({
                type: 'error',
                message: 'CPF do indicador incorreto!',
              });

              setLoader(false);
              return false;
            }
            if (error.response && error.response.status === 500) {
              const errorMessage = error.response.data
                .split(': ')[1]
                .split('\n')[0];
              // console.log('Mensagem de Erro:', errorMessage);
              if (errorMessage.includes('Cliente com tarja')) {
                Swal.fire({
                  text: 'Cliente com tarja judicial. Não pode adquirir plano!',
                  icon: 'warning',
                  showCancelButton: false,
                  confirmButtonText: 'Voltar',
                  allowOutsideClick: false,
                }).then((result) => {
                  if (result.isConfirmed) {
                    history.goBack();
                  }
                });
              } else if (errorMessage.includes('CONSISTENCIAS NOS DA')) {
                showToast({
                  type: 'error',
                  message:
                    'Erro de consistência nos dados. Verifique as informações e tente novamente.',
                });

                setRestricao(true);
                setCPFInvalid(false);
                setSellerMembershipForm(0);
                setValorRestricao(true);
              } else if (errorMessage.includes('Cliente no SPC')) {
                showToast({
                  type: 'error',
                  message: 'Cliente no SPC. Venda não permitida!',
                });

                setRestricao(true);
                setCPFInvalid(false);
                setSellerMembershipForm(0);
                setValorRestricao(true);
              } else {
                showToast({
                  type: 'error',
                  message:
                    errorMessage ||
                    'Erro Interno do Servidor. Tente novamente mais tarde.',
                });
              }
              setRestricao(true);
              setCPFInvalid(false);
              setSellerMembershipForm(0);
              setValorRestricao(true);
            } else if (error.response.status == 500) {
              setTimeout(() => {
                setDesabilitado(true);
              }, 100);
              setTimeout(() => {}, 3000);

              showToast({
                type: 'error',
                message:
                  'Algum erro interno aconteceu. Não foi possível enviar seus dados, entre em contato com o suporte',
              });
              setLoader(false);
              return false;
            }

            // showToast({
            //   type: 'error',
            //   message:
            //     'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
            // });

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        setSend('2');
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });

        setLoader(false);
      }

      return true;
    }
  }

  function validaParcelas(datasEscolhida) {
    const dataAtual = new Date();
    const data = new Date();

    if (formaPagamento === '2') {
      data.setMonth(carMes - 1, datasEscolhida);
    } else {
      data.setMonth(debMes - 1, datasEscolhida);
    }

    // console.log('data antes', data.toLocaleDateString());
    if (data.getMonth() < dataAtual.getMonth()) {
      data.setFullYear(data.getFullYear() + 1);
    }
    // console.log('data depois', data.toLocaleDateString());
    // console.log(dataEscolhida);
    const dataFormatada =
      datasEscolhida.indexOf('/') > 0
        ? datasEscolhida
        : `${datasEscolhida}/${carMes || debMes}/${data.getFullYear()}`; // console.log(dataCartao);

    const dados = {
      dataPrimeiraMensalidade: dataFormatada,
      parcelas: parcelasVendedor,
    };

    apiClei
      .get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/PrimeiraCobranca?dados=${JSON.stringify(dados)}`
        )
      )
      .then((resp) => {
        console.log(resp);

        // setIsPayable(resp.data[0].isPayable);
        // setPrimeiraAdesao(
        //   resp.data[0].dtPrimeiraAdesao.toLocaleString().substr(0, 10)
        // );

        // console.log(isPayable);
        // console.log(primeiraAdesao.toLocaleString().substr(0, 10));
      });
  }

  // {restricao === true && }
  async function valuesAdesao() {
    setLoader(true); // Ativa o loader no início da função

    try {
      const informacoesPlano = {
        tpPlCodigo: flat,
        plFamilia: family,
        qtdeDiarias: dailyQuantities,
        adesao: false,
        vendCodigo: 675,
      };

      const res = await apiClei.post(
        urlGlobal.concat('/ConsultaCrm.asmx/RetornaValoresPlanos'),
        {
          dados: JSON.stringify(informacoesPlano),
        }
      );

      setValorMensalidade(res.data.ValorPlano);
      setValorMensalidadeOriginal(res.data.ValorPlano);

      const dados = {
        tpplcodigo: flat,
        plqtdediarias: dailyQuantities,
        plfamilia: family,
        vendcodigo: 675,
        tipdata: today.toLocaleString(),
      };

      const resp = await apiClei.get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/RetoraValorAdesao?dados=${JSON.stringify(dados)}`
        )
      );

      if (typeOfContract !== '11') {
        setValorAdesaoOriginal(resp.data[0].valAdesao);
        setValorAdesao(resp.data[0].valAdesao);
      } else {
        setValorAdesaoOriginal('0,00');
        setValorAdesao('0,00');
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoader(false); // Desativa o loader no final, independentemente do resultado
    }
  }

  useEffect(() => {
    if (
      (dailyQuantities && dailyQuantities.length > 0) ||
      (family && Object.keys(family).length > 0)
    ) {
      valuesAdesao();
    }
  }, [dailyQuantities, family]);

  // parceiros
  async function buscaParceiro() {
    const repreCodigo = localStorage.getItem('repreCodigo'); // Pega o valor de 'reprecodigo' do localStorage

    // console.log(repreCodigo);
    const endpoint =
      repreCodigo == '1'
        ? `/Contrato.asmx/ListaParceiroCupons?vendCodigo=21300&reprCodigo=1&cupCodigo=&empCod=${codCompany}`
        : `/Contrato.asmx/ListaParceiroCupons?vendCodigo=&reprCodigo=&cupCodigo=&empCod=${codCompany}`;

    // Agora, faz a chamada à API com o endpoint configurado
    apiClei.get(urlGlobal.concat(endpoint)).then((response) => {
      const dados = response.data;

      const vendNomes = [...new Set(dados.map((item) => item.vendNome))];
      setVendNomes(vendNomes);
      setDados(dados);
    });
  }
  useEffect(() => {
    buscaParceiro();
  }, []);

  // busca empresa
  async function buscaEmpresa() {
    apiClei
      .get(urlGlobal.concat('/ConsultaCrm.asmx/RetornaEmpresas?empcod='))
      .then((response) => {
        const dados = response.data;

        setCompany(dados);

        // setDados(dados);
        // console.log(dados);
      });
  }
  useEffect(() => {
    buscaEmpresa();
  }, []);

  const handleVendNomeChange = (event) => {
    const vendNome = event.target.value;
    if (vendNome == 'Selecione') {
      // console.log(vendNome);
      // console.log(dadosAntigos);
      setPlanos([...dadosAntigos]);
      setOfferApply(false);
      setAdFormaPagamento(true);
      setValoresAdesao(true);
    }

    setVendNomeSelecionado(vendNome);
    setCupNomeSelecionado('');
    setCupCodigoSelecionado('');

    // Filtrar os cupons correspondentes ao vendNome selecionado
    const cuponsFiltrados = dados
      .filter((item) => item.vendNome === vendNome)
      .map((item) => item.cupNome);

    setCupNomes(cuponsFiltrados);

    // Encontrar o vendCodigo correspondente ao vendNome selecionado
    const vendCodigo = dados.find(
      (item) => item.vendNome === vendNome
    )?.vendCodigo;
    setVendCodigoSelecionado(vendCodigo);
  };

  const handleCupNomeChange = (event) => {
    const cupNome = event.target.value;
    setCupNomeSelecionado(cupNome);

    // Encontrar o cupom selecionado pelo nome
    const cupomSelecionado = dados.find(
      (item) =>
        item.vendNome === vendNomeSelecionado && item.cupNome === cupNome
    );

    if (cupomSelecionado) {
      setCupCodigoSelecionado(cupomSelecionado.cupCodigo);
    } else {
      setCupCodigoSelecionado('');
    }
  };

  // console.log(dadosAntigos);

  const updateItem = () => {
    setDadosAntigos([...planos]);

    const promises = planos.map((item) =>
      apiClei
        .post(
          'https://apiprod.coobrastur.com.br/ApiDotz/api/VendaSemAdesao/ValidateDiscountCoupon',
          {
            plCodigo: parseInt(item.tpplcodigo),
            plFamilia: false,
            qtdDiarias: parseInt(item.plQtdeDiarias),
            cupomDesconto: cupNomeSelecionado,
            vendCodigo: vendCodigoSelecionado.toString(),
          }
        )
        .then((response) => {
          if (response.data.cupomValid === false) {
            // Retorna o item sem nenhuma modificação
            return item;
          }
          if (cupNomeSelecionado === 'AURIVERDE') {
            showToast({
              type: 'success',
              message: 'Cupom Valido!',
            });
            setCheckOffer(false);
            setOffer(false);
            setSellerMembershipForm(0);
            setAdhesion(true);
            setValoresAdesao(true);
            setParcelasVendedor('');

            return {
              ...item,
              adesao: true,
              valorAdesao: parseFloat(item.valorAdesaoOriginal),
              // valorMensalidade: valorMensalidadeCalculado.toLocaleString(
              //   'pt-br',
              //   {
              //     minimumFractionDigits: 2,
              //   }
              // ),
            };
          } else if (
            cupNomeSelecionado === 'MASTERCLIN50' ||
            cupNomeSelecionado === 'SINDJUDPE'
          ) {
            // Retorna o item sem nenhuma modificação
            showToast({
              type: 'success',
              message: 'Cupom Válido! 50% da desconto na adesão.',
            });
            setCheckOffer(false);
            setOffer(false);
            setSellerMembershipForm(0);
            setAdhesion(true);
            setValoresAdesao(true);
            setParcelasVendedor('');

            return {
              ...item,
              adesao: true,
              descontoInput: parseFloat(item.valorAdesaoOriginal) * 0.5,
              valorAdesao: parseFloat(item.valorAdesaoOriginal) * 0.5,
              // valorMensalidade: valorMensalidadeCalculado.toLocaleString(
              //   'pt-br',
              //   {
              //     minimumFractionDigits: 2,
              //   }
              // ),
            };
          } else {
            showToast({
              type: 'success',
              message: 'Cupom Válido! Isençao da adesão.',
            });
            setCheckOffer(false);
            setOffer(false);
            setSellerMembershipForm(0);
            setAdhesion(false);
            setValoresAdesao(false);
            setParcelasVendedor('');

            return {
              ...item,
              adesao: false,
              descontoInput: parseFloat(item.valorAdesaoOriginal),
              valorAdesao: 0,
              // valorMensalidade: valorMensalidadeCalculado.toLocaleString(
              //   'pt-br',
              //   {
              //     minimumFractionDigits: 2,
              //   }
              // ),
            };
          }
          // }
        })
        .catch((error) => {
          console.error('An error occurred:', error);
          return item;
        })
    );

    Promise.all(promises)
      .then((updatedItems) => {
        setPlanos(updatedItems);
      })
      .catch((error) => {
        console.error('An error occurred:', error);
      });
  };

  // data boleto
  async function buscaDataPagamento() {
    if (formaPagamento == 4) setLoader(true); // Ativa o loader no início da função

    try {
      let valorInicial;
      let numeroParcelas = 0;

      if (parcelasVendedor === '2') {
        numeroParcelas = 2;
      } else if (parcelasVendedor === '1') {
        numeroParcelas = 0;
      } else if (!adhesion) {
        numeroParcelas = 3;
      }

      const response = await apiClei.get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/RetornarDatasValidas?dataEscolhida=${dataEscolhida.toLocaleDateString()}&numeroParcelas=${numeroParcelas}`
        )
      );

      console.log(response);
      valorInicial = response.data;
      valorInicial.unshift({ dataExibicao: 'Selecione', dataReal: '' });
      setDatasPagamento(valorInicial);
    } catch (error) {
      console.error(error);
    } finally {
      // Desativa o loader com um timeout de 3 segundos
      setTimeout(() => {
        setLoader(false);
      }, 3000);
    }
  }

  // verifica se o CPF já é cliente
  async function verificaCliente(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }

    try {
      apiClei
        .post(
          urlGlobal.concat('/ConsultaCrm.asmx/AssociadoAtivos'),
          { cpf: RetiraMascara(userCpf) },
          { headers: { Authorization: token || '' } }
        )
        .then((response) => {
          // console.log('RESPONSE: ', response);
          if (response.data.body.existente === false) {
            // console.log('foi');
            // setCampaign(true);
            // setCampaignValue(100);
            setTypeOfContract('1');
            // Swal.fire({
            //   showCloseButton: true,
            //   title: '<p></p>',
            //   text: `Cliente está participando da campanha "Isenção 1 e 2 mensalidades"`,
            //   confirmButtonText: 'Confirmar',
            //   confirmButtonColor: '#01affd',
            // });
            // verificaRestricaoCoob(userCpf);
          }

          if (response.data.body.existente === true) {
            // setAdhesion(false);
            // console.log('foi');
            setTypeOfContract('1');
            // setAdFormaPagamento(false);
          }
          // if (response.data.body.existente === false) {
          //   // console.log('não foi');
          //   setCampaign(false);
          //   setCampaignValue(0);
          //   // modal
          // }
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  // Validar CPf Cliente
  async function verificaPlanosInativos(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }

    try {
      apiClei
        .get(
          urlGlobal.concat(
            `/ConsultaCrm.asmx/AssociadoRecuperacao?cpf=${RetiraMascara(
              userCpf
            )}&empCodigo=${codCompany}`
          )
        )
        .then((resp) => {
          // console.log(resp);

          apiClei
            .post(
              urlGlobal.concat('/ConsultaCrm.asmx/RetornarPlanosInativos'),
              { in_cpf: RetiraMascara(userCpf), codEmpresa: codCompany },
              { headers: { Authorization: token || '' } }
            )
            .then((response) => {
              // console.log('RESPONSE: ', response);

              if (resp.data[0].isRecuperacao == 1) {
                showToast({
                  type: 'info',
                  message: 'Plano inativo encontrado.',
                });

                setPlPlanoReativ(response.data[0].tpplCodigo);
                setPlCodigoReativ(response.data[0].plCodigo);
                setPlanoResgatado(response.data[0].Descricao);
                setCPFInvalid(false);

                if (typeOfContract === '11') {
                  setPlPlanoReativ(response.data[0].tpplCodigo);
                  setPlCodigoReativ(response.data[0].plCodigo);
                  setPlanoResgatado(response.data[0].Descricao);
                  // setFlat(response.data[0].plano);
                  setValoresAdesao(false);
                }
                setModalReativacao(true);
                // setCampaign(true);
                // setCampaignValue(100);

                verificaCliente(userCpf);
              }

              setFlat('');
              setDailyQuantities('');
              setFamily('');
              setLoader(false);
            })
            .catch((error) => {
              console.log(error);
              showToast({
                type: 'error',
                message:
                  'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
              });
              setLoader(false);
            });
        })
        .catch((error) => {
          console.log(error);
          showToast({
            type: 'error',
            message:
              'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
          });
          setLoader(false);
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  async function RetornaSituacaoUser(userCpf) {
    // console.log(userCpf);
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }

    try {
      apiClei
        .get(
          urlGlobal.concat(
            `/ConsultaCrm.asmx/RetornaSituacaoUsuario?cpf=${RetiraMascara(
              userCpf
            )}`
          )
        )
        .then((response) => {
          console.log(response);

          if (response.data[0].totalAtivos > 0) {
            showToast({
              type: 'info',
              message: `Esse usuário possui outro plano. 
              Sugestão: Ofereça 50% de desconto na adesão.`,
            });

            setLoader(false);
          }
        })
        .catch(() => {
          showToast({
            type: 'error',
            message:
              'Algum erro interno aconteceu. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
          });

          setLoader(false);
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  async function verificarCPF(strCPF) {
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    // debugger;
    strCPF = RetiraMascara(strCPF);
    let Soma;
    let Resto;
    Soma = 0;
    // if (strCPF == '00000000000') return false;

    // debugger;
    if (
      strCPF == '00000000000' ||
      strCPF == '11111111111' ||
      strCPF == '22222222222' ||
      strCPF == '33333333333' ||
      strCPF == '44444444444' ||
      strCPF == '55555555555' ||
      strCPF == '66666666666' ||
      strCPF == '77777777777' ||
      strCPF == '88888888888' ||
      strCPF == '99999999999'
    ) {
      return false;
    }

    console.log(strCPF);
    for (let i = 1; i <= 9; i++)
      Soma += parseInt(strCPF.substring(i - 1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10))) return false;

    Soma = 0;

    for (let i = 1; i <= 10; i++)
      Soma += parseInt(strCPF.substring(i - 1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if (Resto == 10 || Resto == 11) Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11))) return false;
    return true;
  }
  async function verificaRestricao(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    if (verificarCPF(cpf)) {
      try {
        apiClei
          .get(
            urlGlobal.concat(
              `/Usuario.asmx/RestricacaoSerasa?cpf=${RetiraMascara(
                userCpf
              )}&empCodigo=${codCompany}`
            )
            //   urlGlobal.concat('/ConsultaCrm.asmx/consultaCDLAppV2'),
            // .get(
            // HML: https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // PROD https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // `https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=${RetiraMascara(
            //   userCpf
            // )}`
            // { cpf: RetiraMascara(userCpf) },
            // { headers: { Authorization: token || '' } }
          )
          .then((response) => {
            // debugger;
            // console.log('RESPONSE: ', response);
            // console.error('ERROR', response);
            // console.log('PEGAR : ', response.data.body.restricao);
            // console.log(dateTime);

            if (response.data.restriction === false) {
              showToast({
                type: 'success',
                message: 'Resultado da análise de crédito: Alto Score.',
              });

              setRestricao(false);
              setFiador(false);
              setCPFInvalid(false);
              setAditamento(false);
              setPrePago(false);
              setValorRestricao(false);
              setTypeOfContract('1');
              // setPlPlanoReativ('');
              // setPlCodigoReativ('');
              // setPlanoResgatado('');
              setFlat('');
              setDailyQuantities('');
              setSellerMembershipForm(0);
              // setIsPayable('');
              setFamily('');
              RetornaSituacaoUser(userCpf);
              verificaCliente(userCpf);
            }

            if (response.data.restriction === true) {
              showToast({
                type: 'error',
                message: 'Resultado da análise de crédito: Baixo Score.',
              });
              // setTypeOfContract('1');
              setRestricao(true);
              setCPFInvalid(false);
              setSellerMembershipForm(0);
              setValorRestricao(true);
              verificaCliente(userCpf);

              // Swal.fire({
              //   text: `Deseja continuar com o contrato ou enviar uma solicitação de score?`,
              //   showDenyButton: true,
              //   icon: 'warning',
              //   confirmButtonText: 'Continuar',
              //   confirmButtonColor: '#01affd',
              //   denyButtonText: `Enviar Solicitação`,
              //   denyButtonColor: '#01affd',
              // }).then((resp) => {
              //   if (resp) {
              //     if (resp.isDenied) {
              //       Swal.fire('Saved!', '', 'success');

              //       const emailUser = localStorage.getItem('usuEmail');
              //       const { value: nome } = Swal.fire({
              //         // title: 'E-mail para acompanhamento do pedido.',
              //         title: `Digite o nome do Cliente referente ao CPF: ${cpf}`,
              //         input: 'text',
              //         // inputLabel: 'E-mail:',
              //         inputPlaceholder: 'Digite seu nome.',
              //         inputValidator: (value) => {
              //           if (!value) {
              //             return 'Por favor, digite o nome do Cliente.';
              //           }
              //           if (value) {
              //             apiClei
              //               .get(
              //                 `Contrato.asmx/EnviaEmailAppVendasAvaliacaoPrepago`,
              //                 {
              //                   params: {
              //                     assCpf: RetiraMascara(cpf),
              //                     emailVendedor: emailUser,
              //                     assNome: value,
              //                     // assNome: localStorage.setItem(
              //                     //   'usuNome',
              //                     //   response.data.Table[0].usuNome
              //                     // ),
              //                     // emailVendedor: localStorage.setItem(
              //                     //   'usuEmail',
              //                     //   response.data.Table[0].usuEmail
              //                     // ),
              //                   },
              //                 }
              //               )
              //               .then((res) => {
              //                 console.log(res);
              //               });
              //             Swal.fire({
              //               text: 'Solicitação de autorização enviada! Por favor acompanhe o andamento deste pedido através do seu email!',
              //               icon: 'success',
              //               // timer: '2000',
              //               confirmButtonText: 'Continuar',
              //             }).then((resp) => {
              //               if (resp) {
              //                 if (resp.isConfirmed) {
              //                   history.push('/Contracts');
              //                 }
              //               }
              //             });
              //           }
              //         },
              //       });

              //       if (nome) {
              //         Swal.fire(`Entered email: ${nome}`);
              //         // } else {
              //         // apiClei
              //         //   .get(
              //         //     `Contrato.asmx/EnviaEmailAppVendasAvaliacaoPrepago`,
              //         //     {
              //         //       params: {
              //         //         assCpf: cpf,
              //         //         emailVendedor: email,
              //         //       },
              //         //     }
              //         //   )
              //         //   .then((res) => {
              //         //     console.log(res);
              //         //   });
              //         // Swal.fire({
              //         //   text: 'Solicitação de autorização enviada! Por favor acompanhe o andamento deste pedido através do seu email!',
              //         //   icon: 'success',
              //         //   // timer: '2000',
              //         //   confirmButtonText: 'Continuar',
              //         // }).then((resp) => {
              //         //   if (resp) {
              //         //     if (resp.isConfirmed) {
              //         //       history.push('/Contracts');
              //         //     }
              //         //   }
              //         // });
              //       }
              //     }
              //   }
              //   // else if (resp.isDenied) {
              //   //   Swal.fire('Changes are not saved', '', 'info');
              //   // }
              // });
              // console.log('foi');
            }

            // if (response.data.isAutorizavel !== 0) {
            //   Swal.fire({
            //     text: `Deseja continuar com o contrato ou enviar uma solicitação de score?`,
            //     showDenyButton: true,
            //     icon: 'warning',
            //     confirmButtonText: 'Continuar',
            //     confirmButtonColor: '#01affd',
            //     denyButtonText: `Enviar Solicitação`,
            //     denyButtonColor: '#01affd',
            //   }).then((resp) => {
            //     if (resp) {
            //       if (resp.isDenied) {
            //         Swal.fire('Saved!', '', 'success');

            //         const emailUser = localStorage.getItem('usuEmail');
            //         const { value: name } = Swal.fire({
            //           // title: 'E-mail para acompanhamento do pedido.',
            //           title: `Digite o nome do Cliente referente ao CPF: ${
            //             cpf || location.state.cpf
            //           }`,
            //           input: 'text',
            //           // inputLabel: 'E-mail:',
            //           inputPlaceholder: 'Digite seu nome.',
            //           inputValidator: (value) => {
            //             if (!value) {
            //               return 'Por favor, digite o nome do Cliente.';
            //             }
            //             if (value) {
            //               apiClei
            //                 .get(
            //                   `Contrato.asmx/EnviaEmailAppVendasAvaliacaoPrepago`,
            //                   {
            //                     params: {
            //                       assCpf:
            //                         RetiraMascara(cpf) || location.state.cpf,
            //                       // emailVendedor: 'derek.cardoso@gmail.com',
            //                       emailVendedor: emailUser,
            //                       assNome: value,
            //                     },
            //                   }
            //                 )
            //                 .then((res) => {
            //                   console.log(res);
            //                 });
            //               Swal.fire({
            //                 text: 'Solicitação de autorização enviada! Por favor acompanhe o andamento deste pedido através do seu email!',
            //                 icon: 'success',
            //                 // timer: '2000',
            //                 confirmButtonText: 'Continuar',
            //               }).then((resp) => {
            //                 if (resp) {
            //                   if (resp.isConfirmed) {
            //                     history.push('/Contracts');
            //                   }
            //                 }
            //               });
            //             }
            //           },
            //         });

            //         if (name) {
            //           Swal.fire(`Entered email: ${name}`);
            //         }
            //       }
            //     }
            //   });
            // }
            setLoader(false);
            return false;
          })
          .catch((error) => {
            // console.error(error);
            if (error.response && error.response.status === 500) {
              const errorMessage = error.response.data
                .split(': ')[1]
                .split('\n')[0];
              // console.log('Mensagem de Erro:', errorMessage);
              if (
                errorMessage.includes('CALCULO INDISPONIVEL') ||
                errorMessage.includes('CONSISTENCIAS NOS DA')
              ) {
                Swal.fire({
                  text: 'Este não é um CPF válido. Atualize a página e tente novamente.',
                  icon: 'warning',
                  showCancelButton: false,
                  confirmButtonText: 'Atualizar',
                  allowOutsideClick: false,
                }).then((result) => {
                  if (result.isConfirmed) {
                    window.location.reload(); // Recarrega a página
                  }
                });
              } else if (errorMessage.includes('CONSISTENCIAS NOS DA')) {
                showToast({
                  type: 'error',
                  message:
                    'Erro de consistência nos dados. Verifique as informações e tente novamente.',
                });

                setRestricao(true);
                setCPFInvalid(false);
                setSellerMembershipForm(0);
                setValorRestricao(true);
              } else {
                showToast({
                  type: 'error',
                  message:
                    errorMessage ||
                    'Erro Interno do Servidor. Tente novamente mais tarde.',
                });
              }
              setRestricao(true);
              setCPFInvalid(false);
              setSellerMembershipForm(0);
              setValorRestricao(true);
            } else {
              console.error('Erro na requisição:', error);
              showToast({
                type: 'error',
                message: 'CPF Inválido. Verifique os dados e tente novamente.',
              });
            }

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });
        setLoader(false);
      }
    } else {
      showToast({
        type: 'error',
        message: 'CPF Invalido. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  // Validação Inadimplência Coob+
  async function verificaRestricaoCoob(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }

    try {
      // inadimplência coob+
      apiClei
        .post(
          urlGlobal.concat('/ConsultaCrm.asmx/AssociadoInadimplenciaAPP'),
          { cpf: RetiraMascara(userCpf) },
          { headers: { Authorization: token || '' } }
        )
        .then((response) => {
          if (response.data.body.inadimplente === false) {
            verificaRestricao(userCpf);
            // verificaCliente(userCpf);
            setPlPlanoReativ('');
            setPlCodigoReativ('');
            setPlanoResgatado('');
          }

          if (response.data.body.inadimplente === true) {
            setModalGlobal(true);
            setLoader(false);
            return 0;
          }
        });
    } catch (error) {
      // console.log('Error2: ', error.response);
      showToast({
        type: 'error',
        message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  // validação do CPF fiador
  async function verificaRestricaoFia(userCpf) {
    setLoader(true);
    // Validações de campos obrigatórios
    function RetiraMascara(userCpff) {
      userCpff = userCpff.replace(/[^\d]/g, '');
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    if (verificarCPF(cpf)) {
      try {
        apiClei
          .get(
            urlGlobal.concat(
              `/Usuario.asmx/RestricacaoSerasa?cpf=${RetiraMascara(userCpf)}`
            )
            // .post(
            //   urlGlobal.concat('/ConsultaCrm.asmx/consultaCDLAppV2'),
            //   { cpf: RetiraMascara(userCpf) },
            //   { headers: { Authorization: token || '' } }
            // .get(
            // HML: https://api.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // PROD https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=
            // `https://apiprod.coobrastur.com.br/ApiDotz/api/Usuario/HasRestriction?cpf=${RetiraMascara(
            //   userCpf
            // )}`
          )
          .then((response) => {
            // console.log('RESPONSE: ', response);
            // console.log('PEGAR O DADO: ', response.data.body.restricao);

            if (response.data.restriction === false) {
              showToast({
                type: 'success',
                message: 'Resultado da análise de crédito: Alto Score',
              });
              setRestricaoFia(false);
              setValorRestricao(false);
            }

            if (response.data.restriction === true) {
              showToast({
                type: 'error',
                message: 'Resultado da análise de crédito: Baixo Score.',
              });
              setRestricaoFia(true);
              setValorRestricao(true);
            }

            // if (response.data.body.code === 4) {
            //   showToast({
            //     type: 'error',
            //     message: 'O CPF informado é invalido.',
            //   });
            //   setRestricaoFia(true);
            // }
            setLoader(false);
            return false;
          })
          .catch(() => {
            showToast({
              type: 'error',
              message:
                'Consulta Serasa não retornou score. Tente novamente mais tarde. Se o erro persistir, entre em contato com o suporte',
            });

            setLoader(false);
          });
      } catch (error) {
        // console.log('Error2: ', error.response);
        showToast({
          type: 'error',
          message: 'Algum erro interno aconteceu. Tente novamente mais tarde',
        });
        setLoader(false);
      }
    } else {
      showToast({
        type: 'error',
        message: 'CPF Invalido. Tente novamente mais tarde',
      });
      setLoader(false);
    }
  }

  //trazer nic pelo cpf indicador
  async function fetchNicsByCPF(userCpf) {
    function RetiraMascara(userCpff) {
      // Converte para string (caso seja número ou outro tipo)
      userCpff = String(userCpff).replace(/[^\d]/g, ''); // <--- Conversão explícita aqui
      
      return userCpff.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1$2$3$4');
    }
    console.log(cpfIndic);
    if (cpfIndic.length != 14) {
      showToast({
        type: 'error',
        message: 'Por favor, insira um CPF válido.',
      });
      return;
    }

    try {
      setLoader(true);
      const response = await apiClei.get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/ListaNics?in_pesquisa=${RetiraMascara(cpfIndic)}`
        )
      );
      setNicData(response.data); // Armazena os dados da API
    } catch (error) {
      console.log(error);
      showToast({
        type: 'error',
        message: 'Erro ao consultar CPF. Tente novamente mais tarde.',
      });
    } finally {
      setLoader(false);
    }
  }

  const handleNICChange = (e) => {
    const nicSelecionado = e.target.value;
    setNICSelecionado(nicSelecionado);
    setLoader(true);
    setLoader(false);
  };
  async function asyncEndereco(value) {
    try {
      const res = await apiViacep.get(`${value}/json`);

      // Verificar se há erro na resposta da API
      if (res.data.erro) {
        // Tratar erro de CEP não encontrado
        setRua('');
        setBairroAssociado('');
        setCidade('');
        setUfAssociado('');
        setNumeroAssociado('');
        setCepAssociado('');
        setEnderecoLiberado(true); // Libera os campos mesmo com erro

        console.error('CEP não encontrado.');
        return;
      }

      // Caso a resposta seja válida, extrair os dados
      const { logradouro, bairro, localidade, uf, numero, cep } = res.data;

      // Atualizando os estados com base na resposta da API
      setRua(logradouro || ''); // Mesmo se logradouro for vazio, setar para string vazia
      setBairroAssociado(bairro || '');
      setCidade(localidade || '');
      setUfAssociado(uf || '');
      setNumeroAssociado(numero || '');
      setCepAssociado(cep || '');

      // Liberar os campos de endereço, mesmo que o logradouro esteja vazio
      setEnderecoLiberado(true);
    } catch (error) {
      console.error('Erro ao buscar endereço:', error);

      // Opcional: Se houver erro de rede ou outro erro, redefina os estados
      setRua('');
      setBairroAssociado('');
      setCidade('');
      setUfAssociado('');
      setNumeroAssociado('');
      setCepAssociado('');
      setEnderecoLiberado(true); // Libera os campos mesmo em caso de erro
    }
  }

  const stateArray = [
    { value: '', label: 'Selecione' },
    { value: 'AC', label: 'AC' },
    { value: 'AL', label: 'AL' },
    { value: 'AP', label: 'AP' },
    { value: 'AM', label: 'AM' },
    { value: 'BA', label: 'BA' },
    { value: 'CE', label: 'CE' },
    { value: 'DF', label: 'DF' },
    { value: 'ES', label: 'ES' },
    { value: 'GO', label: 'GO' },
    { value: 'MA', label: 'MA' },
    { value: 'MT', label: 'MT' },
    { value: 'MS', label: 'MS' },
    { value: 'MG', label: 'MG' },
    { value: 'PA', label: 'PA' },
    { value: 'PB', label: 'PB' },
    { value: 'PR', label: 'PR' },
    { value: 'PE', label: 'PE' },
    { value: 'PI', label: 'PI' },
    { value: 'RJ', label: 'RJ' },
    { value: 'RN', label: 'RN' },
    { value: 'RS', label: 'RS' },
    { value: 'RO', label: 'RO' },
    { value: 'RR', label: 'RR' },
    { value: 'SC', label: 'SC' },
    { value: 'SP', label: 'SP' },
    { value: 'SE', label: 'SE' },
    { value: 'TO', label: 'TO' },
  ];

  const size = 51;
  const days = new Array(size);

  /* eslint-disable-next-line no-plusplus */
  for (let i = 2; i < size; i++) {
    days[i] = {
      value: i,
      label: `${i} diárias`,
    };
  }

  // console.table(days);

  // const diariasArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 3, label: '3 diárias' },
  //   { value: 5, label: '5 diárias' },
  //   { value: 7, label: '7 diárias' },
  //   { value: 9, label: '9 diárias' },
  //   { value: 11, label: '11 diárias' },
  //   { value: 13, label: '13 diárias' },
  //   { value: 15, label: '15 diárias' },
  //   { value: 17, label: '17 diárias' },
  //   { value: 19, label: '19 diárias' },
  //   { value: 21, label: '21 diárias' },
  //   { value: 23, label: '23 diárias' },
  //   { value: 25, label: '25 diárias' },
  //   { value: 27, label: '27 diárias' },
  //   { value: 29, label: '29 diárias' },
  //   { value: 31, label: '31 diárias' },
  //   { value: 33, label: '33 diárias' },
  //   { value: 35, label: '35 diárias' },
  //   { value: 37, label: '37 diárias' },
  //   { value: 39, label: '39 diárias' },
  //   { value: 41, label: '41 diárias' },
  //   { value: 43, label: '43 diárias' },
  //   { value: 45, label: '45 diárias' },
  //   { value: 47, label: '47 diárias' },
  //   { value: 49, label: '49 diárias' },
  // ];

  // const diariasGoArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 4, label: '4 diárias' },
  //   { value: 7, label: '7 diárias' },
  //   { value: 9, label: '9 diárias' },
  //   { value: 11, label: '11 diárias' },
  //   { value: 13, label: '13 diárias' },
  //   { value: 15, label: '15 diárias' },
  //   { value: 17, label: '17 diárias' },
  //   { value: 19, label: '19 diárias' },
  //   { value: 21, label: '21 diárias' },
  //   { value: 23, label: '23 diárias' },
  //   { value: 25, label: '25 diárias' },
  //   { value: 27, label: '27 diárias' },
  //   { value: 29, label: '29 diárias' },
  //   { value: 31, label: '31 diárias' },
  //   { value: 33, label: '33 diárias' },
  //   { value: 35, label: '35 diárias' },
  //   { value: 37, label: '37 diárias' },
  //   { value: 39, label: '39 diárias' },
  //   { value: 41, label: '41 diárias' },
  //   { value: 43, label: '43 diárias' },
  //   { value: 45, label: '45 diárias' },
  //   { value: 47, label: '47 diárias' },
  //   { value: 49, label: '49 diárias' },
  // ];

  const planoArray = [
    { value: 36, label: 'Vip +' },
    { value: 37, label: 'Master +' },
    { value: 38, label: 'Gold Vip +' },
    { value: 39, label: 'Gold Master +' },
    { value: 40, label: 'Diamante +' },
    // { value: 41, label: 'Go Vip +' },
    // { value: 42, label: 'Go Master +' },
    // { value: 1001, label: 'Banstur Executivo' },
    { value: 1002, label: 'Banstur Ouro' },
    { value: 1016, label: 'Banstur J3 Multi' },
    { value: 3000, label: 'Meridien Club' },
  ];

  const hasPlano1001OrGreater = planos.some(
    (plano) => plano.tpplcodigo >= 1001
  );
  const hasPlano1001 = planos.some((plano) => plano.tpplcodigo < 1000);

  let filteredPlanoArray;

  if (hasPlano1001OrGreater) {
    filteredPlanoArray = planoArray.filter((option) => option.value >= 1001);
  } else if (hasPlano1001) {
    filteredPlanoArray = planoArray.filter((option) => option.value < 1000);
  } else {
    filteredPlanoArray = planoArray;
  }

  // const campaignArray = [
  //   { value: 0, label: 'Selecione' },
  //   { value: 100, label: 'Isenção 1ª e 2ª mensalidades' },
  // ];
  const bolformaPagamento = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Débito em Conta Corrente' },
    { value: 2, label: 'Cartão de Crédito' },
    { value: 4, label: 'Boleto' },
  ];
  const monthOptions = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Janeiro' },
    { value: 2, label: 'Fevereiro' },
    { value: 3, label: 'Março' },
    { value: 4, label: 'Abril' },
    { value: 5, label: 'Maio' },
    { value: 6, label: 'Junho' },
    { value: 7, label: 'Julho' },
    { value: 8, label: 'Agosto' },
    { value: 9, label: 'Setembro' },
    { value: 10, label: 'Outubro' },
    { value: 11, label: 'Novembro' },
    { value: 12, label: 'Dezembro' },
  ];

  /* eslint-disable no-shadow */
  // const currentMonth = new Date().getMonth() + 1; // Aqui pega o mês atual - de 1 a 12
  /* es-lint-enable */

  let monthTeste = new Date(dataEscolhida).getMonth() + 1;
  // if (parcelasVendedor == 1) {
  //   monthTeste = dataEscolhida.getMonth() + 1;
  // }
  if (parcelasVendedor == 2) {
    monthTeste = dataEscolhida.getMonth() + 2;
  }
  // if (parcelasVendedor == 0) {
  //   monthTeste = dataEscolhida.getMonth()  + 1;
  // }

  function nextThreeMonths(currMonth) {
    let options = [];
    let month = currMonth;

    /* eslint-disable-next-line no-plusplus */
    if (parcelasVendedor == 1) {
      for (let i = 0; i < 2; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }

    if (parcelasVendedor == 2) {
      for (let i = 0; i < 2; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }
    if (parcelasVendedor == 0) {
      for (let i = 0; i < 3; i++) {
        if (month > 12) {
          month = 1;
        }

        options = [
          ...options,
          /* eslint-disable-next-line no-loop-func */
          ...monthOptions.filter((o) => o.value === month),
        ];

        /* eslint-disable-next-line no-plusplus */
        month++;
      }
    }

    return options;
  }

  // eslint-disable-next-line no-unused-vars
  async function onSelectMonth(carMes, debMes) {
    const date = new Date();
    // Calcular o último dia do mês com base no mês escolhido e ajuste automático para o ano correto.
    const selectedMonth = carMes || debMes; // Usa carMes ou debMes conforme estiver presente
    const selectedYear =
      date.getMonth() + 1 > selectedMonth
        ? date.getFullYear() + 1
        : date.getFullYear();
    const lastDay = new Date(selectedYear, selectedMonth, 0);

    const arrayDays = [];

    for (let i = 1; i <= lastDay.getDate(); i++) {
      arrayDays.push(i);
    }
    setArrayDays(arrayDays);
  }

  console.log('array days', arrayDays);

  function validDay(dia) {
    if (!dataEscolhida) {
      return false;
    }

    const dataAtual = new Date(dataEscolhida);
    const data = new Date();
    const dataLimit = new Date();

    // Define o limite de dias baseado nas parcelas
    if (parcelasVendedor === 0) {
      dataLimit.setDate(dataAtual.getDate() + 100);
    } else {
      dataLimit.setDate(dataAtual.getDate() + 125);
    }

    // Ajusta o ano e o mês para carMes e debMes
    if (
      dataAtual.getFullYear() == 2024 &&
      (carMes == 1 || carMes == 2 || debMes == 1 || debMes == 2)
    ) {
      data.setFullYear(dataAtual.getFullYear() + 1);
      data.setMonth((carMes || debMes) - 1, dia);
    } else if (formaPagamento == '2') {
      data.setFullYear(dataAtual.getFullYear());
      data.setMonth(carMes - 1, dia);
    } else {
      data.setFullYear(dataAtual.getFullYear());
      data.setMonth(debMes - 1, dia);
    }

    // Valida se a data está fora do limite permitido
    if (data <= dataAtual || data > dataLimit) {
      return false;
    }

    // console.log('data', data);
    console.log('data atual', dataAtual.getFullYear() == 2024);
    return true;
  }

  const filterDays = arrayDays.filter((option) => {
    return validDay(option);
  });

  console.log(filterDays);
  // async function adesaoComDesconto() {
  //   const resultado = planos.map((plano, i) => ({
  //     ...plano,
  //     valorAdesao:
  //       parseFloat(planos[i].valorAdesaoOriginal) -
  //       parseFloat(planos[i].valorAdesaoOriginal) * descontoPorcento,
  //   }));

  //   // console.table(resultado);
  //   setPlanos(resultado);
  // }
  // const estadoCivilArray = [
  //   { value: '', label: 'Selecione' },
  //   { value: 2, label: 'Casado(a)' },
  //   { value: 1, label: 'Solteiro(a)' },
  //   { value: 3, label: 'Divorciado(a)' },
  //   { value: 4, label: 'Viúvo(a)' },
  // ];

  const formaPagamentoAdesaoVendedorArray = [
    { value: 1, label: 'Dinheiro' },
    { value: 2, label: 'Cartão' },
    // { value: '3', label: 'Cheque' },
    { value: 4, label: 'Boleto' },
  ];
  // const cupomdesconto = [
  //   { value: 1, label: 'primeiro' },
  //   { value: 2, label: 'segundo' },
  //   // { value: '3', label: 'Cheque' },
  //   { value: 4, label: 'terceiro' },
  // ];

  const parcelasPagamentoAdesaoArray = [
    { value: 1, label: 'À vista' },
    { value: 2, label: '2x' },
  ];

  const parcelasPagamentoAdesaoVendedorArray = [
    // { value: '', label: 'Selecione' },
    { value: 1, label: 'À vista' },
    { value: 2, label: '2x' },
  ];

  const formaPagamentoAdesaoCoobrasturArray = [
    { value: '', label: 'Selecione' },
    { value: 1, label: 'Boleto' },
  ];

  if (location.search) {
    useEffect(() => {
      allowScroll();
      // console.log(location.pathname); // result: '/secondpage'
      // console.log(location.search); // result: '?query=abc'
      // console.log(location.state.nome);
      // console.log(location.state.cpf);
      // console.log(location.state.email);
      // console.log(location.state.numeroCelDDD);
      // console.log(location.state.cep);

      setNome(location.state.nome);
      setCPF(location.state.cpf);
      // console.log(location.state.cpf);
      if (location.state.cpf && checaCPF !== true) {
        // console.log('foi');
        setTimeout(() => {
          setChecaCPF(true);
          // setFormaPagamento('');
          verificaCliente(location.state.cpf);
          verificaRestricaoCoob(location.state.cpf);
          verificaPlanosInativos(location.state.cpf);
          // setChecaCPF(location.state.cpf);
          // verificaRestricaoCoob(cpf);
          // verificaPlanosInativos(cpf);
        }, 1000);
      }
      setEmail(location.state.email);
      setConfirmaEmail(location.state.email);
      setTelefone(
        `(${location.state.numeroCelDDD})${location.state.numeroCel}`
      );
      setEnderecoLiberado(true);
      setCepAssociado(location.state.cep);
      setRua(location.state.rua);
      setBairroAssociado(location.state.bairro);
      setNumeroAssociado(location.state.numeroCasa);
      setComplemento(location.state.complemento);
      setCidade(location.state.cidade);
      setUfAssociado(location.state.estado);
    }, [location]);
  }
  /* eslint-disable react/jsx-no-bind */

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />
      <ModalGlobal setModalGlobal={setModalGlobal} modalGlobal={modalGlobal} />;
      <ModalReativacao
        onClose={() => setModalReativacao(false)}
        onSend={() => {
          setTypeOfContract('11');
          setModalReativacao(false);
          // setCampaign(true);
          // setCampaignValue(100);

          // Swal.fire({
          //   showCloseButton: true,
          //   title: '<p></p>',
          //   text: `Cliente está participando da campanha "Isenção 1 e 2 mensalidades"`,
          //   confirmButtonText: 'Confirmar',
          //   confirmButtonColor: '#01affd',
          // });
        }}
        setModalReativacao={ModalReativacao}
        modalReativacao={modalReativacao}
      />
      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>
      {/* {console.log(adhesion)} */}
      <div className="container">
        <h2>Novo Contrato</h2>
        <Content>
          <FormContent ref={formRef} onSubmit={handleFormSubmit}>
            <BlockInputs>
              {dadosEmpresa !== true && (
                <h3>
                  Empresa{' '}
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosEmpresa(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosEmpresa === true && (
                <div className="form">
                  <h3>
                    Empresa{' '}
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosEmpresa(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  <Row className="rowContract">
                    <Col md={4}>
                      <label style={{ fontWeight: '700' }}>
                        Empresa Selecionada:
                      </label>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="contract.companySelect"
                          disabled={planos.some((plano) => plano.tpplcodigo)}
                          value={selectedCompany}
                          style={{
                            backgroundColor: planos.some(
                              (plano) => plano.tpplcodigo
                            )
                              ? '#f2f2f2'
                              : 'inherit',
                            // Outros estilos aqui
                          }}
                          onChange={(e) => {
                            const selectedEmpcod = company.find(
                              (empresa) =>
                                empresa.empDescTela === e.target.value
                            )?.empCodigo;
                            // if (codCompany) {
                            //   const selectEmpCod = company.find(
                            //     (empresa) =>
                            //       empresa.empNomefantasia === e.target.value
                            //   )?.empCodigo;
                            // }
                            setCPF('');
                            setSelectedCompany(e.target.value);
                            setCodCompany(selectedEmpcod);

                            if (codCompany == 60) {
                              setAdhesion(false);
                            }
                          }}
                        >
                          <option value="">Selecione</option>

                          {company
                            .filter((empresa) => {
                              // Obter os parâmetros do localStorage
                              const podeVenderCOOB =
                                localStorage.getItem('podeVenderCOOB') ===
                                'true';
                              const podeVenderBNS =
                                localStorage.getItem('podeVenderBNS') ===
                                'true';
                              const podeVenderMRD =
                                localStorage.getItem('podeVenderMRD') ===
                                'true';

                              // Filtrar com base nos parâmetros
                              if (
                                podeVenderCOOB &&
                                empresa.empDescTela === 'Coob+'
                              ) {
                                return true;
                              }

                              if (
                                podeVenderBNS &&
                                empresa.empDescTela === 'Banstur'
                              ) {
                                return true;
                              }

                              if (
                                podeVenderMRD &&
                                empresa.empDescTela === 'Meridien'
                              ) {
                                return true;
                              }

                              return false;
                            })
                            .map((empresa, index) => (
                              <option key={index} value={empresa.empDescTela}>
                                {empresa.empDescTela}
                              </option>
                            ))}
                        </select>
                      </label>
                    </Col>
                  </Row>
                </div>
              )}
            </BlockInputs>

            <BlockInputs>
              {dadosPessoais !== true && (
                <h3>
                  Dados Pessoais
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosPessoais(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}
              {dadosPessoais === true && (
                <div className="form">
                  <h3>
                    Dados Pessoais
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosPessoais(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                  <Row className="rowContract">
                    <Col md={3}>
                      <label>CPF:</label>
                      <InputMask
                        id="contract.cpf"
                        name="assCPF_CNPJ"
                        placeholder="CPF"
                        mask="999.999.999-99"
                        type="text"
                        value={cpf}
                        onPaste={() => {
                          //   e.preventDefault();
                          //   return false;

                          if (
                            cpf.length === 14 &&
                            !cpf.includes('_') &&
                            checaCPF !== true
                          ) {
                            // setAdhesion(false);
                            setChecaCPF(true);
                            setFormaPagamento('');

                            // verificaCliente(cpf);
                            verificaRestricaoCoob(cpf);
                            verificaPlanosInativos(cpf);
                          }

                          if (checaCPF) {
                            setRestricao(false);
                            setChecaCPF(false);
                          }
                        }}
                        onChange={(e) => {
                          setCPF(e.target.value);
                        }}
                        onKeyUp={() => {
                          if (
                            cpf.length === 14 &&
                            !cpf.includes('_') &&
                            checaCPF !== true
                          ) {
                            // setAdhesion(false);
                            setChecaCPF(true);
                            setFormaPagamento('');
                            // verificaCliente(cpf);
                            verificaRestricaoCoob(cpf);
                            verificaPlanosInativos(cpf);
                          }

                          if (cpf.includes('_') && checaCPF) {
                            setRestricao(false);
                            setChecaCPF(false);
                          }
                        }}
                      />
                    </Col>
                    <Col md={9}>
                      <label>Nome:</label>
                      <SimpleInput
                        id="contract.name"
                        name="assNome_RazaoSocial"
                        placeholder="Nome Completo"
                        type="text"
                        value={nome}
                        onChange={(e) => setNome(e.target.value)}
                        onBlur={(e) => {
                          const trimmedValue = e.target.value.trimEnd();
                          setNome(trimmedValue);
                        }}
                      />
                    </Col>
                  </Row>
                  <Row className="rowContract">
                    <Col md={6}>
                      <label>E-mail:</label>
                      <SimpleInput
                        id="contract.email"
                        name="assEmailPessoal"
                        placeholder="E-mail"
                        type="text"
                        value={email}
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </Col>
                    <Col md={6}>
                      <label>Confirmar E-mail:</label>
                      <SimpleInput
                        id="contract.confirmEmail"
                        name="assConfirmaEmailPessoal"
                        placeholder="E-mail"
                        type="text"
                        value={confirmaEmail}
                        onChange={(e) => setConfirmaEmail(e.target.value)}
                        onPaste={(e) => {
                          e.preventDefault();
                          return false;
                        }}
                        onCopy={(e) => {
                          e.preventDefault();
                          return false;
                        }}
                      />
                    </Col>
                  </Row>
                  <Row className="rowContract">
                    <Col md={6}>
                      <label>Número do Celular:</label>
                      <InputMask
                        id="contract.phone"
                        name="assNumCelular"
                        placeholder="Número de celular"
                        mask="(99)99999-9999"
                        type="text"
                        value={telefone}
                        onChange={(e) => setTelefone(e.target.value)}
                      />
                    </Col>

                    <Col md={6}>
                      <label>Data de Nascimento:</label>
                      <InputMask
                        id="contract.birth"
                        name="assBirth"
                        mask="99/99/9999"
                        // maskChar={null}
                        // type="number"
                        value={birthDate}
                        onChange={handleDateChange}
                        placeholder="dd/mm/aaaa"
                      />
                    </Col>
                  </Row>
                  <Row
                    className="row Contract"
                    style={{ marginBottom: '-30px' }}
                  >
                    <Col md={3}>
                      <label>CEP:</label>
                      <InputMask
                        id="contract.cep"
                        name="endCepAssociado"
                        placeholder="CEP"
                        mask="99999-999"
                        type="text"
                        value={cepAssociado}
                        onChange={(e) => {
                          const newCep = e.target.value;
                          setCepAssociado(newCep);

                          // Quando o CEP for digitado, busca o endereço
                          if (newCep.length === 9 && !newCep.includes('_')) {
                            asyncEndereco(newCep);
                          }

                          // Se o CEP for apagado ou incompleto, libere os campos novamente
                          if (newCep.length < 9) {
                            setEnderecoLiberado(false); // Desativa os campos novamente
                            setChecaCEP(false); // Reseta o estado de verificação de CEP
                          }
                        }}
                        onKeyUp={() => {
                          if (
                            cepAssociado.length === 9 && // Verifica se o CEP está completo
                            !cepAssociado.includes('_') &&
                            !checaCEP
                          ) {
                            setChecaCEP(true);
                            // asyncEndereco(cepAssociado);
                          }

                          // Se o CEP for incompleto, desativa o estado de verificação de CEP
                          if (
                            cepAssociado.length < 9 ||
                            cepAssociado.includes('_')
                          ) {
                            setChecaCEP(false);
                            setEnderecoLiberado(false); // Libera os campos para edição novamente
                            setRua('');
                            setBairroAssociado('');
                            setCidade('');
                            setUfAssociado('');
                            setNumeroAssociado('');
                            setComplemento('');
                          }
                        }}
                      />
                    </Col>
                  </Row>
                  {enderecoLiberado && (
                    <>
                      <Row className="rowContract">
                        <Col md={7}>
                          <label>Endereço:</label>
                          <SimpleInput
                            id="contract.addres"
                            name="endLogradouroAssociado"
                            placeholder="Endereço"
                            type="text"
                            value={rua}
                            onChange={(e) => setRua(e.target.value)}
                            disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                          />
                        </Col>
                        <Col md={5}>
                          <label>Bairro:</label>
                          <SimpleInput
                            id="contract.neighbor"
                            name="endBairroAssociado"
                            placeholder="Bairro"
                            type="text"
                            value={bairroAssociado}
                            onChange={(e) => setBairroAssociado(e.target.value)}
                            disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                          />
                        </Col>
                      </Row>
                      <Row
                        className="rowContract"
                        style={{ marginBottom: '-100px' }}
                      >
                        <Col md={2}>
                          <label>Número:</label>
                          <SimpleInput
                            id="contract.number"
                            name="endNumLogradouroAssociado"
                            placeholder="Número"
                            type="number"
                            value={numeroAssociado}
                            onChange={(e) => setNumeroAssociado(e.target.value)}
                            disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                          />
                        </Col>
                        <Col md={4}>
                          <label>Complemento:</label>
                          <SimpleInput
                            id="contract.complement"
                            name="endComplementoAssociado"
                            placeholder="Complemento"
                            type="text"
                            value={complemento}
                            onChange={(e) => setComplemento(e.target.value)}
                            disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                          />
                        </Col>
                        <Col md={4}>
                          <label>Cidade:</label>
                          <SimpleInput
                            id="contract.city"
                            name="endCidadeAssociado"
                            placeholder="Cidade"
                            type="text"
                            value={cidade}
                            onChange={(e) => setCidade(e.target.value)}
                            disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                          />
                        </Col>
                        <Col md={2}>
                          <label>Estado:</label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="contract.state"
                              value={ufAssociado}
                              onChange={(e) => setUfAssociado(e.target.value)}
                              disabled={!enderecoLiberado} // Desabilita o campo quando o endereço não está liberado
                            >
                              {stateArray.map((option) => (
                                <option key={option.value} value={option.value}>
                                  {option.label}
                                </option>
                              ))}
                            </select>
                          </label>
                        </Col>
                      </Row>
                    </>
                  )}
                </div>
              )}
            </BlockInputs>

            {restricao === true && (
              <BlockInputs>
                {/* Bloco de restrição fechado */}
                {dadosRestricao !== true && (
                  <h3>
                    Área de Baixo Score
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosRestricao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosRestricao === true && (
                  // Bloco de restrição fechado
                  <div className="form">
                    <h3>
                      Área de Baixo Score
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosRestricao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>
                    <RadioInputs>
                      <Row className="rowContract">
                        {/* <Col md="1">
                          <div className="inline">
                            <div className="items">
                              <input
                                type="radio"
                                name="restricao"
                                value="Sim"
                                onClick={(e) => {
                                  setValorRestricao(e.target.value);
                                  setRestricao(true);
                                }}
                              />
                              Sim
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                name="restricao"
                                value="Não"
                                onClick={(e) => {
                                  setRestricao(false);
                                  setValorRestricao(e.target.value);
                                  setFiador(false);
                                  setAditamento(false);
                                }}
                              />
                              Não
                            </div>
                          </div>
                        </Col> */}
                        {restricao === true && (
                          <Col md="3">
                            <RadioInputs>
                              <div className="inline">
                                {codCompany == 38 && (
                                  <div className="items">
                                    <input
                                      id="contract.fiador"
                                      type="radio"
                                      value
                                      name="alternative"
                                      onClick={() => {
                                        setFiador(true);
                                        setAditamento(false);
                                        setParcelasPrePagas('0');
                                        setPrePago(false);
                                      }}
                                    />
                                    Fiador
                                  </div>
                                )}
                                {/* {codCompany != 58 && ( */}
                                <div className="items">
                                  <input
                                    id="contract.prepago"
                                    type="radio"
                                    value={false}
                                    name="alternative"
                                    onClick={() => {
                                      // setAditamento(true);
                                      setPrePago(true);
                                      setParcelasPrePagas('9999');
                                      setFiador(false);
                                    }}
                                  />
                                  Pré-Pago
                                </div>
                                {/* )} */}
                              </div>
                            </RadioInputs>
                          </Col>
                        )}
                      </Row>
                    </RadioInputs>

                    {fiador === true && restricao === true && (
                      <div className="form">
                        <Row className="rowContract">
                          <Col md={3}>
                            <label>CPF do Fiador:</label>
                            <InputMask
                              id="contract.fiadorCPF"
                              name="fiaCpf"
                              placeholder="CPF do Fiador"
                              mask="999.999.999-99"
                              type="text"
                              value={cpfFia}
                              onChange={(e) => {
                                setCpfFia(e.target.value);
                                setRestricaoFia(true); // Definindo restrição como verdadeira
                              }}
                              onKeyUp={() => {
                                if (
                                  cpfFia.length > 13 &&
                                  !cpfFia.includes('_') &&
                                  checaCPFFia !== true
                                ) {
                                  setChecaCPFFia(true);
                                  verificaRestricaoFia(cpfFia);
                                }

                                if (
                                  cpfFia.includes('_') &&
                                  checaCPFFia === true
                                ) {
                                  // setRestricaoFia(false);
                                  setChecaCPFFia(false);
                                }
                              }}
                            />
                          </Col>
                        </Row>
                        {restricaoFia === false && (
                          <div>
                            <Row className="rowContract">
                              <Col md={12}>
                                <label>Nome do Fiador:</label>
                                <SimpleInput
                                  id="contract.fiadorName"
                                  name="fianome"
                                  placeholder="Nome do Fiador"
                                  type="text"
                                  value={nomeFia}
                                  onChange={(e) => setNomeFia(e.target.value)}
                                />
                              </Col>
                            </Row>
                            <Row className="rowContract">
                              <Col md={6}>
                                <label>Nome da Mãe do Fiador:</label>
                                <SimpleInput
                                  id="contract.fiadorMname"
                                  name="fiaConjugeFiliacao"
                                  placeholder="Nome da Mãe do Fiador"
                                  type="text"
                                  value={maeFia}
                                  onChange={(e) => setMaeFia(e.target.value)}
                                />
                              </Col>
                              <Col md={6}>
                                <label>Celular do Fiador:</label>
                                <InputMask
                                  id="contract.fiadorPhone"
                                  name="fiaNumeroCelular"
                                  placeholder="Celular do Fiador(com o DDD)"
                                  mask="(99)99999-9999"
                                  type="text"
                                  value={celularFia}
                                  onChange={(e) =>
                                    setCelularFia(e.target.value)
                                  }
                                />
                              </Col>
                            </Row>
                            <Row className="rowContract">
                              <Col md={6}>
                                <label>E-mail do Fiador:</label>
                                <SimpleInput
                                  id="contract.fiadorEmail"
                                  name="fiaEmailPessoal"
                                  placeholder="E-mail do Fiador"
                                  type="email"
                                  value={emailFia}
                                  onChange={(e) => setEmailFia(e.target.value)}
                                />
                              </Col>
                              <Col md={6}>
                                <label>Confirmar E-mail do Fiador:</label>
                                <InputMask
                                  id="contract.fiadorConfirmEmail"
                                  name="confirmaEmailFiador"
                                  placeholder="E-mail do Fiador"
                                  type="email"
                                  value={confirmaEmailFiador}
                                  onChange={(e) =>
                                    setConfirmaEmailFiador(e.target.value)
                                  }
                                  onPaste={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                  onCopy={(e) => {
                                    e.preventDefault();
                                    return false;
                                  }}
                                />
                              </Col>
                            </Row>
                          </div>
                        )}
                      </div>
                    )}

                    {/* {aditamento === true && (
                      <div>
                        <label>Forma de aditamento:</label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            value={select}
                            onChange={(e) => {
                              setSelect(e.target.value);
                              setFormaPagamento('');
                              setDataBoletoEscolhida('');

                              // if (e.target.value === '2') {
                              //   setParcelasPrePagas(0);
                              //   // console.log('foi', parcelasPrePagas);
                              // }
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            <option value="1">AD12</option>
                            {/* <option value="2">Pré-Pago</option> */}
                    {/* <option value="2">Imediato</option> }
                          </select>
                        </label>
                      </div>
                    )} */}

                    {codCompany != 58 && (
                      <p>
                        Caso o associado tenha inadimplência com a Coob+ ou
                        baixo score, deverá constar a opção de pré-pago.
                      </p>
                    )}
                    {codCompany == 58 && (
                      <p>
                        Caso o associado tenha inadimplência com a Coob+ ou
                        baixo score, deverá constar a opção de fiador ou
                        pré-pago.
                      </p>
                    )}
                  </div>
                )}
              </BlockInputs>
            )}
            {typeOfContract != 11 && (
              <BlockInputs>
                {dadosIndicacao !== true && (
                  <h3>
                    Indicação
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosIndicacao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosIndicacao === true && (
                  <div className="form" style={{ marginBottom: '-20px' }}>
                    <h3>
                      Indicação
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosIndicacao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>
                    <RadioInputs>
                      <div className="inline">
                        <div className="items">
                          <input
                            id="contract.indicationYes"
                            type="radio"
                            value
                            name="indicador"
                            onClick={
                              ((e) => setIndicador(e.target.value),
                              () => setIndicador(true))
                            }
                          />
                          Sim
                        </div>

                        <div className="items">
                          <input
                            id="contract.indicationNo"
                            type="radio"
                            value={false}
                            name="indicador"
                            onClick={
                              (() => setIndicador(false),
                              (e) => setIndicador(e.target.value))
                            }
                          />
                          Não
                        </div>
                      </div>
                    </RadioInputs>
                    {indicador === true && (
                      <div className="form">
                        <Row className="rowContract">
                          <Col md={7}>
                            <label>CPF do Indicador:</label>
                            <InputMask
                              id="contract.indicatorCPF"
                              name="cpfIndicador"
                              placeholder="CPF Indicador"
                              mask="999.999.999-99"
                              type="text"
                              value={cpfIndic}
                              onChange={(e) => setCpfIndic(e.target.value)}
                            />
                          </Col>
                          <Col md={5}>
                            <Button
                              className="buttonAddNic"
                              style={{ marginTop: '22px' }}
                              type="button"
                              onClick={fetchNicsByCPF}
                            >
                              Consultar
                            </Button>
                          </Col>
                        </Row>

                        {nicData.length > 0 && (
                          <Row>
                            <Col md={12}>
                              <label>NIC:</label>
                              <label htmlFor="select" className="input-select">
                                <select
                                  className="input-select"
                                  id="contract.nic"
                                  value={nicSelecionado}
                                  onChange={handleNICChange}
                                >
                                  <option value="">Selecione um NIC</option>
                                  {nicData.map((item) => (
                                    <option
                                      key={item.assnic}
                                      value={item.assnic}
                                    >
                                      {item.assnic}
                                    </option>
                                  ))}
                                </select>
                              </label>
                            </Col>
                          </Row>
                        )}
                        {nicSelecionado && (
                          <Row>
                            <Col md={6}>
                              <label>Nome do Indicador:</label>
                              <SimpleInput
                                id="contract.indicatorName"
                                name="nomeIndicador"
                                placeholder="Nome do Indicador"
                                type="text"
                                value={nomeIndic}
                                onChange={(e) => setNomeIndic(e.target.value)}
                              />
                            </Col>
                            <Col md={6}>
                              <label>E-mail do Indicador: (Opcional)</label>
                              <SimpleInput
                                id="contract.indicatorEmail"
                                name="emailIndicador"
                                placeholder="E-mail Indicador"
                                type="email"
                                value={emailIndic}
                                onChange={(e) => setEmailIndic(e.target.value)}
                              />
                            </Col>
                            <Col md={6}>
                              <label>Celular do Indicador: (Opcional)</label>
                              <InputMask
                                id="contract.indicatorCel"
                                name="celularIndicador"
                                placeholder="Celular do Indicador (Com o DDD)"
                                mask="(99)99999-9999"
                                type="text"
                                value={celularIndic}
                                onChange={(e) =>
                                  setCelularIndic(e.target.value)
                                }
                              />
                            </Col>
                          </Row>
                        )}
                      </div>
                    )}
                  </div>
                )}
              </BlockInputs>
            )}

            {selectedCompany !== '' && (
              <BlockInputs>
                {dadosAssociacao !== true && (
                  <h3>
                    Opções de Assinaturas
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosAssociacao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosAssociacao === true && (
                  <div className="form">
                    <h3>
                      Opções de Assinaturas
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosAssociacao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>

                    <Row className="rowContract">
                      <RadioInputs>
                        <div className="inline">
                          <div className="items">
                            <input
                              type="radio"
                              value
                              checked={typeOfContract === '1'}
                              disabled={typeOfContract === '11'}
                              name="associacao"
                              onClick={() => setTypeOfContract('1')}
                            />
                            Normal
                          </div>

                          <div className="items">
                            <input
                              type="radio"
                              value={false}
                              disabled
                              name="associacao"
                              checked={typeOfContract === '11'}
                              onClick={() => {
                                setTypeOfContract('11');
                                setAdhesion(false);
                                verificaPlanosInativos(cpf);
                              }}
                            />
                            Reativação
                          </div>
                          {/* <div className="items">
                          <input
                            type="radio"
                            value={false}
                            name="associacao"
                            onClick={() => {
                              setTypeOfContract('3');
                              verificaPlanosInativos(cpf);
                            }}
                          />
                          Substituição
                        </div> */}
                        </div>
                      </RadioInputs>
                    </Row>

                    {typeOfContract === '11' && (
                      <Row className="rowContract">
                        <Col md={4}>
                          <label>Plano Resgatado</label>
                          <SimpleInput
                            disabled
                            style={{ backgroundColor: '#DADADA' }}
                            name="planoResgato"
                            placeholder="Plano Resgatado"
                            type="text"
                            value={planoResgatado}
                            onChange={(e) => setPlanoResgatado(e.target.value)}
                          />
                        </Col>
                      </Row>
                    )}

                    {typeOfContract === '3' && (
                      <Row className="rowContract">
                        <Col md={4}>
                          <label>Número Contrato Substituido</label>
                          <SimpleInput
                            disabled
                            name="planoSubstituido"
                            placeholder="Plano Substituido"
                            type="text"
                            value={planoSubstituido}
                            onChange={(e) =>
                              setPlanoSubstituido(e.target.value)
                            }
                          />
                        </Col>
                      </Row>
                    )}
                    <Row
                      className="rowContract"
                      style={{ marginBottom: '-20px' }}
                    >
                      <Col md={5}>
                        <label style={{ fontWeight: '700' }}>
                          Código PipeDrive
                        </label>
                        <SimpleInput
                          id="contract.pipeline"
                          name="assCod_pipeline"
                          placeholder="Código Pipedrive"
                          type="number"
                          value={codigoPipeline}
                          onChange={(e) => setCodigoPipeline(e.target.value)}
                        />
                      </Col>
                    </Row>
                    <Row
                      className="rowContract"
                      style={{ marginBottom: '-20px' }}
                    >
                      <Col md={3}>
                        <label
                          style={{ fontWeight: '700', marginBottom: '-53px' }}
                        >
                          Plano{' '}
                          {flat === '' && (
                            <Tooltip
                              iconColor="#909090"
                              text="selecione um plano"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '36' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: Ano todo"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '37' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: 15/03 à 15/12"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '38' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: Ano todo"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '39' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: 15/03 à 15/12"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '40' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                           Tipo de hotéis: Superior Luxo ou Resorts
                           Validade: Ano todo"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat === '41' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: Ano todo"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {flat >= '1001' && (
                            <Tooltip
                              iconColor="#909090"
                              text="Plano selecionado"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {/* {flat === '1002' && (
                          <Tooltip
                            iconColor="#909090"
                            text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: Ano todo"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )} */}
                          {flat === '42' && (
                            <Tooltip
                              iconColor="#909090"
                              text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: 15/03 à 15/12"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            name="flat"
                            value={flat}
                            onChange={(e) => {
                              // console.log(flat);
                              setFlat(e.target.value);
                              // setAdhesion(true);
                              if (e.target.value <= 40) {
                                // setAdhesion(false);
                                setDailyQuantities(false);
                                setFamily('');
                                setValorMensalidade('0,00');
                                setSellerMembershipForm(0);
                                setOfferApply(false);
                                setOffer(false);
                                setValorAdesao('0,00');
                                setDescontoAdesao('0,00');
                                setFinalAdesao('0,00');
                              }
                              if (
                                e.target.value <= 40 &&
                                dailyQuantities === '4'
                              ) {
                                setDailyQuantities(false);
                              }
                              if (e.target.value > 40) {
                                // setAdhesion(false);
                                setDailyQuantities(false);
                                setFamily(false);
                                setSellerMembershipForm(0);
                                setValorMensalidade('0,00');
                                setOfferApply(false);
                                setOffer(false);
                                setValorAdesao('0,00');
                                setDescontoAdesao('0,00');
                                setFinalAdesao('0,00');
                                // setAdhesion(true);
                              }
                              if (
                                e.target.value >= 1001 &&
                                e.target.value < 1999
                              ) {
                                // setAdhesion(false);
                                setDailyQuantities(false);
                                setFamily(true);
                                setSellerMembershipForm(0);
                                setValorMensalidade('0,00');
                                setOfferApply(false);
                                setOffer(false);
                                setValorAdesao('0,00');
                                setDescontoAdesao('0,00');
                                setFinalAdesao('0,00');
                              }
                              if (e.target.value >= 2000) {
                                setAdhesion(false);
                                setFamily(false);
                                setValorAdesao('0,00');
                                setDescontoAdesao('0,00');
                                setFinalAdesao('0,00');
                              }
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            {filteredPlanoArray
                              .filter((option) => {
                                return (
                                  (codCompany === 38 && option.value < 999) ||
                                  (codCompany === 58 &&
                                    option.value > 1000 &&
                                    option.value < 1999) ||
                                  (codCompany === 60 && option.value > 2000)
                                );
                              })
                              .map((option) => (
                                <option key={option.value} value={option.value}>
                                  {option.label}
                                </option>
                              ))}
                          </select>
                        </label>
                      </Col>
                      <Col md={3}>
                        <label
                          style={{ fontWeight: '700', marginBottom: '-53px' }}
                        >
                          Diárias{' '}
                          {dailyQuantities === '' && (
                            <Tooltip
                              iconColor="#909090"
                              text="selecione um dia"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                          {dailyQuantities !== '' && (
                            <Tooltip
                              iconColor="#909090"
                              text="Diária escolhida"
                              color="#fff"
                              backgroundColor="#909090"
                            />
                          )}
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="select"
                            name="dailyQuantities"
                            value={dailyQuantities}
                            onChange={(e) => {
                              setDailyQuantities(e.target.value);
                              setPayment(1);
                            }}
                          >
                            {' '}
                            <option defaultValue>Selecione</option>
                            {flat >= 41
                              ? days
                                  .filter((option) =>
                                    flat >= 1001
                                      ? option.label == '7 diárias'
                                      : true
                                  )
                                  .map((option) => (
                                    <option
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </option>
                                  ))
                              : days.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                          </select>
                        </label>
                      </Col>
                      {flat < 41 && (
                        <Col md={3}>
                          <RadioInputs>
                            <label
                              className="rowFamily"
                              style={{ fontWeight: '700' }}
                            >
                              Família
                              {family === '' && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="quantidades de hóspedes"
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === true && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano familía pode incluir até 3 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === false && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano pode incluir até 2 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                            </label>
                            <div className="inlineFamily">
                              <div className="items">
                                <input
                                  type="radio"
                                  value="1"
                                  name="family"
                                  checked={family === true}
                                  onClick={() => {
                                    setFamily(true);

                                    if (typeOfContract == '1') {
                                      setAdhesion(true);
                                    }
                                    // setAdhesion(true);
                                    // valuesAdesao();
                                  }}
                                />
                                Sim
                              </div>

                              <div className="items">
                                <input
                                  type="radio"
                                  value="2"
                                  name="family"
                                  checked={family === false}
                                  onClick={() => {
                                    setFamily(false);
                                    // setAdhesion(true);
                                    if (typeOfContract == '1') {
                                      setAdhesion(true);
                                    }
                                    // valuesAdesao();
                                  }}
                                />
                                Não
                              </div>
                            </div>
                          </RadioInputs>
                        </Col>
                      )}
                      {flat >= 41 && (
                        <Col md={3}>
                          <RadioInputs>
                            <label
                              className="rowFamily"
                              style={{ fontWeight: '700' }}
                            >
                              Família
                              {family === '' && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="quantidades de hóspedes"
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === true && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano família pode incluir até 3 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                              {family === false && (
                                <Tooltip
                                  iconColor="#909090"
                                  text="O plano pode incluir até 2 hóspedes."
                                  color="#fff"
                                  backgroundColor="#909090"
                                />
                              )}
                            </label>
                            <div className="inlineFamily">
                              <div className="items">
                                <input
                                  type="radio"
                                  value="true"
                                  name="family"
                                  disabled
                                  checked={family === true}
                                  onChange={() => {
                                    setFamily(true);
                                    //   // setAdhesion(true);
                                    //   // valuesAdesao();
                                  }}
                                />
                                Sim
                              </div>

                              <div className="items">
                                <input
                                  type="radio"
                                  value="false"
                                  name="family"
                                  disabled
                                  checked={family === false}
                                  onChange={() => {
                                    setFamily(false);
                                    // setAdhesion(true);
                                    // valuesAdesao();
                                  }}
                                />
                                Não
                              </div>
                            </div>
                          </RadioInputs>
                        </Col>
                      )}

                      {/* <Col md={3}>
                      <button
                        type="button"
                        style={{
                          color: 'white',
                          backgroundColor: '#01affd',
                          fontWeight: '700',
                          marginTop: '15px',
                        }}
                        onClick={() => {
                          if (flat == '' || dailyQuantities == '') {
                            showToast({
                              type: 'error',
                              message: 'Preencha os dados do plano.',
                            });

                            setLoader(false);
                            return false;
                          }
                          if (family === '') {
                            showToast({
                              type: 'error',
                              message: 'Preencha os dados do plano.',
                            });

                            setLoader(false);
                            return false;
                          }
                          if (typeOfContract == '') {
                            showToast({
                              type: 'error',
                              message: 'Escolha um tipo de assinatura.',
                            });

                            setLoader(false);
                            return false;
                          }

                          if (
                            descontoAdesao > valorAdesao &&
                            valorAdesao !== ''
                          ) {
                            showToast({
                              type: 'error',
                              message:
                                'Valor do Desconto não pode ser maior que adesão.',
                            });

                            setLoader(false);
                            return false;
                          }
                          setCheckOffer(false);
                          setOffer(false);
                          setOfferApply(false);
                          addItem();
                        }}
                      >
                        Adicionar
                      </button>
                    </Col> */}
                    </Row>
                    <Col style={{ marginBottom: '25px' }}>
                      <div className="adesaoValues">
                        <label>Valor da Mensalidade:</label>
                        <span style={{ marginLeft: '5%' }}>
                          R$ {finalValue || '0,00'}
                        </span>
                        {/* <SimpleInput
                              name="valorAdesao"
                              type="number"
                              maxLength="3400"
                              value={valorAdesao}
                              onChange={(e) => {
                                setValorAdesao(e.target.value);
                              }}
                            /> */}
                      </div>
                    </Col>
                    {/* iniciar validação aqui */}
                    {typeOfContract !== '11' && (
                      <Col>
                        <div className="adesaoValues">
                          <label>Valor da Adesão:</label>

                          <span style={{ marginLeft: '8.5%' }}>
                            R${' '}
                            {valorAdesao.toLocaleString('pt-br', {
                              minimumFractionDigits: 2,
                            }) || '0,00'}
                          </span>
                        </div>
                      </Col>
                    )}

                    <Row className="rowContract">
                      <div
                        className="rowAdhesion"
                        style={{ paddingTop: '50px' }}
                      >
                        {typeOfContract !== '11' && codCompany !== 60 && (
                          <div>
                            <Col>
                              <div className="checkbox">
                                <input
                                  type="checkbox"
                                  checked={checkOffer}
                                  style={{ marginRight: 10 }}
                                  // checked={infoTurma.includes(item.turma)}
                                  onChange={(e) => {
                                    if (e.target.checked) {
                                      setCheckOffer(true);
                                      setOffer(true);
                                      setDescontoAdesao('0,00');
                                      setFinalAdesao('0,00');
                                      setOfferApply(false);
                                    } else {
                                      setCheckOffer(false);
                                      setOffer(false);
                                      setOfferApply(false);
                                      setDescontoAdesao('0,00');
                                      setFinalAdesao('0,00');

                                      // const resultado = planos.map((plano, i) => ({
                                      //   ...plano,
                                      //   valorAdesao: parseFloat(
                                      //     planos[i].valorAdesaoOriginal
                                      //   ),
                                      // }));
                                      // setPlanos(resultado);
                                    }
                                  }}
                                />{' '}
                                <label>Oferecer Desconto na Adesão</label>
                              </div>
                            </Col>

                            {offer === false && (
                              <div className="buttonDiscount">
                                <SimpleInput
                                  id="nodescontoAdesao"
                                  name="assDescontoAdesao"
                                  placeholder="Digite o valor do desconto"
                                  type="text"
                                  disabled
                                  style={{
                                    width: '30%',
                                    marginLeft: 10,
                                    cursor: 'not-allowed',
                                  }}
                                />
                                <ButtonDisabled
                                  // type="push"
                                  disabled
                                  style={{
                                    // width: '20%',
                                    marginLeft: '5px',
                                    fontFamily: 'Tahoma',

                                    textTransform: 'uppercase',
                                    padding: '10px',
                                    cursor: 'not-allowed',
                                  }}
                                >
                                  {' '}
                                  Aplicar Desconto
                                </ButtonDisabled>
                              </div>
                            )}
                            {offer !== false && (
                              <div className="buttonDiscount">
                                <InputCurrency
                                  id="descontoAdesao"
                                  // name="assDescontoAdesao"
                                  placeholder="Digite o valor do desconto"
                                  type="text"
                                  // value={descontoAdesao}
                                  onChange={(e) => {
                                    parseFloat(
                                      setDescontoAdesao(
                                        e.target.value.substring(2)
                                      )
                                    );

                                    setOfferApply(false);
                                    // const resultado = planos.map((plano, i) => ({
                                    //   ...plano,
                                    //   valorAdesao: parseFloat(
                                    //     planos[i].valorAdesaoOriginal
                                    //   ),
                                    // }));
                                    // setPlanos(resultado);
                                  }}
                                />
                                <ButtonDisabled
                                  // type="push"
                                  style={{
                                    // width: '40%',
                                    marginLeft: '5px',
                                    fontFamily: 'Tahoma',
                                    // fontSize: '18px',
                                    textTransform: 'uppercase',
                                    padding: '10px',
                                    // marginBottom: '15px',
                                    backgroundColor: '#01affd',
                                    // cursor: 'pointer',
                                  }}
                                  onClick={() => {
                                    // debugger;
                                    if (
                                      descontoAdesao > valorAdesao &&
                                      valorAdesao !== ''
                                    ) {
                                      showToast({
                                        type: 'error',
                                        message:
                                          'Valor do Desconto não pode ser maior que adesão.',
                                      });

                                      setOfferApply(false);
                                    } else {
                                      setOfferApply(true);
                                    }
                                    if (
                                      flat === '' ||
                                      dailyQuantities === '' ||
                                      family === ''
                                    ) {
                                      showToast({
                                        type: 'error',
                                        message:
                                          'Escolha um plano para oferecer desconto na adesão. O desconto é individual para cada plano escolhido. ',
                                      });

                                      setOfferApply(false);
                                    }

                                    if (descontoAdesao == '0,00') {
                                      setOfferApply(false);
                                    }
                                    if (descontoAdesao == valorAdesao) {
                                      setCheckOffer(false);
                                      setOffer(false);
                                      // setOfferApply(false);
                                      // setAdFormaPagamento(false);
                                      // setAdFormaPagamento(false);
                                      setSellerMembershipForm(0);
                                      setAdhesion(false);
                                      // setDataEscolhida(null);
                                      // const data = new Date();
                                      // data.setMonth(data.getMonth() - 1);
                                      // setDataEscolhida(data);
                                    } else {
                                      setAdFormaPagamento(true);
                                      // setAdhesion(true);
                                      // const amanha = new Date();
                                      // amanha.setDate(amanha.getDate() + 1);
                                      // setDataEscolhida(amanha);
                                    }
                                    if (
                                      valorAdesao !== '' &&
                                      descontoAdesao !== ''
                                    ) {
                                      const resultado =
                                        valorAdesao -
                                        descontoAdesao.replace('.', '');
                                      // console.log('valorAdesao', valorAdesao);
                                      // console.log('descontoadesao', descontoAdesao);

                                      setFinalAdesao(resultado);
                                      // console.log(finalAdesao);
                                    }
                                    // console.log(finalAdesao);
                                    // adesaoComDesconto();
                                  }}
                                >
                                  Aplicar Desconto
                                </ButtonDisabled>
                              </div>
                            )}
                            {offerApply === true && (
                              <div
                                className="paymentAdesao"
                                style={{
                                  marginLeft: 10,
                                  marginTop: '-100px',
                                }}
                              >
                                <Row className="rowAdesao">
                                  <Col
                                    className="adesaoValues"
                                    style={{ marginTop: 20 }}
                                  >
                                    <label style={{ marginRight: 67 }}>
                                      Desconto da Adesão
                                    </label>
                                    <span>
                                      R${' '}
                                      {descontoAdesaoFinal.toLocaleString(
                                        'pt-br',
                                        {
                                          minimumFractionDigits: 2,
                                        }
                                      )}
                                    </span>
                                  </Col>
                                </Row>

                                <Row className="rowAdesao">
                                  <Col className="adesaoValues">
                                    <label>Total da Adesão</label>
                                    <span
                                      id="adesaoTotal"
                                      style={{ marginLeft: 107 }}
                                    >
                                      R${' '}
                                      {totalAdesao.toLocaleString('pt-br', {
                                        minimumFractionDigits: 2,
                                      })}
                                    </span>
                                  </Col>
                                </Row>
                              </div>
                            )}
                          </div>
                        )}
                        <div style={{ alignSelf: 'center' }}>
                          <Col md={3}>
                            <button
                              className="buttonAdd"
                              type="button"
                              // style={{ width: 100 }}
                              onClick={() => {
                                if (flat == '' || dailyQuantities == '') {
                                  showToast({
                                    type: 'error',
                                    message: 'Preencha os dados do plano.',
                                  });

                                  setLoader(false);
                                  return false;
                                }
                                if (family === '') {
                                  showToast({
                                    type: 'error',
                                    message: 'Preencha os dados do plano.',
                                  });

                                  setLoader(false);
                                  return false;
                                }
                                if (typeOfContract == '') {
                                  showToast({
                                    type: 'error',
                                    message: 'Escolha um tipo de assinatura.',
                                  });

                                  setLoader(false);
                                  return false;
                                }

                                if (
                                  descontoAdesao > valorAdesao &&
                                  valorAdesao !== ''
                                ) {
                                  showToast({
                                    type: 'error',
                                    message:
                                      'Valor do Desconto não pode ser maior que adesão.',
                                  });

                                  setLoader(false);
                                  return false;
                                }

                                setCheckOffer(false);
                                setOffer(false);
                                setOfferApply(false);
                                addItem();
                                // setAdhesion(true);
                                // console.table(planos);
                              }}
                            >
                              Adicionar Plano
                            </button>
                          </Col>
                        </div>
                      </div>
                    </Row>

                    <div style={{ marginTop: '20px', marginBottom: '-20px' }}>
                      <h3>Planos Selecionados</h3>
                    </div>

                    {/* {planos.map((planos) => ( */}
                    <Table
                      plan={planos}
                      remove={removeitem}
                      // parcelas={parcelasPrePagas}
                      tipo={typeOfContract}
                    />
                    {/* ))} */}
                    {/* <Row>
                    <Col md={2}>
                      <div style={{ marginBottom: '-20px' }}>
                        <h6>Planos</h6>
                        {planos.map((planos) => (
                          <p key={planos.id}>{planos.tpplcodigo}</p>
                        ))}
                      </div>
                    </Col>
                    <Col md={2}>
                      <div style={{ marginBottom: '-20px' }}>
                        <h6>Diárias</h6>
                        {planos.map((planos) => (
                          <p key={planos.id}>{planos.plQtdeDiarias} diárias</p>
                        ))}
                      </div>
                    </Col>
                    <Col md={1}>
                      <div style={{ marginBottom: '-20px' }}>
                        <h6>Tipo</h6>
                        {planos.map((planos) => (
                          <p key={planos.id}>{planos.tipoVenda}</p>
                        ))}
                      </div>
                    </Col>
                    <Col md={2}>
                      <div style={{ marginBottom: '-20px' }}>
                        <h6>Família</h6>
                        {planos.map((planos) => (
                          <p key={planos.id}>{planos.plfamilia}</p>
                        ))}
                      </div>
                    </Col>
                    <Col md={2}>
                      <div style={{ marginBottom: '-20px' }}>
                        <h6>Valor Adesão</h6>
                        {planos.map((planos) => (
                          <p key={planos.id}>R$ {}</p>
                        ))}
                      </div>
                    </Col>
                    <Col md={2}>
                      <h6>Valor Mensalidade</h6>
                      <div style={{ marginBottom: '-20px' }}>
                        {planos.map((planos) => (
                          <div
                            style={{
                              marginBottom: '-55px',
                              display: 'flex',
                              flexDirection: 'row',
                            }}
                          >
                            <p key={planos.id} style={{ marginRight: '10px' }}>
                              R$ {planos.valorM}{' '}
                            </p>
                            <TooltipLess
                              text="excluir plano"
                              color="#fff"
                              backgroundColor="#FF0000"
                              remove={() => removeitem(planos.id)}
                            />
                          </div>
                        ))}
                      </div>
                    </Col>
                  </Row> */}
                    {/* {family === true && <p>O plano inclui 3 hóspedes.</p>}
                  {family === false && <p>O plano inclui 2 hóspedes.</p>} */}
                    {/* {typeOfContract === '2' && setAdhesion(false)} */}

                    {/* {adhesion === true && typeOfContract === '1' && !family && ( */}
                    {valoresAdesao !== true && (
                      <div>
                        {/* <Row className="rowContract">
                        <Col md={3}>
                          <div className="inline">
                            <label>
                              Adesão
                              <Tooltip
                                text="Selecione a opção de quem irá receber a adesão."
                                color="#fff"
                                backgroundColor="#909090"
                              />
                            </label>
                          </div>
                        </Col>
                      </Row> 
                      <Row className="rowVenda">
                        <Col md={4}>
                          <div className="inline">
                            <div className="items">
                              <input
                                type="radio"
                                value="1"
                                name="payment"
                                onClick={(e) => setPayment(e.target.value)}
                              />
                              Vendedor
                            </div>

                            <div className="items">
                              <input
                                type="radio"
                                value="2"
                                name="payment"
                                onClick={(e) => setPayment(e.target.value)}
                              />
                              Coob+
                            </div>
                          </div>
                        </Col>
                      </Row> */}
                      </div>
                    )}
                  </div>
                )}
              </BlockInputs>
            )}
            {isValid && (
              <BlockInputs>
                <h3>Parceiros/Convênios</h3>
                <Row
                  style={{
                    marginBottom: '-55px',
                    marginTop: '50px',
                    // marginLeft: 5,
                  }}
                >
                  <Col>
                    <label style={{ fontWeight: 700 }}>Parceiros</label>
                    <label htmlFor="select" className="input-select">
                      <select
                        id="select"
                        value={vendNomeSelecionado}
                        onChange={handleVendNomeChange}
                      >
                        <option defaultValue>Selecione</option>
                        {vendNomes.map((vendNome) => (
                          <option key={vendNome} value={vendNome}>
                            {vendNome}
                          </option>
                        ))}
                      </select>
                    </label>
                  </Col>
                </Row>
                <Row
                  style={{
                    marginBottom: '-25px',
                    marginTop: '30px',
                    // marginLeft: 5,
                  }}
                >
                  <Col>
                    <label style={{ fontWeight: 700 }}>Cupom de Desconto</label>
                    <label htmlFor="select" className="input-select">
                      <select
                        id="select"
                        value={cupNomeSelecionado}
                        onChange={handleCupNomeChange}
                      >
                        <option value="">Selecione</option>
                        {cupNomes.map((cupNome) => (
                          <option key={cupNome} value={cupNome}>
                            {cupNome}
                          </option>
                        ))}
                      </select>
                    </label>
                  </Col>
                </Row>

                {/* <div style={{ alignSelf: 'center' }}> */}
                <Row style={{ alignSelf: 'center' }}>
                  <Col md={3}>
                    <button
                      className={`buttonParceiro ${
                        cupNomeSelecionado == 'Selecione' ||
                        cupCodigoSelecionado == ''
                          ? 'buttonParceiro-disabled'
                          : ''
                      }`}
                      type="button"
                      onClick={() => {
                        updateItem();
                      }}
                      // disabled={vendNomeSelecionado == 'Selecione'}
                    >
                      Aplicar Cupom
                    </button>
                  </Col>
                </Row>
                {/* </div> */}
              </BlockInputs>
            )}
            {/* {console.log(totalAdesaoTable)} */}
            {valoresAdesao === true && totalAdesaoTable != 0 && (
              <BlockInputs>
                {dadosAdesao !== true && (
                  <h3>
                    Forma de Pagamento da Adesão
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosAdesao(true)}
                    >
                      <img src={arrowDown} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>
                )}
                {dadosAdesao === true && (
                  <div className="form">
                    <h3>
                      Forma de Pagamento da Adesão
                      <ButtonDivisor
                        type="push"
                        onClick={() => setDadosAdesao(false)}
                      >
                        <img src={arrowUP} alt="" width="100%" />
                      </ButtonDivisor>
                    </h3>

                    <Row className="rowContract">
                      <div
                        className="rowAdhesion"
                        style={{ paddingTop: '30px' }}
                      >
                        {/* <Col>
                          <div className="adesaoValues">
                            <label>Valor total da mensalidade:</label>

                            <span style={{ marginLeft: '5%' }}>
                              R${' '}
                              {totalMensal.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </div>
                        </Col> */}

                        <Col>
                          <div className="adesaoValues">
                            <label>Valor Total da Adesão:</label>

                            <span
                              style={{ marginLeft: '8.8%' }}
                              id="contact.totalAD"
                            >
                              R${' '}
                              {totalAdesaoTable.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </div>
                        </Col>
                      </div>
                      {adFormaPagamento === true && (
                        <div style={{ marginLeft: 10, marginTop: '2npm 0px' }}>
                          <Row
                            style={{
                              marginBottom: '-25px',
                              marginTop: '50px',
                              // marginLeft: 5,
                            }}
                          >
                            <Col>
                              <label style={{ fontWeight: 700 }}>
                                Forma de Pagamento da Adesão
                              </label>
                              <label htmlFor="select" className="input-select">
                                <select
                                  id="contract.paymentAD"
                                  value={sellerMembershipForm}
                                  onChange={(e) => {
                                    setSellerMembershipForm(e.target.value);
                                    setFormaPagamento('');
                                    setParcelasVendedor('');
                                    setDataEscolhida(tomorrow);
                                    // console.log(sellerMembershipForm);
                                  }}
                                >
                                  <option defaultValue>Selecione</option>
                                  {getFilteredOptions(
                                    formaPagamentoAdesaoVendedorArray
                                  )}
                                </select>
                              </label>
                            </Col>
                          </Row>
                          <div style={{ marginBottom: '-80px' }}>
                            <Row>
                              <Col>
                                <label style={{ fontWeight: 700 }}>
                                  Parcelas da Adesão
                                </label>
                                <label
                                  htmlFor="select"
                                  className="input-select"
                                >
                                  <select
                                    id="contract.parcAD"
                                    value={parcelasVendedor}
                                    onChange={(e) => {
                                      setParcelasVendedor(e.target.value);
                                      setFormaPagamento('');
                                      setDataEscolhida(tomorrow);
                                    }}
                                  >
                                    <option defaultValue>Selecione</option>

                                    {parcelasPagamentoAdesaoVendedorArray
                                      .filter((option) =>
                                        sellerMembershipForm == 1
                                          ? option.value == 1
                                          : true
                                      )
                                      .map((option) => (
                                        <option
                                          key={option.value}
                                          value={option.value}
                                        >
                                          {option.label}
                                        </option>
                                      ))}
                                  </select>
                                </label>
                              </Col>
                            </Row>
                          </div>

                          {parcelasVendedor == '1' && (
                            <Row style={{ marginBottom: '-200px' }}>
                              <Col>
                                <div>
                                  <label
                                    style={{
                                      fontSize: '16px',
                                      fontWeight: 700,
                                    }}
                                  >
                                    {' '}
                                    Data da Primeira Adesão{' '}
                                  </label>
                                  <DatePickers style={{ marginLeft: '-10px' }}>
                                    <DatePicker
                                      className="daterange"
                                      selected={dataEscolhida}
                                      onChange={(date) => {
                                        setDataEscolhida(date);
                                        setFormaPagamento('');
                                        // setIsPayable('');
                                        setCarMes('');
                                        setCarDia('');
                                        setDebDia('');
                                        setDebMes('');
                                      }}
                                      includeDateIntervals={[
                                        {
                                          start: subDays(new Date(), 0),
                                          end: addDays(new Date(), 60),
                                        },
                                      ]}
                                      placeholderText="dd-mm-yyyy"
                                      locale={pt}
                                      dateFormat="P"
                                    />
                                  </DatePickers>
                                </div>
                              </Col>
                            </Row>
                          )}
                          {parcelasVendedor == '2' && (
                            <Row style={{ marginBottom: '-200px' }}>
                              <Col>
                                <div>
                                  <label
                                    style={{
                                      fontSize: '16px',
                                      fontWeight: 700,
                                    }}
                                  >
                                    {' '}
                                    Data da Primeira Adesão{' '}
                                  </label>
                                  <DatePickers style={{ marginLeft: '-10px' }}>
                                    <DatePicker
                                      className="daterange"
                                      selected={dataEscolhida}
                                      onChange={(date) => {
                                        setDataEscolhida(date);
                                        setFormaPagamento('');
                                        // setIsPayable('');
                                        setCarMes('');
                                        setCarDia('');
                                        setDebDia('');
                                        setDebMes('');
                                      }}
                                      includeDateIntervals={[
                                        {
                                          start: subDays(new Date(), 0),
                                          end: addDays(new Date(), 40),
                                        },
                                      ]}
                                      placeholderText="dd-mm-yyyy"
                                      locale={pt}
                                      dateFormat="P"
                                    />
                                  </DatePickers>
                                </div>
                              </Col>
                            </Row>
                          )}
                        </div>
                      )}
                      {payment === '2' && (
                        <>
                          <Col md={3}>
                            <label>Forma de pagamento:</label>
                            <label htmlFor="select" className="input-select">
                              <select
                                id="select"
                                value={halfAdhesion}
                                onChange={(e) => {
                                  setHalfAdhesion(e.target.value);
                                  setDataEscolhida('');
                                }}
                              >
                                {formaPagamentoAdesaoCoobrasturArray.map(
                                  (option) => (
                                    <option
                                      key={option.value}
                                      value={option.value}
                                    >
                                      {option.label}
                                    </option>
                                  )
                                )}
                              </select>
                            </label>
                          </Col>
                          <Col md={3}>
                            <label>Parcelas:</label>
                            <label htmlFor="select" className="input-select">
                              <select
                                id="select"
                                value={parcelasCoobrastur}
                                onChange={(e) =>
                                  setParcelasCoobrastur(e.target.value)
                                }
                              >
                                {parcelasPagamentoAdesaoArray.map((option) => (
                                  <option
                                    key={option.value}
                                    value={option.value}
                                  >
                                    {option.label}
                                  </option>
                                ))}
                              </select>
                            </label>
                          </Col>
                          <Col md={3}>
                            <label>Data da cobrança:</label>
                            <InputMask
                              name="dataCoobrastur"
                              placeholder="Data de Cobrança"
                              mask="99/99/9999"
                              type="text"
                              value={dataAdesao}
                              onChange={(e) => setDataAdesao(e.target.value)}
                            />
                          </Col>
                          <Col md={12}>
                            <label>Observação:</label>
                            <SimpleInput
                              name="obsCoobrastur"
                              placeholder="Observação(opcional)"
                              type="text"
                            />
                          </Col>
                        </>
                      )}
                    </Row>

                    {adhesion === false && offerApply === true && (
                      <div
                        className="paymentAdesao"
                        style={{
                          marginLeft: 10,
                          marginTop: '30px',
                        }}
                      >
                        {/* <Row
                          className="rowAdesao"
                          style={{ marginTop: '10px' }}
                        >
                          <Col className="adesaoValues">
                            <label>Subtotal da Adesão</label>
                            <span style={{ marginLeft: 80 }}>
                              R${' '}
                              {valorAdesao.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row> */}

                        <Row className="rowAdesao">
                          <Col className="adesaoValues">
                            <label style={{ marginRight: 67 }}>
                              Descontos da Adesão
                            </label>
                            <span>
                              R${' '}
                              {descontoAdesaoFinal.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row>

                        <Row className="rowAdesao">
                          <Col className="adesaoValues">
                            <label>Total da Adesão</label>
                            <span id="adesaoTotal" style={{ marginLeft: 107 }}>
                              R${' '}
                              {totalAdesao.toLocaleString('pt-br', {
                                minimumFractionDigits: 2,
                              })}
                            </span>
                          </Col>
                        </Row>
                      </div>
                    )}
                  </div>
                )}
              </BlockInputs>
            )}
            {/* <BlockInputs>
              <h3>Campanhas</h3>
              {campaign === true && (
                <Row className="rowContract">
                  <Col md={4}>
                    <label
                      style={{
                        fontWeight: '700',
                        marginLeft: '3px',
                        marginBottom: '3px',
                      }}
                    />

                    <label htmlFor="select" className="input-select-campaign">
                      <select
                        id="select"
                        disabled
                        value={campaingValue}
                        // onChange={(e) => setCampaignValue(e.target.value)}
                      >
                        {' '}
                        {campaignArray.map((option) => (
                          <option key={option.value} value={option.value}>
                            {option.label}
                          </option>
                        ))}
                      </select>
                    </label>
                  </Col>
                </Row>
              )}
              {campaign !== true && (
                <Row className="rowContract">
                  <Col md={4}>
                    <h6 className="noCampaign">
                      O usuário não se enquadra em nenhuma das campanhas
                      vigentes.
                    </h6>
                  </Col>
                </Row>
              )}
            </BlockInputs> */}
            <BlockInputs>
              {dadosBoleto !== true && (
                <h3>
                  Forma de Pagamento da Mensalidade
                  <ButtonDivisor
                    type="push"
                    onClick={() => setDadosBoleto(true)}
                  >
                    <img src={arrowDown} alt="" width="100%" />
                  </ButtonDivisor>
                </h3>
              )}

              {dadosBoleto === true && (
                <div className="form">
                  <h3>
                    Forma de Pagamento da Mensalidade
                    <ButtonDivisor
                      type="push"
                      onClick={() => setDadosBoleto(false)}
                    >
                      <img src={arrowUP} alt="" width="100%" />
                    </ButtonDivisor>
                  </h3>

                  <Row className="rowContract">
                    <Col md={4}>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="contract.paymentForm"
                          value={formaPagamento}
                          onClick={() => buscaDataPagamento()}
                          onChange={(e) => {
                            setDataBoletoEscolhida('');
                            setFormaPagamento(e.target.value);
                            // setIsPayable('');
                            setCarMes('');
                            setCarDia('');
                            setDebDia('');
                            setDebMes('');
                            // console.log(formaPagamento);
                          }}
                        >
                          {bolformaPagamento
                            .filter(
                              (option) =>
                                // !(hasPlano1001OrGreater && option.value === 2)
                                !(codCompany === 58 && option.value === 2)
                            )
                            .map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                        </select>
                      </label>
                    </Col>
                  </Row>

                  {formaPagamento == 2 && (
                    <div className="carStyle" style={{ marginBottom: '-90px' }}>
                      <div style={{ marginRight: '20px' }}>
                        <label style={{ fontWeight: '700' }}>
                          Mês da Cobrança
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="contract.creditMonth"
                            value={carMes}
                            onChange={(e) => {
                              setCarMes(e.target.value);
                              onSelectMonth(e.target.value);
                              setCarDia('');
                              // setIsPayable('');
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            {nextThreeMonths(monthTeste).map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                          </select>{' '}
                        </label>
                      </div>
                      {carMes && (
                        <div>
                          <label style={{ fontWeight: '700' }}>
                            Dia da Cobrança
                          </label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="contract.creditDay"
                              value={carDia}
                              onChange={(e) => {
                                setCarDia(e.target.value);
                                validaParcelas(e.target.value);
                              }}
                            >
                              <option defaultValue>Selecione</option>
                              {filterDays.map((option) => (
                                <option
                                  // disabled={validDay(option)}
                                  key={option}
                                  value={option}
                                >
                                  {option}
                                </option>
                              ))}
                            </select>
                          </label>
                        </div>
                      )}
                    </div>
                  )}
                  {formaPagamento == 1 && (
                    <div className="carStyle" style={{ marginBottom: '-90px' }}>
                      <div style={{ marginRight: '20px' }}>
                        <label style={{ fontWeight: '700' }}>
                          Mês da Cobrança
                        </label>
                        <label htmlFor="select" className="input-select">
                          <select
                            id="contract.debitMonth"
                            value={debMes}
                            onChange={(e) => {
                              setDebMes(e.target.value);
                              onSelectMonth(e.target.value);
                              setDebDia('');
                              // setIsPayable('');
                            }}
                          >
                            <option defaultValue>Selecione</option>
                            {nextThreeMonths(monthTeste).map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))}
                          </select>{' '}
                        </label>
                      </div>
                      {debMes && (
                        <div>
                          <label style={{ fontWeight: '700' }}>
                            Dia da Cobrança
                          </label>
                          <label htmlFor="select" className="input-select">
                            <select
                              id="contract.debitDay"
                              value={debDia}
                              onChange={(e) => {
                                setDebDia(e.target.value);
                                validaParcelas(e.target.value);
                              }}
                            >
                              <option defaultValue>Selecione</option>
                              {filterDays
                                .filter((option) =>
                                  formaPagamento == 1
                                    ? option == 5 ||
                                      option == 10 ||
                                      option == 15 ||
                                      option == 20 ||
                                      // option == 25 ||
                                      option == 30
                                    : true
                                )
                                .map((option) => (
                                  <option key={option} value={option}>
                                    {option}
                                  </option>
                                ))}
                            </select>
                          </label>
                        </div>
                      )}
                    </div>
                  )}

                  {formaPagamento == 4 && (
                    <div style={{ marginTop: '15px', marginBottom: '0px' }}>
                      <label>Data de Cobrança Escolhida</label>
                      <label htmlFor="select" className="input-select">
                        <select
                          id="contract.bol"
                          value={dataBoletoEscolhida}
                          onChange={(e) => {
                            setDataBoletoEscolhida(e.target.value);
                            validaParcelas(e.target.value);
                            setBoletoMonth(e.target.value);
                          }}
                        >
                          {Array.isArray(datasPagamento) &&
                            datasPagamento.map((data) => (
                              <option
                                key={data.dataExibicao}
                                value={data.dataReal}
                              >
                                {data.dataExibicao}
                              </option>
                            ))}
                        </select>
                      </label>
                    </div>
                  )}
                  {/* {console.log(dataEscolhida.getMonth() + 1)} */}
                  {sellerMembershipForm != 0 &&
                    dataEscolhida.getMonth() + 1 ==
                      (carMes || debMes || monthBoleto) && (
                      <span style={{ color: '#01affd', marginTop: 45 }}>
                        {' '}
                        <strong>
                          {' '}
                          Caso você tenha escolhido o mesmo mês de adesão como o
                          mês de cobrança da mensalidade, por favor, ignore esta
                          mensagem.{' '}
                        </strong>
                      </span>
                    )}
                  {/* {console.log('data adesao', dataEscolhida.getMonth() + 1)}
                  {console.log(
                    'data boleto',
                    dataEscolhida.getMonth() + 1 ==
                      (carMes || debMes || monthBoleto)
                  )} */}
                  {/* {dataEscolhida} */}
                  {/* {isPayable == '0' &&
                    (localStorage.getItem('repreCodigo').startsWith(13) ||
                      localStorage.getItem('repreCodigo').startsWith(22)) && (
                      <label
                        style={{
                          color: 'red',
                          fontSize: '20px',
                          fontWeight: 700,
                        }}
                      >
                        {' '}
                        A Adesão ficará para o dia {primeiraAdesao}{' '}
                      </label>
                    )} */}
                  {/* {typeOfContract !== '11' &&
                    (dataBoletoEscolhida || carDia || debDia) &&
                    finalAdesao !== 0 && (
                      <div style={{ marginTop: '-20px' }}>
                        <label
                          style={{
                            fontSize: '16px',
                            fontWeight: 700,
                          }}
                        >
                          {' '}
                          Data da Primeira Adesão{' '}
                        </label>
                        <SimpleInput
                          name="valorAdesao"
                          type="number"
                          maxLength="3400"
                          disabled
                          placeholder={primeiraAdesao}
                          style={{ width: '30%' }}
                        />
                      </div>
                    )}
                  {isPayable == '0' && finalAdesao !== 0 && (
                    <div style={{ marginTop: '-20px' }}>
                      {(localStorage.getItem('repreCodigo').startsWith(13) ||
                        localStorage.getItem('repreCodigo').startsWith(22)) && (
                        <p style={{ color: 'red', fontWeight: 700 }}>
                          Data de cobrança da adesão inválida!
                        </p>
                      )}
                      <p style={{ color: 'red', fontWeight: 700 }}>
                        Verifique os parâmetros de Pagamento da mensalidade.
                      </p>
                    </div>
                  )} */}
                  {/*  retornar data primeira adesão

                  "A adesão ficará para o dia ....."

                  if ( isPayable == 0 && repreCod 13 || 22) {
                    showToast error
                    data da primeira cobrança da primeira mensalidade não pode ser inferior
                     a última adesão
                  }


                  resultado desconto == valor adesão { adesão == false}; 
                   */}
                </div>
              )}
            </BlockInputs>

            {desabilitado === false && <div />}
            <Button
              id="contract.btnConcl"
              type="submit"
              // onClick={() => {
              //   updateItem();
              // setAdhesion(false);
              // if (prepago === true) {
              //   setTypeOfContract('2');
              //   const resultado = planos.map((plano) => ({
              //     ...plano,
              //     tipoVenda: plano.typeOfContract == 1 ? 2 : typeOfContract,
              //   }));
              //   setPlanos(resultado);
              // }
              // }}
            >
              {' '}
              Concluir
            </Button>

            {isModalVisible ? (
              <Modal
                onClose={() => {
                  setIsModalVisible(false);
                  setIsSendModalClick(false);
                  // setDailyQuantities('');
                  allowScroll();
                }}
                onSend={() => {
                  setIsSendModalClick(true);
                  allowScroll();
                }}
                dados={dadosCliente}
                associacao={dAssociacao}
                dFiador={dadosFia}
              />
            ) : null}
          </FormContent>
        </Content>
      </div>
    </Container>
  );
}
