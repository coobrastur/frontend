import styled from 'styled-components';
import { Form } from '@unform/web';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  color: #000000;

  padding: 50px 0;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }

  input {
    color: #000000;
  }

  #nodescontoAdesao {
    background-color: #9d9d9c;
    // padding: 5px;
  }

  #descontoAdesao {
    border: 1px solid #c4c4c4;
    box-sizing: border-box;
    border-radius: 6px;

    color: rgba(0, 0, 0, 0.3);

    font-size: 18px;

    margin-bottom: 20px;
    padding: 10px;

    ::placeholder {
      color: rgba(0, 0, 0, 0.3);
    }
  }

  input::placeholder {
    color: rgba(0, 0, 0, 0.3);
  }

  @media (max-width: 1100px) {
    h2 {
      text-align: center;
    }
  }
  @media (max-width: 500px) {
    #nodescontoAdesao {
      width: 90% !important;
    }
  }
`;

export const Content = styled.div`
  border-radius: 10px;
  width: 100%;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }

  h3 {
    color: #01affd;
    font-size: 1.8em;
    margin-bottom: 10px;
  }

  p {
    color: rgba(0, 0, 0, 0.4);
  }

  .rowContract {
    margin: 0;
    margin-bottom: -7%;
  }
  .checkbox {
    display: flex;
    flex-direction: row;
    // background-color: #ccff00;
  }
  .rowAdesao {
    // margin: 0;
    margin-bottom: -5%;
    // background-color: #ccff00;
  }

  h6 {
    @media (min-width: 1080px) {
      font-size: 14px;
      width: 200%;
    }
    @media (max-width: 1080px) {
      font-size: 14px;
      width: 100%;
    }
  }

  .rowFamily {
    margin-left: -20%;
    margin-bottom: -7%;
  }

  .plusFlat {
    color: #01affd;
    font-weight: 700;
    font-size: 26px;
    line-height: 2;
    margin-left: 5px;
    transition: font-size 0.3s;
  }
  .plusFlat:hover {
    cursor: pointer;
    font-size: 28px;
    // line-height: 2.4;
  }
  .rowAdhesion {
    width: 100%;
    margin-top: -5%;
    margin-bottom: -10%;

    // display: flex;
    // align-items: normal;
    // justify-content: flex-start;
    // flex-flow: row wrap;

    // margin-top: -4%;
    // margin-bottom: -20%;

    // background-color: #ccff00;
  }

  .adesaoValues {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    // margin-top: 2%;
    // background-color: #ccff00;

    span {
      // margin-left: 100px;
      font-size: 16px;
      font-weight: 700;
    }
  }
  .rowOb {
    margin-top: -5%;
  }
  .rowVenda {
    margin-top: -20%;
    margin-bottom: -10%;
  }

  .carStyle {
    display: flex;
    flex-direction: row;
    width: 50%;

    margin-top: 50px;
    margin-bottom: 0px;

    @media (max-width: 881px) {
      flex-direction: column;
    }
  }

  .input-select select {
    border: 1px solid #c4c4c4;
    width: 100%;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;
    padding: 9px 0px 9px 2px;
  }

  .input-select-campaign select {
    border: 1px solid #c4c4c4;
    width: 100%;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;
    padding: 9px 0px 9px 10px;
    -webkit-appearance: none;
    -moz-appearance: none;

    @media (min-width: 1080px) {
      width: 115%;
    }
  }

  .daterange {
    z-index: 0;

    border: 1px solid #c4c4c4;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;

    @media (min-width: 1081px) {
      box-sizing: border-box;
      font-size: 16px;
      width: 100%;
      height: 15px;
      margin: 10px;
      padding: 10px;
      height: 30px;
      border-radius: 5px;
    }
    @media (max-width: 1081px) {
      width: 95%;
      height: 30px;
      margin: 10px;
      padding: 10px;
      border-radius: 5px;
    }
  }
  .daterange_Birth {
    z-index: 0;

    border: 1px solid #c4c4c4;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;

    @media (min-width: 1081px) {
      box-sizing: border-box;
      font-size: 16px;
      width: 100%;
      height: 15px;
      margin: 0px 10px;
      padding: 23px;
      border-radius: 5px;
    }
    @media (max-width: 1081px) {
      width: 95%;
      height: 30px;
      margin: 10px;
      padding: 10px;
      border-radius: 5px;
    }
  }

  .buttonDiscount {
    // margin-bottom: 100px;
    display: flex;
    flex-direction: row;
    align-items: baseline;

    @media (max-width: 1100px) {
      flex-direction: column;
    }
  }

  .buttonAdd {
    color: white;
    background-color: #01affd;
    font-weight: 700;

    font-family: 'Livvic', sans-serif;
    font-size: 1.4em;

    margin-top: 17px;
    width: 75%;
    height: 7vh;

    border-radius: 10px;
  }

  .buttonAdd {
    color: white;
    background-color: #01affd;
    font-weight: 700;

    font-family: 'Livvic', sans-serif;
    font-size: 1.4em;

    width: 40vw;
    height: 20vh;

    border-radius: 10px;
  }
  .buttonParceiro {
    color: white;
    background-color: #01affd;
    font-weight: 700;
    cursor: pointer;

    font-family: 'Livvic', sans-serif;
    font-size: 1.4em;

    width: 20vw;
    height: 10vh;

    border-radius: 10px;
    margin-top: 10px;

    :hover {
      background-color: #0c92ce;
    }
  }

  .buttonParceiro-disabled {
    color: white;
    background-color: #9d9d9c;
    font-weight: 700;
    cursor: not-allowed;

    font-family: 'Livvic', sans-serif;
    font-size: 1.4em;

    width: 20vw;
    height: 10vh;

    border-radius: 10px;
    margin-top: 10px;

    :hover {
      background-color: #9d9d9c;
    }
  }

  @media (max-width: 768px) {
    .buttonAdd {
      width: 60vw;
      height: 15vh;
    }
  }

  @media (max-width: 480px) {
    .buttonAdd {
      width: 80vw;
      height: 10vh;
    }
  }
`;

export const BackArrow = styled.div`
  position: fixed;
  top: 30px;
  left: 20px;
  transition: all.2s;

  img:hover {
    transform: scale(1.2);
  }
`;

export const FormContent = styled(Form)`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  div {
    margin-bottom: 50px;
    display: flex;
    flex-direction: column;
  }
`;

export const DatePickers = styled.div`
  div {
    margin-bottom: 0px;
    display: inline-block;
    // flex-direction: row ;
    // background-color: red;
  }
`;
export const BlockInputs = styled.div`
  background-color: #fff;
  box-shadow: 0px 0px 10px #ddd;
  border-radius: 10px;

  padding: 20px;

  .inline {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;
  }

  // .items {
  //   display: flex;
  //   align-items: center;
  //   justify-content: center;
  //   flex-direction: row;

  //   margin: 0px 20px;
  // }

  input[type='radio'] {
    border: 1px solid blue;
  }
`;

export const RadioInputs = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  div {
    margin-bottom: 0px;
  }

  .inline {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;

    margin-bottom: 20px;
  }
  .inlineFamily {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;

    margin-bottom: -50px;
  }

  .items {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;

    margin: 0px 20px;
  }

  input[type='radio'] {
    border: 1px solid blue;
  }
`;

export const Button = styled.button`
  background-color: #01affd;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  font-size: 1.4em;

  margin: 0px auto;
  padding: 10px 20px;
  text-decoration: none;
  cursor: pointer;

  width: 100%;

  :hover {
    background-color: #0c92ce;
  }
`;
export const ButtonDisabled = styled.div`
  background-color: #9d9d9c;
  border-radius: 5px;
  color: white;

  font-family: 'Livvic', sans-serif;
  fontSize: '14px',
  
  align-items: center;
  margin: 0px 5px;
  // padding: 20px 20px;
  text-decoration: none;
  cursor: pointer;

  width: 20%;

  @media (max-width: 1100px) {
    width: 30%;
  }
  @media (max-width: 500px) {
    width: 90%;
    text-align: center;
  }
`;

export const ButtonDivisor = styled.button`
  background: transparent;
  cursor: pointer;
  width: 30px;
  margin-right: 0px;
  float: right;
  justify-content: center;
  display: flex;
  margin: 20px;
  align-items: center;
`;
