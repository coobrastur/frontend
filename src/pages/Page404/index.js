// Dependencies
import React from 'react';
import { useHistory } from 'react-router-dom';

// Images
import notFound from '../../assets/notFound.svg';

// styles
import { Container, Content, Button } from './styles';

function Page404() {
  const history = useHistory();

  return (
    <Container className="container">
      <Content>
        <img src={notFound} alt="" />
        <p>
          Ops, essa página não existe🥺. Mas não fique triste, clique no botão
          abaixo para continuar
        </p>
        <Button type="button" onClick={() => history.push('/')}>
          Voltar ao Início
        </Button>
      </Content>
    </Container>
  );
}

export default Page404;
