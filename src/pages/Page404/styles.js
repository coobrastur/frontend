import styled from 'styled-components';

export const Container = styled.div`
  height: 100vh;
  width: 100%;

  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Content = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  img {
    width: 50%;
  }

  p {
    color: #333;
    margin: 10px 0 20px;
    text-align: center;
  }
`;

export const Button = styled.button`
  background-color: #08436f;
  border-radius: 5px;
  color: white;

  font-family: 'Ubuntu', sans-serif;
  font-size: 25px;

  margin: 50px auto 0;
  padding: 10px 20px;
  text-decoration: none;

  width: 300px;
  cursor: pointer;

  :hover {
    background-color: #113854;
  }
`;
