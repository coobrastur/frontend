// Dependencies
import React, { useState } from 'react';
// import React, { useEffect, useRef, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Row, Col } from 'react-grid-system';
// import { useSelector } from 'react-redux';
import { apiClei } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';
import Tooltip from '../../components/Tooltip';
// import SimpleInput from '../../components/SimpleInput';
// import InputMask from '../../components/InputMask';
// import InputCurrency from '../../components/InputCurrency';
import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';
// import Modal from '../../components/ModalEdit';
// import ModalGlobal from '../../components/ModalNoEdit';
// import ModalReativacao from '../../components/ModalReativacao';

// Images
import imageBackArrow from '../../assets/arrow-left.png';
// import arrowUP from '../../assets/arrowUP.svg';
// import arrowDown from '../../assets/arrowDown.svg';

// Styles
import {
  Container,
  Content,
  RadioInputs,
  BackArrow,
  BlockInputs,
  FormContent,
} from './styles';

export default function Prices() {
  const history = useHistory();
  const urlGlobal = 'https://telemarketing.coobrastur.com.br/wsAppVendasV2';

  const [loader, setLoader] = useState();
  const [flat, setFlat] = useState('');
  const [dailyQuantities, setDailyQuantities] = useState('');
  const [family, setFamily] = useState('');
  const [valorAdesao, setValorAdesao] = useState('');
  const [valorMensalidade, setValorMensalidade] = useState('0,00');
  const finalValue = parseFloat(
    valorMensalidade.replace(',', '.')
  ).toLocaleString('pt-br', { minimumFractionDigits: 2 });

  const planoArray = [
    { value: '', label: 'Selecione' },
    { value: 36, label: 'Vip +' },
    { value: 37, label: 'Master +' },
    { value: 38, label: 'Gold Vip +' },
    { value: 39, label: 'Gold Master +' },
    { value: 40, label: 'Diamante +' },
    { value: 41, label: 'Go Vip +' },
    { value: 42, label: 'Go Master +' },
    { value: 1002, label: 'Banstur Ouro' },
    { value: 1016, label: 'Banstur J3 Multi' },
    { value: 3000, label: 'Meridien Club' },
  ];

  const size = 51;
  const days = new Array(size);

  /* eslint-disable-next-line no-plusplus */
  for (let i = 2; i < size; i++) {
    days[i] = {
      value: i,
      label: `${i} diárias`,
    };
  }

  // retorna valor mensalidade

  function mensalidadeRetorna() {
    const informacoesPlano = {
      tpPlCodigo: flat,
      plFamilia: family,
      qtdeDiarias: dailyQuantities,
      adesao: false,
      vendCodigo: 675,
    };

    apiClei
      .post(urlGlobal.concat('/ConsultaCrm.asmx/RetornaValoresPlanos'), {
        dados: JSON.stringify(informacoesPlano),
      })
      .then((res) => {
        setValorMensalidade(res.data.ValorPlano);
        // console.log(finalValue);
      })
      .catch((e) => {
        console.log(e);
      });
  }

  // retorna valor Adesão

  function adesaoRetorna() {
    const currentDate = Date.now();
    const today = new Date(currentDate);

    const dados = {
      tpplcodigo: flat,
      plqtdediarias: dailyQuantities,
      plfamilia: family,
      vendcodigo: 675,
      tipdata: today.toLocaleString(),
    };

    apiClei
      .get(
        urlGlobal.concat(
          `/ConsultaCrm.asmx/RetoraValorAdesao?dados=${JSON.stringify(dados)}`
        )
      )
      .then((resp) => {
        // setValorAdesaoOriginal(resp.data[0].valAdesao);
        setValorAdesao(resp.data[0].valAdesao);

        return false;
      })
      .catch((e) => {
        console.log(e);
      });
  }

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />
      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>
      <div className="container">
        <Content>
          <FormContent>
            <BlockInputs>
              <h3>Consulta de valores de planos</h3>
              <Row className="rowContract">
                <Col md={3}>
                  <label style={{ fontWeight: '700', marginBottom: '-53px' }}>
                    Plano{' '}
                    {flat === '' && (
                      <Tooltip
                        text="selecione um plano"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '36' && (
                      <Tooltip
                        text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: Ano todo"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '37' && (
                      <Tooltip
                        text="2 a 50 diárias
                          Tipo de hotéis: Convencional
                          Validade: 15/03 à 15/12"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '38' && (
                      <Tooltip
                        text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: Ano todo"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '39' && (
                      <Tooltip
                        text="2 a 50 diárias
                           Tipo de hotéis: Superior
                           Validade: 15/03 à 15/12"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '40' && (
                      <Tooltip
                        text="2 a 50 diárias
                           Tipo de hotéis: Superior Luxo ou Resorts
                           Validade: Ano todo"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '41' && (
                      <Tooltip
                        text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: Ano todo"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat === '42' && (
                      <Tooltip
                        text="2 a 50 diárias
                           Tipo de hotéis: Convencional
                           Validade: 15/03 à 15/12"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {flat >= '1001' && (
                      <Tooltip
                        text="Plano selecionado"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                  </label>
                  <label htmlFor="select" className="input-select">
                    <select
                      id="select"
                      name="flat"
                      value={flat}
                      onChange={(e) => {
                        setFlat(e.target.value);
                        // setAdhesion(false);
                        if (e.target.value <= '40') {
                          // setAdhesion(false);
                          setDailyQuantities(false);
                          setFamily('');
                          setValorMensalidade('0,00');
                          setValorAdesao('0,00');
                        }
                        if (e.target.value <= '40' && dailyQuantities === '4') {
                          setDailyQuantities(false);
                        }
                        if (e.target.value > '40') {
                          // setAdhesion(false);
                          setFamily(false);
                          setValorMensalidade('0,00');
                          setValorAdesao('0,00');
                          setDailyQuantities(false);
                          // setAdhesion(true);
                        }
                        if (e.target.value >= '1001' && e.target.value < '1999') {
                          // setAdhesion(false);
                          setDailyQuantities(false);
                          setFamily(true);
                          setSellerMembershipForm(0);
                          setValorMensalidade('0,00');
                          setOfferApply(false);
                          setOffer(false);
                          setValorAdesao('0,00');
                          setDescontoAdesao('0,00');
                          setFinalAdesao('0,00');
                        }
                        if (e.target.value >= '2000') {
                          setFamily(false);
                          setValorAdesao('0,00');
                          setDescontoAdesao('0,00');
                          setFinalAdesao('0,00');
                        }
                      }}
                    >
                      {planoArray.map((option) => (
                        <option key={option.value} value={option.value}>
                          {option.label}
                        </option>
                      ))}
                    </select>
                  </label>
                </Col>
                <Col md={3}>
                  <label style={{ fontWeight: '700', marginBottom: '-53px' }}>
                    Diárias{' '}
                    {dailyQuantities === '' && (
                      <Tooltip
                        text="selecione um dia"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                    {dailyQuantities !== '' && (
                      <Tooltip
                        text="Diária escolhida"
                        color="#fff"
                        backgroundColor="#909090"
                      />
                    )}
                  </label>
                  <label htmlFor="select" className="input-select">
                    <select
                      id="select"
                      name="dailyQuantities"
                      value={dailyQuantities}
                      onChange={(e) => {
                        setDailyQuantities(e.target.value);
                      }}
                    >
                      {' '}
                      <option defaultValue>Selecione</option>
                      {flat >= 41
                        ? days
                            .filter((option) =>
                              flat >= 1001 ? option.label == '7 diárias' : true
                            )
                            .map((option) => (
                              <option key={option.value} value={option.value}>
                                {option.label}
                              </option>
                            ))
                        : days.map((option) => (
                            <option key={option.value} value={option.value}>
                              {option.label}
                            </option>
                          ))}
                    </select>
                  </label>
                </Col>
                {flat < 41 && (
                  <Col md={3}>
                    <RadioInputs>
                      <label
                        className="rowFamily"
                        style={{ fontWeight: '700' }}
                      >
                        Família
                        {family === '' && (
                          <Tooltip
                            iconColor="#909090"
                            text="quantidades de hóspedes"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {family === true && (
                          <Tooltip
                            iconColor="#909090"
                            text="O plano familía pode incluir até 3 hóspedes."
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {family === false && (
                          <Tooltip
                            iconColor="#909090"
                            text="O plano pode incluir até 2 hóspedes."
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                      </label>
                      <div className="inlineFamily">
                        <div className="items">
                          <input
                            type="radio"
                            value="1"
                            name="family"
                            checked={family === true}
                            onClick={() => {
                              setFamily(true);

                              if (typeOfContract == '1') {
                                setAdhesion(true);
                              }
                              // setAdhesion(true);
                              // valuesAdesao();
                            }}
                          />
                          Sim
                        </div>

                        <div className="items">
                          <input
                            type="radio"
                            value="2"
                            name="family"
                            checked={family === false}
                            onClick={() => {
                              setFamily(false);
                              // setAdhesion(true);
                              if (typeOfContract == '1') {
                                setAdhesion(true);
                              }
                              // valuesAdesao();
                            }}
                          />
                          Não
                        </div>
                      </div>
                    </RadioInputs>
                  </Col>
                )}
                {flat >= 41 && (
                  <Col md={3}>
                    <RadioInputs>
                      <label
                        className="rowFamily"
                        style={{ fontWeight: '700' }}
                      >
                        Família
                        {family === '' && (
                          <Tooltip
                            iconColor="#909090"
                            text="quantidades de hóspedes"
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {family === true && (
                          <Tooltip
                            iconColor="#909090"
                            text="O plano família pode incluir até 3 hóspedes."
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                        {family === false && (
                          <Tooltip
                            iconColor="#909090"
                            text="O plano pode incluir até 2 hóspedes."
                            color="#fff"
                            backgroundColor="#909090"
                          />
                        )}
                      </label>
                      <div className="inlineFamily">
                        <div className="items">
                          <input
                            type="radio"
                            value="true"
                            name="family"
                            disabled
                            checked={family === true}
                            onChange={() => {
                              setFamily(true);
                              //   // setAdhesion(true);
                              //   // valuesAdesao();
                            }}
                          />
                          Sim
                        </div>

                        <div className="items">
                          <input
                            type="radio"
                            value="false"
                            name="family"
                            disabled
                            checked={family === false}
                            onChange={() => {
                              setFamily(false);
                              // setAdhesion(true);
                              // valuesAdesao();
                            }}
                          />
                          Não
                        </div>
                      </div>
                    </RadioInputs>
                  </Col>
                )}
                <Col md={3}>
                  <button
                    type="button"
                    style={{
                      color: 'white',
                      backgroundColor: '#01affd',
                      fontWeight: '700',
                      marginTop: '15px',
                    }}
                    onClick={() => {
                      if (flat == '' || dailyQuantities == '') {
                        showToast({
                          type: 'error',
                          message: 'Preencha os dados do plano.',
                        });

                        setLoader(false);
                        return false;
                      }
                      if (family === '') {
                        showToast({
                          type: 'error',
                          message: 'Preencha os dados do plano.',
                        });

                        setLoader(false);
                        return false;
                      }
                      // if (typeOfContract == '') {
                      //   showToast({
                      //     type: 'error',
                      //     message: 'Escolha um tipo de assinatura.',
                      //   });

                      //   setLoader(false);
                      //   return false;
                      // }
                      mensalidadeRetorna();
                      adesaoRetorna();
                    }}
                  >
                    Consultar
                  </button>
                </Col>
              </Row>

              <Row>
                <Col md={12}>
                  <div
                    className="adesaoValues"
                    style={{ marginTop: '25px', marginBottom: '-30px' }}
                  >
                    <label>Valor da Mensalidade:</label>
                    <span style={{ marginLeft: '5%' }}>
                      R$ {finalValue || '0,00'}
                    </span>
                  </div>
                </Col>
                <Col md={12}>
                  <div
                    className="adesaoValues"
                    style={{ marginBottom: '-40px' }}
                  >
                    {/* <Col md={4}> */}
                    <label>Valor da Adesão:</label>
                    {/* </Col> */}

                    {/* <Col md={2}> */}
                    <span style={{ marginLeft: '50px' }}>
                      R${' '}
                      {valorAdesao.toLocaleString('pt-br', {
                        minimumFractionDigits: 2,
                      }) || '0,00'}
                    </span>
                    {/* </Col> */}
                  </div>
                  {/* <SimpleInput
                            name="valorAdesao"
                            type="number"
                            maxLength="3400"
                            value={valorAdesao}
                             
                          /> */}
                </Col>
              </Row>
            </BlockInputs>
          </FormContent>
        </Content>
      </div>
    </Container>
  );
}
