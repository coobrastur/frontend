import styled from 'styled-components';
import { Form } from '@unform/web';

export const Container = styled.div`
  background-color: #e5e5e5;
  height: 100vh;
  width: 100vw;
  color: #000000;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 50px 0;
  overflow: hidden;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }

  input {
    color: #000000;
  }

  #nodescontoAdesao {
    background-color: #9d9d9c;
    // padding: 5px;
  }

  #descontoAdesao {
    border: 1px solid #c4c4c4;
    box-sizing: border-box;
    border-radius: 6px;

    color: rgba(0, 0, 0, 0.3);

    font-size: 18px;

    margin-bottom: 20px;
    padding: 10px;

    ::placeholder {
      color: rgba(0, 0, 0, 0.3);
    }
  }

  input::placeholder {
    color: rgba(0, 0, 0, 0.3);
  }

  @media (max-width: 1100px) {
    h2 {
      text-align: center;
    }
  }
`;

export const Content = styled.div`
  border-radius: 10px;
  width: 100%;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }

  h3 {
    color: #01affd;
    font-size: 1.8em;
    margin-bottom: 45px;
  }

  p {
    color: rgba(0, 0, 0, 0.4);
  }

  .rowContract {
    margin: 0;
    margin-bottom: -5%;
  }
  .checkbox {
    display: flex;
    flex-direction: row;
    // background-color: #ccff00;
  }
  .rowAdesao {
    // margin: 0;
    margin-bottom: -5%;
    // background-color: #ccff00;
  }

  h6 {
    @media (min-width: 1080px) {
      font-size: 14px;
      width: 200%;
    }
    @media (max-width: 1080px) {
      font-size: 14px;
      width: 100%;
    }
  }

  // .rowFamily {
  //   // margin-left: -20%;
  //   // margin-bottom: -7%;
  // }

  .plusFlat {
    color: #01affd;
    font-weight: 700;
    font-size: 26px;
    line-height: 2;
    margin-left: 5px;
    transition: font-size 0.3s;
  }
  .plusFlat:hover {
    cursor: pointer;
    font-size: 28px;
    // line-height: 2.4;
  }
  .rowAdhesion {
    width: 100%;
    margin-top: -5%;
    margin-bottom: -10%;

    // display: flex;
    // align-items: normal;
    // justify-content: flex-start;
    // flex-flow: row wrap;

    // margin-top: -4%;
    // margin-bottom: -20%;

    // background-color: #ccff00;
  }

  .adesaoValues {
    display: flex;
    flex-direction: row;
    // justify-content: center;
    // margin-top: 2%;
    // background-color: #ccff00;

    span {
      // margin-left: 100px;
      font-size: 16px;
      font-weight: 700;
    }
  }
  .rowOb {
    margin-top: -5%;
  }
  .rowVenda {
    margin-top: -20%;
    margin-bottom: -10%;
  }

  .carStyle {
    display: flex;
    flex-direction: row;
    width: 50%;

    margin-bottom: 0px;

    @media (max-width: 881px) {
      flex-direction: column;
    }
  }

  .input-select select {
    border: 1px solid #c4c4c4;
    width: 100%;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;
    padding: 9px 0px 9px 2px;
  }

  .input-select-campaign select {
    border: 1px solid #c4c4c4;
    width: 100%;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;
    padding: 9px 0px 9px 10px;
    -webkit-appearance: none;
    -moz-appearance: none;

    @media (min-width: 1080px) {
      width: 115%;
    }
  }

  .daterange {
    z-index: 0;

    border: 1px solid #c4c4c4;
    box-sizing: border-box;
    border-radius: 6px;
    color: #000000;
    font-size: 18px;

    @media (min-width: 1081px) {
      box-sizing: border-box;
      font-size: 16px;
      width: 100%;
      height: 15px;
      margin: 10px;
      padding: 10px;
      height: 30px;
      border-radius: 5px;
    }
    @media (max-width: 1081px) {
      width: 95%;
      height: 30px;
      margin: 10px;
      padding: 10px;
      border-radius: 5px;
    }
  }
`;

export const BackArrow = styled.div`
  position: fixed;
  top: 30px;
  left: 20px;
  transition: all.2s;

  img:hover {
    transform: scale(1.2);
  }
`;

export const FormContent = styled(Form)`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  div {
    margin-bottom: 50px;
    display: flex;
    flex-direction: column;
  }
`;

export const BlockInputs = styled.div`
  background-color: #fff;
  box-shadow: 0px 0px 10px #ddd;
  border-radius: 10px;

  padding: 20px;

  .inline {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;
  }

  // .items {
  //   display: flex;
  //   align-items: center;
  //   justify-content: center;
  //   flex-direction: row;

  //   margin: 0px 20px;
  // }

  input[type='radio'] {
    border: 1px solid blue;
  }
`;

export const RadioInputs = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  div {
    margin-bottom: 0px;
  }

  .inline {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;

    margin-bottom: 20px;
  }
  .inlineFamily {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: row;
    padding: 15px;

    margin-bottom: -50px;
  }

  .items {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: row;

    margin: 0px 20px;
  }

  input[type='radio'] {
    border: 1px solid blue;
  }
`;
