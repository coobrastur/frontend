// Dependencies
import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
// import { Row, Col } from 'react-grid-system';
// import { apiClei } from '../../services/api';

// components
import MenuHamburguer from '../../components/MenuHamburguer';
// import Tooltip from '../../components/Tooltip';
// import { showToast } from '../../components/Alert';
import Loader from '../../components/Loader';

import table1 from '../../assets/table1.jpg';
import table2 from '../../assets/table2.jpg';
import TableBanstur from '../../assets/TabelaBanstur.png';
import TableMeridien from '../../assets/TabelaMeridien.jpg';

// Images
import imageBackArrow from '../../assets/arrow-left.png';

// Styles
import {
  Container,
  Content,
  // RadioInputs,
  BackArrow,
  BlockInputs,
  FormContent,
  Image,
  ImagesContainer,
} from './styles';

export default function Proportionality() {
  // const imagePath1 = '../';
  const history = useHistory();

  /* eslint-disable-next-line no-unused-vars */
  const [loader, setLoader] = useState();

  return (
    <Container>
      <MenuHamburguer />
      <Loader loader={loader} />
      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>
      <div className="container">
        <Content>
          <FormContent>
            <BlockInputs>
              {/* <h3 style={{ textAlign: 'Center' }}>
                Tabela de Proporcionalidade
              </h3> */}
              {/* <Row className="rowContract">
                <Col md={3}> */}
              <div className="images">
                <ImagesContainer>
                  <Image src={table1} alt="Descrição da imagem 1" />

                  <Image src={table2} alt="Descrição da imagem 1" />
                </ImagesContainer>
              </div>
              {/* </Col>
              </Row> */}
            </BlockInputs>
            <BlockInputs>
              <div className="images">
                <ImagesContainer>
                  <Image src={TableBanstur} alt="Descrição da imagem 1" />
                </ImagesContainer>
              </div>
              {/* </Col>
              </Row> */}
            </BlockInputs>
            <BlockInputs>
              <div className="images">
                <ImagesContainer>
                  <Image src={TableMeridien} alt="Descrição da imagem 1" />
                </ImagesContainer>
              </div>
              {/* </Col>
              </Row> */}
            </BlockInputs>
          </FormContent>
        </Content>
      </div>
    </Container>
  );
}
