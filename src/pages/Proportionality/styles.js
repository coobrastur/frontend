import styled from 'styled-components';
import { Form } from '@unform/web';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100vw;
  color: #000000;
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 50px 0;
`;

export const Content = styled.div`
  border-radius: 10px;
  width: 100%;
  max-width: 1200px;
  padding: 40px; /* Aumente o espaçamento interno */

  h3 {
    margin-top: 30px;
    color: #01affd;
    font-size: 1.8em;
    margin-bottom: 45px;
    text-align: center;
  }

  .images {
    display: flex;
    justify-content: space-evenly; /* Centralize as imagens */
    align-items: center;
    margin-top: 20px;
  }

  .modal {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: white;
    padding: 20px;
  }

  .overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: rgba(0, 0, 0, 0.8);
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .fullscreen {
    width: 100%;
    height: 100%;
    object-fit: contain;
  }
`;

export const BackArrow = styled.div`
  position: fixed;
  top: 30px;
  left: 20px;
  transition: all.2s;

  img:hover {
    transform: scale(1.2);
  }
`;

export const FormContent = styled(Form)`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  div {
    margin-bottom: 50px;
    display: flex;
    flex-direction: column;
  }
`;

export const BlockInputs = styled.div`
  background-color: #fff;
  box-shadow: 0px 0px 10px #ddd;
  border-radius: 10px;
`;

export const ImagesContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const Image = styled.img`
  max-width: 100%;
  height: auto;
  margin: 20px; /* Aumente o espaçamento entre as imagens */
  width: 600px; /* Ajuste o tamanho da largura da imagem */
`;
