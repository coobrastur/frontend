// Dependencies
import React, { useEffect, useState } from 'react';
import { useLocation, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { apiClei } from '../../services/api';

// Components
import MenuHamburguer from '../../components/MenuHamburguer';

// Images
import imageBackArrow from '../../assets/arrow-left.png';
// import { getMensalidade, formatReal } from '../../utils/data';

// Styles
import {
  Container,
  BackArrow,
  BlockItems,
  BlockItem,
  BlockItemSpotlight,
} from './styles';

function ViewContract() {
  const history = useHistory();
  const location = useLocation();
  const { id } = location.state;
  const [contract, setContract] = useState({});
  // const [valorMensalidade, setValorMensalidade] = useState('');
  const { token } = useSelector((state) => state.auth);
  // const mesa = new Date();

  async function getContracts() {
    const dados = {
      token,
      id,
    };

    await apiClei
      .get(`Contrato.asmx/ObterContratoPorId`, {
        params: {
          dados,
        },
      })
      .then((response) => {
        setContract(response.data[0]);
        console.log(response.data[0]);

        // setValorMensalidade(
        //   formatReal(
        //     getMensalidade({
        //       plano: response.data[0].tpplcodigo,
        //       diarias: response.data[0].plQtdeDiarias,
        //       familia: response.data[0].plfamilia,
        //     })
        //   )
        // );
      });
  }

  useEffect(() => {
    getContracts();
  }, []);

  const hoje = new Date();
  const mesAtual = hoje.getMonth() + 1;

  const mesPagamento = contract.carMes || contract.debMes || contract.bolMes;
  const mesPagamentoNumero = parseInt(mesPagamento, 10);

  let anoPagamento = hoje.getFullYear();

  // if (mesPagamentoNumero < mesAtual) {
  //   anoPagamento++;
  // }

  const dataPagamento = `${
    contract.carDia || contract.debDia || contract.bolDia
  }/${mesPagamentoNumero}/${anoPagamento}`;

  return (
    <Container>
      <MenuHamburguer />
      <BackArrow onClick={() => history.goBack()}>
        <img src={imageBackArrow} alt="" />
      </BackArrow>
      {contract && (
        <div className="container">
          <BlockItems>
            <h2>Dados do Contrato</h2>
            <BlockItemSpotlight>
              <>
                <p>
                  <span>Status: </span>
                  {!contract.status ? '---' : contract.status}
                </p>
              </>
            </BlockItemSpotlight>

            <BlockItem>
              <h3>Dados Pessoais</h3>
              <p>
                <span>CPF: </span> {contract.assCPF_CNPJ}
              </p>

              <p>
                <span>Nome: </span> {contract.assNome_RazaoSocial}
              </p>

              <p>
                <span>Email: </span> {contract.assEmailPessoal}
              </p>

              <p>
                <span>Número de celular: </span>
                {!contract.assNumCelularDDD
                  ? '---'
                  : `(${contract.assNumCelularDDD}) ${contract.assNumCelular}`}
              </p>
              <p>
                <span>Data de Nascimento: </span>
                {new Date(contract.dtNascimento).toLocaleDateString('pt-BR', {
                    timeZone: 'UTC',
                  })}
              </p>

              <p>
                <span>CEP: </span> {contract.endCepAssociado}
              </p>
              <p>
                <span>UF: </span> {contract.endUF}
              </p>

              <p>
                <span>Cidade: </span> {contract.endCidadeAssociado}
              </p>

              <p>
                <span>Bairro: </span> {contract.endBairroAssociado}
              </p>

              <p>
                <span>Endereço: </span> {contract.endLogradouroAssociado}
              </p>

              <p>
                <span>Número:</span> {contract.endNumLogradouroAssociado}
              </p>

              <p>
                <span>Complemento: </span>
                {!contract.endComplementoAssociado
                  ? 'Não informado'
                  : contract.endComplementoAssociado}
              </p>
            </BlockItem>

            <BlockItem>
              <h3>Outros Dados</h3>
              <p>
                <span>Indicador: </span>
                {contract.indicacao === true ? 'SIM' : 'NÃO'}
              </p>

              {contract.indicacao && (
                <>
                  <p>
                    <span>Nome Indicador: </span>
                    {!contract.nomeIndicador ? '---' : contract.nomeIndicador}
                  </p>

                  <p>
                    <span>CPF Indicador: </span>
                    {!contract.cpfIndicador ? '---' : contract.cpfIndicador}
                  </p>

                  <p>
                    <span>Nic do Indicador: </span>
                    {contract.assnicIndicador}
                  </p>
                  <p>
                    <span>Email Indicador: </span>
                    {!contract.emailIndicador ? '---' : contract.emailIndicador}
                  </p>

                  <p>
                    <span>Celular Indicador: </span>
                    {!contract.dddCelularIndicador
                      ? '---'
                      : `(${contract.dddCelularIndicador}) ${contract.celularIndicador}`}
                  </p>
                  {/* <p>
                    <span>Valor Adesão: </span>
                    {!contract.emailIndicador ? '---' : contract.emailIndicador}
                  </p> */}
                </>
              )}

              <p>
                <span>Restrição: </span>
                {contract.assRestricao === true ? 'SIM' : 'NÃO'}
              </p>

              {contract.restriction && (
                <p>
                  <span>Alternativa de restrição: </span>
                  {contract.alternativeRestriction}
                </p>
              )}

              <p>
                <span>Aditamento: </span>
                {contract.aditamento ? 'SIM' : 'NÃO'}
              </p>

              {contract.addition && (
                <p>
                  <span>Tipo de aditamento: </span>
                  {!contract.formaaditamento ? '---' : contract.formaaditamento}
                </p>
              )}

              <p>
                <span>Fiador: </span>
                {!contract.fiador ? 'NÃO' : 'SIM'}
              </p>

              {contract.fiador && (
                <>
                  <p>
                    <span>Nome do Fiador: </span>
                    {contract.fianome}
                  </p>
                  <p>
                    <span>CPF do Fiador: </span>
                    {contract.fiaCpf}
                  </p>

                  {/* <p>
                    <span>Data de Nascimento do Fiador: </span>
                    {new Date(contract.fiaDtNascimento).toLocaleDateString(
                      'pt-BR',
                      {
                        timeZone: 'UTC',
                      }
                    )}
                  </p> */}

                  {/* <p>
                    <span>Nacionalidade do Fiador: </span>
                    {contract.fianacionalidade}
                  </p> */}

                  {/* <p>
                    <span>Estado Civil do Fiador: </span>
                    {!contract.fiaConjuge ? '---' : contract.fiaConjuge}
                  </p> */}

                  <p>
                    <span>Mãe do Fiador: </span>
                    {contract.fiaConjugeFiliacao}
                  </p>

                  {/* <p>
                    <span>Pai do Fiador: </span>
                    {contract.fiaConjugeFiliacao2}
                  </p> */}

                  <p>
                    <span>Email do Fiador: </span>
                    {contract.fiaEmailPessoal}
                  </p>

                  {/* <p>
                    <span>Telefone do Fiador: </span>
                    {!contract.fiaddFone
                      ? '---'
                      : `${contract.fiaddFone}) ${contract.fiaFone}`}
                  </p> */}

                  <p>
                    <span>Celular do Fiador: </span>
                    {!contract.fiaNumeroCelularDDD
                      ? '---'
                      : `(${contract.fiaNumeroCelularDDD}) ${contract.fiaNumeroCelular}`}
                  </p>

                  <p>
                    <span>Assinatura do Fiador: </span>
                    {!contract.assinaturaFiador
                      ? '---'
                      : contract.assinaturaFiador}
                  </p>
                </>
              )}

              <p>
                <span>Pré-Pago: </span>
                {contract.parcelasPrePagas === 9999 ? 'SIM' : 'NÃO'}
              </p>

              <p>
                <span>Forma de Pagamento: </span>
                {contract.formaPagamento === null && 'Não informado'}
                {contract.formaPagamento === 1 && 'Débito em Conta Corrente'}
                {contract.formaPagamento === 3 && 'Cartão de Crédito'}
                {contract.formaPagamento === 4 && 'Boleto'}
              </p>
              <p>
                <span>Data da Mensalidade: </span>
                {dataPagamento}
                {/* {new Date(contract.dataAdesao).toLocaleDateString('pt-BR', {
                  timeZone: 'UTC',
                })} */}
              </p>
              {contract.adesao === false && (
                <p>
                  <span>Valor Adesão: </span>
                  Não se Aplica
                </p>
              )}

              {contract.adesao !== false && (
                <p>
                  <span>Valor Adesão: </span>
                  R${' '}
                  {parseFloat(contract.valorAdesao)
                    .toFixed(2)
                    .toLocaleString('pt-br', {
                      minimumFractionDigits: 2,
                    })}
                </p>
              )}
              {/* <p>
                <span>Desconto da adesão: </span>
                R${' '}
                {parseFloat(contract.adesaoValorDesconto).toLocaleString(
                  'pt-br',
                  { minimumFractionDigits: 2 }
                )}
              </p> */}
              {(contract.formaPagamentoAdesao == '0' ||
                contract.adesao === false) && (
                <p>
                  <span>Forma de Pagamento da Adesão: </span>
                  Não se Aplica
                </p>
              )}

              {contract.formaPagamentoAdesao !== '0' &&
                contract.adesao !== false && (
                  <p>
                    <span>Forma de Pagamento da Adesão: </span>
                    {contract.formaPagamentoAdesao}
                  </p>
                )}
              {contract.adesao == false && (
                <p>
                  <span>Parcelas da Adesão: </span>
                  Não se Aplica
                </p>
              )}

              {contract.adesao !== false && (
                <p>
                  <span>Parcelas da Adesão: </span>
                  {contract.parcelamentoAdesao == '' && 'Não se Aplica'}
                  {contract.parcelamentoAdesao == 1 && 'À vista'}
                  {contract.parcelamentoAdesao == 2 && '2x'}
                  {contract.parcelamentoAdesao == 3 && '3x'}
                </p>
              )}

              {contract.parcelamentoAdesao == '' ? (
                <p>
                  <span>Data da Adesão: </span>
                  Não se Aplica
                </p>
              ) : (
                <p>
                  <span>Data da Adesão: </span>
                  {new Date(contract.dataAdesao).toLocaleDateString('pt-BR', {
                    timeZone: 'UTC',
                  })}
                </p>
              )}
            </BlockItem>

            {/* <BlockItem>
              <h3>Campanhas</h3>
              <p
                style={{
                  marginLeft: '22px',
                  fontWeight: '700',
                  marginBottom: '3px',
                }}
              >
                {contract.campanha === 0 &&
                  'O usuário não se enquadra em nenhuma das campanhas vigentes.'}
                {contract.campanha === 100 && 'Isenção 1ª e 2ª mensalidades'}
              </p>
            </BlockItem> */}
            <BlockItem>
              <h3> Opção de Assinatura </h3>

              <p>
                <span>Tipo de Contrato: </span>
                {contract.tipoVenda === 1 && 'Normal'}
                {contract.tipoVenda === 2 && 'Pré-Pago'}
                {contract.tipoVenda === 11 && 'Reativação'}
                {/* {contract.tipoVenda === 1 && 'Normal'}
                {contract.tipoVenda === 2 && 'Aditamento'}
                {contract.tipoVenda === 11 && 'Recuperação'}
                {contract.tipoVenda === 53 && 'Substituição'} */}
              </p>
              <p>
                <span>Número PipeDrive: </span>
                {contract.nroPipeline}
              </p>
              <p>
                <span>Empresa: </span>
                {contract.empCodigo === 38 && 'Coob+'}
                {contract.empCodigo === 58 && 'Banstur'}
                {contract.empCodigo === 60 && 'Meridien'}
              </p>
              <p>
                <span>Plano: </span>
                {contract.tpplcodigo === 36 && 'Vip'}
                {contract.tpplcodigo === 37 && 'Master'}
                {contract.tpplcodigo === 38 && 'Gold Vip'}
                {contract.tpplcodigo === 39 && 'Gold Master'}
                {contract.tpplcodigo === 40 && 'Diamante'}
                {contract.tpplcodigo === 41 && 'Go Vip'}
                {contract.tpplcodigo === 42 && 'Go Master'}
                {contract.tpplcodigo === 1001 && 'Banstur Executivo'}
                {contract.tpplcodigo === 1002 && 'Banstur Ouro'}
                {contract.tpplcodigo === 1016 && 'Banstur J3 Multi'}
                {contract.tpplcodigo === 3000 && 'Meridien Club'}
              </p>

              <p>
                <span>Quantidade de Diárias: </span>
                {!contract.plQtdeDiarias ? '---' : contract.plQtdeDiarias}
              </p>

              <p>
                <span>Família: </span>
                {contract.plfamilia === true ? 'SIM' : 'NÃO'}
              </p>

              <p>
                <span>Mensalidade: </span>
                {parseFloat(contract.valorMensalidade).toLocaleString('pt-br', {
                  minimumFractionDigits: 2,
                })}
              </p>
              {contract.vendNome == null ? (
                <p>
                  <span>Parceiro: </span>
                  Não se Aplica
                </p>
              ) : (
                <p>
                  <span>Parceiro: </span>
                  {contract.vendNome}
                </p>
              )}
              {contract.cupNome == null ? (
                <p>
                  <span>Cupom: </span>
                  Não se Aplica
                </p>
              ) : (
                <p>
                  <span>Cupom: </span>
                  {contract.cupNome}
                </p>
              )}
              {/* {console.log(valorMensalidade)} */}
            </BlockItem>

            {/* <BlockItem> */}
            {/* <p>
                <span>Adesão: </span>
                {contract.adesao === true ? 'SIM' : 'NÃO'}
              </p> */}

            {/* {contract.adesao && (
                <> */}
            {/* <p>
                    <span>Valor Adesão: </span>
                    {!contract.valorAdesao ? '---' : contract.valorAdesao}
                  </p> */}

            {/* <p>
                    <span>Adesão do Vendedor: </span>
                    {!contract.tipoAdesaoVendedor
                      ? '---'
                      : contract.tipoAdesaoVendedor}
                  </p> */}

            {/* <p>
                    <span>Forma de adesão: </span>
                    {!contract.tipoAdesaoVendedor ? ' ' : 'Vendedor'}
                    {!contract.tipoAdesaoCoobrastur ? ' ' : 'Coobrastur'}
                  </p> */}

            {/* <p>
                    <span>Data vendedor: </span>
                    {!contract.dataVendedor
                      ? '---'
                      : new Date(contract.dataVendedor).toLocaleDateString(
                          'pt-BR',
                          {
                            timeZone: 'UTC',
                          }
                        )}
                  </p> */}

            {/* <p>
                    <span>Descrição vendedor: </span>
                    {!contract.obsVendedor ? '---' : contract.obsVendedor}
                  </p> */}

            {/* <p>
                    <span>Forma de adesão Coobrastur: </span>
                    {!contract.tipoAdesaoCoobrastur
                      ? '---'
                      : contract.tipoAdesaoCoobrastur}
                  </p> */}

            {/* <p>
                    <span>Data Coobrastur: </span>
                    {!contract.dataCoobrastur
                      ? '---'
                      : new Date(contract.dataCoobrastur).toLocaleDateString(
                          'pt-BR',
                          {
                            timeZone: 'UTC',
                          }
                        )}
                  </p> */}

            {/* <p>
                    <span>Descrição Coobrastur: </span>
                    {!contract.obsCoobrastur ? '---' : contract.obsCoobrastur}
                  </p> */}
            {/* </>
              )} */}
            {/* </BlockItem> */}

            {/* <BlockItem>
            <p>
              <span>Terceiro: </span>
              {contract.terceiro === true ? 'SIM' : 'NÃO'}
            </p>

            {contract.terceiro && (
              <>
                <p>
                  <span>CPF do Terceiro: </span>
                  {contract.cpfTerceiro}
                </p>

                <p>
                  <span>Nome do Terceiro: </span>
                  {contract.nomeTerceiro}
                </p>

                <p>
                  <span>Data de nascimento do Terceiro: </span>
                  {new Date(contract.nascimentoTerceiro).toLocaleDateString(
                    'pt-BR',
                    {
                      timeZone: 'UTC',
                    }
                  )}
                </p>

                <p>
                  <span>Nacionaliade do Terceiro: </span>
                  {contract.nacionalidadeTerceiro}
                </p>

                <p>
                  <span>Estado civíl do Terceiro: </span>
                  {contract.estadoCivilTerceiro}
                </p>

                <p>
                  <span>Conjugue do Terceiro: </span>
                  {contract.nomeConjugeTerceiro}
                </p>

                <p>
                  <span>Nome da Mãe do Terceiro: </span>
                  {contract.nomeMaeTerceiro}
                </p>

                <p>
                  <span>Nome do Pai do Terceiro: </span>
                  {contract.nomePaiTerceiro}
                </p>

                <p>
                  <span>Email do Terceiro: </span>
                  {contract.emailTerceiro}
                </p>

                <p>
                  <span>Celular do Terceiro: </span>
                  {`(${contract.dddCelularTerceiro}) ${contract.numCelularTerceiro}`}
                </p>

                <p>
                  <span>Telefone do Terceiro: </span>
                  {`(${contract.dddTelefoneTerceiro}) ${contract.TelefoneTerceiro}`}
                </p>

                <p>
                  <span>Assinatura do Terceiro: </span>
                  {!contract.assinaturaTerceiro
                    ? '---'
                    : contract.assinaturaTerceiro}
                </p>
              </>
            )}
          </BlockItem> */}
          </BlockItems>
        </div>
      )}
    </Container>
  );
}

export default ViewContract;
