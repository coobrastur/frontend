import styled from 'styled-components';

export const Container = styled.div`
  background-color: #e5e5e5;
  width: 100%;
  min-height: 100vh;

  h2 {
    color: #01affd;
    font-size: 2.8em;
    margin-bottom: 30px;
  }
`;

export const BlockItems = styled.div`
  padding: 50px 0px;

  width: 100%;
  margin: 0 auto;

  @media (max-width: 1100px) {
    padding: 10px;

    width: 100%;
    margin: 0 auto;
  }
`;

export const BackArrow = styled.div`
  position: fixed;
  top: 30px;
  left: 20px;
  transition: all.2s;

  img:hover {
    transform: scale(1.2);
  }
`;

export const BlockItem = styled.div`
  background-color: white;
  border-radius: 10px;

  padding: 20px;
  margin-bottom: 30px;

  h3 {
    color: #01affd;
    font-family: 'Livvic', sans-serif;
    font-size: 2em;
    padding-bottom: 10px;
    padding-left: 20px;
  }

  p span {
    font-family: 'Livvic', sans-serif;
    color: #000;
    font-size: 20px;
    padding-left: 20px;
  }

  p {
    // font-family: 'Livvic', sans-serif;
    // font-size: 25 px;
    color: #000;
    line-height: 35px;
  }

  .buttons {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  @media (max-width: 1100px) {
    .buttons {
      grid-template-columns: 1fr;
      grid-template-rows: repeat(3, 1fr);
    }
  }
`;

export const Line = styled.div`
  width: 100%;
  height: 2px;
  background-color: rgba(0, 0, 0, 0.4);

  margin: 30px 0;
`;

export const BlockItemSpotlight = styled.div`
  background-color: #fff;
  border-radius: 10px;
  outline: 3px solid #01affd;

  padding: 20px;
  margin-bottom: 30px;

  h3 {
    color: #01affd;
    font-family: 'Livvic', sans-serif;
    font-size: 30px;
  }

  p span {
    font-family: 'Livvic', sans-serif;
    color: #01affd;
    font-size: 22px;
  }

  p {
    font-family: 'Livvic', sans-serif;
    font-size: 18 px;
    color: #01affd;
    line-height: 35px;
  }
`;
