// Dependencies
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { useSelector } from 'react-redux';

// Pages
import Login from '../pages/Login';
import MyAccount from '../pages/MyAccount';
import Contracts from '../pages/Contracts';
// import AllContracts from '../pages/AllContracts';
import ViewContract from '../pages/ViewContract';
import NewContract from '../pages/NewContract';
import EditContract from '../pages/EditContract';
import ChangeCupom from '../pages/ChangeCupom';
// import AllUsers from '../pages/AllUsers';
import Page404 from '../pages/Page404';
import Prices from '../pages/Prices';
import Proportionality from '../pages/Proportionality';

export default function App() {
  const { token } = useSelector((state) => state.auth);

  return (
    <Router basename="/web-vendas">
      {!token && (
        <Switch>
          <Route path="/" exact>
            <Login />
          </Route>

          <Route path="/404">
            <Page404 />
          </Route>

          <Redirect to="/404" />
        </Switch>
      )}

      {token && (
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/contracts" component={Contracts} />
          <Route path="/myAccount" component={MyAccount} />
          <Route path="/viewcontract" component={ViewContract} />
          <Route path="/newContract" component={NewContract} />
          <Route path="/prices" component={Prices} />
          <Route path="/proportionality" component={Proportionality} />
          <Route path="/editContract" component={EditContract} />
          <Route path="/trocar-cupom" component={ChangeCupom} />
          <Route path="/404" component={Page404} />
        </Switch>
      )}

      {/* {token && nivCodigo > 1 && (
        <Switch>
          <Route path="/" component={Login} exact />
          <Route path="/allcontracts" component={AllContracts} />
          <Route path="/allusers" component={AllUsers} />
          <Route path="/contracts" component={Contracts} />
          <Route path="/myAccount" component={MyAccount} />
          <Route path="/viewcontract" component={ViewContract} />
          <Route path="/newContract" component={NewContract} />
          <Route path="/editContract" component={EditContract} />
          <Route path="/payment/:token" component={Payment} />
          <Route path="/confirm" component={Confirm} />
          <Route component="/404" />
        </Switch>
      )} */}
    </Router>
  );
}
