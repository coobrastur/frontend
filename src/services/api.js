import axios from 'axios';

const api = axios.create({
  baseURL: 'http://localhost:5000/',
});

const apiProd = axios.create({
  baseURL:
    'https://telemarketing.coobrastur.com.br/wsAppVendas/ConsultaCrm.asmx/',
});

// const apiClei = axios.create({
//   baseURL: 'http://10.1.2.174/telemarketing/wstelemarketing/',
// });

const apiClei = axios.create({
  baseURL: 'https://telemarketing.coobrastur.com.br/wsAppVendasV2/',
});

const apiViacep = axios.create({
  baseURL: 'https://viacep.com.br/ws/',
});

export { api, apiProd, apiViacep, apiClei };
