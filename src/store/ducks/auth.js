const initialState = {
  token: false,
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        token: action.payload.token,
        codigoVendas: action.payload.codigoVendas,
        usuCPF_CNPJ: action.payload.usuCPF_CNPJ,
        usuCodigo: action.payload.usuCodigo,
        usuEmail: action.payload.usuEmail,
        usuNome: action.payload.usuNome,
        usuUsuario: action.payload.usuUsuario,
        repreCodigo: action.payload.repreCodigo,
        venda_com_adesao: action.payload.venda_com_adesao,
      };

    default:
      return state;
  }
}

export function login(
  token,
  codigoVendas,
  usuCPF_CNPJ,
  usuCodigo,
  usuEmail,
  usuNome,
  usuUsuario,
  repreCodigo,
  venda_com_adesao
) {
  return {
    type: 'LOGIN',
    payload: {
      token,
      codigoVendas,
      usuCPF_CNPJ,
      usuCodigo,
      usuEmail,
      usuNome,
      usuUsuario,
      repreCodigo,
      venda_com_adesao,
    },
  };
}
