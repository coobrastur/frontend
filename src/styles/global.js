import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        /* display: none; <- Crashes Chrome on hover */
        -webkit-appearance: none;
        margin: 0; /* <-- Apparently some margin are still there even though it's hidden */
    }

    input[type=number] {
        -moz-appearance:textfield;
    }
  * {
    // background-color: red;
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    box-sizing: border-box;
    font-family: 'Nunito', sans-serif;
    -webkit-font-smoothing: antialiased !important;
  }

  html {
    --scroll-behavior: smooth;
    scroll-behavior: smooth;
    overflow-x: hidden;
  }

  .container {
    width: 100%;
    max-width: 1000px;
    margin: 0 auto;

    @media(max-width: 1550px) {
      max-width: 900px;
    }

    @media(max-width: 1400px) {
      max-width: 800px;
    }

    @media(max-width: 1250px) {
      max-width: 700px;
    }

    @media(max-width: 1100px) {
      max-width: 600px;
    }

    @media(max-width: 950px) {
      max-width: 550px;
    }

    @media(max-width: 850px) {
      max-width: 500px;
    }

    @media(max-width: 750px) {
      max-width: 450px;
    }

    @media(max-width: 650px) {
      max-width: 500px;
    }

    @media(max-width: 550px) {
      max-width: 400px;
    }

    @media(max-width: 450px) {
      max-width: 360px;
    }
  }
 
`;
